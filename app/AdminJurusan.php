<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminJurusan extends Model
{
    // 
    protected $connection = 'presensi';
    protected $table = 'admin_jurusan';
    protected $primaryKey = 'kode_admin_jurusan';
    protected $guarded = [];

    public function jabatan_pegawai(){
        return $this->belongsTo(DataJabatanPegawai::class,'kode_admin_jurusan','id_pegawai');
    }
    public function user(){
        return $this->belongsTo(App\User::class,'user_id');
    }
}
