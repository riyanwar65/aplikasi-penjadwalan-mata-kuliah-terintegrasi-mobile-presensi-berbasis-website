<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Auth;
use DB;
// class User extends Authenticatable Model
class User extends Authenticatable
{
    
    use Notifiable;
    
    protected $connection = 'presensi';
    public $primaryKey = 'user_id';

    protected $fillable = [
        'user_id', 'uuid', 'password','device_name','token_firebase','created_at','updated_at'
    ];

    protected $hidden = [
        'password', 'token_firebase',
    ];   
    
    // protected $guard = 'admin';    

    public function usersistem(){
      return $this->hasOne(UserSistem::class,'user_id','user_id');
    }

    public function dosen(){
        return $this->belongsTo(Dosen::class,'user_id','user_id');
    }

    public function detail_user(){
        $cek_admin =  DB::connection('presensi')
                        ->table('admin_jurusan')
                        ->join('detail_jabatan_pegawai','detail_jabatan_pegawai.id_pegawai','kode_admin_jurusan')
                        ->where('kode_admin_jurusan',Auth::user()->user_id)
                        ->where('deleted_at',NULL)
                        ->count();        
        $cek_dosen = DB::connection('siakad')
                    ->table('dosen')
                    ->join('polindra_presensi.detail_jabatan_pegawai','polindra_presensi.detail_jabatan_pegawai.id_pegawai','dosen.user_id')
                    ->join('polindra_presensi.data_jabatan','polindra_presensi.data_jabatan.id','polindra_presensi.detail_jabatan_pegawai.id_jabatan')
                    ->where('dosen.user_id',Auth::user()->user_id)
                    ->where('polindra_presensi.detail_jabatan_pegawai.deleted_at',NULL)
                    ->count();
        if ($cek_admin > 0) {
            $admin = DB::connection('presensi')
                ->table('admin_jurusan')
                ->join('detail_jabatan_pegawai','detail_jabatan_pegawai.id_pegawai','kode_admin_jurusan')
                ->join('data_jabatan','detail_jabatan_pegawai.id_jabatan','data_jabatan.id')
                ->where('kode_admin_jurusan',Auth::user()->user_id)
                ->where('deleted_at',NULL)
                ->first();
            return $admin;
        }
        if ($cek_dosen > 0){
            $dosen = DB::connection('siakad')
                ->table('dosen')
                ->join('polindra_presensi.detail_jabatan_pegawai','polindra_presensi.detail_jabatan_pegawai.id_pegawai','dosen.user_id')
                ->join('polindra_presensi.data_jabatan','polindra_presensi.data_jabatan.id','polindra_presensi.detail_jabatan_pegawai.id_jabatan')
                ->where('dosen.user_id',Auth::user()->user_id)
                ->where('deleted_at',NULL)
                ->first();
            return $dosen;
        }
    }

    // public function cek_login(){
    //     $cek = DB::connection('presensi')->table('detail_jabatan_pegawai')->where('id_pegawai',Auth::user()->user_id)->count();
    //     return $cek;
    // }

    // public function cek_adminsiakad(){
    //     $cek = DB::connection('presensi')->table('users')->where('user_id',Auth::user()->user_id)->first();
    //     return $cek;
    // }

    public function cek_level(){
        $cek_admin =  DB::connection('presensi')
                        ->table('admin_jurusan')
                        ->join('detail_jabatan_pegawai','detail_jabatan_pegawai.id_pegawai','kode_admin_jurusan')
                        ->where('kode_admin_jurusan',Auth::user()->user_id)
                        ->where('deleted_at',NULL)
                        ->count();        
        $cek_dosen = DB::connection('siakad')
                    ->table('dosen')
                    ->join('polindra_presensi.detail_jabatan_pegawai','polindra_presensi.detail_jabatan_pegawai.id_pegawai','dosen.user_id')
                    ->join('polindra_presensi.data_jabatan','polindra_presensi.data_jabatan.id','polindra_presensi.detail_jabatan_pegawai.id_jabatan')
                    ->where('dosen.user_id',Auth::user()->user_id)
                    ->where('polindra_presensi.detail_jabatan_pegawai.deleted_at',NULL)
                    ->count();
        if ($cek_admin > 0) {
            $admin = DB::connection('presensi')
                ->table('admin_jurusan')                
                ->join('detail_jabatan_pegawai','detail_jabatan_pegawai.id_pegawai','kode_admin_jurusan')
                ->join('data_jabatan','detail_jabatan_pegawai.id_jabatan','data_jabatan.id')
                ->where('kode_admin_jurusan',Auth::user()->user_id)
                ->where('deleted_at',NULL)
                ->first();
            return $admin;
        }
        if ($cek_dosen > 0){
            $dosen = DB::connection('siakad')
                ->table('dosen')
                ->join('polindra_presensi.detail_jabatan_pegawai','polindra_presensi.detail_jabatan_pegawai.id_pegawai','dosen.user_id')
                ->join('polindra_presensi.data_jabatan','polindra_presensi.data_jabatan.id','polindra_presensi.detail_jabatan_pegawai.id_jabatan')
                ->where('dosen.user_id',Auth::user()->user_id)
                ->where('deleted_at',NULL)
                ->first();
            return $dosen;
        }
    }

    public function nama(){
        $cek_adminsiakad = DB::connection('presensi')
                          ->table('users')
                        //   ->join('detail_jabatan_pegawai','detail_jabatan_pegawai.id_pegawai','user_id')
                          ->where('id_pegawai',Auth::user()->user_id)
                        //   ->where('deleted_at',NULL)
                          ->first();
        return $cek_adminsiakad;
    }

    public function jabatan_pegawai(){
        return $this->hasOne(JabatanPegawai::class,'id_pegawai','user_id');
    }    
}
