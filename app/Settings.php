<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    //
    protected $table   = 'settings';
    protected $guarded = [];
    protected $connection = 'presensi';
    const CROSSOVER = 'total_gen';
    const MUTASI    = 'mutasi';
}
