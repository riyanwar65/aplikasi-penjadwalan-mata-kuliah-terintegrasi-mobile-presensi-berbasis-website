<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dosen extends Model
{
    //
    protected $connection = 'siakad';
    protected $primaryKey = 'dosen_kode';
    protected $table = 'dosen';

    public $timestamps = false;
    protected $guarded = [];
    
    public function user_dosen(){
        return $this->hasOne(User::class,'user_id','user_id');
    }

    public function dosen_foto(){
        return $this->hasOne(DosenFoto::class,'dosen_kode','dosen_kode');
    }

    public function jabatan_pegawai(){
        return $this->belongsTo(DataJabatanPegawai::class,'dosen_kode','id_pegawai');
    }
}
