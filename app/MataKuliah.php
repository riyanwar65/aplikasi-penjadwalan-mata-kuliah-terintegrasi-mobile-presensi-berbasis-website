<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MataKuliah extends Model
{
    //    
    protected $guarded =[];
    protected $table = 'mata_kuliah';
    protected $connection = 'presensi';

    public function bkd(){
        return $this->belongsTo(Bkd::class);
    }
}
