<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataJabatanPegawai extends Model
{
    //
    protected $connection ='presensi';
    protected $table='detail_jabatan_pegawai';
    protected $primaryKey='id_detail_jabatan';
    protected $guard=[];

    public function jabatan(){
        return $this->hasOne(DataJabatan::class,'id','id_jabatan');
    }

    public function admin_pegawai(){
        return $this->hasOne(AdminJurusan::class,'kode_admin_jurusan');
    }

    public function dosen_pegawai(){
        return $this->hasOne(Dosen::class,'dosen_kode');
    }
}
