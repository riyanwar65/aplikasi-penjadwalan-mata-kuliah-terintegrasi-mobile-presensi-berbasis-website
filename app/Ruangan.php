<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ruangan extends Model
{
    //
    protected $connection = 'presensi';
    protected $table = 'ruangan';
    protected $primaryKey = 'ruangan_id';
    protected $guarded = [];

    public function ruangan_siakad(){
        return $this->hasOne(RuanganSiakad::class,'ruangan_id');
    }
}
