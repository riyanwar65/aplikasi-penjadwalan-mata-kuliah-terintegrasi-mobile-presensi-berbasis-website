<?php

namespace App\Exports;

use App\BKDExport;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;

class BKDExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return DB::connection('presensi')
        ->table('bkd')
        ->join('polindra_siakad_v1_db.dosen','polindra_siakad_v1_db.dosen.dosen_kode','bkd.dosen_kode')
        ->join('mata_kuliah','mata_kuliah.kode_matkul','bkd.kode_matkul')
        ->join('polindra_siakad_v1_db.data_kelas','polindra_siakad_v1_db.data_kelas.kelas_kode','bkd.kelas_kode')
        ->join('polindra_siakad_v1_db.program_studi','polindra_siakad_v1_db.program_studi.program_studi_kode','polindra_siakad_v1_db.data_kelas.program_studi_kode')
        ->join('polindra_siakad_v1_db.program_jurusan','polindra_siakad_v1_db.program_jurusan.jurusan_kode','polindra_siakad_v1_db.program_studi.jurusan_kode')
        ->get();
    }
}
