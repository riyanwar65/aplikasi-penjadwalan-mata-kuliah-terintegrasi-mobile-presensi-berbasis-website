<?php

namespace App\Exports;

use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class JadwalExport implements FromView
{
    // public $semester;
    // public $tahunajaran;

    public function view():View{
        $semester = "genap";
        $tahunajaran = "2019/2020";
        return view('contents.jadwal.export',[
            'semester'=>"genap",
            'tahunajaran'=>"2019/2020"
        ]);          
    }

}
