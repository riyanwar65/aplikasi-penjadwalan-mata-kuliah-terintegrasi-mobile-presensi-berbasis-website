<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JabatanPegawai extends Model
{
    //
    protected $connection = 'presensi';
    protected $table = 'detail_jabatan_pegawai';
    protected $primaryKey = 'id_detail_jabatan';
    protected $guarded = [];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
