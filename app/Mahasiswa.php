<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    //
    protected $table = 'mahasiswa';    
    protected $connection = 'siakad';
    protected $guarded = [];

    public function mahasiswa_biodata(){
      return  $this->hasOne('\App\MahasiswaBiodata','mahasiswa_kode','mahasiswa_kode');                
    }

    public function mahasiswa_kelas(){
        return $this->hasOne(Kelas::class,'kelas_kode','kelas_kode');
    }

    public function kelas_tahun(){
        return $this->hasOne(DataKelasTahun::class,'kelas_kode','kelas_kode');
    }
}
