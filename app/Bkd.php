<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bkd extends Model
{
    //
    protected $connection = 'presensi';
    protected $table = 'bkd';
    protected $guarded = [];
    // protected $primaryKey = 'kode_bkd';

    public function matkul(){
        return $this->hasOne(MataKuliah::class,'kode_matkul','kode_matkul');
    }
}
