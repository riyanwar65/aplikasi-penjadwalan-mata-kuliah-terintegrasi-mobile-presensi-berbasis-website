<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RuanganSiakad extends Model
{
    //
    protected $connection = 'siakad';
    protected $table = 'data_ruangan';
    protected $primaryKey = 'ruangan_id';
    protected $guarded = [];    
    public $timestamps = false;

    public function ruangan(){
        return $this->hasOne(Ruangan::class,'ruangan_id');
    }

    
}
