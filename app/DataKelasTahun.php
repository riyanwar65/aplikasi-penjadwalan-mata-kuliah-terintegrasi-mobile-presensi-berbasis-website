<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataKelasTahun extends Model
{
    protected $table = 'data_kelas_tahun';    
    protected $connection = 'siakad';
    protected $guarded = [];

    public function mahasiswa(){
        return $this->belongsTo(Mahasiswa::class);                
    }
    //
}
