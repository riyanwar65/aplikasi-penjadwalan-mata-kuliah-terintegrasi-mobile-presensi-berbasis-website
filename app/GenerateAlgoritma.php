<?php

namespace App;

use DB;
use App\Schedule;
use Session;
class GenerateAlgoritma
{
    //
    public function randKromosom($banyak_bkd, $input_year, $input_semester){
    // public function randKromosom($kromosom, $banyak_bkd, $input_year, $input_semester){
        Schedule::where('jurusan',Session::get('jurusan'))->whereIn('semester',$input_semester)->where('tahun_ajaran',$input_year)->delete();                                        
        $bkd_praktek = DB::connection('presensi')->table('bkd')                                           
                       ->where('tahun_ajaran',$input_year)
                       ->where('status_bkd','Aktif')
                       ->whereIn('semester',$input_semester)
                       ->get();             
        $temp =  [];
        $tampung_sesi = [[]];
        $tampung_bkd = [[]];
        $sum = 0;
        foreach ($bkd_praktek as $key => $value) {             
            $temp[$key]=[
                'harus_sesi'=>($value->jumlah_sks_praktek*2)+$value->jumlah_sks_teori,
                'kode_bkd'=>$value->kode_bkd,                
            ];            
        }        
        foreach ($bkd_praktek as $key => $value) {                                 
            $_sesi = DB::connection('presensi')->table('sesi')
            ->where('jam_masuk','<>','11:40')
            ->inRandomOrder()                
            ->get(); 
            for ($i=0; $i <$temp[$key]['harus_sesi'] ; $i++) {                 
                $tampung_sesi[$key][$i] = $_sesi[$i]->id_sesi;
                $tampung_bkd[$key][$i] = [
                    "bkd"=>$temp[$key]['kode_bkd'],   
                ];                
            }          
        }                                                 
        // for ($i=0; $i < $kromosom ; $i++) { 
            // $values = [];
            for ($j=0; $j < $banyak_bkd ; $j++) { 
                $bkd = DB::connection('presensi')->table('bkd')
                        ->where('tahun_ajaran',$input_year)
                        ->where('jurusan',Session::get('jurusan'))
                        ->whereIn('semester',$input_semester)
                        ->where('status_bkd','Aktif')
                        // ->inRandomOrder()
                        ->get();
                $hari = ['senin','selasa','rabu','kamis','jumat'];
                shuffle($hari);
                $sehari = $hari[0];
                $ruangan = DB::connection('presensi')->table('ruangan')
                            ->where('jurusan',Session::get('jurusan'))
                            ->inRandomOrder()
                            ->first();    
                $sesi = DB::connection('presensi')->table('sesi')
                        ->where('jam_masuk','<>','11:40')
                        ->inRandomOrder()
                        ->first();                                                  
                // $jumlah_sks = $matkul->sks_teori + $matkul->sks_praktek;                
                // if ($bkd->kode_bkd==$tampung_bkd[$j][0]['bkd']) {                           
                    $params = [
                        'kode_bkd' => $bkd[$j]->kode_bkd,                    
                        'hari'=>$sehari,
                        'sesi'=>json_encode($tampung_sesi[$j]),
                        'jurusan'=>Session::get('jurusan'),                    
                        'tahun_ajaran'=>$input_year,
                        'semester'=>$bkd[$j]->semester,
                        'ruangan_id'=>$ruangan->ruangan_id,
                        'type'      => 1,
                    ];                                              
                    $schedule = Schedule::create($params);     
                // }         
                                                                            
            }
            // $data[]=$values;
        // }   
        return 1;
    }
    

    private function convertsks($tahun,$semester){
        $bkd_praktek = DB::connection('presensi')->table('bkd')                       
                    //    ->join('mata_kuliah','mata_kuliah.kode_matkul','bkd.kode_matkul')
                       ->where('tahun_ajaran',$tahun)
                       ->where('status_bkd','Aktif')
                       ->whereIn('semester',$semester)
                       ->get();    
        $sesi = DB::connection('presensi')->table('sesi')
                ->where('jam_masuk','<>','11:40')
                ->inRandomOrder()                
                ->get();           
        $temp =  [];
        $tampung = [[]];
        $sum = 0;
        foreach ($bkd_praktek as $key => $value) {             
            $temp[$key]=[
                'harus_sesi'=>($value->jumlah_sks_praktek*2)+$value->jumlah_sks_teori,
                'kode_bkd'=>$value->kode_bkd,                
            ];            
        }
        foreach ($bkd_praktek as $key => $value) {             
            for ($i=0; $i <$temp[$key]['harus_sesi'] ; $i++) {                 
                $tampung[$key][$i] = [
                    "sesi"=>$sesi[$i]->id_sesi,
                    "bkd"=>$temp[$key]['kode_bkd']
                ];
            }          
        }        
        return $tampung;        
    }

    public function checkPinalty(){
        $schedule = Schedule::select(DB::raw('kode_bkd,hari,sesi,type,count(*) as `jumlah`'))
            ->groupBy('kode_bkd')
            ->groupBy('hari')
            ->groupBy('sesi')
            ->groupBy('type')
            ->where('schedule.jurusan',Session::get('jurusan'))
            ->having('jumlah', '>', 1)
            ->get();
        
        $result_schedules = $this->increaseProccess($schedule);

        $schedule = Schedule::select(DB::raw('kode_bkd,hari,ruangan_id,type,count(*) as `jumlah`'))
            ->groupBy('kode_bkd')
            ->groupBy('hari')
            ->groupBy('ruangan_id')
            ->groupBy('type')  
            ->where('schedule.jurusan',Session::get('jurusan'))   
            ->having('jumlah', '>', 1)
            ->get();
        
        $result_schedules = $this->increaseProccess($schedule);

        $schedule = Schedule::select(DB::raw('sesi,hari,ruangan_id,type,count(*) as `jumlah`'))
            ->groupBy('sesi')
            ->groupBy('hari')
            ->groupBy('ruangan_id')
            ->groupBy('type')
            ->where('schedule.jurusan',Session::get('jurusan'))
            ->having('jumlah', '>', 1)
            ->get();
        
        $result_schedules = $this->increaseProccess($schedule);

        $schedule = DB::connection('presensi')->table('schedule')
            ->join('bkd','bkd.kode_bkd','schedule.kode_bkd')
            ->join('polindra_siakad_v1_db.dosen','polindra_siakad_v1_db.dosen.dosen_kode','bkd.dosen_kode')
            ->select(DB::raw('bkd.dosen_kode, hari, sesi, type, count(*) as `jumlah`'))
            ->groupBy('bkd.dosen_kode')
            ->groupBy('hari')
            ->groupBy('sesi')
            ->groupBy('type')
            ->where('schedule.jurusan',Session::get('jurusan'))
            ->having('jumlah', '>', 1)
            ->get();
        
        $result_schedules = $this->increaseProccess($schedule);

        $schedule = Schedule::where('sesi',6)->where('jurusan',Session::get('jurusan'))->get();

        if(!empty($schedule)){
            foreach ($schedule as $key => $s) {
                $s->value = $s->value + 1;
                $s->value_process = $s->value_process."+ 1";
                $s->save();
            }
        }

        $schedule =  Schedule::where('jurusan',Session::get('jurusan'))->get();
        foreach ($schedule as $key => $s) {
            $s->value = 1 / (1+$s->value);
            $s->save();
        }
        return $schedule;
    }

    public function increaseProccess($schedule = ''){
        if (!empty($schedule)) {
            foreach ($schedule as $key => $s) {
                if ($s->jumlah > 1) {
                    $schedule_wheres = Schedule::where('type', $s->type)->where('jurusan',Session::get('jurusan'))->get();
                    foreach ($schedule_wheres as $key => $sw)
                    {
                        $sw->value         = $sw->value + ($s->jumlah - 1);
                        $sw->value_process = $sw->value_process . " + " . ($s->jumlah - 1);
                        $sw->save();
                    }
                }
            }
        }
        return $schedule;
    }
}
