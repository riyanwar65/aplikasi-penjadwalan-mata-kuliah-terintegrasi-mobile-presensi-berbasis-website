<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sesi extends Model
{
    //
    protected $table='sesi';
    protected $primaryKey='id_sesi';
    protected $connection='presensi';
    protected $guarded=[];
    public $timestamps = false;

}
