<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DosenFoto extends Model
{
    //
    protected $connection = 'siakad';
    protected $primaryKey = 'dosen_kode';
    protected $table = 'dosen_foto';

    public $timestamps = false;
    protected $guarded = [];

    public function dosen(){
        return $this->belongsTo(Dosen::class);
    }
}
