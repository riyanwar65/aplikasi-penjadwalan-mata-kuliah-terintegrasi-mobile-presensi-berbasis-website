<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dosen;
use App\User;
use DB;
use Session;
use Carbon\Carbon;
class DosenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // dd(Dosen::with(['user_dosen'])->first());        

        if (Session::get('jabatan')=="0") {
            $dosen = DB::connection('siakad')->table('dosen')            
            ->get();
            return view('contents.dosen.index',['dosen'=>$dosen]);
        }else{
            if (Session::get('jurusan')=="TI") {
                return $this->getDatadosenPerJurusan("D3TIK","D4RPL");
            }if(Session::get('jurusan')=="TM"){
                return $this->getDatadosenPerJurusan("D3TMN","D4TPM");
            }if(Session::get('jurusan')=="TP"){
                return $this->getDatadosenPerJurusan("D3TPU",NULL);
            }if(Session::get('jurusan')=="KP"){
                return $this->getDatadosenPerJurusan("D3KP",NULL);
            }            
        }        
    }

    private function getDatadosenPerJurusan($prodi1,$prodi2){
        if ($prodi2==null) {
            $dosen = DB::connection('siakad')->table('dosen')                        
            ->where('dosen.program_studi_kode',$prodi1)
            ->orWhere('dosen.program_studi_kode',"")
            ->orWhere('dosen.program_studi_kode',NULL)
            ->orWhere('dosen.dosen_jenis',"dosen_luar")
            ->get();
            return view('contents.dosen.index',['dosen'=>$dosen]);
        }else{
            $dosen = DB::connection('siakad')->table('dosen')            
            ->where('dosen.program_studi_kode',$prodi1)
            ->orWhere('dosen.program_studi_kode',$prodi2)
            ->orWhere('dosen.program_studi_kode',"")
            ->orWhere('dosen.program_studi_kode',NULL)
            ->orWhere('dosen.dosen_jenis',"dosen_luar")
            ->get();
            return view('contents.dosen.index',['dosen'=>$dosen]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $dosen = Dosen::with(['dosen_foto'])->where('user_id',$id)->first();
        return response()->json($dosen);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
               
        $cek = DB::connection('presensi')->table('polindra_presensi.users')
        ->join('polindra_siakad_v1_db.dosen','polindra_siakad_v1_db.dosen.user_id','polindra_presensi.users.user_id')
        ->where('polindra_presensi.users.user_id',$id)->get();
        // $reset = Dosen::with(['user_dosen'])->whereHas('user_dosen',function($query){
        //     $query->where('user_id',$id);
        // })->count();
        // return response()->json(3);        
        if (count($cek)>0) {
            $reset = DB::connection('presensi')->table('users')->where('user_id',$id)->update([
                'uuid'=>NULL,
                'updated_at'=>Carbon::now('Asia/Jakarta')
            ]);        
            return response()->json([
                'status'=>true,
                'code'=>404,
                'Message'=>'UUID berhasil di Reset',                
            ]);
        }else{
            return response()->json([
                'status'=>false,
                'code'=>404,
                'Message'=>'UUID Belum Terdaftar'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //                
    }
    
}
