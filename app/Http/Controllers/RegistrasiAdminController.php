<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\AdminJurusan;
use App\DataJabatan;
use App\DataJabatanPegawai;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Hash;
class RegistrasiAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $jabatan = DataJabatan::all();
        $jurusan = DB::connection('siakad')->table('program_jurusan')->get();
        return view('contents.registrasi_admin.index',compact('jabatan','jurusan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $namalengkap = $request->first." ".$request->last;   
        $username = strtolower(str_replace(' ', '', $request->first).mt_rand(1000,9999)); 
        $email = $request->email;
        $password = Str::random(10);
        $data=[
            "username"=>$username,
            "password"=>$password,
            "nama"=>$namalengkap
        ];
        Mail::send('contents.sendemail',$data, function ($message) use ($email){
            $message->to($email)->subject('Akun Aplikasi Web Manajemen Penjadwalan');
            $message->from(env('MAIL_FROM_ADDRESS',NULL),'Admin Aplikasi Web Manajemen Penjadwalan');
        });
        
        $admin = new AdminJurusan;
        $admin->kode_admin_jurusan=$username;
        $admin->nama_admin_jurusan=$namalengkap;
        $admin->asal_jurusan=$request->jurusan;
        $admin->email=$email;
        $admin->alamat=$request->alamat;
        $admin->no_hp=$request->nohp;
        $admin->id_jabatan=$request->jabatan;
        $admin->created_at=Carbon::now('Asia/Jakarta');
        $admin->updated_at=Carbon::now('Asia/Jakarta');
        $admin->save();

        $akun = new User;
        $akun->user_id=$username;
        $akun->password=Hash::make($password);
        $akun->email_recovery=$email;
        $akun->created_at=Carbon::now('Asia/Jakarta');
        $akun->updated_at=Carbon::now('Asia/Jakarta');
        $akun->save();

        $jabatanpegawai = new DataJabatanPegawai;
        $jabatanpegawai->id_detail_jabatan="DETAILJBTN".mt_rand(1000,9999);
        $jabatanpegawai->id_pegawai = $username;
        $jabatanpegawai->id_jabatan = $request->jabatan;
        // $jabatanpegawai->periode = $request->awal."/".$request->akhir;
        $jabatanpegawai->jurusan = $request->jurusan;
        $jabatanpegawai->created_at=Carbon::now('Asia/Jakarta');
        $jabatanpegawai->updated_at=Carbon::now('Asia/Jakarta');
        $jabatanpegawai->save();

        return redirect(route('jabatan.index'))->with('success','Data Registrasi Admin jurusan telah disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $admin = DB::connection('presensi')
                ->table('detail_jabatan_pegawai')
                ->join('admin_jurusan','admin_jurusan.kode_admin_jurusan','detail_jabatan_pegawai.id_pegawai')                
                ->where('admin_jurusan.kode_admin_jurusan',$id)
                ->update([
                    'admin_jurusan.nama_admin_jurusan'=>$request->namalengkap,
                    'admin_jurusan.asal_jurusan'=>$request->jurusan,
                    'detail_jabatan_pegawai.periode'=>$request->awal."/".$request->akhir,
                    'admin_jurusan.email'=>$request->email,
                    'admin_jurusan.no_hp'=>$request->nohp,
                    'detail_jabatan_pegawai.id_jabatan'=>$request->jabatan,
                    'admin_jurusan.id_jabatan'=>$request->jabatan,
                    'admin_jurusan.updated_at'=>Carbon::now('Asia/Jakarta'),
                    'detail_jabatan_pegawai.updated_at'=>Carbon::now('Asia/Jakarta'),
                ]);        
        return redirect(route('jabatan.index'))->with('success','Data Berhasil Di Perbaharui');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $hapus = DB::connection('presensi')->table('detail_jabatan_pegawai')->where('id_pegawai',$id)->update([
            'deleted_at'=>Carbon::now('Asia/Jakarta')
        ]);
            
        return response()->json([
            'status'=>true,
            'message'=>"Data berhasil di Arsipkan",
        ]);
    }
}
