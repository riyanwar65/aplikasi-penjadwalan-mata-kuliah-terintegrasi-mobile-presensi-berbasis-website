<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use PDF;
use Carbon\Carbon;

class AbsensiController extends Controller
{
    //
    public function index(){
        $temp = [];
        $kelas = DB::connection('siakad')
                ->table('mahasiswa')
                ->join('data_kelas','mahasiswa.kelas_kode','data_kelas.kelas_kode')
                ->join('program_studi','program_studi.program_studi_kode','data_kelas.program_studi_kode')
                ->join('program_jurusan','program_jurusan.jurusan_kode','program_studi.jurusan_kode')
                ->where('program_jurusan.jurusan_kode',Session::get('jurusan'))   
                ->where('tingkat','<>','0') 
                ->where('mahasiswa.mahasiswa_status','Aktif')
                ->where('mahasiswa.tahun_index','<>','2016')                
                ->groupBy('data_kelas.kelas_kode')            
                ->get();        
        foreach ($kelas as $key => $value) {
            $cek =explode('18',$value->kelas_nama);
            if(count($cek)!=2){
                $temp[$key]=$cek;
            }
        }
        return view('permissions.adminjurusan.absensi',compact('temp'));
    }

    public function rekap($kelas){
        $rekap = DB::connection('presensi')                
                ->table('absensi')
                ->join('polindra_siakad_v1_db.mahasiswa','polindra_siakad_v1_db.mahasiswa.mahasiswa_nim','absensi.mahasiswa_nim')
                ->join('polindra_siakad_v1_db.mahasiswa_kelas','polindra_siakad_v1_db.mahasiswa_kelas.mahasiswa_kode','polindra_siakad_v1_db.mahasiswa.mahasiswa_kode')
                ->where('polindra_siakad_v1_db.mahasiswa_kelas.kelas_kode',$kelas)                
                ->orderBy('polindra_siakad_v1_db.mahasiswa.mahasiswa_nama','ASC')
                ->groupBy('absensi.mahasiswa_nim')
                ->get();
        // return view('exports.rekap',compact('rekap','kelas'));
        $pdf = PDF::loadview('exports.rekap',compact('rekap','kelas'))->setPaper('a4', 'landscape');
        return $pdf->download('Rekap Seluruh Absensi Kelas '.$kelas.'.pdf');                     
    }
}
