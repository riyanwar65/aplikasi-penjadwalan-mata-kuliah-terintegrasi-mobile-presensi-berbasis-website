<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
class TestController extends Controller
{
    
    //
    public function index(){
        return view('contents.jadwal.cek_3');       
    }

    public function kelas(){
        $kelas = DB::connection('presensi')->table('temp_jadwal') 
                ->select("kelas_nama")                                                                         
                ->join('polindra_siakad_v1_db.data_kelas','polindra_siakad_v1_db.data_kelas.kelas_kode','temp_jadwal.kelas')                                                                   
                ->where('temp_jadwal.jurusan',Session::get('jurusan'))
                ->where('temp_jadwal.status_jadwal','aktif')            
                ->orderBy('kelas_nama','asc')
                ->groupBy('kelas_nama')                
                ->get();
        return response()->json($kelas);
    }

    public function harisesi(Request $request){
        $genap = [2,4,6,8];
        $ganjil = [1,3,5,7];       
        if ($request->semester=="genap") {
            return $this->dataFilter($request->tahun_ajaran,$genap,$request->ruangan);            
        }else{
            return $this->dataFilter($request->tahun_ajaran,$ganjil,$request->ruangan);            
        }          
    }

    public function jadwalsemua(Request $request){
        $genap = [2,4,6,8];
        $ganjil = [1,3,5,7];       
        if ($request->semester=="genap") {
            return $this->dataFilterJadwal($request->tahun_ajaran,$genap);            
        }else{
            return $this->dataFilterJadwal($request->tahun_ajaran,$ganjil);            
        }          
    }

    private function dataFilterJadwal($tahun_ajaran,$semester){
        $hari = ['senin','selasa','rabu','kamis','jumat'];
        $sesi = DB::connection('presensi')->table('sesi')->get();        
        $kelas = DB::connection('presensi')->table('temp_jadwal') 
                // ->select("data_kelas.kelas_nama",'nama_matkul','jenis','ruangan_nama','hari','sesi_masuk','dosen_nama')                                                       
                ->join('color_dosen','color_dosen.dosen_kode','temp_jadwal.dosen_kode')
                ->join('polindra_siakad_v1_db.dosen','polindra_siakad_v1_db.dosen.dosen_kode','temp_jadwal.dosen_kode')
                ->join('polindra_siakad_v1_db.data_ruangan','polindra_siakad_v1_db.data_ruangan.ruangan_id','temp_jadwal.ruangan_id')
                ->join('mata_kuliah','mata_kuliah.kode_matkul','temp_jadwal.kode_matkul')                 
                ->join('polindra_siakad_v1_db.data_kelas','polindra_siakad_v1_db.data_kelas.kelas_kode','temp_jadwal.kelas')                                    
                ->join('polindra_siakad_v1_db.program_jurusan','temp_jadwal.jurusan','polindra_siakad_v1_db.program_jurusan.jurusan_kode')                
                ->where('temp_jadwal.jurusan',Session::get('jurusan'))
                ->where('temp_jadwal.tahun_ajaran',$tahun_ajaran)    
                ->where('temp_jadwal.status_jadwal','aktif')            
                ->whereIn('temp_jadwal.semester',$semester)
                ->orderBy('temp_jadwal.kelas','asc')                
                ->get();
        $temp = [[]];
        foreach ($hari as $key => $value) {
            foreach ($sesi as $key2 => $value2) {
                $temp[$key][$key2]=[
                    'hari'=>$value,
                    'sesi'=>$value2,                    
                ];
            }
        }
        return response()->json(
            [
            'harisesi'=>$temp,
            'kelas'=>$kelas,
            ]
        );
    }

    private function dataFilter($tahun_ajaran,$semester,$ruangan){        
        $hari = ['senin','selasa','rabu','kamis','jumat'];
        $sesi = DB::connection('presensi')->table('sesi')->get();        
        $kelas = DB::connection('presensi')->table('temp_jadwal') 
                // ->select("data_kelas.kelas_nama",'nama_matkul','jenis','ruangan_nama','hari','sesi_masuk','dosen_nama')  
                ->join('color_dosen','color_dosen.dosen_kode','temp_jadwal.dosen_kode')                                                     
                ->join('polindra_siakad_v1_db.dosen','polindra_siakad_v1_db.dosen.dosen_kode','temp_jadwal.dosen_kode')
                ->join('polindra_siakad_v1_db.data_ruangan','polindra_siakad_v1_db.data_ruangan.ruangan_id','temp_jadwal.ruangan_id')
                ->join('mata_kuliah','mata_kuliah.kode_matkul','temp_jadwal.kode_matkul')                 
                ->join('polindra_siakad_v1_db.data_kelas','polindra_siakad_v1_db.data_kelas.kelas_kode','temp_jadwal.kelas')                                    
                ->join('polindra_siakad_v1_db.program_jurusan','temp_jadwal.jurusan','polindra_siakad_v1_db.program_jurusan.jurusan_kode')                
                ->where('temp_jadwal.jurusan',Session::get('jurusan'))
                ->where('temp_jadwal.status_jadwal','aktif')            
                ->where('temp_jadwal.tahun_ajaran',$tahun_ajaran)
                ->where('temp_jadwal.ruangan_id',$ruangan)
                ->whereIn('temp_jadwal.semester',$semester)
                ->orderBy('temp_jadwal.kelas','asc')                
                ->get();
        $temp = [[]];
        foreach ($hari as $key => $value) {
            foreach ($sesi as $key2 => $value2) {
                $temp[$key][$key2]=[
                    'hari'=>$value,
                    'sesi'=>$value2,                    
                ];
            }
        }
        return response()->json(
            [
            'harisesi'=>$temp,
            'kelas'=>$kelas,
            ]
        );
    }
}
