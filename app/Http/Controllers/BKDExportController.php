<?php

namespace App\Http\Controllers;

// use App\BKDExport;
use App\Exports\BKDExport;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Http\Request;
use DB;
use PDF;
use Session;
class BKDExportController extends Controller
{
    //
    public function filtertampilan(Request $request){
        $this->validate($request,[
            'semester'=>'required',
            'tahun_ajaran'=>'required',            
        ],[
            'required'=>':attribute Harus di isi'
        ]);
        $ganjil =[1,3,5,7];
        $genap =[2,4,6,8];
        if ($request->semester=="genap") {
            $bkd = DB::connection('presensi')->table('bkd')->whereIn('semester',$genap)->get();            
            if (count($bkd)>1) {
                $bkd_genap = DB::connection('presensi')
                            ->table('bkd')
                            ->join('polindra_siakad_v1_db.dosen','polindra_siakad_v1_db.dosen.dosen_kode','bkd.dosen_kode')
                            ->join('mata_kuliah','mata_kuliah.kode_matkul','bkd.kode_matkul')
                            ->join('polindra_siakad_v1_db.data_kelas','polindra_siakad_v1_db.data_kelas.kelas_kode','bkd.kode_kelas')
                            ->join('polindra_siakad_v1_db.program_studi','polindra_siakad_v1_db.program_studi.program_studi_kode','polindra_siakad_v1_db.data_kelas.program_studi_kode')
                            ->join('polindra_siakad_v1_db.program_jurusan','polindra_siakad_v1_db.program_jurusan.jurusan_kode','polindra_siakad_v1_db.program_studi.jurusan_kode')
                            ->whereIn('bkd.semester',$genap)
                            ->where('status_bkd','Aktif')
                            ->where('bkd.tahun_ajaran',$request->tahun_ajaran)
                            ->get();      
                            return response()->json([
                                "status"=>true,                
                                "bkd"=>$bkd_genap
                            ]); 
            }else{
                return response()->json([
                    "status"=>false,                
                    "message"=>"Data yang di Minta tidak ada"
                ]);    
            }
        }else{
            $bkd = DB::connection('presensi')->table('bkd')->where('status_bkd','Aktif')->whereIn('semester',$ganjil)->get();            
            if (count($bkd)>1) {
                $bkd_ganjil = DB::connection('presensi')
                            ->table('bkd')
                            ->join('polindra_siakad_v1_db.dosen','polindra_siakad_v1_db.dosen.dosen_kode','bkd.dosen_kode')
                            ->join('mata_kuliah','mata_kuliah.kode_matkul','bkd.kode_matkul')
                            ->join('polindra_siakad_v1_db.data_kelas','polindra_siakad_v1_db.data_kelas.kelas_kode','bkd.kode_kelas')
                            ->join('polindra_siakad_v1_db.program_studi','polindra_siakad_v1_db.program_studi.program_studi_kode','polindra_siakad_v1_db.data_kelas.program_studi_kode')
                            ->join('polindra_siakad_v1_db.program_jurusan','polindra_siakad_v1_db.program_jurusan.jurusan_kode','polindra_siakad_v1_db.program_studi.jurusan_kode')
                            ->orderBy('bkd.dosen_kode','desc')
                            ->where('status_bkd','Aktif')
                            ->whereIn('bkd.semester',$ganjil)
                            ->where('bkd.tahun_ajaran',$request->tahun_ajaran)
                            ->get();      
                return response()->json([
                    "status"=>true,                
                    "bkd"=>$bkd_ganjil
                ]);    
            }else{
                return response()->json([
                    "status"=>false,                
                    "message"=>"Data yang di Minta tidak ada"
                ]);    
            }
        }        
    }
    
    public function exportpdf(Request $request){        
        $bkd_pdf = DB::connection('presensi')
        ->table('bkd')
        ->join('polindra_siakad_v1_db.dosen','polindra_siakad_v1_db.dosen.dosen_kode','bkd.dosen_kode')
        ->join('mata_kuliah','mata_kuliah.kode_matkul','bkd.kode_matkul')
        ->join('polindra_siakad_v1_db.data_kelas','polindra_siakad_v1_db.data_kelas.kelas_kode','bkd.kode_kelas')
        ->join('polindra_siakad_v1_db.program_studi','polindra_siakad_v1_db.program_studi.program_studi_kode','polindra_siakad_v1_db.data_kelas.program_studi_kode')
        ->join('polindra_siakad_v1_db.program_jurusan','polindra_siakad_v1_db.program_jurusan.jurusan_kode','bkd.jurusan')                    
        ->where('status_bkd','Aktif')
        ->where('bkd.jurusan',Session::get('jurusan'))        
        ->orderBy('polindra_siakad_v1_db.dosen.dosen_nama','ASC')
        ->get();   
        if (count($bkd_pdf)>0) {            
            $jurusan = DB::connection('presensi')        
            ->table('bkd')        
            ->join('polindra_siakad_v1_db.program_jurusan','polindra_siakad_v1_db.program_jurusan.jurusan_kode','bkd.jurusan')                    
            ->where('status_bkd','Aktif')
            ->where('bkd.jurusan',Session::get('jurusan'))        
            ->first();   
            if ($jurusan->semester%2==0) {
                $jenissemester = "Genap";            
                $pdf = PDF::loadview('exports.exportbkd',compact('jurusan','bkd_pdf','jenissemester'))->setPaper('a4', 'landscape');
                return $pdf->download('BKD SEMESTER '.$jenissemester." ".$jurusan->tahun_ajaran." ".$jurusan->jurusan_nama.'.pdf');
            }else{
                $jenissemester = "Ganjil";            
                $pdf = PDF::loadview('exports.exportbkd',compact('jurusan','bkd_pdf','jenissemester'))->setPaper('a4', 'landscape');
                return $pdf->download('BKD SEMESTER '.$jenissemester." ".$jurusan->tahun_ajaran." ".$jurusan->jurusan_nama.'.pdf');
            }        
        }else{
            return redirect()->back()->with('error','Data tidak ada yang aktif');
        }
        // $jenissemester = $request->semester;
        // $tahunajaran = $request->tahun_ajaran;
        // $ganjil =[1,3,5,7];
        // $genap =[2,4,6,8];
        // if ($request->semester=="genap") {
        //     $bkd_pdf = DB::connection('presensi')
        //             ->table('bkd')
        //             ->join('polindra_siakad_v1_db.dosen','polindra_siakad_v1_db.dosen.dosen_kode','bkd.dosen_kode')
        //             ->join('mata_kuliah','mata_kuliah.kode_matkul','bkd.kode_matkul')
        //             ->join('polindra_siakad_v1_db.data_kelas','polindra_siakad_v1_db.data_kelas.kelas_kode','bkd.kode_kelas')
        //             ->join('polindra_siakad_v1_db.program_studi','polindra_siakad_v1_db.program_studi.program_studi_kode','polindra_siakad_v1_db.data_kelas.program_studi_kode')
        //             ->join('polindra_siakad_v1_db.program_jurusan','polindra_siakad_v1_db.program_jurusan.jurusan_kode','polindra_siakad_v1_db.program_studi.jurusan_kode')                    
        //             ->where('status_bkd','Aktif')
        //             ->where('bkd.tahun_ajaran',$request->tahun_ajaran)
        //             ->orderBy('polindra_siakad_v1_db.dosen.dosen_nama','ASC')
        //             ->get();      
        //             $pdf = PDF::loadview('exports.exportbkd',compact('bkd_pdf','jenissemester','tahunajaran'))->setPaper('a4', 'landscape');
        //     return $pdf->download('BKD SEMESTER GENAP '.$request->tahun_ajaran." ".$bkd_pdf[0]->jurusan_nama.'.pdf');
        // }else{
        //     $bkd_pdf = DB::connection('presensi')
        //             ->table('bkd')
        //             ->join('polindra_siakad_v1_db.dosen','polindra_siakad_v1_db.dosen.dosen_kode','bkd.dosen_kode')
        //             ->join('mata_kuliah','mata_kuliah.kode_matkul','bkd.kode_matkul')
        //             ->join('polindra_siakad_v1_db.data_kelas','polindra_siakad_v1_db.data_kelas.kelas_kode','bkd.kode_kelas')
        //             ->join('polindra_siakad_v1_db.program_studi','polindra_siakad_v1_db.program_studi.program_studi_kode','polindra_siakad_v1_db.data_kelas.program_studi_kode')
        //             ->join('polindra_siakad_v1_db.program_jurusan','polindra_siakad_v1_db.program_jurusan.jurusan_kode','polindra_siakad_v1_db.program_studi.jurusan_kode')
        //             ->whereIn('bkd.semester',$ganjil)
        //             ->where('status_bkd','Aktif')
        //             ->where('bkd.tahun_ajaran',$request->tahun_ajaran)
        //             ->orderBy('polindra_siakad_v1_db.dosen.dosen_nama','ASC')
        //             ->get();      
        //     $pdf = PDF::loadview('exports.exportbkd',compact('bkd_pdf','jenissemester','tahunajaran'))->setPaper('a4', 'landscape')->setWarnings(false)->output();
        //     return $pdf->download('BKD SEMESTER GENAP '.$request->tahun_ajaran." ".$bkd_pdf[0]->jurusan_nama.'.pdf');
        // }        
    }
}

