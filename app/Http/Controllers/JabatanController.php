<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataJabatan;
use App\DataJabatanPegawai;
use Carbon\Carbon;
use DB;
class JabatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //     
        // $adminjurusan = DataJabatanPegawai::with(['jabatan','admin_pegawai'])->where('deleted_at',NULL)->get();
        // $dosenjurusan = DataJabatanPegawai::with(['jabatan','dosen_pegawai'])->where('deleted_at',NULL)->get();
        $adminjurusan = DB::connection('presensi')->table('detail_jabatan_pegawai')
                            ->join('admin_jurusan','admin_jurusan.kode_admin_jurusan','detail_jabatan_pegawai.id_pegawai')
                            ->join('data_jabatan','data_jabatan.id','detail_jabatan_pegawai.id_jabatan')
                            ->where('detail_jabatan_pegawai.deleted_at',NULL)
                            ->get();
        $dosenjurusan = DB::connection('presensi')->table('polindra_presensi.detail_jabatan_pegawai')
                            ->join('polindra_siakad_v1_db.dosen','polindra_siakad_v1_db.dosen.user_id','polindra_presensi.detail_jabatan_pegawai.id_pegawai')
                            ->join('polindra_presensi.data_jabatan','polindra_presensi.detail_jabatan_pegawai.id_jabatan','polindra_presensi.data_jabatan.id')
                            ->where('polindra_presensi.detail_jabatan_pegawai.deleted_at',NULL)
                            ->get();
        // dd($dosenjurusan);
        $no=1;
        $n=1;
        $jabatan = DataJabatan::paginate(2);  
        return view('contents.jabatan.index',compact('jabatan','adminjurusan','no','dosenjurusan','n'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $jabatan = ucfirst($request->jabatan);
        $cek = DataJabatan::where('jabatan',$jabatan)->first();
        if ($cek) {
            return response()->json([
                'status'=>false,
                'message'=>'Data Sudah ada'
            ]);
        }else{
            $insert = new DataJabatan;
            $insert->jabatan = $jabatan;
            $insert->created_at=Carbon::now('Asia/Jakarta');
            $insert->save();        
            return response()->json([
                'status'=>true,
                'message'=>"Data Berhasil di simpan",    
                'jabatan'=>$insert
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = DataJabatan::find($id);
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $update = DataJabatan::find($id);
        $update->jabatan=ucfirst($request->jabatan);
        $update->save();

        return redirect(route('jabatan.index'))->with('success','Data Berhasil di Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cek = DataJabatanPegawai::where('id_jabatan',$id)->where('deleted_at',NULL)->count();
        if ($cek>0) {            
            return response()->json([
                'status'=>true,
                'message'=>"Tidak dapat menghapus, Data sudah dipakai"
            ]);
        }else{
            $hapus = DataJabatan::find($id);
            $hapus->delete();
            return response()->json([
                'status'=>false,
                'message'=>"Data Berhasil di Hapus, Silahkan masukan data lagi"
            ]);
        }
    }    
}
