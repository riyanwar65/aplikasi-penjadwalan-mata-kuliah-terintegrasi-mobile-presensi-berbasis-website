<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\Exports\JadwalExport;
use Excel;
// use mikehaertl\wkhtmlto\Pdf;
use PDF;
class JadwalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cek_bkd = DB::connection('presensi')->table('bkd')->whereRaw('LOWER(`status_bkd`)="aktif"')->get();
        $tahun_ajaran = DB::connection('presensi')->table('bkd')->where('jurusan',Session::get('jurusan'))->groupBy('tahun_ajaran')->get();         
        return view('contents.jadwal.index',compact('tahun_ajaran','cek_bkd'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function temp($tahun_ajaran,$semester){
        $pengecekan = DB::connection('presensi')->table("temp_jadwal")
                    ->where('jurusan',Session::get('jurusan'))
                    ->whereIn('semester',$semester)
                    ->where('tahun_ajaran',$tahun_ajaran)
                    ->update([
                        'status_jadwal'=>"tidak aktif"
                    ]);        
            $data = [[]];
            $tampung =[];
            $test = DB::connection('presensi')
            ->table('schedule')
            ->join('bkd','bkd.kode_bkd','schedule.kode_bkd')
            ->join('polindra_siakad_v1_db.dosen','polindra_siakad_v1_db.dosen.dosen_kode','bkd.dosen_kode')
            ->join('mata_kuliah','mata_kuliah.kode_matkul','bkd.kode_matkul')
            ->join('polindra_siakad_v1_db.data_kelas','polindra_siakad_v1_db.data_kelas.kelas_kode','bkd.kode_kelas')                    
            ->where('schedule.jurusan',Session::get('jurusan')) 
            ->where('type',1)        
            ->get();
            $sesi = DB::connection('presensi')->table('sesi')->get();
            foreach ($test as $key => $value) {          
                $tampung[$key]=json_decode($value->sesi);
                for ($i=0; $i < count(json_decode($value->sesi)); $i++) {                                     
                     $insert = DB::connection('presensi')->table('temp_jadwal')->insert([
                         'id_temp_jadwal'=>Str::random(20),
                         'tahun_ajaran'=>"2019/2020",
                         'hari'=>$value->hari,
                         'kelas'=>$value->kelas_kode,
                         'jurusan'=>$value->jurusan,
                         'dosen_kode'=>$value->dosen_kode,
                         'status_jadwal'=>"aktif",
                         'kode_matkul'=>$value->kode_matkul,
                         'ruangan_id'=>$value->ruangan_id,
                         'semester'=>$value->semester,
                         'sesi_masuk'=>$tampung[$key][$i],                    
                         'created_at'=>Carbon::now('Asia/Jakarta')
                     ]);
                }
            }                   
    }

    public function cek(Request $request){    
        $genap = [2,4,6,8];
        $ganjil = [1,3,5,7];
        if ($request->semester=="genap") {
            $schedules = DB::connection('presensi')->table('schedule')                                
                ->join('bkd','bkd.kode_bkd','schedule.kode_bkd')
                ->join('polindra_siakad_v1_db.dosen','polindra_siakad_v1_db.dosen.dosen_kode','bkd.dosen_kode')
                ->join('mata_kuliah','mata_kuliah.kode_matkul','bkd.kode_matkul')
                ->join('polindra_siakad_v1_db.data_kelas','polindra_siakad_v1_db.data_kelas.kelas_kode','bkd.kode_kelas')                                    
                ->join('polindra_siakad_v1_db.data_ruangan','polindra_siakad_v1_db.data_ruangan.ruangan_id','schedule.ruangan_id')
                ->orderBy('hari','desc')
                ->where('schedule.jurusan',Session::get('jurusan')) 
                ->where('schedule.tahun_ajaran',$request->tahun_ajaran)                
                ->whereIn('schedule.semester',$genap)
                ->where('status_bkd','Aktif')
                ->where('type',1)
                ->groupBy('schedule.ruangan_id')
                ->get();            
            if (count($schedules)>0) {
                $semester = $request->semester;
                $tahun_ajaran = $request->tahun_ajaran;
                return view('contents.jadwal.cek_3',compact('schedules','tahun_ajaran','semester'));
            }else{
                return redirect()->back()->with('error','Data Tidak ditemukan');
            }
        }else{
            $schedules = DB::connection('presensi')->table('schedule')                                
                ->join('bkd','bkd.kode_bkd','schedule.kode_bkd')
                ->join('polindra_siakad_v1_db.dosen','polindra_siakad_v1_db.dosen.dosen_kode','bkd.dosen_kode')
                ->join('mata_kuliah','mata_kuliah.kode_matkul','bkd.kode_matkul')
                ->join('polindra_siakad_v1_db.data_kelas','polindra_siakad_v1_db.data_kelas.kelas_kode','bkd.kode_kelas')                                    
                ->join('polindra_siakad_v1_db.data_ruangan','polindra_siakad_v1_db.data_ruangan.ruangan_id','schedule.ruangan_id')
                ->orderBy('hari','desc')
                ->where('schedule.jurusan',Session::get('jurusan')) 
                ->where('schedule.tahun_ajaran',$request->tahun_ajaran)                
                ->whereIn('schedule.semester',$ganjil)
                ->where('type',1)
                ->where('status_bkd','Aktif')
                ->groupBy('schedule.ruangan_id')
                ->get();            
            if (count($schedules)>0) {
                $semester = $request->semester;
                $tahun_ajaran = $request->tahun_ajaran;
                return view('contents.jadwal.cek_3',compact('schedules','tahun_ajaran','semester'));
            }else{
                return redirect()->back()->with('error','Data Tidak ditemukan');
            }
        }        
    }

    public function filterlab(Request $request){
        $data = [[]];
        $tampung =[];
        $test = DB::connection('presensi')
                ->table('schedule')
                ->join('bkd','bkd.kode_bkd','schedule.kode_bkd')
                ->join('polindra_siakad_v1_db.dosen','polindra_siakad_v1_db.dosen.dosen_kode','bkd.dosen_kode')
                ->join('mata_kuliah','mata_kuliah.kode_matkul','bkd.kode_matkul')
                ->join('polindra_siakad_v1_db.data_kelas','polindra_siakad_v1_db.data_kelas.kelas_kode','bkd.kode_kelas')                    
                ->where('schedule.jurusan',Session::get('jurusan')) 
                ->where('type',1)
                ->where('status_bkd','Aktif')
                ->where('ruangan_id',$request->ruangan)
                ->get();
        $sesi = DB::connection('presensi')->table('sesi')->get();
        $day = DB::connection('presensi')->table('schedule')->where('ruangan_id',$request->ruangan)->groupBy('hari')->get();        
        foreach ($test as $key => $value) {          
            $tampung[$key]=json_decode($value->sesi);
            for ($i=0; $i < count(json_decode($value->sesi)); $i++) { 
                $data[$key][$i]=[
                    "dosen"=>$value->dosen_nama,
                    'dosen_kode'=>$value->dosen_kode,
                    'bkd'=>$value->kode_bkd,
                    'kode_matkul'=>$value->kode_matkul,
                    'matkul'=>$value->nama_matkul,
                    'kelas'=>$value->kelas_nama,
                    'hari'=>$value->hari,
                    'sesi'=>json_decode($value->sesi),                    
                ];
            }
        }        
        return response()->json([
            'generate'=>$data,  
            'sesi'=>$sesi,
        ]);
    }

    public function ceksemua(Request $req){
        $cek = DB::connection('presensi')    
               ->table('schedule')
               ->select(DB::raw('kode_bkd count(kode_bkd) as `jumlah`'))
               ->whereIn('kode_bkd',$req->bkd) 
               ->where('type',1)
               ->having('jumlah','>',1)
               ->get();                                     
        return response()->json($cek);
    }

    public function export(Request $request){
        // return Excel::download(new JadwalExport, 'jadwal.xlsx');
        $semester = $request->semester;
        $tahunajaran = $request->tahun_ajaran;        
        // $jadwal = DB::connection('presensi')->table('temp_jadwal')
        //         // ->select('temp_jadwal.kelas','nama_matkul','dosen_nama')                 
        //         ->join('color_dosen','color_dosen.dosen_kode','temp_jadwal.dosen_kode')                                                     
        //         ->join('polindra_siakad_v1_db.dosen','polindra_siakad_v1_db.dosen.dosen_kode','temp_jadwal.dosen_kode')
        //         ->join('polindra_siakad_v1_db.data_ruangan','polindra_siakad_v1_db.data_ruangan.ruangan_id','temp_jadwal.ruangan_id')
        //         ->join('mata_kuliah','mata_kuliah.kode_matkul','temp_jadwal.kode_matkul')                 
        //         ->join('polindra_siakad_v1_db.data_kelas','polindra_siakad_v1_db.data_kelas.kelas_kode','temp_jadwal.kelas')                                    
        //         ->join('polindra_siakad_v1_db.program_jurusan','temp_jadwal.jurusan','polindra_siakad_v1_db.program_jurusan.jurusan_kode')                
        //         ->where('temp_jadwal.jurusan',Session::get('jurusan'))
        //         ->where('temp_jadwal.status_jadwal','aktif')            
        //         ->where('temp_jadwal.tahun_ajaran',$tahunajaran)
        //         // ->groupBy('temp_jadwal.kelas')                
        //         ->orderBy('temp_jadwal.kelas','asc')                
        //         ->get();
       
        // $render = view('contents.jadwal.export',compact('semester','tahunajaran'))->render();
        return view('contents.jadwal.export',compact('semester','tahunajaran'));                
        // $pdf = new Pdf;
        // $pdf->addPage($render);
        // // $pdf->setOptions(['javascript-delay' => 5000]);
        // $pdf->saveAs(public_path('report.pdf'));
        // if (!$pdf->saveAs(public_path('report.pdf'))) {
        //     $error = $pdf->getError();
        //     // ... handle error here
        //     echo $error;
        // }else{
        //     echo "sukses";
        // }
        // return response()->download(public_path('report.pdf'));
        // $pdf = new PDF;        
        // return $pdf = PDF::setOptions(['javascript-delay',5000])
        //         ->setOptions(['isJavascriptEnabled',true])
        //         ->loadView('contents.jadwal.export',compact('semester','tahunajaran'))->setPaper('a4', 'landscape')->setWarnings(false)->download('Jadwal '.Session::get('jurusan').'.pdf');
                // ->loadView('contents.jadwal.export',compact('semester','tahunajaran'))->setPaper('a4', 'landscape')->download('Jadwal '.Session::get('jurusan').'.pdf');                
        // return $pdf->;                
    }

    private function doExportPdf($tahunajaran,$semester){
        
    }
}
