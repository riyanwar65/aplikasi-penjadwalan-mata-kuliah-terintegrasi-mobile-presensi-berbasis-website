<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class MataKuliahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $matkul = DB::connection('presensi')                    
                    ->table('polindra_presensi.mata_kuliah')
                    ->join('polindra_siakad_v1_db.program_studi','polindra_siakad_v1_db.program_studi.program_studi_kode','polindra_presensi.mata_kuliah.program_studi')->orderBy('updated_at','desc')->get();                            
        $prodi = DB::connection('siakad')->table('program_studi')->get();
        return view('contents.matakuliah.index',compact('matkul','prodi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //         
       $kode_matkul = Carbon::today('Asia/Jakarta')->format('Ymdhis').rand(1200,9999);
       $request->validate([
        'nama_matkul'=>'required',
        'prodi'=>'required',
        'semester'=>'required',
        'teori'=>'required',
        'praktek'=>'required',
       ],[
           'required'=>':attribute Wajib di isi'
       ]);
       $d3aja = str_split($request->prodi);
        $d3 = $d3aja[0].$d3aja[1];        
       if ($d3=="D3"&&$request->semester>6) {
            $prodi = DB::connection('siakad')->table('program_studi')->where('program_studi_kode',$request->prodi)->first();
           return redirect()->route('matakuliah.index')->with('alert-danger','Semester '.$prodi->program_studi_nama.' tidak lebih dari 6 Semester');
       }  
        $cek = DB::connection('presensi')->table('mata_kuliah')->whereRaw('LOWER(`nama_matkul`) LIKE ? ',[trim(strtolower($request->nama_matkul)).'%'])->where('program_studi',$request->prodi)->get();                
        if (count($cek)>0) {
            return redirect()->route('matakuliah.index')->with('alert-danger','Data Sudah ada, tidak dapat menambahkan data');    
        } else {
            DB::connection('presensi')->table('mata_kuliah')->insert([
                'kode_matkul'=>$kode_matkul,
                 'nama_matkul'=>$request->nama_matkul,
                 'program_studi'=>$request->prodi,
                 'semester'=>$request->semester,
                 'sks_teori'=>$request->teori,
                 'sks_praktek'=>$request->praktek,
                 'created_at'=>Carbon::now('Asia/Jakarta'),
                 'updated_at'=>Carbon::now('Asia/Jakarta'),
            ]);
            return redirect()->route('matakuliah.index')->with('success','Data berhasil disimpan');              
        }               
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $editmatkul = DB::connection('presensi')->table('polindra_presensi.mata_kuliah')
                        ->join('polindra_siakad_v1_db.program_studi','polindra_siakad_v1_db.program_studi.program_studi_kode','polindra_presensi.mata_kuliah.program_studi')
                        ->where('kode_matkul',$id)->first();        
        return response()->json([
            'status'=>true,
            'code'=>200,
            'message'=>"Data Berhasil di Ambil",
            'matkul'=>$editmatkul
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //        
        $update = DB::connection('presensi')->table('mata_kuliah')->where('kode_matkul',$id)->update([
            'nama_matkul'=>$request->nama_matkul,
            'program_studi'=>$request->prodi,
            'semester'=>$request->semester,
            'sks_teori'=>$request->teori,
            'sks_praktek'=>$request->praktek,
            'updated_at'=>Carbon::now('Asia/Jakarta')
        ]);
        return redirect()->route('matakuliah.index')->with('success','Data berhasil diupdate');              

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::connection('presensi')->table('mata_kuliah')->where('kode_matkul',$id)->delete();
        return response()->json([
            'status'=>true,
            'code'=>200,
            'message'=>"Data Berhasil di Hapus"
        ]);
    }
}
