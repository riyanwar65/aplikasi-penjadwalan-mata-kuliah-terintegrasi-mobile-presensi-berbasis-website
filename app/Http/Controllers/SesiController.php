<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sesi;
use Carbon\Carbon;
class SesiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $sesi=Sesi::all();
        return view('contents.sesi.index',compact('sesi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('contents.sesi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd(str_replace(' ','',$request->jam_masuk));
        $this->validate($request,[
            'jam_masuk'=>'required|unique:presensi.sesi,jam_masuk',
            'jam_keluar'=>'required|unique:presensi.sesi,jam_keluar',
            'sesi'=>'required|unique:presensi.sesi,id_sesi'
        ],[
            'required'=>':attribute, Tidak Boleh Kosong',
            'unique'=>':attribute, Data Sudah Ada'
        ]);
        $masuk = str_replace(' ','',$request->jam_masuk);
        $keluar = str_replace(' ','',$request->jam_keluar);
        $cek = Sesi::where('jam_masuk',$masuk)->orWhere('jam_keluar',$keluar)->count();

        if ($cek>0) {
            return redirect()->route('sesi.create')->with(['jam_masuk'=>'Data Sudah Ada',
            'jam_keluar'=>'Data Sudah Ada']);
        }else{
            $insert = new Sesi;
            $insert->id_sesi = $request->sesi;
            $insert->jam_masuk = str_replace(' ','',$request->jam_masuk);
            $insert->jam_keluar = str_replace(' ','',$request->jam_keluar);
            $insert->save();
        }
        

        return redirect(route('sesi.index'))->with('success','Data berhasil di tambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $edit = Sesi::find($id);
        $masuk = Carbon::parse($edit->jam_masuk)->format('H:i');
        $keluar = Carbon::parse($edit->jam_keluar)->format('H:i');        
        return view('contents.sesi.edit',compact('edit','masuk','keluar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        // dd($request->all());             
        $update = Sesi::find($id);
        if ($update->id_sesi==$request->sesi) {            
            $update->id_sesi=$request->sesi;
            $update->jam_masuk=str_replace(' ','',$request->jam_masuk);
            $update->jam_keluar = str_replace(' ','',$request->jam_keluar);
            $update->update();
        }else{               
            $cek = Sesi::where('id_sesi',$request->sesi)->count();
            if ($cek>0) {                
                return redirect()->route('sesi.edit',$id)->with('errors','ID Sesi Sudah ada');
            }else{
                $update->id_sesi=$request->sesi;
                $update->jam_masuk=str_replace(' ','',$request->jam_masuk);
                $update->jam_keluar = str_replace(' ','',$request->jam_keluar);
                $update->update();
            }                
        }      
        
        return redirect()->route('sesi.index')->with('success','Data diperbaharui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $delete = Sesi::find($id);
        $delete->delete();
        
        return response()->json([
            'message'=>'Data berhasil di Hapus',
            'status'=>true,
            'code'=>200
        ],200);
    }
}
