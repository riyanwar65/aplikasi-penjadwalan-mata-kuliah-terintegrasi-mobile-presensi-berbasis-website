<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use DB;
use Session;
use Carbon\Carbon;
class SetColorController extends Controller
{
    //
    public function set(){
        if (Session::get('jurusan')=="TI") {
            return $this->getDatadosenPerJurusan("D3TIK","D4RPL");
        }if(Session::get('jurusan')=="TM"){
            return $this->getDatadosenPerJurusan("D3TMN","D4TPM");
        }if(Session::get('jurusan')=="TP"){
            return $this->getDatadosenPerJurusan("D3TPU",NULL);
        }if(Session::get('jurusan')=="KP"){
            return $this->getDatadosenPerJurusan("D3KP",NULL);
        }
        
    }
    private function getDatadosenPerJurusan($prodi1,$prodi2){
        if ($prodi2==null) {
            $dosen = DB::connection('siakad')->table('dosen')                        
            ->where('dosen.program_studi_kode',$prodi1)
            ->orWhere('dosen.program_studi_kode',"")
            ->orWhere('dosen.program_studi_kode',NULL)
            ->orWhere('dosen.dosen_jenis',"dosen_luar")
            ->get();
            $color = DB::connection('presensi')->table('color_dosen')
            ->join('polindra_siakad_v1_db.dosen','polindra_siakad_v1_db.dosen.dosen_kode','color_dosen.dosen_kode')
                ->get();
            return view('contents.setcolor.index',compact('dosen','color'));
        }else{
            $dosen = DB::connection('siakad')->table('dosen')            
            ->where('dosen.program_studi_kode',$prodi1)
            ->orWhere('dosen.program_studi_kode',$prodi2)
            ->orWhere('dosen.program_studi_kode',"")
            ->orWhere('dosen.program_studi_kode',NULL)
            ->orWhere('dosen.dosen_jenis',"dosen_luar")
            ->get();
            $color = DB::connection('presensi')->table('color_dosen')
            ->join('polindra_siakad_v1_db.dosen','polindra_siakad_v1_db.dosen.dosen_kode','color_dosen.dosen_kode')
            ->get();
        return view('contents.setcolor.index',compact('dosen','color'));
        }
    }

    public function setPost(Request $requset){        
        $setcolor = DB::connection('presensi')->table('color_dosen')->where('dosen_kode',$requset->dosen)->first();
        if ($setcolor) {
            return redirect()->back()->with('error','Tidak bisa menambahkan data karena data sudah ada');
        }else{    
            $cek = DB::connection('presensi')->table('color_dosen')->where('color_utama',$requset->warna_utama)->first();           
            if ($cek) {
                return redirect()->back()->with('error','Warna utama sudah ada');
            }else{
                $id = Str::random('15');
                $insert = DB::connection('presensi')->table('color_dosen')->insert([
                    'id_color'=>$id,
                    'dosen_kode'=>$requset->dosen,
                    'color_utama'=>$requset->warna_utama,
                    'color_font'=>$requset->warna_font,
                    'jurusan'=>Session::get('jurusan'),
                    'created_at'=>Carbon::now('Asia/Jakarta'),
                ]);
                return redirect()->back()->with('success','Berhasil Menambahkan data');
            }                
        }            
    }
    public function setUpdate($id,Request $requset){
        $cek = DB::connection('presensi')->table('color_dosen')->where('color_utama',$requset->warna_utama)->first();                   
        if ($cek) {
            return redirect()->back()->with('error','Warna utama sudah ada');
        }else{            
            $update = DB::connection('presensi')->table('color_dosen')->where('id_color',$id)->update([                              
                'color_utama'=>$requset->warna_utama,
                'color_font'=>$requset->warna_font,
            ]);
            return redirect()->back()->with('success','Berhasil Diperbaharui');
        }                
    }   
    public function hapus($id){
        $ambil = DB::connection('presensi')->table('color_dosen')            
            ->where('id_color',$id)
            ->first();
        $cek = DB::connection('presensi')->table('bkd')->where('dosen_kode',$ambil->dosen_kode)->get();
        if(count($cek)>0){
            return response()->json(
                ['status'=>false,'message'=>"Data sudah ada pada BKD"]
            );
        }else{
            DB::connection('presensi')->table('color_dosen')            
            ->where('id_color',$id)
            ->delete();
            return response()->json(
                ['status'=>true,'message'=>"Data berhasil di hapus"]
            );
        }
    }             
}
