<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use DB;
class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {    
        $tot = $this->totalMahasiswaTI();
        $semester = $this->jenisSemester();
        $totbkd = $this->totBKDaktif();  
        $tahun_ajaran = $this->tahunAjaran();              
        $kelas = $this->kelas();     
        $ruangan = $this->ruangan(); 
        $dosen = $this->totaldosen();

        $totMhsSiakad = $this->totalMahasiswa();
        $totbkdSiakad = $this->totBKD();
        $totKelas = $this->totKelas();
        $totDosen = $this->totDosen();
        $totRuangan = $this->totRuangan();

        if (strtolower(Session::get('jabatan'))=="sekertaris jurusan"
        ||strtolower(Session::get('jabatan'))=="sekertaris"
        ||strtolower(Session::get('jabatan'))=="sekjur") {        
            return view('permissions.sekertarisjurusan.dashboard',compact('dosen','tot','totbkd','semester','tahun_ajaran','kelas','ruangan'));
        }
        if (strtolower(Session::get('jabatan'))=="admin jurusan"
        ||strtolower(Session::get('jabatan'))=="admin") {
            return view('permissions.adminjurusan.dashboard',compact('dosen','tot','totbkd','semester','tahun_ajaran','kelas','ruangan'));
        }if (strtolower(Session::get('jabatan'))=="0") {
            return view('permissions.adminsiakad.dashboard',compact('totMhsSiakad','totDosen','totKelas','totbkdSiakad','semester','tahun_ajaran','totKelas','totRuangan'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // Khusus Siakad   
    private function totalMahasiswa(){
        $totalMhs = DB::connection('siakad')->table('mahasiswa')
        ->join('program_studi','program_studi.program_studi_kode','mahasiswa.program_studi_kode')
        ->join('program_jurusan','program_studi.jurusan_kode','program_jurusan.jurusan_kode')
        ->join('data_kelas','data_kelas.kelas_kode','mahasiswa.kelas_kode')
        ->where('ijasah_status','<>',1)        
        ->get();
        return $totalMhs;
    }

    private function totBKD(){
        $bkdaktif = DB::connection('presensi')->table('bkd')->whereRaw('LOWER(`status_bkd`)="aktif"')->get();
        return $bkdaktif;     
    }

    private function totKelas(){
        $kelas = DB::connection('presensi')->table('bkd')->whereRaw('LOWER(`status_bkd`)="aktif"')->groupBy('kode_kelas')->get(['kode_kelas']);
        return $kelas;
    }

    private function totDosen(){
        $dosen = DB::connection('presensi')->table('temp_jadwal')->groupBy('dosen_kode')->get();
        return $dosen;
    }

    private function totRuangan(){
        $ruangan = DB::connection('presensi')->table('ruangan')->get();
        return $ruangan;
    }
    // END OF KHUSUS SIAKAD

    private function totalMahasiswaTI(){
        $totalMhsjurusan = DB::connection('siakad')->table('mahasiswa')
        ->join('program_studi','program_studi.program_studi_kode','mahasiswa.program_studi_kode')
        ->join('program_jurusan','program_studi.jurusan_kode','program_jurusan.jurusan_kode')
        ->join('data_kelas','data_kelas.kelas_kode','mahasiswa.kelas_kode')
        ->where('ijasah_status','<>',1)
        ->where('program_jurusan.jurusan_kode',Session::get('jurusan'))
        ->get();
        return $totalMhsjurusan;
    }

    private function totBKDaktif(){
        $bkdaktif = DB::connection('presensi')->table('bkd')->where('jurusan',Session::get('jurusan'))->whereRaw('LOWER(`status_bkd`)="aktif"')->get();
        return $bkdaktif;     
    }    

    private function jenisSemester(){
        $jenis = DB::connection('presensi')->table('bkd')->where('jurusan',Session::get('jurusan'))->whereRaw('LOWER(`status_bkd`)="aktif"')->groupBy('semester')->get(['semester']);
        foreach ($jenis as $key => $value) {
            if ($value->semester%2==0) {
                $semester = "genap";
                return $semester;
            }else{
                $semester = "ganjil";
                return $semester;
            }
        }   
        
    }
    

    private function tahunAjaran(){
        $tahun_ajaran = DB::connection('presensi')->table('bkd')->where('jurusan',Session::get('jurusan'))->whereRaw('LOWER(`status_bkd`)="aktif"')->groupBy('semester')->first(['tahun_ajaran']);
        return $tahun_ajaran;
    }

    private function kelas(){
        $kelas = DB::connection('presensi')->table('bkd')->where('jurusan',Session::get('jurusan'))->whereRaw('LOWER(`status_bkd`)="aktif"')->groupBy('kode_kelas')->get(['kode_kelas']);
        return $kelas;
    }
    
    private function ruangan(){
        $ruangan = DB::connection('presensi')->table('ruangan')->where('jurusan',Session::get('jurusan'))->get();
        return $ruangan;
    }

    private function totaldosen(){
        $dosen = DB::connection('presensi')->table('temp_jadwal')->where('jurusan',Session::get('jurusan'))->groupBy('dosen_kode')->get();
        return $dosen;
    }
}
