<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mahasiswa;
use App\Kelas;
use App\User;
use DB;
use Session;
class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $no =1;  
        if (Session::get('jabatan')=="0") {
            $mhs = DB::connection('siakad')->table('mahasiswa')
                 ->join('data_kelas','mahasiswa.kelas_kode','data_kelas.kelas_kode')
                 ->join('mahasiswa_biodata','mahasiswa_biodata.mahasiswa_kode','mahasiswa.mahasiswa_kode')                 
                 ->join('program_studi','program_studi.program_studi_kode','data_kelas.program_studi_kode')                 
                 ->join('program_jurusan','program_studi.jurusan_kode','program_jurusan.jurusan_kode')                 
                 ->where('data_kelas.tingkat','<>','0')
                 ->where('mahasiswa.ijasah_status','<>','1')                 
                 ->orderBy('tahun_index','desc')->get();  
            return view('contents.mahasiswa.index',compact('mhs','no'));
        }else{
            $mhs = DB::connection('siakad')->table('mahasiswa')
                ->join('data_kelas','mahasiswa.kelas_kode','data_kelas.kelas_kode')
                ->join('mahasiswa_biodata','mahasiswa_biodata.mahasiswa_kode','mahasiswa.mahasiswa_kode')                 
                ->join('program_studi','program_studi.program_studi_kode','data_kelas.program_studi_kode')                 
                ->join('program_jurusan','program_studi.jurusan_kode','program_jurusan.jurusan_kode')
                ->where('program_jurusan.jurusan_kode',Session::get('jurusan'))
                ->where('data_kelas.tingkat','<>','0')
                ->where('mahasiswa.ijasah_status','<>','1')                 
                ->orderBy('tahun_index','desc')->get();  
            return view('contents.mahasiswa.index',compact('mhs','no'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $detailMhs = Mahasiswa::with('mahasiswa_biodata','mahasiswa_kelas','kelas_tahun')->where('mahasiswa_kode',$id)->first();
        return response()->json(
            [
             'mahasiswa_detail'=>$detailMhs
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //        
        $user = DB::connection('presensi')->table('polindra_presensi.users')->join('polindra_system_v1_db.com_user','polindra_system_v1_db.com_user.user_id','polindra_presensi.users.user_id')
        ->join('polindra_siakad_v1_db.mahasiswa','polindra_siakad_v1_db.mahasiswa.user_id','polindra_presensi.users.user_id')
        ->where('polindra_presensi.users.user_id',$id)->first();
        if ($user) {         
            $user = User::find($id);
            $user->uuid=NULL;
            $user->save();
            return response()->json([
                'status'=>true,
                'Message'=>"UUID berhasil di Reset"
            ]);   
        }else{
            return response()->json([
                'status'=>false,
                'Message'=>"UUID Tidak Ditemukan"
            ]);   
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
