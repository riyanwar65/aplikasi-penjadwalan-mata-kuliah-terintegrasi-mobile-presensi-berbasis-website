<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dosen;
use App\AdminJurusan;
use App\DataJabatan;
use App\DataJabatanPegawai;
use DB;
use Carbon\Carbon;
use Session;
class JabatanPegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $dosen = Dosen::join('program_studi','program_studi.program_studi_kode','dosen.program_studi_kode')
                ->join('program_jurusan','program_jurusan.jurusan_kode','program_studi.jurusan_kode')
                ->where('program_jurusan.jurusan_kode',Session::get('jurusan'))
                ->where('dosen_jenis','homebase')
                ->get();
        $admin = AdminJurusan::all();
        $jabatan = DataJabatan::all();
        return view('contents.jabatanpegawai.index',compact('dosen','admin','jabatan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $jabatanpegawai = new DataJabatanPegawai;
        $jabatanpegawai->id_detail_jabatan="DETAILJBTN".mt_rand(1000,9999);
        $jabatanpegawai->id_pegawai = $request->dosen;
        $jabatanpegawai->id_jabatan = $request->jabatan;
        $jabatanpegawai->periode = $request->awal."/".$request->akhir;
        $jabatanpegawai->jurusan = $request->jurusan;
        $jabatanpegawai->created_at=Carbon::now('Asia/Jakarta');
        $jabatanpegawai->updated_at=Carbon::now('Asia/Jakarta');
        $jabatanpegawai->save();

        return redirect(route('jabatan.index'))->with('success','Data Jabatan Dosen Jurusan Berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $cek_1 = DB::connection('presensi')->table('detail_jabatan_pegawai')
                ->join('admin_jurusan','admin_jurusan.kode_admin_jurusan','detail_jabatan_pegawai.id_pegawai')
                ->where('admin_jurusan.kode_admin_jurusan',$id)
                ->count();
        $cek_2 = DB::connection('presensi')->table('polindra_presensi.detail_jabatan_pegawai')
                ->join('polindra_siakad_v1_db.dosen','polindra_siakad_v1_db.dosen.user_id','polindra_presensi.detail_jabatan_pegawai.id_pegawai')
                ->where('polindra_siakad_v1_db.dosen.user_id',$id)
                ->count();
        if ($cek_1>0) {
            $admin = DB::connection('presensi')->table('detail_jabatan_pegawai')
                ->join('data_jabatan','data_jabatan.id','detail_jabatan_pegawai.id_jabatan')
                ->join('admin_jurusan','admin_jurusan.kode_admin_jurusan','detail_jabatan_pegawai.id_pegawai')
                ->where('admin_jurusan.kode_admin_jurusan',$id)
                ->first();
            $jabatan = DataJabatan::all();
            $jurusan = DB::connection('siakad')->table('program_jurusan')->get();
            $periode = explode('/',$admin->periode);   
            $namalengkap = explode(' ',$admin->nama_admin_jurusan);            
            return view('contents.registrasi_admin.edit',compact('admin','jabatan','periode','namalengkap','jurusan'));
        }else if($cek_2>0){
            $dosenjabatan = DB::connection('presensi')->table('polindra_presensi.detail_jabatan_pegawai')
                ->join('polindra_presensi.data_jabatan','polindra_presensi.data_jabatan.id','polindra_presensi.detail_jabatan_pegawai.id_jabatan')
                ->join('polindra_siakad_v1_db.dosen','polindra_siakad_v1_db.dosen.user_id','polindra_presensi.detail_jabatan_pegawai.id_pegawai')
                ->where('polindra_siakad_v1_db.dosen.user_id',$id)
                ->first();
            $jabatan = DataJabatan::all();
            $jurusan = DB::connection('siakad')->table('program_jurusan')->get();
            $dosen = $this->cek_jurusan($dosenjabatan->jurusan);            
            $periode = explode('/',$dosenjabatan->periode);   
            return view('contents.jabatanpegawai.edit',compact('dosen','jabatan','periode','dosenjabatan','jurusan'));
        }else {
            return "Ga ada datanya";
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $dosenjabatan = DB::connection('presensi')->table('detail_jabatan_pegawai')                
                ->where('id_pegawai',$id)
                ->update([
                    'id_pegawai'=>$request->dosen,
                    'id_jabatan'=>$request->jabatan,
                    'periode'=>$request->awal."/".$request->akhir,
                    'jurusan'=>$request->jurusan,
                    'updated_at'=>Carbon::now('Asia/Jakarta')
                ]);

        return redirect(route('jabatan.index'))->with('success','Data Berhasil di Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $hapus = DB::connection('presensi')->table('detail_jabatan_pegawai')->where('id_pegawai',$id)->update([
            'deleted_at'=>Carbon::now('Asia/Jakarta')
        ]);
            
        return response()->json([
            'status'=>true,
            'message'=>"Data berhasil di Arsipkan",
        ]);
    }

    // public function jabatanadmin(Request $request){
    //     return $request->all();
    // }
    
    private function cek_jurusan($jurusan){
        if ($jurusan=='TI') {
            return Dosen::where('program_studi_kode','D4RPL')->orWhere('program_studi_kode','D3TIK')->get();
        }else if($jurusan=='TM'){
            return Dosen::where('program_studi_kode','D3TMN')->orWhere('program_studi_kode','D4TPM')->get();
        }else if($jurusan=='TPTU'){
            return Dosen::where('program_studi_kode','D3TPU')->get();
        }else if($jurusan=='KP'){
            return Dosen::where('program_studi_kode','D3KP')->get();
        }else{
            return 0;
        }
    }
}
