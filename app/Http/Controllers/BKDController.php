<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Session;
class BKDController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $bkd = DB::connection('presensi')->table('bkd')->join('polindra_siakad_v1_db.dosen','polindra_siakad_v1_db.dosen.dosen_kode','bkd.dosen_kode')
                ->select('bkd.*','polindra_siakad_v1_db.dosen.dosen_nama','polindra_siakad_v1_db.dosen.dosen_kode','polindra_siakad_v1_db.dosen.dosen_gelar_depan','polindra_siakad_v1_db.dosen.dosen_gelar_belakang','polindra_siakad_v1_db.data_kelas.*','mata_kuliah.*')
                ->join('polindra_siakad_v1_db.data_kelas','polindra_siakad_v1_db.data_kelas.kelas_kode','bkd.kode_kelas')
                ->join('mata_kuliah','mata_kuliah.kode_matkul','bkd.kode_matkul')
                ->where('status_bkd','Aktif')
                ->where('jurusan',Session::get('jurusan'))
                ->orderBy('bkd.updated_at','desc')
                ->get();   
        $tahun_ajaran = DB::connection('presensi')->table('bkd')->where('status_bkd','Aktif')->groupBy('tahun_ajaran')->get();                
        return view('contents.bkd.index',compact('bkd','tahun_ajaran'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //       
        $jurusan = DB::connection('siakad')->table('program_jurusan')->where('jurusan_kode',Session::get('jurusan'))->get();
        $program_studi = DB::connection('siakad')->table('program_studi')->where('jurusan_kode',Session::get('jurusan'))->get();
        $semester = DB::connection('presensi')->table('mata_kuliah')->where('semester','<>','NULL')->groupBy('semester')->get();
        return view('contents.bkd.create',compact('jurusan','semester','program_studi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //           
        $tahun_ajaran = $request->awal."/".$request->akhir;                     
        $request->validate([
            'prodi'=>'required',
            'dosenprodi'=>'required',
            'matkul'=>'required',
            'kelas'=>'required',
            'semester'=>'required',
            'sks_praktek'=>'required',
            'sks_teori'=>'required'
        ],[
            'required'=>':attribute Tidak Boleh Kosong'
        ]); 
        if ($request->semester%2==0) {
            $jenis = "ganjil";
            $ganjil = [1,3,5,7];
            $genap = [2,4,6,8];
            $get = DB::connection('presensi')->table('bkd')
                   ->where('jurusan',Session::get('jurusan'))
                   ->whereIn('semester',$genap)
                   ->first();
            if ($get) {             
                if($get->tahun_ajaran!=$tahun_ajaran){
                    $cek = DB::connection('presensi')->table('bkd')
                        ->where('jurusan',Session::get('jurusan'))
                        ->whereRaw('LOWER(`status_bkd`)="aktif"')
                        ->where('tahun_ajaran',"<>",$tahun_ajaran)
                        ->whereIn('semester',$genap)
                        ->update([
                            'status_bkd'=>"Tidak Aktif"
                            ]);    
                }
            }else{
            $cek = DB::connection('presensi')->table('bkd')
                  ->where('jurusan',Session::get('jurusan'))
                  ->whereRaw('LOWER(`status_bkd`)="aktif"')
                //   ->where('tahun_ajaran',"<>",$tahun_ajaran)
                  ->whereIn('semester',$ganjil)
                  ->update([
                      'status_bkd'=>"Tidak Aktif"
                  ]);  
            }
        }else{
            $jenis = "genap";
            $ganjil = [1,3,5,7];
            $genap = [2,4,6,8];
            $get = DB::connection('presensi')->table('bkd')
                   ->where('jurusan',Session::get('jurusan'))
                   ->whereIn('semester',$ganjil)
                   ->first();
            if ($get) {             
                if($get->tahun_ajaran!=$tahun_ajaran){
                    $cek = DB::connection('presensi')->table('bkd')
                    ->where('jurusan',Session::get('jurusan'))
                    ->whereRaw('LOWER(`status_bkd`)="aktif"')
                    ->where('tahun_ajaran',"<>",$tahun_ajaran)
                    ->whereIn('semester',$ganjil)
                    ->update([
                        'status_bkd'=>"Tidak Aktif"
                    ]);    
                }
            }else{
            $cek = DB::connection('presensi')->table('bkd')
                  ->where('jurusan',Session::get('jurusan'))
                  ->whereRaw('LOWER(`status_bkd`)="aktif"')
                //   ->where('tahun_ajaran',"<>",$tahun_ajaran)
                  ->whereIn('semester',$genap)
                  ->update([
                      'status_bkd'=>"Tidak Aktif"
                  ]);                
            }
        }                   
        return $this->simpanData($request);
    }

    private function simpanData($request){
        $tahun_ajaran = $request->awal."/".$request->akhir;
        $total_sks = $request->sks_praktek+$request->sks_teori;       
        $kode_bkd ="BKD".Carbon::today('Asia/Jakarta')->format('Ymd').rand(1200,9999);        
        DB::connection('presensi')->table('bkd')->insert([
            'kode_bkd'=>$kode_bkd,
            'kode_kelas'=>$request->kelas,
            'tahun_ajaran'=>$tahun_ajaran,
            'dosen_kode'=>$request->dosenprodi,
            'kode_matkul'=>$request->matkul,
            'semester'=>$request->semester,
            'jumlah_sks_teori'=>$request->sks_teori,
            'jumlah_sks_praktek'=>$request->sks_praktek,
            'total_sks'=>$total_sks,
            'status_bkd'=>'Aktif',
            'jurusan'=>$request->prodi,
            'created_at'=>Carbon::now('Asia/Jakarta'),
            'updated_at'=>Carbon::now('Asia/Jakarta')
        ]);
        return redirect()->route('bkd.index')->with('success','Data Berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $ambil_jurusan = DB::connection('presensi')->table('bkd')->where('kode_bkd',$id)->first();
        $program_studi = DB::connection('siakad')->table('program_studi')->where('jurusan_kode',Session::get('jurusan'))->get();
        $semester = DB::connection('presensi')->table('mata_kuliah')->where('semester','<>','NULL')->groupBy('semester')->get();        
        $bkd = DB::connection('presensi')->table('polindra_presensi.bkd')
                ->join('polindra_siakad_v1_db.data_kelas','polindra_siakad_v1_db.data_kelas.kelas_kode','bkd.kode_kelas')
                ->join('polindra_siakad_v1_db.dosen','polindra_siakad_v1_db.dosen.dosen_kode','polindra_presensi.bkd.dosen_kode')
                ->join('polindra_presensi.mata_kuliah','polindra_presensi.mata_kuliah.kode_matkul','polindra_presensi.bkd.kode_matkul')
                ->join('polindra_siakad_v1_db.program_jurusan','polindra_siakad_v1_db.program_jurusan.jurusan_kode','polindra_presensi.bkd.jurusan')
                ->where('polindra_siakad_v1_db.program_jurusan.jurusan_kode',$ambil_jurusan->jurusan)
                ->where('polindra_presensi.bkd.kode_bkd',$id)
                ->first();        
        $tahunajaran=explode("/",$bkd->tahun_ajaran);
        return view('contents.bkd.edit',compact('bkd','tahunajaran','program_studi','semester'));   
        // dd($bkd);
    }    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $tahun_ajaran = $request->awal."/".$request->akhir;
        $total_sks = $request->sks_praktek+$request->sks_teori; 
        $update = DB::connection('presensi')->table('bkd')->where('kode_bkd',$id)->update([            
            'kode_kelas'=>$request->kelas,
            'tahun_ajaran'=>$tahun_ajaran,
            'dosen_kode'=>$request->dosenprodi,
            'kode_matkul'=>$request->matkul,
            'semester'=>$request->semester,
            'jumlah_sks_teori'=>$request->sks_teori,
            'jumlah_sks_praktek'=>$request->sks_praktek,
            'total_sks'=>$total_sks,            
            'updated_at'=>Carbon::now('Asia/Jakarta')
        ]);
        return redirect()->route('bkd.index')->with('success','Data Berhasil di update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::connection('presensi')->table('bkd')->where('kode_bkd',$id)->delete();
        return response()->json([
            'status'=>true,
            'code'=>200,
            'message'=>"Data Berhasil di Hapus",
        ]);
    }
    public function programstudi(){
        $programstudi = DB::connection('siakad')->table('data_kelas')->get('program_studi_kode')->groupBy('program_studi_kode');        
        return response()->json([
            'status'=>true,
            'code'=>200,
            'message'=>"Data berhasil di ambil",
            'data'=>$programstudi
        ]);
    }
    public function dosenprodi($id){   
        $dosen = DB::connection('siakad')
                ->table('dosen')                
                ->join('program_studi','program_studi.program_studi_kode','dosen.program_studi_kode')
                ->join('program_jurusan','program_jurusan.jurusan_kode','program_studi.jurusan_kode')
                ->where('program_jurusan.jurusan_kode',Session::get('jurusan'))                  
                ->orWhere('dosen.program_studi_kode',"")
                ->orWhere('dosen.program_studi_kode',NULL)
                ->orWhere('dosen.dosen_jenis',"dosen_luar")
                ->get();                
        $dosen_luar = DB::connection('siakad')
                    ->table('dosen')                
                    ->where('program_studi_kode',"")
                    ->get();                
        return response()->json([
            'status'=>true,
            'code'=>200,
            'message'=>'Data Berhasil di Ambil',
            'dosen'=>$dosen,
            'dosen_luar'=>$dosen_luar
        ]);            
    }
    public function kelasprodi($id,$semester){
        if ($semester=="1"||$semester=="2") {
            return $this->filterkelas($id,"1");
        }if ($semester=="3"||$semester=="4") {
            return $this->filterkelas($id,"2");
        }if ($semester=="5"||$semester=="6") {
            return $this->filterkelas($id,"3");
        }if ($semester=="7"||$semester=="8") {
            if ($id == "D3TIK") {
                return response()->json([
                    'status'=>false,
                    'code'=>200,
                    'message'=>'Data Ga ada',                    
                ]);      
            }else{
                return $this->filterkelas($id,"3");
            }            
        }        
    }

    private function filterkelas($id,$tingkat){
        $kelas = DB::connection('siakad')
                ->table('mahasiswa')
                ->join('data_kelas','mahasiswa.kelas_kode','data_kelas.kelas_kode')
                ->join('program_studi','program_studi.program_studi_kode','data_kelas.program_studi_kode')
                ->join('program_jurusan','program_jurusan.jurusan_kode','program_studi.jurusan_kode')
                ->where('program_jurusan.jurusan_kode',Session::get('jurusan'))   
                ->where('data_kelas.program_studi_kode',$id)
                ->where('data_kelas.tingkat',$tingkat)
                ->where('tingkat','<>','0') 
                ->where('mahasiswa.mahasiswa_status','Aktif')
                ->where('mahasiswa.tahun_index','<>','2016')                
                ->groupBy('data_kelas.kelas_kode')            
                ->get();   
        return response()->json([
            'status'=>true,
            'code'=>200,
            'message'=>'Data Berhasil di Ambil',
            'kelas'=>$kelas
        ]);        
    }

    public function bkdmatkul($id,$semester){
        $matkul = DB::connection('presensi')
                  ->table('polindra_presensi.mata_kuliah')
                  ->join('polindra_siakad_v1_db.program_studi','polindra_presensi.mata_kuliah.program_studi','polindra_siakad_v1_db.program_studi.program_studi_kode')
                  ->join('polindra_siakad_v1_db.program_jurusan','polindra_siakad_v1_db.program_studi.jurusan_kode','polindra_siakad_v1_db.program_jurusan.jurusan_kode')
                  ->where('mata_kuliah.program_studi',$id)
                  ->where('polindra_siakad_v1_db.program_jurusan.jurusan_kode',Session::get('jurusan'))
                  ->where('polindra_presensi.mata_kuliah.semester',$semester)
                  ->get();        
        return response()->json([
            'matkul'=>$matkul
        ]);      
    } 
}
