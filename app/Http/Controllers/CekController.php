<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Illuminate\Support\Str;
use Carbon\Carbon;
class CekController extends Controller
{
    //
    public function cek($tahun_ajaran,$semester){
        $pengecekan = DB::connection('presensi')->table("temp_jadwal")
                    ->where('jurusan',Session::get('jurusan'))->whereIn('semester',$semester)
                    ->where('tahun_ajaran',$tahun_ajaran)
                    ->update([
                        'status_jadwal'=>"tidak aktif"
                    ]);        
            $data = [[]];
            $tampung =[];
            $test = DB::connection('presensi')
            ->table('schedule')
            ->join('bkd','bkd.kode_bkd','schedule.kode_bkd')
            ->join('polindra_siakad_v1_db.dosen','polindra_siakad_v1_db.dosen.dosen_kode','bkd.dosen_kode')
            ->join('mata_kuliah','mata_kuliah.kode_matkul','bkd.kode_matkul')
            ->join('polindra_siakad_v1_db.data_kelas','polindra_siakad_v1_db.data_kelas.kelas_kode','bkd.kode_kelas')                    
            ->where('schedule.jurusan',Session::get('jurusan')) 
            ->where('type',1)        
            ->get();
            $sesi = DB::connection('presensi')->table('sesi')->get();
            foreach ($test as $key => $value) {          
                $tampung[$key]=json_decode($value->sesi);
                for ($i=0; $i < count(json_decode($value->sesi)); $i++) {                                     
                     $insert = DB::connection('presensi')->table('temp_jadwal')->insert([
                         'id_temp_jadwal'=>Str::random(20),
                         'tahun_ajaran'=>"2019/2020",
                         'hari'=>$value->hari,
                         'kelas'=>$value->kelas_kode,
                         'jurusan'=>$value->jurusan,
                         'status_jadwal'=>"aktif",
                         'dosen_kode'=>$value->dosen_kode,
                         'kode_matkul'=>$value->kode_matkul,
                         'ruangan_id'=>$value->ruangan_id,
                         'semester'=>$value->semester,
                         'sesi_masuk'=>$tampung[$key][$i],                    
                         'created_at'=>Carbon::now('Asia/Jakarta')
                     ]);
                }
            }                   
    }

    public function editcek(Request $request){ 
        $genap = [2,4,6,8];
        $ganjil = [1,3,5,7];       
        if ($request->semester=="genap") {
            return $this->dataFilter($request->tahun_ajaran,$genap,$request->ruangan);            
        }else{
            return $this->dataFilter($request->tahun_ajaran,$ganjil,$request->ruangan);            
        }        
    }

    private function dataFilter($tahun_ajaran,$semester,$ruangan){
        $data = [];
        $cek = DB::connection('presensi')->table('temp_jadwal')
            ->join('polindra_siakad_v1_db.dosen','polindra_siakad_v1_db.dosen.dosen_kode','temp_jadwal.dosen_kode')
            ->join('mata_kuliah','mata_kuliah.kode_matkul','temp_jadwal.kode_matkul')
            ->join('polindra_siakad_v1_db.data_kelas','polindra_siakad_v1_db.data_kelas.kelas_kode','temp_jadwal.kelas')                   
            ->where('temp_jadwal.jurusan',Session::get('jurusan'))   
            ->where('temp_jadwal.status_jadwal','aktif')          
            ->where('temp_jadwal.ruangan_id',$ruangan)
            ->where('temp_jadwal.tahun_ajaran',$tahun_ajaran)
            ->whereIn('temp_jadwal.semester',$semester)
            ->get();
        $sesi = DB::connection('presensi')->table('sesi')->get();
        foreach($cek as $key=>$value){            
            $data[$key]=[
                "dosen"=>$value->dosen_nama,
                'id_temp_jadwal'=>$value->id_temp_jadwal,
                'dosen_kode'=>$value->dosen_kode,
                'kode_matkul'=>$value->kode_matkul,
                'matkul'=>$value->nama_matkul,
                'kelas'=>$value->kelas_nama,
                'jenis'=>$value->jenis,
                'hari'=>$value->hari,
                'sesi'=>$value->sesi_masuk,  
            ];
        }
        return response()->json([
            'generate'=>$data,  
            'sesi'=>$sesi,
        ]);
    }

    public function tambahjenis(Request $request){        
        $cek = DB::connection('presensi')->table('temp_jadwal')
        ->where('temp_jadwal.status_jadwal','aktif')          
        ->where('dosen_kode',$request->kddos)
        ->where('kode_matkul',$request->kdmatkul)
        ->where('kelas',$request->kelas)        
        ->get();
        $cek3 = DB::connection('presensi')->table('temp_jadwal')
        ->where('temp_jadwal.status_jadwal','aktif')          
        ->where('dosen_kode',$request->kddos)
        ->where('kode_matkul',$request->kdmatkul)
        ->where('kelas',$request->kelas)
        ->where('jenis',$request->tipe)
        ->get();
        $cek2 = DB::connection('presensi')->table('bkd')
                ->where('dosen_kode',$request->kddos)
                ->where('kode_matkul',$request->kdmatkul)
                ->where("status_bkd","Aktif")
                ->where('kode_kelas',$request->kelas)
                ->first();
        if (count($cek)>0) {
            $banyak = count($cek3);
            if ($request->tipe=="praktek") {
                if ($banyak<($cek2->jumlah_sks_praktek*2)) {         
                    $update = DB::connection('presensi')->table('temp_jadwal')->where('id_temp_jadwal',$request->temp)->update([
                        'jenis'=>$request->tipe,
                    ]);        
                    return response()->json([
                        'status'=>true,
                        'code'=>200,
                        'prakteksks'=>($cek2->jumlah_sks_praktek*2)." Banyak : ".$banyak,                    
                        'message'=>"Tipe Jadwal ".$request->tipe." berhasil di tambahkan",                
                    ]);       
                }else if ($cek2->jumlah_sks_praktek=="0") {                    
                    return response()->json([
                        'status'=>false,
                        'code'=>200,
                        'message'=>"Tipe jadwal ".$request->tipe." hanya berjumlah ".$cek2->jumlah_sks_praktek,                
                    ]);  
                }else{
                    return response()->json([
                        'status'=>false,
                        'code'=>200,
                        'message'=>"Tipe jadwal ".$request->tipe." hanya berjumlah ".$cek2->jumlah_sks_praktek." SKS dengan total Sesi ".($cek2->jumlah_sks_praktek*2),                
                    ]);  
                }
            }else{
                if ($banyak<$cek2->jumlah_sks_teori) {         
                    $update = DB::connection('presensi')->table('temp_jadwal')->where('id_temp_jadwal',$request->temp)->update([
                        'jenis'=>$request->tipe,
                    ]);        
                    return response()->json([
                        'status'=>true,
                        'code'=>200,
                        'message'=>"Tipe Jadwal ".$request->tipe." berhasil di tambahkan",                
                    ]);       
                }else if ($cek2->jumlah_sks_teori=="0") {                    
                    return response()->json([
                        'status'=>false,
                        'code'=>200,
                        'message'=>"Tipe jadwal ".$request->tipe." hanya berjumlah ".$cek2->jumlah_sks_teori,                
                    ]);  
                }
                else{
                    return response()->json([
                        'status'=>false,
                        'code'=>200,
                        'message'=>"Tipe jadwal ".$request->tipe." hanya berjumlah ".$cek2->jumlah_sks_teori,                
                    ]);  
                }
            } 
        }else{
            $update = DB::connection('presensi')->table('temp_jadwal')->where('id_temp_jadwal',$request->temp)->update([
                'jenis'=>$request->tipe,
            ]);  
            return response()->json([
                'status'=>true,
                'code'=>200,
                'message'=>"Tipe Jadwal ".$request->tipe." berhasil di tambahkan",                
            ]);  
        }        
    } 
    public function perubahansesi(Request $request){
        // Pengecekan data 
        $cek = DB::connection('presensi')
                ->table('temp_jadwal')
                ->join('sesi','sesi.id_sesi','temp_jadwal.sesi_masuk')
                ->join('polindra_siakad_v1_db.dosen','polindra_siakad_v1_db.dosen.dosen_kode','temp_jadwal.dosen_kode')
                ->join('polindra_siakad_v1_db.data_ruangan','polindra_siakad_v1_db.data_ruangan.ruangan_id','temp_jadwal.ruangan_id')
                ->join('mata_kuliah','mata_kuliah.kode_matkul','temp_jadwal.kode_matkul')                                
                ->where('temp_jadwal.status_jadwal','aktif')          
                ->where('temp_jadwal.hari',$request->hari_target)
                ->where('temp_jadwal.sesi_masuk',$request->sesi_target) 
                ->where('temp_jadwal.kelas',$request->kelas_bawaan)               
                ->first();
        if ($request->kelas_target!=$request->kelas_bawaan) {
            return response()->json([
                "status"=>false,
                "code"=>503,
                "message"=>"Kelas Tidak Sesuai"
            ]);
        }else{
            // if ($cek->id_temp_jadwal==$request->temp_id) {
            //     $update = DB::connection('presensi')->table('temp_jadwal')->where('id_temp_jadwal',$request->temp_id)->update([
            //         'sesi_masuk'=>$request->sesi_target,
            //         'hari'=>$request->hari_target,
            //     ]);
            //     return response()->json([
            //         'status'=>true,
            //         'code'=>200,
            //         'message'=>"Data Berhasil",
            //     ]);
            // }            
            if(!$cek){                
                $update = DB::connection('presensi')->table('temp_jadwal')->where('id_temp_jadwal',$request->temp_id)->update([
                    'sesi_masuk'=>$request->sesi_target,
                    'hari'=>$request->hari_target,
                ]);
                return response()->json([
                    'status'=>true,
                    'code'=>200,
                    'message'=>"Data Berhasil",
                    'data'=>$update,
                ]);   
            }else{
                if ($cek->id_temp_jadwal==$request->temp_id) {
                    return response()->json([
                        'status'=>true,
                        'code'=>200,
                        'message'=>"Data Berhasil",
                    ]);
                }
                return response()->json([
                    'status'=>false,
                    'code'=>503,
                    'message'=>"Pada hari ".$cek->hari." sudah ada ".$cek->nama_matkul." ".$cek->dosen_nama." ".$cek->jam_masuk." ".$cek->ruangan_nama,
                    'data'=>$cek
                ]);
            }                                                      
        }
    } 
    
    public function publish(Request $request){
        $genap = [2,4,6,8];
        $ganjil = [1,3,5,7];  
        $cek = DB::connection('presensi')->table('jadwal')->get();
        if (count($cek)>0) {
            DB::connection('presensi')->table('jadwal')->update([
                'status_perkuliahan'=>"Tidak Aktif",
                'status_jam_ganti'=>"Tidak Aktif",
            ]);
        }        
        $tahun_ajaran =$request->tahun_ajaran;
        // $tahun_ajaran ="2019/2020";           
        if ($request->semester=='genap') {
            $this->integrasi($tahun_ajaran,$genap);
        }else{
            $this->integrasi($tahun_ajaran,$ganjil);
        }        
    }

    private function integrasi($tahun_ajaran,$semester){
        $created_at = Carbon::now('Asia/Jakarta');   
            $cek = DB::connection('presensi')
            ->table('temp_jadwal')
            ->select(DB::raw('id_temp_jadwal,temp_jadwal.semester,temp_jadwal.tahun_ajaran,ruangan_nama,temp_jadwal.ruangan_id,
                            temp_jadwal.kode_matkul,temp_jadwal.dosen_kode,
                            dosen_nama,nama_matkul,temp_jadwal.dosen_kode,kelas,hari,temp_jadwal.kode_matkul,sesi_masuk,count(*) as `jumlah`'))
            ->join('polindra_siakad_v1_db.dosen','polindra_siakad_v1_db.dosen.dosen_kode','temp_jadwal.dosen_kode')
            ->join('mata_kuliah','mata_kuliah.kode_matkul','temp_jadwal.kode_matkul')
            ->join('polindra_siakad_v1_db.data_kelas','polindra_siakad_v1_db.data_kelas.kelas_kode','temp_jadwal.kelas')                   
            ->join('polindra_siakad_v1_db.data_ruangan','polindra_siakad_v1_db.data_ruangan.ruangan_id','temp_jadwal.ruangan_id')                   
            ->whereIn('temp_jadwal.semester',$semester)
            ->where('temp_jadwal.status_jadwal','aktif')          
            ->where('temp_jadwal.tahun_ajaran',$tahun_ajaran)
            ->where('jurusan',Session::get('jurusan'))
            ->groupBy('temp_jadwal.dosen_kode')
            ->groupBy('hari')
            ->groupBy('temp_jadwal.kode_matkul')
            ->groupBy('temp_jadwal.ruangan_id')
            ->groupBy('temp_jadwal.kelas')
            ->orderBy('sesi_masuk','asc')
            ->having('jumlah', '>', 1)
            ->get();  
        $temp=[];          
        $sesi = [[]];
        foreach ($cek as $key => $value) {
            $test = DB::connection('presensi')->table('temp_jadwal')
                ->join('polindra_siakad_v1_db.dosen','polindra_siakad_v1_db.dosen.dosen_kode','temp_jadwal.dosen_kode')
                ->join('mata_kuliah','mata_kuliah.kode_matkul','temp_jadwal.kode_matkul')
                ->join('polindra_siakad_v1_db.data_kelas','polindra_siakad_v1_db.data_kelas.kelas_kode','temp_jadwal.kelas')                   
                ->where('temp_jadwal.status_jadwal','aktif')          
                ->where('temp_jadwal.kode_matkul',$value->kode_matkul)
                ->where('temp_jadwal.dosen_kode',$value->dosen_kode)                
                ->where('temp_jadwal.hari',$value->hari)    
                ->where('temp_jadwal.kelas',$value->kelas)   
                ->whereIn('temp_jadwal.semester',$semester)
                ->where('temp_jadwal.tahun_ajaran',$tahun_ajaran)
                ->where('jurusan',Session::get('jurusan'))
                ->orderBy('sesi_masuk','asc')         
                ->get();
            for ($i=0; $i < $value->jumlah; $i++) { 
                $sesi[$key][$i]=$test[$i]->sesi_masuk;                
            }
        }            
        foreach ($cek as $key2 => $value2) {            
            // for ($j=0; $j < $value2->jumlah; $j++) {                 
                $temp[$key2]=[
                    'id_jadwal'=>$value2->id_temp_jadwal,
                    "dosen"=>$value2->dosen_nama,
                    "nama_matkul"=>$value2->nama_matkul,
                    'ruangan_id'=>$value2->ruangan_id,
                    'dosen_kode'=>$value2->dosen_kode,
                    'matkul'=>$value2->kode_matkul,
                    'kelas'=>$value2->kelas,
                    'semester'=>$value2->semester,
                    'ruangan'=>$value2->ruangan_nama,
                    "hari"=>$value2->hari,
                    'tahun_ajaran'=>$value2->tahun_ajaran,
                    "dosen_kode"=>$value2->dosen_kode,
                    "kode_matkul"=>$value2->kode_matkul,
                    "sesi"=>json_encode($sesi[$key2])
                ];                
            // }
        } 
        $fix = [];           
        foreach ($temp as $k => $v) {                                             
            $arr = json_decode($v['sesi']);  
            $max= max($arr);
            $min= min($arr);  
            $fix[$k]=[                                        
                'dosen_kode'=>$v['dosen_kode'],
                'kode_matkul'=>$v['kode_matkul'],
                'tahun_ajaran'=>$v['tahun_ajaran'],
                'semester'=>$v['semester'],
                'ruangan_id'=>$v['ruangan_id'],                    
                'kelas'=>$v['kelas'], 
                'id_jadwal'=>Str::random(20),
                'hari'=>$v['hari'],                
                'id_sesi_masuk'=>$min,
                'id_sesi_selesai'=>$max,
                'toleransi_keterlambatan'=>15,
                'status_perkuliahan'=>"Aktif",
                'created_at'=>$created_at,
                'mulai_perkuliahan'=>"by_jam_mulai"
            ];      
        }
        DB::connection('presensi')->table('jadwal')->insert($fix);
        return response()->json($fix);
    }
}
