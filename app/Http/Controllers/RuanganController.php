<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ruangan;
use Carbon\Carbon;
use DB;
use Session;
class RuanganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $key=1;
        $ruangan = DB::connection('presensi')->table('polindra_presensi.ruangan')
                    ->join('polindra_siakad_v1_db.data_ruangan','polindra_siakad_v1_db.data_ruangan.ruangan_id','polindra_presensi.ruangan.ruangan_id')
                    ->get();
        // dd($ruangan);
        return view('contents.ruangan.index',compact('ruangan','key'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Harus Filter Jurusan
        $ruangan = DB::connection('presensi')->table('ruangan')->where('ruangan_id',$id)->where('jurusan',Session::get('jurusan'))->first();
        return view('contents.ruangan.edit',compact('ruangan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $update = DB::connection('presensi')->table('ruangan')->where('ruangan_id',$id)->update([
            'latitude'=>$request->latitude,
            'longitude'=>$request->longitude,
            'radius'=>$request->radius,
            'updated_at'=>Carbon::now('Asia/Jakarta')
        ]);
        
        return redirect()->route('ruangan.index')->with('success','Data Berhasil di update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::connection('presensi')->table('ruangan')->where('ruangan_id',$id)->delete();

        return response()->json([
            'message'=>'Data Berhasil di Hapus'
        ]);
    }
}
