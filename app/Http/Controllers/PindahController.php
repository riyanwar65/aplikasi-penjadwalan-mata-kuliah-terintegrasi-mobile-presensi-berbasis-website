<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Illuminate\Support\Str;
use Carbon\Carbon;
class PindahController extends Controller
{
    //
    public function pindahjadwal(){
        $jadwal = DB::connection('presensi')->table('jadwal')
                ->join('mata_kuliah','mata_kuliah.kode_matkul','jadwal.kode_matkul')
                ->join('polindra_siakad_v1_db.data_ruangan','polindra_siakad_v1_db.data_ruangan.ruangan_id','jadwal.ruangan_id')
                ->join('polindra_siakad_v1_db.dosen','polindra_siakad_v1_db.dosen.dosen_kode','jadwal.dosen_kode')
                ->join('polindra_siakad_v1_db.data_kelas','polindra_siakad_v1_db.data_kelas.kelas_kode','jadwal.kelas')
                ->join('polindra_siakad_v1_db.program_studi','polindra_siakad_v1_db.program_studi.program_studi_kode','polindra_siakad_v1_db.data_kelas.program_studi_kode')
                ->join('polindra_siakad_v1_db.program_jurusan','polindra_siakad_v1_db.program_jurusan.jurusan_kode','polindra_siakad_v1_db.program_studi.jurusan_kode')
                ->where('status_perkuliahan','Aktif')                
                ->where('polindra_siakad_v1_db.program_jurusan.jurusan_kode',Session::get('jurusan'))
                ->get();
        $dosen = DB::connection('presensi')
                ->table('color_dosen')
                ->join('polindra_siakad_v1_db.dosen','polindra_siakad_v1_db.dosen.dosen_kode','color_dosen.dosen_kode')
                ->join('bkd','bkd.dosen_kode','color_dosen.dosen_kode')
                ->groupBy('bkd.dosen_kode')
                ->get();
        // $tahun_ajaran = DB::connection('presensi')->table('bkd')->where('status_bkd','Aktif')->where('jurusan',Session::get('jurusan'))->groupBy('tahun_ajaran')->get();                        
        return view('contents.jadwal.pindahjadwal',compact('jadwal','dosen'));
    }

    public function pindah(Request $request){
        $hari = ['senin','selasa','rabu','kamis','jumat'];
        $sesi = DB::connection('presensi')->table('sesi')->get();        
        $kelas = DB::connection('presensi')->table('temp_jadwal') 
                // ->select("data_kelas.kelas_nama",'nama_matkul','jenis','ruangan_nama','hari','sesi_masuk','dosen_nama')  
                ->join('color_dosen','color_dosen.dosen_kode','temp_jadwal.dosen_kode')                                                     
                ->join('polindra_siakad_v1_db.dosen','polindra_siakad_v1_db.dosen.dosen_kode','temp_jadwal.dosen_kode')
                ->join('polindra_siakad_v1_db.data_ruangan','polindra_siakad_v1_db.data_ruangan.ruangan_id','temp_jadwal.ruangan_id')
                ->join('mata_kuliah','mata_kuliah.kode_matkul','temp_jadwal.kode_matkul')                 
                ->join('polindra_siakad_v1_db.data_kelas','polindra_siakad_v1_db.data_kelas.kelas_kode','temp_jadwal.kelas')                                    
                ->join('polindra_siakad_v1_db.program_jurusan','temp_jadwal.jurusan','polindra_siakad_v1_db.program_jurusan.jurusan_kode')                
                ->where('temp_jadwal.jurusan',Session::get('jurusan'))
                // ->where('temp_jadwal.tahun_ajaran',$tahun_ajaran)
                // ->where('temp_jadwal.ruangan_id',$ruangan)
                // ->whereIn('temp_jadwal.semester',$semester)
                ->orderBy('temp_jadwal.kelas','asc')                
                ->get();
        $temp = [[]];
        foreach ($hari as $key => $value) {
            foreach ($sesi as $key2 => $value2) {
                $temp[$key][$key2]=[
                    'hari'=>$value,
                    'sesi'=>$value2,                    
                ];
            }
        }
        return response()->json(
            [
            'harisesi'=>$temp,
            'kelas'=>$kelas,
            ]
        );
    }

    public function updatepindah(Request $request){
        // Pengecekan data 
        $cek = DB::connection('presensi')
                ->table('temp_jadwal')
                ->join('sesi','sesi.id_sesi','temp_jadwal.sesi_masuk')
                ->join('polindra_siakad_v1_db.dosen','polindra_siakad_v1_db.dosen.dosen_kode','temp_jadwal.dosen_kode')
                ->join('polindra_siakad_v1_db.data_ruangan','polindra_siakad_v1_db.data_ruangan.ruangan_id','temp_jadwal.ruangan_id')
                ->join('mata_kuliah','mata_kuliah.kode_matkul','temp_jadwal.kode_matkul')                                
                ->where('temp_jadwal.hari',$request->hari_target)
                ->where('temp_jadwal.sesi_masuk',$request->sesi_target) 
                ->where('temp_jadwal.kelas',$request->kelas_bawaan)               
                ->first();        
        if ($request->kelas_target!=$request->kelas_bawaan) {
            return response()->json([
                "status"=>false,
                "code"=>503,
                "message"=>"Kelas Tidak Sesuai"
            ]);
        }else{                       
            if(!$cek){                
                $update = DB::connection('presensi')->table('temp_jadwal')->where('id_temp_jadwal',$request->temp_id)->update([
                    'sesi_masuk'=>$request->sesi_target,
                    'hari'=>$request->hari_target,
                ]);                
                $ghari = DB::connection('presensi')->table('temp_jadwal')
                    ->select('sesi_masuk','kelas','dosen_kode','ruangan_id','hari')
                    ->where('dosen_kode',$request->kd_dosen)
                    ->where('kode_matkul',$request->kd_matkul)            
                    ->where('ruangan_id',$request->ruangan)
                    ->where('kelas',$request->kelas_bawaan)
                    ->groupBy('hari')
                    ->get();                  
                $hari = [];                                
                foreach ($ghari as $key => $value){
                    $hari[$key] = $value->hari;
                }
                return $this->prosespindah($hari,$request->kd_dosen,$request->kd_matkul,$request->ruangan,$request->kelas_bawaan);                                                      
            }else{
                if ($cek->id_temp_jadwal==$request->temp_id) {
                    return response()->json([
                        'status'=>true,
                        'code'=>200,
                        'message'=>"Data Berhasil",
                    ]);
                }
                return response()->json([
                    'status'=>false,
                    'code'=>503,
                    'message'=>"Pada hari ".$cek->hari." sudah ada ".$cek->nama_matkul." ".$cek->dosen_nama." ".$cek->jam_masuk." ".$cek->ruangan_nama,
                    'data'=>$cek
                ]);
            }                                                      
        }
    }

    private function prosespindah($hari,$kddosen,$kdmatkul,$ruangan,$kelasbawaan){        
        $temp = [];                   
        if (count($hari)>1) {  
            $tampung = [];                 
            $created_at = Carbon::now('Asia/Jakarta');                     
            $get = DB::connection('presensi')->table('temp_jadwal')
                ->select('sesi_masuk','kelas','dosen_kode','ruangan_id','hari','tahun_ajaran','semester')
                ->where('dosen_kode',$kddosen)
                ->where('kode_matkul',$kdmatkul)
                ->whereIn('hari',$hari)
                ->where('ruangan_id',$ruangan)
                ->where('kelas',$kelasbawaan)
                ->orderBy('hari','DESC')                
                ->get();   
            $hapus = DB::connection('presensi')->table('jadwal')
                ->where('dosen_kode',$kddosen)
                ->where('kode_matkul',$kdmatkul)                    
                ->where('ruangan_id',$ruangan)
                ->where('kelas',$kelasbawaan)
                ->where('status_perkuliahan','Aktif')
                ->where('tahun_ajaran',$get[0]->tahun_ajaran)
                ->delete(); 
            foreach ($get as $key => $value) {               
                $tampung[$key]=$value->hari;   
            }  
            $h = array_unique($tampung);                    
            $t = [];
            $no = 0;
            foreach ($h as $key => $value) {
                $t[$no++]=$value;                                    
            }
            for ($i=0; $i <count($t) ; $i++) {              
                if ($t[$i]=="senin") {                    
                    $getd = DB::connection('presensi')->table('temp_jadwal')
                    ->select('sesi_masuk','kelas','dosen_kode','ruangan_id','hari','tahun_ajaran','semester')
                    ->where('dosen_kode',$kddosen)
                    ->where('kode_matkul',$kdmatkul)
                    ->where('hari',"senin")
                    ->where('ruangan_id',$ruangan)
                    ->where('kelas',$kelasbawaan)
                    ->orderBy('hari','DESC')                
                    ->get();   
                    foreach ($getd as $key => $value) {
                        $temp[$key]=$value->sesi_masuk;
                    }
                    $arr = $temp;  
                    $max= max($arr);
                    $min= min($arr); 
                    $insert = DB::connection('presensi')->table('jadwal')
                            ->insert(
                                ['dosen_kode'=>$kddosen,
                                'kode_matkul'=>$kdmatkul,
                                'tahun_ajaran'=>$getd[0]->tahun_ajaran,
                                'semester'=>$getd[0]->semester,
                                'ruangan_id'=>$ruangan,                    
                                'kelas'=>$kelasbawaan, 
                                'id_jadwal'=>Str::random(20),
                                'hari'=>"senin",                
                                'id_sesi_masuk'=>$min,
                                'id_sesi_selesai'=>$max,
                                'toleransi_keterlambatan'=>15,
                                'status_perkuliahan'=>"Aktif",
                                'created_at'=>$created_at,
                                'mulai_perkuliahan'=>"by_jam_mulai"]
                            );
                    // $update = DB::connection('presensi')->table('jadwal')                       
                    //     ->where('dosen_kode',$kddosen)
                    //     ->where('kode_matkul',$kdmatkul)
                    //     ->where('hari',"senin")
                    //     ->where('ruangan_id',$ruangan)
                    //     ->where('kelas',$kelasbawaan)                                      
                    //     ->update([
                    //         'id_sesi_masuk'=>$min,
                    //         'id_sesi_selesai'=>$max
                    //     ]);  
                    continue;   
                }else if($t[$i]=="selasa"){
                    $getd = DB::connection('presensi')->table('temp_jadwal')
                    ->select('sesi_masuk','kelas','dosen_kode','ruangan_id','hari','tahun_ajaran','semester')
                    ->where('dosen_kode',$kddosen)
                    ->where('kode_matkul',$kdmatkul)
                    ->where('hari',"selasa")
                    ->where('ruangan_id',$ruangan)
                    ->where('kelas',$kelasbawaan)
                    ->orderBy('hari','DESC')                
                    ->get();   
                    foreach ($getd as $key => $value) {
                        $temp[$key]=$value->sesi_masuk;
                    }
                    $arr = $temp;  
                    $max= max($arr);
                    $min= min($arr); 
                    $insert = DB::connection('presensi')->table('jadwal')
                            ->insert(
                                ['dosen_kode'=>$kddosen,
                                'kode_matkul'=>$kdmatkul,
                                'tahun_ajaran'=>$getd[0]->tahun_ajaran,
                                'semester'=>$getd[0]->semester,
                                'ruangan_id'=>$ruangan,                    
                                'kelas'=>$kelasbawaan, 
                                'id_jadwal'=>Str::random(20),
                                'hari'=>"selasa",                
                                'id_sesi_masuk'=>$min,
                                'id_sesi_selesai'=>$max,
                                'toleransi_keterlambatan'=>15,
                                'status_perkuliahan'=>"Aktif",
                                'created_at'=>$created_at,
                                'mulai_perkuliahan'=>"by_jam_mulai"]
                            );
                    // $update = DB::connection('presensi')->table('jadwal')                       
                    //     ->where('dosen_kode',$kddosen)
                    //     ->where('kode_matkul',$kdmatkul)
                    //     ->where('hari',"senin")
                    //     ->where('ruangan_id',$ruangan)
                    //     ->where('kelas',$kelasbawaan)                                      
                    //     ->update([
                    //         'id_sesi_masuk'=>$min,
                    //         'id_sesi_selesai'=>$max
                    //     ]);  
                    continue;    
                }else if($t[$i]=="rabu"){
                    $getd = DB::connection('presensi')->table('temp_jadwal')
                    ->select('sesi_masuk','kelas','dosen_kode','ruangan_id','hari','tahun_ajaran','semester')
                    ->where('dosen_kode',$kddosen)
                    ->where('kode_matkul',$kdmatkul)
                    ->where('hari',"rabu")
                    ->where('ruangan_id',$ruangan)
                    ->where('kelas',$kelasbawaan)
                    ->orderBy('hari','DESC')                
                    ->get();   
                    foreach ($getd as $key => $value) {
                        $temp[$key]=$value->sesi_masuk;
                    }
                    $arr = $temp;  
                    $max= max($arr);
                    $min= min($arr); 
                    $insert = DB::connection('presensi')->table('jadwal')
                            ->insert(
                                ['dosen_kode'=>$kddosen,
                                'kode_matkul'=>$kdmatkul,
                                'tahun_ajaran'=>$getd[0]->tahun_ajaran,
                                'semester'=>$getd[0]->semester,
                                'ruangan_id'=>$ruangan,                    
                                'kelas'=>$kelasbawaan, 
                                'id_jadwal'=>Str::random(20),
                                'hari'=>"rabu",                
                                'id_sesi_masuk'=>$min,
                                'id_sesi_selesai'=>$max,
                                'toleransi_keterlambatan'=>15,
                                'status_perkuliahan'=>"Aktif",
                                'created_at'=>$created_at,
                                'mulai_perkuliahan'=>"by_jam_mulai"]
                            );
                    // $update = DB::connection('presensi')->table('jadwal')                       
                    //     ->where('dosen_kode',$kddosen)
                    //     ->where('kode_matkul',$kdmatkul)
                    //     ->where('hari',"senin")
                    //     ->where('ruangan_id',$ruangan)
                    //     ->where('kelas',$kelasbawaan)                                      
                    //     ->update([
                    //         'id_sesi_masuk'=>$min,
                    //         'id_sesi_selesai'=>$max
                    //     ]);  
                    continue;  
                }else if($t[$i]=="kamis"){
                    $getd = DB::connection('presensi')->table('temp_jadwal')
                    ->select('sesi_masuk','kelas','dosen_kode','ruangan_id','hari','tahun_ajaran','semester')
                    ->where('dosen_kode',$kddosen)
                    ->where('kode_matkul',$kdmatkul)
                    ->where('hari',"kamis")
                    ->where('ruangan_id',$ruangan)
                    ->where('kelas',$kelasbawaan)
                    ->orderBy('hari','DESC')                
                    ->get();   
                    foreach ($getd as $key => $value) {
                        $temp[$key]=$value->sesi_masuk;
                    }
                    $arr = $temp;  
                    $max= max($arr);
                    $min= min($arr); 
                    $insert = DB::connection('presensi')->table('jadwal')
                            ->insert(
                                ['dosen_kode'=>$kddosen,
                                'kode_matkul'=>$kdmatkul,
                                'tahun_ajaran'=>$getd[0]->tahun_ajaran,
                                'semester'=>$getd[0]->semester,
                                'ruangan_id'=>$ruangan,                    
                                'kelas'=>$kelasbawaan, 
                                'id_jadwal'=>Str::random(20),
                                'hari'=>"kamis",                
                                'id_sesi_masuk'=>$min,
                                'id_sesi_selesai'=>$max,
                                'toleransi_keterlambatan'=>15,
                                'status_perkuliahan'=>"Aktif",
                                'created_at'=>$created_at,
                                'mulai_perkuliahan'=>"by_jam_mulai"]
                            );
                    // $update = DB::connection('presensi')->table('jadwal')                       
                    //     ->where('dosen_kode',$kddosen)
                    //     ->where('kode_matkul',$kdmatkul)
                    //     ->where('hari',"senin")
                    //     ->where('ruangan_id',$ruangan)
                    //     ->where('kelas',$kelasbawaan)                                      
                    //     ->update([
                    //         'id_sesi_masuk'=>$min,
                    //         'id_sesi_selesai'=>$max
                    //     ]);  
                    continue;    
                }else if($t[$i]=="jumat"){
                    $getd = DB::connection('presensi')->table('temp_jadwal')
                    ->select('sesi_masuk','kelas','dosen_kode','ruangan_id','hari','tahun_ajaran','semester')
                    ->where('dosen_kode',$kddosen)
                    ->where('kode_matkul',$kdmatkul)
                    ->where('hari',"jumat")
                    ->where('ruangan_id',$ruangan)
                    ->where('kelas',$kelasbawaan)
                    ->orderBy('hari','DESC')                
                    ->get();   
                    foreach ($getd as $key => $value) {
                        $temp[$key]=$value->sesi_masuk;
                    }
                    $arr = $temp;  
                    $max= max($arr);
                    $min= min($arr); 
                    $update =$insert = DB::connection('presensi')->table('jadwal')
                    ->insert(
                        ['dosen_kode'=>$kddosen,
                        'kode_matkul'=>$kdmatkul,
                        'tahun_ajaran'=>$getd[0]->tahun_ajaran,
                        'semester'=>$getd[0]->semester,
                        'ruangan_id'=>$ruangan,                    
                        'kelas'=>$kelasbawaan, 
                        'id_jadwal'=>Str::random(20),
                        'hari'=>"jumat",                
                        'id_sesi_masuk'=>$min,
                        'id_sesi_selesai'=>$max,
                        'toleransi_keterlambatan'=>15,
                        'status_perkuliahan'=>"Aktif",
                        'created_at'=>$created_at,
                        'mulai_perkuliahan'=>"by_jam_mulai"]
                    );
                    // $update = DB::connection('presensi')->table('jadwal')                       
                    //     ->where('dosen_kode',$kddosen)
                    //     ->where('kode_matkul',$kdmatkul)
                    //     ->where('hari',"senin")
                    //     ->where('ruangan_id',$ruangan)
                    //     ->where('kelas',$kelasbawaan)                                      
                    //     ->update([
                    //         'id_sesi_masuk'=>$min,
                    //         'id_sesi_selesai'=>$max
                    //     ]);  
                    continue;     
                } 
            }
        }else{
            // Hapus data yang lebih dari 1 hari            
            $created_at = Carbon::now('Asia/Jakarta');             
            $get = DB::connection('presensi')->table('temp_jadwal')
                ->select('sesi_masuk','kelas','dosen_kode','ruangan_id','hari','tahun_ajaran','semester')
                ->where('dosen_kode',$kddosen)
                ->where('kode_matkul',$kdmatkul)
                ->where('hari',$hari)
                ->where('ruangan_id',$ruangan)
                ->where('kelas',$kelasbawaan)
                ->get(); 
            $hapus = DB::connection('presensi')->table('jadwal')
                ->where('dosen_kode',$kddosen)
                ->where('kode_matkul',$kdmatkul)                    
                ->where('ruangan_id',$ruangan)
                ->where('kelas',$kelasbawaan)
                ->where('status_perkuliahan','Aktif')
                ->where('tahun_ajaran',$get[0]->tahun_ajaran)
                ->delete(); 
            foreach ($get as $key => $value) { 
                $temp[$key]=$value->sesi_masuk;                                                     
            }         
            $arr = $temp;  
            $max= max($arr);
            $min= min($arr);
            $insert = DB::connection('presensi')->table('jadwal')
                    ->insert(
                        ['dosen_kode'=>$kddosen,
                        'kode_matkul'=>$kdmatkul,
                        'tahun_ajaran'=>$get[0]->tahun_ajaran,
                        'semester'=>$get[0]->semester,
                        'ruangan_id'=>$ruangan,                    
                        'kelas'=>$kelasbawaan, 
                        'id_jadwal'=>Str::random(20),
                        'hari'=>$hari[0],                
                        'id_sesi_masuk'=>$min,
                        'id_sesi_selesai'=>$max,
                        'toleransi_keterlambatan'=>15,
                        'status_perkuliahan'=>"Aktif",
                        'created_at'=>$created_at,
                        'mulai_perkuliahan'=>"by_jam_mulai"]
                    );
            return response()->json([
                'status'=>true,
                'code'=>200,
                'message'=>"Data Berhasil",
                'data'=>"Berhasil"
            ]); 
        }                      
    }

    public function harirequest(Request $request){
        $hari = DB::connection('presensi')->table('temp_jadwal')
                ->select('hari')
                ->where('dosen_kode',$request->kddos)
                ->where('kode_matkul',$request->kdmatkul)
                ->where('kelas',$request->kelas)
                ->groupBy('hari')
                ->orderBy('hari','desc')
                ->get();
        return response()->json([
            'status'=>true,
            'code'=>200,
            'data'=>$hari,
        ]);
    }

    public function gantiruangan(Request $request){
        $cek = DB::connection('presensi')->table('temp_jadwal')
                ->select('ruangan_id')
               ->where('hari',$request->hari)
               ->groupBy('temp_jadwal.ruangan_id')
               ->get();
        $tampung = [];
        foreach ($cek as $key => $value) {
            $tampung[$key]=$value->ruangan_id;
        }                       
        $ruangan_available = DB::connection('presensi')->table('temp_jadwal')
                ->select('polindra_siakad_v1_db.data_ruangan.ruangan_nama','temp_jadwal.ruangan_id')
                ->join('polindra_siakad_v1_db.data_ruangan','polindra_siakad_v1_db.data_ruangan.ruangan_id','temp_jadwal.ruangan_id')                
                ->whereNotIn('temp_jadwal.ruangan_id',$tampung)
                ->groupBy('temp_jadwal.ruangan_id')
                ->get();
        return response()->json([
            'data'=>$ruangan_available
        ]);
    }

    public function updateruangan(Request $request){
        $update_temp_jadwal = DB::connection('presensi')
                            ->table('temp_jadwal')
                            ->where('dosen_kode',$request->dosenkode)
                            ->where('kelas',$request->kelas)
                            ->where('kode_matkul',$request->matkul)
                            ->where('hari',$request->hari)
                            ->update([
                                'ruangan_id'=>$request->ruangan
                            ]);
        $update_jadwal = DB::connection('presensi')
                        ->table('jadwal')
                        ->where('dosen_kode',$request->dosenkode)
                        ->where('kelas',$request->kelas)
                        ->where('kode_matkul',$request->matkul)
                        ->where('hari',$request->hari)
                        ->update([
                            'ruangan_id'=>$request->ruangan
                        ]);

        return response()->json([
            'status'=>true,
            'code'=>true,
            'message'=>"Ruangan Berhasil di ganti",
        ]);
    }
}
