<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\GenerateAlgoritma;
use App\Settings;
use App\Schedule;
use App\Http\Controllers\JadwalController as TempJadwal;
class GenerateJadwalController extends Controller
{
    public function index(){        
        $tahun_ajaran = DB::connection('presensi')->table('bkd')->where('status_bkd','Aktif')->groupBy('tahun_ajaran')->get();  
        return view('contents.jadwal.generate',compact('tahun_ajaran'));
    }    
    public function generateJadwal(Request $req){        
        $genap = [2,4,6,8];
        $ganjil = [1,3,5,7];        
        $tahun_ajaran = $req->tahunajaran;        
        if ($req->semester=='genap') {
            $genap_generate = DB::connection('presensi')->table('bkd')->where('status_bkd','Aktif')->where('tahun_ajaran',$tahun_ajaran)->whereIn('semester',$genap)->get();
            if (count($genap_generate)>0) {
                return $this->prosesgenerate($tahun_ajaran,$genap);
            }else{
                return response()->json([
                    'status'=>false,
                    'code'=>200,
                    'message'=>"Data Yang di minta tidak ada",
                ]);
            }
        }else{
            $ganjil_generate = DB::connection('presensi')->table('bkd')->where('status_bkd','Aktif')->where('tahun_ajaran',$tahun_ajaran)->whereIn('semester',$ganjil)->get();            
            if (count($ganjil_generate)>0) {
                return $this->prosesgenerate($tahun_ajaran,$ganjil);
            }else{            
                return response()->json([
                    'status'=>false,
                    'code'=>200,
                    'message'=>"Data Yang di minta tidak ada",
                ]);
            }
        }
        
    }

    private function prosesgenerate($tahun_ajaran,$semester){
        
        // $init_crossover = 2.5;
        // $init_mutasi = 2;
        // $init_generasi = 1;
        // $init_kromosom = 3;
        $input_year = $tahun_ajaran;
        $input_semester = $semester;
        $dosen = DB::connection('presensi')->table('bkd')->select('dosen_kode')->where('status_bkd','Aktif')->where('jurusan',Session::get('jurusan'))->groupBy('dosen_kode')->get();        
        $banyak_bkd =  DB::connection('presensi')->table('bkd')->where('tahun_ajaran',$input_year)->where('status_bkd','Aktif')->whereIn('semester',$semester)->where('jurusan',Session::get('jurusan'))->count();
        $banyak_dosen = count($dosen);

        // $kromosom = $init_kromosom * $init_generasi;
        // $crossover = $init_kromosom * $init_crossover;

        $generate = new GenerateAlgoritma;
        // $data_kromosom =  $generate->randKromosom($kromosom,$banyak_bkd,$input_year,$input_semester);
        $data_kromosom =  $generate->randKromosom($banyak_bkd,$input_year,$input_semester);
        // $result_schedules = $generate->checkPinalty();

        // $total_gen = Settings::firstOrNew(['key'=>'total_gen']);
        // $total_gen->name  = 'Total Gen';
        // $total_gen->value = $crossover;
        // $total_gen->save();

        // $mutasi        = Settings::firstOrNew(['key' => 'mutasi']);
        // $mutasi->name  = 'Mutasi';
        // $mutasi->value = (3 * $banyak_bkd) * $init_kromosom * $init_mutasi;
        // $mutasi->save();

        $temp_jadwal = new TempJadwal;
        $temp_jadwal->temp($tahun_ajaran,$semester);

        return response()->json([
            'status'=>true,
            'code'=>200,
            'message'=>"Generate Berhasil dilakukan",
            'data'=>$data_kromosom
        ]);
    }

    public function result($id){
        $years = DB::connection('presensi')->table('bkd')->where('status_bkd','Aktif')->groupBy('tahun_ajaran')->pluck('tahun_ajaran','tahun_ajaran');
        $kromosom = Schedule::select('type')->groupBy('type')->get()->count();
        $crossover      = Settings::where('key', Settings::CROSSOVER)->first();
        $mutasi         = Settings::where('key', Settings::MUTASI)->first();
        $value_schedule = Schedule::where('type', $id)->where('tahun_ajaran',$tahun_ajaran)->whereIn('semester',$semester)->first();
        $schedules = DB::connection('presensi')->table('schedule')
                    // ->select('dosen_nama','hari','nama_matkul','kelas_nama','jam_masuk','jam_keluar','ruangan_nama')
                    ->join('bkd','bkd.kode_bkd','schedule.kode_bkd')
                    ->join('polindra_siakad_v1_db.dosen','polindra_siakad_v1_db.dosen.dosen_kode','bkd.dosen_kode')
                    ->join('mata_kuliah','mata_kuliah.kode_matkul','bkd.kode_matkul')
                    ->join('polindra_siakad_v1_db.data_kelas','polindra_siakad_v1_db.data_kelas.kelas_kode','bkd.kode_kelas')                    
                    ->join('sesi','schedule.sesi','sesi.id_sesi')
                    ->join('polindra_siakad_v1_db.data_ruangan','polindra_siakad_v1_db.data_ruangan.ruangan_id','schedule.ruangan_id')
                    ->orderBy('hari','desc')
                    ->where('type',$id)
                    ->get();            
        for ($i=1; $i <=$kromosom ; $i++) { 
            $value_schedules = DB::connection('presensi')->table('schedule')->where('type', $i)->first();
            // dd($value_schedules);
            $data_kromosom[] = [
                'value_schedules' => $value_schedules->value,

            ];
        }
        
        return view('contents.jadwal.test',compact('schedules', 'years', 'data_kromosom', 'id', 'value_schedule', 'crossover', 'mutasi'));
    }

    public function cekgenerate(Request $req){
        $genap = [2,4,6,8];
        $ganjil = [1,3,5,7];  
        if ($req->semester=="genap") {
            $cek = Schedule::where('tahun_ajaran',$req->tahunajaran)->where('jurusan',Session::get('jurusan'))->whereIn('semester',$genap)->get();
            if (count($cek)>0) {
                return response()->json([
                    'status'=>true,
                    'code'=>200,
                    'message'=>'Apakah Anda yakin ingin generate jadwal lagi ?',
                ]);
            }else{
                return response()->json([
                    'status'=>false,
                    'code'=>200,
                    'message'=>'Silahkan generate lagi',
                ]);
            }
        }else{
            $cek = Schedule::where('tahun_ajaran',$req->tahunajaran)->where('jurusan',Session::get('jurusan'))->whereIn('semester',$ganjil)->get();
            if (count($cek)>0) {
                return response()->json([
                    'status'=>true,
                    'code'=>200,
                    'message'=>'Apakah Anda yakin ingin generate jadwal lagi ?',
                ]);
            }else{
                return response()->json([
                    'status'=>false,
                    'code'=>200,
                    'message'=>'Silahkan generate lagi',
                ]);
            }
        }
    }

    public function cekhari($hari){                                
        $cekhari = DB::connection('presensi')->table('schedule')
        ->select('sesi')        
        ->where('jurusan',Session::get('jurusan')) 
        ->where('hari',$hari)        
        ->where('type',1)
        ->get();        
        $cekhariruangan = DB::connection('presensi')->table('schedule')
        ->select('ruangan_id')        
        ->where('jurusan',Session::get('jurusan')) 
        ->where('hari',$hari)        
        ->where('type',1)
        ->get();        
        $ceksesiarray = json_decode(json_encode($cekhari),true);    
        $cekruanganarray = json_decode(json_encode($cekhariruangan),true);        
        $sesi = DB::connection('presensi')->table('sesi')->where('jam_masuk','<>','11:40')
                ->whereNotIn('id_sesi',$ceksesiarray)
                // ->groupBy('id_sesi')
                ->get();
        $ruangan = DB::connection('presensi')->table('ruangan')
                ->join('polindra_siakad_v1_db.data_ruangan','polindra_siakad_v1_db.data_ruangan.ruangan_id','ruangan.ruangan_id')
                ->whereNotIn('ruangan.ruangan_id',$cekruanganarray)->get();
        
        if (count($sesi)>0) {                        
            return response()->json([
                'status'=>true,
                'code'=>200,
                'sesi'=>$sesi,
                'ruangan'=>$ruangan,
            ]);
        }else{            
            return response()->json([
                'status'=>false,
                'code'=>200,
                'message'=>"Jam Penuh, tidak ada yang available"
            ]);
        }
    }
}
