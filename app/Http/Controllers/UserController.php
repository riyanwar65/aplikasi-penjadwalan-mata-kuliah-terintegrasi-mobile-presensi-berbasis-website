<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    //
    public function index(){
             
        return view('contents.login');
    }

    public function login(Request $request){
        if (Auth::attempt(['user_id' => $request->user_id, 'password' => $request->password])) {
            $user = Auth::user();
            $success['token'] =  $user->createToken('nApp')->accessToken;
            return view('contents.dashboard',compact('user',));
        }else{
            return redirect('/login')->with('alert','Ga bisa Login');
        }        
    }
    public function logout(\Illuminate\Http\Request $request){        
        dd($request->bearerToken());
        try {
            JWTAuth::setToken(JWTAuth::getToken())->invalidate();
            return redirect('/login')->with('success','Anda Sudah Logout');
        } catch (JWTException $e) {
            JWTAuth::unsetToken();
            return redirect('/login')->with('success','Failed to logout, please try again.');
        }
    }
}
