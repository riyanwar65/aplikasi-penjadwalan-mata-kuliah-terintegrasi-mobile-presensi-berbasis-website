<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class LoginController extends Controller
{

    public function showLoginForm(){
        return view('auth.login');
    }

    public function logout(){
        Session::flush();
        return redirect('/login')->with('success','Anda Berhasil Logout');
    }

    public function login(Request $request){
        $userid = $request->user_id;
        $password = $request->password;
        $data = DB::connection('presensi')->table('users')->where('user_id',$userid)->first();
        $cek = DB::connection('siakad')->table('dosen')->where('dosen_nidn',$userid)->first();        
        if ($cek) {
            $cek_sekjur = DB::connection('siakad')
                        ->table('dosen')
                        ->join('polindra_presensi.users','polindra_presensi.users.user_id','dosen.user_id')
                        ->join('polindra_presensi.detail_jabatan_pegawai','polindra_presensi.detail_jabatan_pegawai.id_pegawai','dosen.user_id')
                        ->join('polindra_presensi.data_jabatan','polindra_presensi.data_jabatan.id','polindra_presensi.detail_jabatan_pegawai.id_jabatan')
                        ->join('program_jurusan','polindra_presensi.detail_jabatan_pegawai.jurusan','program_jurusan.jurusan_kode')
                        ->where('dosen.dosen_nidn',$userid)
                        ->where('detail_jabatan_pegawai.deleted_at',NULL)
                        ->count();
            if ($cek_sekjur>0){
                return $this->sekjur($userid,$password);
            }
        }
        if($data){
            $cek_admin = DB::connection('presensi')
                         ->table('admin_jurusan')                         
                         ->join('detail_jabatan_pegawai','detail_jabatan_pegawai.id_pegawai','admin_jurusan.kode_admin_jurusan')
                         ->join('data_jabatan','detail_jabatan_pegawai.id_jabatan','data_jabatan.id')
                         ->join('polindra_siakad_v1_db.program_jurusan','polindra_siakad_v1_db.program_jurusan.jurusan_kode','detail_jabatan_pegawai.jurusan')
                         ->where('detail_jabatan_pegawai.id_pegawai',$userid)
                         ->where('detail_jabatan_pegawai.deleted_at',NULL)
                         ->count();            
            $cek_adminsiakad = DB::connection('presensi')
                        ->table('users')                         
                        ->join('detail_jabatan_pegawai','detail_jabatan_pegawai.id_pegawai','users.user_id')                  
                        ->where('users.user_id',$userid)
                        ->where('detail_jabatan_pegawai.deleted_at',NULL)
                        ->count();            
            if ($cek_admin>0) {
                return $this->admin($userid,$password);
            }else if ($cek_adminsiakad>0){
                return $this->adminsiakad($userid,$password);
            }else{
                $message = "Anda Tidak Memiliki Akses";
                return $this->exception($message);
            }            
        }else{
            $message = "Data yang anda input tidak ditemukan";
            return $this->exception($message);
        }
    }

    protected function exception($message){
        return redirect('/login')->with('error',$message);
    }

    protected function admin($userid,$password){
        $admin = DB::connection('presensi')        
                ->table('admin_jurusan')   
                ->join('users','users.user_id','admin_jurusan.kode_admin_jurusan')                      
                ->join('detail_jabatan_pegawai','detail_jabatan_pegawai.id_pegawai','admin_jurusan.kode_admin_jurusan')
                ->join('data_jabatan','detail_jabatan_pegawai.id_jabatan','data_jabatan.id')
                ->join('polindra_siakad_v1_db.program_jurusan','polindra_siakad_v1_db.program_jurusan.jurusan_kode','detail_jabatan_pegawai.jurusan')
                ->where('detail_jabatan_pegawai.id_pegawai',$userid)
                ->where('detail_jabatan_pegawai.deleted_at',NULL)
                ->first();                
        if (Hash::check($password, $admin->password)) {
            Session::put('user_id',$admin->user_id);
            Session::put('jabatan',$admin->jabatan);
            Session::put('jurusan',$admin->asal_jurusan);
            Session::put('nama',$admin->nama_admin_jurusan);
            Session::put('login',TRUE);
            return redirect()->route('dashboard');
        }else{
            $message = "Password yang di inputkan salah";
            return $this->exception($message);
        }        
    }

    protected function adminsiakad($userid,$password){
        $admin = DB::connection('presensi')        
                ->table('users')                   
                ->join('detail_jabatan_pegawai','detail_jabatan_pegawai.id_pegawai','users.user_id')                
                ->where('detail_jabatan_pegawai.id_pegawai',$userid)
                ->where('detail_jabatan_pegawai.deleted_at',NULL)
                ->first();                
        if (Hash::check($password, $admin->password)) {
            Session::put('user_id',$admin->user_id);
            Session::put('jabatan',$admin->id_jabatan);
            Session::put('nama',$admin->user_id);
            Session::put('login',TRUE);
            return redirect()->route('dashboard');
        }else{
            $message = "Password yang di inputkan salah";
            return $this->exception($message);
        }        
    }

    protected function sekjur($userid,$password){
        $sekjur = DB::connection('siakad')        
                ->table('dosen')   
                ->join('polindra_presensi.users','polindra_presensi.users.user_id','dosen.user_id')                      
                ->join('polindra_presensi.detail_jabatan_pegawai','polindra_presensi.detail_jabatan_pegawai.id_pegawai','dosen.user_id')
                ->join('polindra_presensi.data_jabatan','polindra_presensi.detail_jabatan_pegawai.id_jabatan','polindra_presensi.data_jabatan.id')
                ->join('program_jurusan','program_jurusan.jurusan_kode','polindra_presensi.detail_jabatan_pegawai.jurusan')
                ->where('dosen.dosen_nidn',$userid)
                ->where('polindra_presensi.detail_jabatan_pegawai.deleted_at',NULL)
                ->first();                
        if (Hash::check($password, $sekjur->password)) {
            Session::put('user_id',$sekjur->user_id);
            Session::put('jabatan',$sekjur->jabatan);
            Session::put('jurusan',$sekjur->jurusan_kode);
            Session::put('nama',$sekjur->dosen_nama);
            Session::put('login',TRUE);
            return redirect()->route('dashboard');
        }else{
            $message = "Password yang di inputkan salah";
            return $this->exception($message);
        }        
    }

    public function gantipassword(Request $request){
        DB::connection('presensi')->table('users')->where('user_id',$request->userid)->update([
            'password'=>Hash::make($request->newpass)
        ]);
        return response()->json("sukses");
    }

    public function lupapassword(){
        return view('auth.passwords.email');
    }

    public function prosescekemail(Request $request){
        $cek = DB::connection('presensi')->table('users')
                ->join('detail_jabatan_pegawai','detail_jabatan_pegawai.id_pegawai','users.user_id')
                ->where('users.email_recovery',$request->email)
                ->first();
        $r_token = Str::random(32);
        if ($cek) {
            $token = DB::connection('presensi')->table('users')
                    ->join('detail_jabatan_pegawai','detail_jabatan_pegawai.id_pegawai','users.user_id')
                    ->where('users.email_recovery',$request->email)
                    ->update([
                        'users.remember_token'=>$r_token
                    ]);    
            $ambil = DB::connection('presensi')->table('users')
                ->join('detail_jabatan_pegawai','detail_jabatan_pegawai.id_pegawai','users.user_id')
                ->where('users.email_recovery',$request->email)
                ->first();       
            $data=[                
                "username"=>$ambil->user_id,
                "link"=>route('update.password',['token'=>$ambil->remember_token])
            ];             
            $email = $request->email;
            Mail::send('contents.resetemail',$data, function ($message) use ($email){
                $message->to($email)->subject('Link Reset Password');
                $message->from(env('MAIL_FROM_ADDRESS',NULL),'Admin Aplikasi Web Manajemen Penjadwalan');
            });
            return redirect()->back()->with('success','Reset Link sudah terkirim ke email');
        }else{
            return redirect()->back()->with('error','Email tidak terdaftar');
        }
    }

    public function updatepassword(){
        return view('auth.passwords.reset');
    }
    
    public function prosesupdatepassword(Request $request){
        if ($request->password==$request->password_confirmation) {            
            $update = DB::connection('presensi')->table('users')
            ->whereRaw('remember_token',$request->tokenuser)
            ->update([
                'password'=>Hash::make($request->password)
            ]);
            return redirect('/login')->with('success','Password Berhasil diupdate');
        }else{
            return redirect()->back()->with('error','Password tidak cocok');
        }
    }
}
