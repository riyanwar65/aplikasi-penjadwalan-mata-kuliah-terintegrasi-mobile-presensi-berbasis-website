<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MahasiswaBiodata extends Model
{
    //
    protected $table = 'mahasiswa_biodata';
    protected $connection = 'siakad';
    protected $guarded = [];

    public function mahasiswa(){
        return $this->belongsTo('\App\Mahasiswa','mahasiswa_kode');                
    }    
    
}
