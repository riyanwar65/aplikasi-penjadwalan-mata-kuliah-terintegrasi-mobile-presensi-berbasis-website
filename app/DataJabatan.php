<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataJabatan extends Model
{
    //
    protected $guarded = [];
    protected $table = 'data_jabatan';
    protected $connection ='presensi';

    public function jabatan_pegawai(){
        return $this->belongsTo(DataJabatanPegawai::class,'id_jabatan','id');
    }
}
