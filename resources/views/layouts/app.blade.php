<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{'Aplikasi Penjadwalan Mata Kuliah terintegrasi Mobile Presensi'}}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">        
        <main class="py-4">
            @yield('content')
        </main>
    </div>
    <script language='JavaScript'>
        var txt="Aplikasi Penjadwalan Mata Kuliah terintegrasi Mobile Presensi";
        var speed=300;
        var refresh=null;
        function action() { 
            document.title=txt;
            txt=txt.substring(1,txt.length)+txt.charAt(0);
            refresh=setTimeout("action()",speed);
        }
        action();
    </script>
</body>
</html>
