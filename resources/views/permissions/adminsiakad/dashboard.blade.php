@extends('master')
@section('title','Dashboard')
@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Dashboard</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Dashboard</li>
                </ol>
            </div>
        </div>
    </div>
</div>

    <div class="content mt-3">        
        <!--/.col-->
        <div class="col-sm-6 col-lg-4">
            <div class="card text-white bg-flat-color-2">
                <div class="card-body pb-0">                    
                    <h4 class="mb-0">
                        
                    </h4>
                    <p class="text-light">Mahasiswa</p>

                    <div class="chart-wrapper px-0" style="height:70px;" height="70">
                        <h1 class="count">{{count($totMhsSiakad)}}</h1>                        
                    </div>

                </div>
            </div>
        </div>        
        <!--/.col-->
{{-- 
        <div class="col-sm-6 col-lg-4">
            <div class="card text-white bg-flat-color-3">
                <div class="card-body pb-0">                   
                    <h4 class="mb-0">
                        
                    </h4>
                    <p class="text-light">Semester</p>

                </div>

                <div class="chart-wrapper px-0" style="height:70px;" height="70">                    
                    <h1> &nbsp;{{strtoupper($semester)}}</h1>
                </div>
            </div>
        </div>
        <!--/.col--> --}}

        <div class="col-sm-6 col-lg-4">
            <div class="card text-white bg-flat-color-4">
                <div class="card-body pb-0">                    
                    <h4 class="mb-0">
                        
                    </h4>
                    <p class="text-light">Total BKD {{strtoupper($semester)}}</p>

                    <div class="chart-wrapper px-3" style="height:70px;" height="70">
                        <h1 class="count">{{count($totbkdSiakad)}}</h1>            
                    </div>

                </div>
            </div>
        </div>

        <div class="col-sm-6 col-lg-4 col-md-4">
            <div class="card text-white bg-flat-color-5">
                <div class="card-body pb-0">                    
                    <h4 class="mb-0">
                        
                    </h4>
                    <p class="text-light">Total Dosen</p>

                    <div class="" style="height:70px;" height="70">
                        <h1 class="count">{{count($totDosen)}}</h1>
                    </div>

                </div>
            </div>
        </div>
        <!--/.col-->    

        <div class="col-sm-6 col-lg-4 col-md-4">
            <div class="card text-white bg-flat-color-1">
                <div class="card-body pb-0">                    
                    <h4 class="mb-0">
                        
                    </h4>
                    <p class="text-light">Total Kelas</p>

                    <div class="" style="height:70px;" height="70">
                        <h1 class="count">{{count($totKelas)}}</h1>              
                    </div>

                </div>
            </div>
        </div>
        <!--/.col-->  

        <div class="col-sm-6 col-lg-4 col-md-4">
            <div class="card text-white bg-flat-color-3">
                <div class="card-body pb-0">                    
                    <h4 class="mb-0">
                        
                    </h4>
                    <p class="text-light">Total Ruangan</p>

                    <div class="" style="height:70px;" height="70">
                        <h1 class="count">{{count($totRuangan)}}</h1>              
                    </div>

                </div>
            </div>
        </div>
        <!--/.col-->  
</div> 
@endsection