{{-- Manajemen Jabatan --}}
<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <h2>Manajemen Jabatan</h2>
        </div>
    </div>      
</div>   
<div class="col-md-8">                              
    <div class="card">
        <div class="card-header">
            <strong>Daftar Jabatan</strong>
        </div>
        <div class="card-body card-block">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Jabatan</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody id="jabatan"> 
                    @foreach($jabatan as $jbtn)
                        <tr>
                            <td>{{$jbtn->jabatan}}</td>
                            <td align="center">
                                <a href="#editjabatan" data-idedit="{{$jbtn->id}}" data-toggle="modal" data-target="#exampleModal" class="btn btn-sm btn-warning" title="Edit"><i class="fa fa-edit"></i></a>
                                <a href="#delete" data-id="{{$jbtn->id}}" class="btn btn-sm btn-danger" title="Hapus"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>                            
            </table>
            {{$jabatan->links()}}
        </div>
        <div class="card-footer">
        </div>    
    </div>
</div>
<div class="col-md-4">
    <form id="jabatanpost" class="form-horizontal">                   
        <div class="card">
            <div class="card-header">
                <strong>Form Input Jabatan</strong>
            </div>
            <div class="card-body card-block">                       
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Jabatan</label></div>
                    <div class="col-12 col-md-9">
                        <input type="text" name="jabatan" id="jbtn" class="form-control" placeholder="Masukan jabatan">                                
                    </div>
                </div>                                   
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-success btn-sm">
                    <i class="fa fa-dot-circle-o"></i> Simpan
                </button>
            </div>    
        </div>
    </form>
</div>
