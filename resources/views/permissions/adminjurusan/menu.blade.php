<li class="active">
    <a href="{{route('dashboard')}}"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
</li>
<h3 class="menu-title">General</h3><!-- /.menu-title -->
<li>
    <a href="{{route('dosen.index')}}"> <i class="menu-icon fa fa-user"></i>Dosen</a>                        
</li>
<li>
    <a href="{{route('mahasiswa.index')}}"> <i class="menu-icon fa fa-user"></i>Mahasiswa</a>                        
</li>
<li class="menu-item-has-children dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="menu-icon fa fa-edit"></i> BKD</a>
    <ul class="sub-menu children dropdown-menu">
        {{-- <li><i class="menu-icon fa fa-plus"></i><a href="{{route('bkd.create')}}"> Create BKD</a></li> --}}
        <li><i class="menu-icon fa fa-eye"></i><a href="{{route('bkd.index')}}">Lihat BKD</a></li>
    </ul>
</li>
<li>
    <a href="{{route('absensi')}}"><i class="menu-icon fa fa-calendar-o"></i> Absensi</a>
</li>
<h3 class="menu-title">Master Data</h3><!-- /.menu-title -->

<li>
    <a href="{{route('jabatan.index')}}"> <i class="menu-icon fa fa-tasks"></i>Jabatan</a>                       
</li>
<li>
    <a href="{{route('matakuliah.index')}}"> <i class="menu-icon fa fa-edit"></i>Mata Kuliah</a>                       
</li>                    
<h3 class="menu-title">Setting</h3><!-- /.menu-title -->
<li>
    <a href="{{route('pindahjadwal')}}"><i class="menu-icon fa fa-cog"></i> Pindah Jadwal</a>
</li>
<li>
    <a href="{{route('sesi.index')}}"><i class="menu-icon fa fa-cog"></i>Sesi</a>
</li> 
<li>
    <a href="{{route('ruangan.index')}}"><i class="menu-icon fa fa-cog"></i>Ruangan</a>
</li> 