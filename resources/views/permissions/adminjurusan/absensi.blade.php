@extends('master')
@section('title','Absensi')
@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        @foreach ($temp as $v)
                        <a href="{{route('rekapabsensi',["kelas"=>$v[0]])}}">
                            <div class="col-sm-6 col-lg-3">
                                <div class="card text-white bg-flat-color-2">
                                    <div class="card-body">
                                        <center>
                                            <h1>{{$v[0]}}</h1>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </a>
                        @endforeach                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection