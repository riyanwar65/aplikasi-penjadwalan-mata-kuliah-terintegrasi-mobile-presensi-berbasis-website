<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <h2>Pendataan Jabatan untuk Pegawai</h2>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <div class="col-md-6 col-sm-4">
                <div class="text-left">
                    <strong>Daftar Pendataan Jabatan untuk Dosen Pegawai</strong> 
                </div>
            </div>
            <div class="col-md-6 col-sm-4">
                <div class="text-right">
                    <a href="{{route('jabatan-pegawai.index')}}" target="_blank" rel="noopener noreferrer" class="btn btn-sm btn-success">Tambah Data</a>
                </div>
            </div>                        
        </div>
        <div class="card-body">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    {{-- <a class="nav-link active" id="home-tab" data-toggle="tab" href="#admin" role="tab" aria-controls="home" aria-selected="true">Admin Jurusan</a> --}}
                </li>
                <li class="nav-item">
                    <a class="nav-link active" id="profile-tab" data-toggle="tab" href="#dosen" role="tab" aria-controls="profile" aria-selected="true">Dosen Jurusan</a>
                </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                {{-- <div class="tab-pane fade show active" id="admin" role="tabpanel" aria-labelledby="home-tab">
                    <br>
                    <a href="{{route('regis-admin.index')}}" target="_blank" rel="noopener noreferrer" class="btn btn-sm btn-success">Registrasi Admin Jurusan</a>
                    <br><br>
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Jabatan</th>
                                <th>Periode</th>
                                <th>Jurusan</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody id="admin"> 
                            @foreach ($adminjurusan as $admin)                                                                                    
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td>{{$admin->nama_admin_jurusan}}</td>
                                    <td>{{$admin->jabatan}}</td>
                                    <td>{{$admin->periode}}</td>
                                    <td>{{$admin->jurusan}}</td>
                                    <td>
                                        <a href="{{url('jabatan-pegawai/'.$admin->id_pegawai.'/edit')}}" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i> Edit</a>
                                        <a href="#hapusjabatanadmin" data-id="{{$admin->id_pegawai}}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Hapus</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div> --}}
                <div class="tab-pane fade show active" id="dosen" role="tabpanel" aria-labelledby="profile-tab">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Jabatan</th>
                                <th>Periode</th>
                                <th>Jurusan</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody id="admin"> 
                            @foreach ($dosenjurusan as $dosen)                                                                                    
                                <tr>
                                    <td>{{$n++}}</td>
                                    <td>{{$dosen->dosen_nama}}</td>
                                    <td>{{$dosen->jabatan}}</td>
                                    <td>{{$dosen->periode}}</td>
                                    <td>{{$dosen->jurusan}}</td>
                                    <td>
                                        <a href="{{url('jabatan-pegawai/'.$dosen->id_pegawai.'/edit')}}" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i> Edit</a>
                                        <a href="#hapusjabatandosen" data-id="{{$dosen->id_pegawai}}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Hapus</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                </div>                        
        </div>
    </div>      
</div> 