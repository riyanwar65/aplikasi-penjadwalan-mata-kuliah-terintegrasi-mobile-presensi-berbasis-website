<li class="active">
    <a href="{{route('dashboard')}}"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
</li>
<h3 class="menu-title">General</h3><!-- /.menu-title -->
<li>
    <a href="{{route('jadwal.index')}}"><i class="menu-icon fa fa-calendar"></i> Jadwal</a>
</li>
<li class="menu-item-has-children dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="menu-icon fa fa-edit"></i> BKD</a>
    <ul class="sub-menu children dropdown-menu">
        <li><i class="menu-icon fa fa-plus"></i><a href="{{route('bkd.create')}}"> Create BKD</a></li>
        <li><i class="menu-icon fa fa-eye"></i><a href="{{route('bkd.index')}}">Lihat BKD</a></li>        
    </ul>
</li>
<li>
    <a href="{{route('setcolor')}}"><i class="menu-icon fa fa-cog"></i> Set Color</a></li>
</li>