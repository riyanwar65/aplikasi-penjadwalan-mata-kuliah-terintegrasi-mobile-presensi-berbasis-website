@extends('master')
@section('title','Registrasi Admin')    
@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                @if (count($errors) > 0)
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>                         
                            @foreach ($errors->all() as $error)
                                {{ $error }} <br>
                            @endforeach
                    </div>
                @endif
                <form action="{{route('regis-admin.store')}}" method="post" enctype="multipart/form-data" class="form-horizontal">   
                    @csrf                         
                    <div class="card">
                    <div class="card-header">
                        <strong>Registrasi Admin</strong> Form Input
                    </div>
                        <div class="card-body card-block">
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="text-input" class=" form-control-label">Jurusan</label></div>
                                <div class="col-12 col-md-9">
                                    <select name="jurusan" id="Jurusan" class="form-control" required>                                        
                                        <option value="0" disabled="true" selected="true">Pilih</option>
                                        @foreach ($jurusan as $jr)
                                            <option value="{{$jr->jurusan_kode}}">{{$jr->jurusan_nama}}</option>
                                        @endforeach                                        
                                    </select>
                                </div>
                            </div> 
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Jabatan</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <select name="jabatan" id="jabatan" class="form-control">
                                        <option value="0" disabled="true" selected="true">Pilih</option>                                                    
                                        @foreach ($jabatan as $jb2)
                                            <option value="{{$jb2->id}}">{{$jb2->jabatan}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>   
                            {{-- <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Periode</label>
                                </div>
                                <div class="col-4 col-md-3"><input type="number" name="awal" placeholder="Periode Awal" min="2000"  class="form-control"></div> /
                                <div class="col-4 col-md-3"><input type="number" name="akhir" placeholder="Periode Akhir" min="2000" class="form-control"></div>
                            </div> --}}
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="text-input" class=" form-control-label">Nama Lengkap</label></div>
                                <div class="col-6 col-md-3">
                                    <input type="text" name="first" required id="nama" class="form-control" placeholder="First Name">                                
                                </div>
                                <div class="col-6 col-md-3">
                                    <input type="text" name="last" id="nama" class="form-control" placeholder="Last Name">                                
                                </div>
                            </div>    
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="text-input" class=" form-control-label">Email</label></div>
                                <div class="col-12 col-md-9">
                                    <input type="email" required name="email" id="email" class="form-control" placeholder="Email">                                
                                </div>
                            </div>                                
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="text-input" class=" form-control-label">No. Hp</label></div>
                                <div class="col-12 col-md-9">
                                    <input type="number" required name="nohp" id="nohp" class="form-control" min="0" placeholder="Contoh: 081122223333">                                
                                </div>
                            </div>  
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="text-input" class=" form-control-label">Alamat</label></div>
                                <div class="col-12 col-md-9">
                                    <textarea name="alamat" required id="alamat" cols="30" rows="10" placeholder="Alamat" class="form-control"></textarea>                              
                                </div>
                            </div>                                                           
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success">
                                <i class="fa fa-dot-circle-o"></i> Simpan
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>            
    </div>
</div>
@endsection