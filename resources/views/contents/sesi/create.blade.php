@extends('master')
@section('title','Tambah Jam Sesi')
@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                @if (Session::get('jam_masuk'))                
                    <br>
                    <div class="alert alert-danger alert-block">
                    {{Session::get('jam_masuk')}}
                    </div>                          
                @endif                    
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-6 col-sm-4">
                            <div class="text-left">                                
                                <strong>Tambah Sesi</strong>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-4">
                            <div class="text-right">
                                <a href="{{route('sesi.index')}}" class="btn btn-sm btn-warning" ><i class="fa fa-arrow-left"></i> Daftar Sesi</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{route('sesi.store')}}" method="POST">
                            @csrf                                                                        
                                <label for="">Sesi</label>
                                <input type="number" name="sesi" id="" class="form-control" min="1">
                                @if (Session::has('errors'))
                                <br>
                                <div class="alert alert-danger alert-block">
                                    {{$errors->first('sesi')}}
                                </div>
                                @endif
                                <label for="">Jam Masuk</label>
                                <input type="text" id="timepicker-24-hr" name="jam_masuk" class="jam_masuk form-control">                                
                                <label for="">Jam Keluar</label>
                                <input type="text" id="jam_keluar" name="jam_keluar" class="jam_keluar form-control">                                 
                                <br>                                          
                                <button type="submit" class="btn btn-success btn-sm">Simpan</button>
                        </form>
                    </div>
                </div>
            </div>                     
        </div>        
    </div><!-- .animated -->        
</div><!-- .content -->
@push('script')
<script src="{{asset('assets/js/axios.min.js')}}"></script>
<script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/sweetalert2.js')}}"></script>
<script src="{{asset('assets/js/wickedpicker.min.js')}}"></script>
{{-- <script src="https://code.jquery.com/jquery-3.4.0.min.js"></script> --}}
<script>
    $('.jam_masuk').wickedpicker({twentyFour: true});
    $('.jam_keluar').wickedpicker({twentyFour: true});
</script>
@endpush
@endsection