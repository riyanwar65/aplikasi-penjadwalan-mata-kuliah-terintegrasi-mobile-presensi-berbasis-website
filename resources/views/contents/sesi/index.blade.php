@extends('master')
@section('title','Jam Sesi')
@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">            
            <div class="col-md-12">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if ($message = Session::get('alert-danger'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>                         
                            @foreach ($errors->all() as $error)
                                  {{ $error }} <br>
                            @endforeach
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-6 col-sm-4">
                            <div class="text-left">
                                <strong class="card-title">Jam Sesi</strong>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-4">
                            <div class="text-right">
                                <a href="{{route('sesi.create')}}" class="btn btn-sm btn-success" >Tambah Sesi</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                            <thead>
                                <tr>                                    
                                    <th>Sesi Ke</th>
                                    <th>Jam Masuk</th>
                                    <th>Jam Keluar</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="matakuliah">  
                                @foreach ($sesi as $s)                                                                    
                               <tr>                                   
                                   <td>{{$s->id_sesi}}</td>
                                   <td>{{$s->jam_masuk}}</td>
                                   <td>{{$s->jam_keluar}}</td>
                                   <td>
                                       <a href="{{route('sesi.edit',$s->id_sesi)}}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Edit</a>
                                       <a href="#hapus" data-id="{{$s->id_sesi}}" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Delete</a>
                                   </td>
                               </tr>
                               @endforeach 
                            </tbody>
                        </table>
                    </div>                
                </div>
            </div>           
        </div>        
    </div><!-- .animated -->        
</div><!-- .content -->
@push('script')
<script src="{{asset('assets/js/axios.min.js')}}"></script>
<script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/sweetalert2.js')}}"></script>
<script src="{{asset('assets/js/wickedpicker.min.js')}}"></script>
<script>
    $('.jam_masuk').wickedpicker({twentyFour: true});
    $('.jam_keluar').wickedpicker({twentyFour: true});
</script>
<script>
    $(document).on('click','a[href="#hapus"]',function(){
            var id = $(this).data('id');                   
            Swal.fire({
                title: 'Peringatan',
                text: `Apa Kamu yakin ingin menghapusnya ?`,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Hapus'
                }).then((result) => {
                    if (result.value) {                        
                        axios.delete(`/sesi/${id}`,{_token:"{{csrf_token()}}"}).then((response)=>{
                            Swal.fire(
                                'Sukses',
                                `${response.data.message}`,
                                'success'
                            ).then(function(){
                                window.location.reload();
                            });            
                        });
                    }
                })
        });
</script>
@endpush
@endsection