@extends('master')
@section('title','Edit Jam Sesi')
@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                @if (Session::get('jam_masuk'))                
                    <br>
                    <div class="alert alert-danger alert-block">
                    {{Session::get('jam_masuk')}}
                    </div>                          
                @endif                    
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-6 col-sm-4">
                            <div class="text-left">                                
                                <strong>Edit Sesi</strong>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-4">
                            <div class="text-right">
                                <a href="{{route('sesi.index')}}" class="btn btn-sm btn-warning" ><i class="fa fa-arrow-left"></i> Daftar Sesi</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{route('sesi.update',$edit->id_sesi)}}" method="POST">
                            @csrf 
                            @method('PUT')                                                                       
                            <label for="">Sesi</label>
                            <input type="number" name="sesi" id="" class="form-control" min="1" value="{{$edit->id_sesi}}">                                                                
                            @if(Session::get('errors'))
                            <br>
                            <div class="alert alert-danger alert-block">
                                {{Session::get('errors')}}
                            </div>                          
                            @enderror
                            <label for="">Jam Masuk</label>
                            <input type="text" id="timepicker-24-hr" name="jam_masuk" class="jam_masuk form-control" >                                
                            <label for="">Jam Keluar</label>
                            <input type="text" id="jam_keluar" name="jam_keluar" class="jam_keluar form-control" >                                 
                            <br>                                          
                            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
                        </form>
                    </div>
                </div>
            </div>                     
        </div>        
    </div><!-- .animated -->        
</div><!-- .content -->
@push('script')
<script src="{{asset('assets/js/axios.min.js')}}"></script>
<script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/sweetalert2.js')}}"></script>
<script src="{{asset('assets/js/wickedpicker.min.js')}}"></script>
{{-- <script src="https://code.jquery.com/jquery-3.4.0.min.js"></script> --}}
<script>
    $('.jam_masuk').wickedpicker({
        twentyFour: true,        
        now:'{{$masuk}}'
    });
    $('.jam_keluar').wickedpicker({
        twentyFour: true,
        now:'{{$keluar}}'
    });
</script>
@endpush
@endsection