@extends('master')
@section('title','Edit Data Jabatan Dosen Jurusan')    
@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                @if (count($errors) > 0)
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>                         
                            @foreach ($errors->all() as $error)
                                {{ $error }} <br>
                            @endforeach
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6 text-left">
                                <strong>Tambah Data Jabatan Pegawai</strong>
                            </div>
                            <div class="col-md-6 text-right">
                                <a href="{{route('jabatan.index')}}" class="btn btn-sm btn-success"><i class="fa fa-arrow-left"></i> Menu Jabatan</a>
                            </div>
                        </div>
                       {{-- <strong>Tambah Data Jabatan Pegawai</strong> --}}
                    </div>
                        <div class="card-body card-block">                                                           
                                    <form action="{{route('jabatan-pegawai.update',$dosenjabatan->id_pegawai)}}" method="post" enctype="multipart/form-data" class="form-horizontal">   
                                        @csrf         
                                        @method('PUT')                                                    
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="text-input" class=" form-control-label">Dosen</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <select name="dosen" id="datadosen" class="form-control">
                                                    <option value="0" disabled="true" selected="true">Pilih</option>
                                                    @foreach ($dosen as $ds)
                                                        <option value="{{$ds->user_id}}">{{$ds->dosen_gelar_depan." ".$ds->dosen_nama.",".$ds->dosen_gelar_belakang}}</option>
                                                    @endforeach                                                    
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="text-input" class=" form-control-label">Jabatan</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <select name="jabatan" id="jabatan" class="form-control">                                                    
                                                    <option value="0" disabled="true" selected="true">Pilih</option>                                                    
                                                    @foreach ($jabatan as $jb)
                                                        <option value="{{$jb->id}}">{{$jb->jabatan}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="text-input" class=" form-control-label">Periode</label>
                                            </div>
                                            <div class="col-4 col-md-3"><input type="number" name="awal" placeholder="Periode Awal" min="2000"  class="form-control" value="{{$periode[0]}}"></div> /
                                            <div class="col-4 col-md-3"><input type="number" name="akhir" placeholder="Periode Akhir" min="2000" class="form-control" value="{{$periode[1]}}"></div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Asal Jurusan</label></div>
                                            <div class="col-12 col-md-9">
                                                <select name="jurusan" id="jurusan" class="form-control">
                                                    <option value="0" disabled="true" selected="true">Pilih</option>
                                                    <option value="TI">Teknik Informatika</option>
                                                    <option value="TM">Teknik Mesin</option>
                                                    <option value="TPTU">Teknik Pendingin dan Tata Udara</option>
                                                    <option value="KP">Keperawatan</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group">                                            
                                            <div class="col col-md-3">
                                                <button type="submit" class="btn btn-sm btn-success">Simpan</button>                                                                                        
                                            </div>
                                        </div>
                                    </form>                                                                                                                   
                        </div>
                </div>
            </div>
        </div>            
    </div>
</div>
@push('script')
<script src="{{asset('assets/js/axios.min.js')}}"></script>
<script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/sweetalert2.js')}}"></script>
<script src="{{asset('assets/js/select2.min.js')}}"></script>
<script>    
    console.log('{{$dosenjabatan->id_pegawai}}');
    $("#jurusan option[value='{{$dosenjabatan->jurusan}}']").prop('selected',true);
    $("#jabatan option[value='{{$dosenjabatan->id_jabatan}}']").prop('selected',true);            
    $("#datadosen option[value='{{$dosenjabatan->id_pegawai}}']").prop('selected',true);    
</script>
<script>
    $('#datadosen').select2({
        width:'resolve',
    });
</script>
@endpush
@endsection