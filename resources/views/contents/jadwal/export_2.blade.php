<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>

<table class="table table-striped table-bordered" id="detail" width="100%"> 
    @php
        $kelas = DB::connection('presensi')
                ->table('bkd')        
                ->select('kode_kelas')
                ->where('status_bkd',"Aktif")
                ->where('jurusan',Session::get('jurusan'))
                ->groupBy('kode_kelas')
                ->get();
        $sesi = DB::connection('presensi')
                ->table('sesi')
                ->get();        
        $hari = ['senin','selasa','rabu','kamis','jumat'];
        $temp = [[]];
        foreach ($hari as $key => $value) {
            foreach ($sesi as $key2 => $value2) {
                $temp[$key][$key2]=[
                    'hari'=>$value,
                    'sesi'=>$value2,
                ];
            }
        }
    @endphp
    <thead id="head">  
        <tr id="kelas">                                                            
            <th>Hari</th>
            <th>Jam</th>
            @foreach ($kelas as $k)
               <th>{{$k->kode_kelas}}</th> 
            @endforeach  
        </tr>                                  
    </thead>   
    <tbody id="body">
       @for ($i = 0; $i < count($temp); $i++)
           @for ($j = 0; $j < count($temp[$i]); $j++)
           <tr>
               <td>{{$temp[$i][$j]['hari']}}</td>
               <td>{{$temp[$i][$j]['sesi']->id_sesi}}</td>     
            </tr>                          
                    {{-- {{dd($jadwal)}} --}}
                @for ($k = 0; $k < count($kelas); $k++)
                    @if ($kelas[$k]->kode_kelas=="D3TI1A")                    
                        @php
                            $jw = DB::connection('presensi')
                                  ->table('temp_jadwal')
                                  ->join('color_dosen','color_dosen.dosen_kode','temp_jadwal.dosen_kode')                                                     
                                  ->join('polindra_siakad_v1_db.dosen','polindra_siakad_v1_db.dosen.dosen_kode','temp_jadwal.dosen_kode')
                                  ->join('polindra_siakad_v1_db.data_ruangan','polindra_siakad_v1_db.data_ruangan.ruangan_id','temp_jadwal.ruangan_id')
                                  ->join('mata_kuliah','mata_kuliah.kode_matkul','temp_jadwal.kode_matkul')                 
                                  ->join('polindra_siakad_v1_db.data_kelas','polindra_siakad_v1_db.data_kelas.kelas_kode','temp_jadwal.kelas')                                    
                                  ->join('polindra_siakad_v1_db.program_jurusan','temp_jadwal.jurusan','polindra_siakad_v1_db.program_jurusan.jurusan_kode')                
                                  ->where('temp_jadwal.jurusan',Session::get('jurusan'))
                                  ->where('temp_jadwal.status_jadwal','aktif')            
                                  ->where('temp_jadwal.tahun_ajaran',$tahunajaran)
                                  ->where('temp_jadwal.kelas',"D3TI1A")
                                  ->orderBy('temp_jadwal.kelas','asc')                
                                  ->get();
                        @endphp  
                        @foreach ($jw as $item)
                            @if ($item->hari=="senin"&&$item->sesi_masuk=="1")
                                <td>{{$item->nama_matkul}}</td>
                            @elseif ($item->hari=="senin"&&$item->sesi_masuk=="2")
                                <td>{{$item->nama_matkul}}</td>
                            @else 
                                <td>{{$item->hari=="senin"&&$item->sesi_masuk=="2"}}</td>
                                {{-- <td></td> --}}
                            @endif
                        @endforeach
                    @else 

                    @endif
                @endfor                
            
           @endfor
       @endfor
    </tbody>                
</table>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>  
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
{{-- <script src="{{asset('assets/js/jquery-ui.min.js')}}"></script>  --}}
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
</body>
</html>