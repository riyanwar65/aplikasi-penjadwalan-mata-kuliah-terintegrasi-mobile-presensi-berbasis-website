@extends('master')
@section('title','Cek')
@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">                        
        <div class="row"> 
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                    <table class="table table-striped table-bordered" id="detail" width="100%" border="1"> 
                        <thead id="head">  
                            <tr id="kelas">                                                            
                                <th>Hari</th>
                                <th>Jam</th>  
                            </tr>                                  
                        </thead>   
                        <tbody id="body">
                            
                        </tbody>                
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('script')
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>  
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
{{-- <script src="{{asset('assets/js/jquery-ui.min.js')}}"></script>  --}}
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.16.6/xlsx.min.js" integrity="sha512-X53zyt2zVcpa26yjS4j4nUzh6bEiymyqZ7zuZk6NoYqwZJcSzrScgtpsN1JPtcdROIhhnhBmtClj7eVe21qjyg==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.16.6/xlsx.full.min.js" integrity="sha512-y/6xOMDHnSz1X2azryTilOlwPm/yWBDI9zY87kTAHiaDAl+32nW2kT+1M8TtHDikgVaO0a/XcEujCW8lm2z+Dg==" crossorigin="anonymous"></script>
{{-- <script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script> --}}
{{-- <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" ></script> --}}
<script src="{{asset('assets/js/sweetalert2.js')}}"></script>    
{{-- <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script> --}}
<script>    
    // function exportTableToExcel(id, type, fn) {
    //     var wb = XLSX.utils.table_to_book(document.getElementById(id), {sheet:"SheetJS"});
    //     var fname ='test.' + type;
    //     XLSX.writeFile(wb, fname);
    // }    
    setTimeout(function(){
        Swal.fire({
            title: 'Peringatan',
            text: `Apakah Anda Ingin Mendownloadnya ?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Download'
        }).then((result) => {
            if (result.value) {
                var wb = XLSX.utils.table_to_book(document.getElementById("detail"), {sheet:"Jadwal {{Session::get('jurusan')}} {{$semester}}"});
                var fname =`Jadwal {{ucfirst($semester)." ".$tahunajaran." ".Session::get('jurusan')}}.xlsx`;
                XLSX.writeFile(wb, fname);                
                Swal.fire({
                    title:'Sukses',
                    text:"Data Berhasil di unduh",
                    icon:'success',
                }).then(()=>{
                    // console.log(data);
                    window.location.href="{{route('jadwal.index')}}";
                });                                                      
            }
        })
    },3000);
</script>
<script type="text/javascript">    
    var a = 0;
    var b = 0;
    var c = 0;   
    var jmlh = 1;              
    $(document).ready(function(){
        var semester = "{{$semester}}";    
        var tahun_ajaran = "{{$tahunajaran}}";                  
        axios.get(`kelas-test`).then((res)=>{   
            $("#kelas").html(`
                <th>Hari</th>
                <th>Jam</th>  
            `);           
            for (let i = 0; i < res.data.length; i++) {
                var data = res.data[i].kelas_nama;
                console.log(data);
                var html = `                                
                    <th>${data}</th>
                `;
                $("#kelas").append(html);
            }           
        });                          
        axios.post(`datajadwal`,{            
            tahun_ajaran:tahun_ajaran,
            semester:semester
        }).then((res)=>{                  
            console.log(res.data.kelas);
            for (var i = 0; i < res.data.harisesi.length; i++) {                
                for (var j = 0; j < res.data.harisesi[i].length; j++) {                     
                    var hari = res.data.harisesi[i][j].hari;
                    var sesi = res.data.harisesi[i][j].sesi;                                                          
                    var html = `
                        <tr id="${jmlh++}">
                            <td id="${hari}">${hari}</td>
                            <td>${sesi.jam_masuk} - ${sesi.jam_keluar}</td>   
                            <td class="tempatgeser" id="D3TI1A" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D3TI1B" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D3TI1C" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D3TI2A" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D3TI2B" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D3TI2C" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                          
                            <td class="tempatgeser" id="D3TI3A" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D3TI3B" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D3TI3C" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                          
                            <td class="tempatgeser" id="D4RPL1A" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D4RPL2B" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D4RPL2C" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D4RPL3A" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                                 
                        </tr>
                    `;                    
                    $("#body").append(html);                    
                }                
            }     
            jmlh=1;
            var hari;
            for (let k = 0; k < res.data.kelas.length; k++) {
                // console.log(res.data.kelas[k].kelas_nama);
                var kelas = res.data.kelas[k].kelas_nama;
                var data = res.data.kelas[k];
                // console.log(data);
                // KELAS D3TI1A           
                console.log(data);     
                if (data.kelas_nama=='D3TI1A') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(3)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(3)`).html(istirahat); 
                    $(`#16 td:nth-child(3)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(3)`).html(istirahat); 
                    $(`#26 td:nth-child(3)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(3)`).html(istirahat); 
                    $(`#36 td:nth-child(3)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(3)`).html(istirahat); 
                    $(`#46 td:nth-child(3)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(3)`).html(istirahat); 
                        if (data.hari=='senin') {                            
                                if (data.sesi_masuk==1) {                                    
                                    var html = `                                        
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">                                        
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#1 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#2 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#3 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#4 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#5 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#7 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#8 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#9 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#10 td:nth-child(3)`).append(html);
                                    
                                }                            
                        }else if (data.hari=='selasa') {
                            if (data.sesi_masuk==1) {                                
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#11 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#12 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#13 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#14 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#15 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#17 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#18 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#19 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#20 td:nth-child(3)`).append(html);
                                    
                                }
                        }else if (data.hari=='rabu') {
                            if (data.sesi_masuk==1) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#21 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#22 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#23 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#24 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#25 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#27 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#28 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#29 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#30 td:nth-child(3)`).append(html);
                                    
                                }
                        }else if (data.hari=='kamis') {
                            if (data.sesi_masuk==1) {                                
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#31 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#32 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#33 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#34 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#35 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#37 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#38 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#39 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#40 td:nth-child(3)`).append(html);
                                    
                                }
                        }else if (data.hari=='jumat') {
                            if (data.sesi_masuk==1) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#41 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#42 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#43 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#44 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#45 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#47 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#48 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#49 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#50 td:nth-child(3)`).append(html);
                                    
                                }
                        }                    
                }
                // END OF KELAS D3TI1A

                // KELAS D3TI1B
                else if (data.kelas_nama=='D3TI1B') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(4)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(4)`).html(istirahat); 
                    $(`#16 td:nth-child(4)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(4)`).html(istirahat); 
                    $(`#26 td:nth-child(4)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(4)`).html(istirahat); 
                    $(`#36 td:nth-child(4)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(4)`).html(istirahat); 
                    $(`#46 td:nth-child(4)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(4)`).html(istirahat);     
                        if (data.hari=='senin') {                            
                                if (data.sesi_masuk==1) {
                                    $(`#1 td:nth-child(4)`).html("");
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#1 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#2 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#3 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#4 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#5 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#7 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#8 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#9 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#10 td:nth-child(4)`).append(html);
                                    
                                }                            
                        }else if (data.hari=='selasa') {
                            if (data.sesi_masuk==1) {                                
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#11 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#12 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#13 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#14 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#15 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#17 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#18 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#19 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#20 td:nth-child(4)`).append(html);
                                    
                                }
                        }else if (data.hari=='rabu') {
                            if (data.sesi_masuk==1) {                                
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#21 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#22 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#23 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#24 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#25 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#27 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#28 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#29 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#30 td:nth-child(4)`).append(html);
                                    
                                }
                        }else if (data.hari=='kamis') {
                            if (data.sesi_masuk==1) {                                
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#31 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#32 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#33 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#34 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#35 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#37 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#38 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#39 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#40 td:nth-child(4)`).append(html);
                                    
                                }
                        }else if (data.hari=='jumat') {
                            if (data.sesi_masuk==1) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#41 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#42 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#43 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#44 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#45 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#47 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#48 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#49 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#50 td:nth-child(4)`).append(html);
                                    
                                }
                        }                    
                }// END OF Kelas D3TI1B

                // KELAS D3TI1C
                else if (data.kelas_nama=='D3TI1C') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(5)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(5)`).html(istirahat); 
                    $(`#16 td:nth-child(5)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(5)`).html(istirahat); 
                    $(`#26 td:nth-child(5)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(5)`).html(istirahat); 
                    $(`#36 td:nth-child(5)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(5)`).html(istirahat); 
                    $(`#46 td:nth-child(5)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(5)`).html(istirahat);     
                    if (data.hari=='senin') {                            
                                if (data.sesi_masuk==1) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#1 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#2 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#3 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#4 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#5 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#7 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#8 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#9 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#10 td:nth-child(5)`).append(html);
                                    
                                }                            
                        }else if (data.hari=='selasa') {
                            if (data.sesi_masuk==1) {                                
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#11 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#12 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#13 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#14 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#15 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#17 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#18 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#19 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#20 td:nth-child(5)`).append(html);
                                    
                                }
                        }else if (data.hari=='rabu') {
                            if (data.sesi_masuk==1) {                                
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#21 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#22 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#23 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#24 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#25 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#27 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#28 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#29 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#30 td:nth-child(5)`).append(html);
                                    
                                }
                        }else if (data.hari=='kamis') {
                            if (data.sesi_masuk==1) {                                
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#31 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#32 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#33 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#34 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#35 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#37 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#38 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#39 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#40 td:nth-child(5)`).append(html);
                                    
                                }
                        }else if (data.hari=='jumat') {
                            if (data.sesi_masuk==1) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#41 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#42 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#43 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#44 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#45 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#47 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#48 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#49 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#50 td:nth-child(5)`).append(html);
                                    
                                }
                        }                    
                }// END OF Kelas D3TI1C

                // KELAS D3TI2A
                else if (data.kelas_nama=='D3TI2A') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(6)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(6)`).html(istirahat); 
                    $(`#16 td:nth-child(6)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(6)`).html(istirahat); 
                    $(`#26 td:nth-child(6)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(6)`).html(istirahat); 
                    $(`#36 td:nth-child(6)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(6)`).html(istirahat); 
                    $(`#46 td:nth-child(6)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(6)`).html(istirahat);     
                        if (data.hari=='senin') {                            
                                if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#1 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#2 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#3 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#4 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#5 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#7 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#8 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#9 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#10 td:nth-child(6)`).append(html);
                                    
                                }                            
                        }else if (data.hari=='selasa') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#11 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#12 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#13 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#14 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#15 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#17 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#18 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#19 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#20 td:nth-child(6)`).append(html);
                                    
                                }
                        }else if (data.hari=='rabu') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#21 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#22 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#23 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#24 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#25 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#27 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#28 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#29 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#30 td:nth-child(6)`).append(html);
                                    
                                }
                        }else if (data.hari=='kamis') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#31 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#32 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#33 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#34 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#35 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#37 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#38 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#39 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#40 td:nth-child(6)`).append(html);
                                    
                                }
                        }else if (data.hari=='jumat') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#41 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#42 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#43 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#44 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#45 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#47 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#48 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#49 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#50 td:nth-child(6)`).append(html);
                                    
                                }
                        }                    
                }// END OF Kelas D3TI2A
                
                // KELAS D3TI2B
                else if (data.kelas_nama=='D3TI2B') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(7)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(7)`).html(istirahat); 
                    $(`#16 td:nth-child(7)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(7)`).html(istirahat); 
                    $(`#26 td:nth-child(7)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(7)`).html(istirahat); 
                    $(`#36 td:nth-child(7)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(7)`).html(istirahat); 
                    $(`#46 td:nth-child(7)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(7)`).html(istirahat);     
                        if (data.hari=='senin') {                            
                                if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#1 td:nth-child(7)`).append(html);                                    
                                    
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#2 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#3 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#4 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#5 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#7 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#8 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#9 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#10 td:nth-child(7)`).append(html);
                                    
                                }                            
                        }else if (data.hari=='selasa') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#11 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#12 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#13 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#14 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#15 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#17 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#18 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#19 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#20 td:nth-child(7)`).append(html);
                                    
                                }
                        }else if (data.hari=='rabu') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#21 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#22 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#23 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#24 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#25 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#27 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#28 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#29 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#30 td:nth-child(7)`).append(html);
                                    
                                }
                        }else if (data.hari=='kamis') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#31 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#32 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#33 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#34 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#35 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#37 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#38 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#39 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#40 td:nth-child(7)`).append(html);
                                    
                                }
                        }else if (data.hari=='jumat') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#41 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#42 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#43 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#44 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#45 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#47 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#48 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#49 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#50 td:nth-child(7)`).append(html);
                                    
                                }
                        }                    
                }// END OF Kelas D3TI2B

                // KELAS D3TI2C
                else if (data.kelas_nama=='D3TI2C') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(8)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(8)`).html(istirahat); 
                    $(`#16 td:nth-child(8)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(8)`).html(istirahat); 
                    $(`#26 td:nth-child(8)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(8)`).html(istirahat); 
                    $(`#36 td:nth-child(8)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(8)`).html(istirahat); 
                    $(`#46 td:nth-child(8)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(8)`).html(istirahat);     
                        if (data.hari=='senin') {                            
                                if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#1 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#2 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#3 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#4 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#5 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#7 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#8 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#9 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#10 td:nth-child(8)`).append(html);
                                    
                                }                            
                        }else if (data.hari=='selasa') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#11 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#12 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#13 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#14 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#15 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#17 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#18 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#19 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#20 td:nth-child(8)`).append(html);
                                    
                                }
                        }else if (data.hari=='rabu') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#21 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#22 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#23 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#24 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#25 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#27 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#28 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#29 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#30 td:nth-child(8)`).append(html);
                                    
                                }
                        }else if (data.hari=='kamis') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#31 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#32 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#33 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#34 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#35 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#37 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#38 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#39 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#40 td:nth-child(8)`).append(html);
                                    
                                }
                        }else if (data.hari=='jumat') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#41 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#42 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#43 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#44 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#45 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#47 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#48 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#49 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                        </a>
                                    `;
                                    $(`#50 td:nth-child(8)`).append(html);
                                    
                                }
                        }                    
                }// END OF Kelas D3TI2C

                // KELAS D3TI3A
                else if (data.kelas_nama=='D3TI3A') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(9)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(9)`).html(istirahat); 
                    $(`#16 td:nth-child(9)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(9)`).html(istirahat); 
                    $(`#26 td:nth-child(9)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(9)`).html(istirahat); 
                    $(`#36 td:nth-child(9)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(9)`).html(istirahat); 
                    $(`#46 td:nth-child(9)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(9)`).html(istirahat);     
                    if (data.hari=='senin') {                            
                        if (data.sesi_masuk==1) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#1 td:nth-child(9)`).append(html);
                            
                        }if (data.sesi_masuk==2) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#2 td:nth-child(9)`).append(html);
                            
                        }if (data.sesi_masuk==3) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#3 td:nth-child(9)`).append(html);
                            
                        }if (data.sesi_masuk==4) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#4 td:nth-child(9)`).append(html);
                            
                        }if (data.sesi_masuk==5) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#5 td:nth-child(9)`).append(html);
                            
                        }if (data.sesi_masuk==7) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#7 td:nth-child(9)`).append(html);
                            
                        }if (data.sesi_masuk==8) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#8 td:nth-child(9)`).append(html);
                            
                        }if (data.sesi_masuk==9) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#9 td:nth-child(9)`).append(html);
                            
                        }if (data.sesi_masuk==10) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#10 td:nth-child(9)`).append(html);
                            
                        }                            
                    }else if (data.hari=='selasa') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#11 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#12 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#13 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#14 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#15 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#17 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#18 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#19 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#20 td:nth-child(9)`).append(html);
                                
                            }
                    }else if (data.hari=='rabu') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#21 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#22 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#23 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#24 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#25 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#27 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#28 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#29 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#30 td:nth-child(9)`).append(html);
                                
                            }
                    }else if (data.hari=='kamis') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#31 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#32 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#33 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#34 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#35 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#37 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#38 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#39 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#40 td:nth-child(9)`).append(html);
                                
                            }
                    }else if (data.hari=='jumat') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#41 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#42 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#43 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#44 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#45 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#47 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#48 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#49 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#50 td:nth-child(9)`).append(html);
                                
                            }
                    }                    
                }// END OF Kelas D3TI3A  
                
                // KELAS D3TI3B
                else if (data.kelas_nama=='D3TI3B') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(10)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(10)`).html(istirahat); 
                    $(`#16 td:nth-child(10)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(10)`).html(istirahat); 
                    $(`#26 td:nth-child(10)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(10)`).html(istirahat); 
                    $(`#36 td:nth-child(10)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(10)`).html(istirahat); 
                    $(`#46 td:nth-child(10)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(10)`).html(istirahat);     
                    if (data.hari=='senin') {                            
                        if (data.sesi_masuk==1) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#1 td:nth-child(10)`).append(html);
                            
                        }if (data.sesi_masuk==2) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#2 td:nth-child(10)`).append(html);
                            
                        }if (data.sesi_masuk==3) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#3 td:nth-child(10)`).append(html);
                            
                        }if (data.sesi_masuk==4) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#4 td:nth-child(10)`).append(html);
                            
                        }if (data.sesi_masuk==5) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#5 td:nth-child(10)`).append(html);
                            
                        }if (data.sesi_masuk==7) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#7 td:nth-child(10)`).append(html);
                            
                        }if (data.sesi_masuk==8) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#8 td:nth-child(10)`).append(html);
                            
                        }if (data.sesi_masuk==9) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#9 td:nth-child(10)`).append(html);
                            
                        }if (data.sesi_masuk==10) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#10 td:nth-child(10)`).append(html);
                            
                        }                            
                    }else if (data.hari=='selasa') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#11 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#12 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#13 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#14 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#15 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#17 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#18 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#19 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#20 td:nth-child(10)`).append(html);
                                
                            }
                    }else if (data.hari=='rabu') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#21 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#22 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#23 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#24 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#25 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#27 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#28 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#29 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#30 td:nth-child(10)`).append(html);
                                
                            }
                    }else if (data.hari=='kamis') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#31 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#32 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#33 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#34 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#35 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#37 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#38 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#39 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#40 td:nth-child(10)`).append(html);
                                
                            }
                    }else if (data.hari=='jumat') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#41 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#42 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#43 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#44 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#45 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#47 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#48 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#49 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#50 td:nth-child(10)`).append(html);
                                
                            }
                    }                    
                }// END OF Kelas D3TI3B   
                
                // KELAS D3TI3C
                else if (data.kelas_nama=='D3TI3C') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(11)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(11)`).html(istirahat); 
                    $(`#16 td:nth-child(11)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(11)`).html(istirahat); 
                    $(`#26 td:nth-child(11)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(11)`).html(istirahat); 
                    $(`#36 td:nth-child(11)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(11)`).html(istirahat); 
                    $(`#46 td:nth-child(11)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(11)`).html(istirahat);     
                    if (data.hari=='senin') {                            
                        if (data.sesi_masuk==1) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#1 td:nth-child(11)`).append(html);
                            
                        }if (data.sesi_masuk==2) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#2 td:nth-child(11)`).append(html);
                            
                        }if (data.sesi_masuk==3) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#3 td:nth-child(11)`).append(html);
                            
                        }if (data.sesi_masuk==4) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#4 td:nth-child(11)`).append(html);
                            
                        }if (data.sesi_masuk==5) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#5 td:nth-child(11)`).append(html);
                            
                        }if (data.sesi_masuk==7) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#7 td:nth-child(11)`).append(html);
                            
                        }if (data.sesi_masuk==8) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#8 td:nth-child(11)`).append(html);
                            
                        }if (data.sesi_masuk==9) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#9 td:nth-child(11)`).append(html);
                            
                        }if (data.sesi_masuk==10) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#10 td:nth-child(11)`).append(html);
                            
                        }                            
                    }else if (data.hari=='selasa') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#11 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#12 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#13 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#14 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#15 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#17 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#18 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#19 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#20 td:nth-child(11)`).append(html);                                
                                
                            }
                    }else if (data.hari=='rabu') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#21 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#22 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#23 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#24 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#25 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#27 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#28 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#29 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#30 td:nth-child(11)`).append(html);                                
                                
                            }
                    }else if (data.hari=='kamis') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#31 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#32 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#33 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#34 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#35 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#37 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#38 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#39 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#40 td:nth-child(11)`).append(html);                                
                                
                            }
                    }else if (data.hari=='jumat') {
                        if (data.sesi_masuk==1) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#41 td:nth-child(11)`).append(html);                                
                            
                        }if (data.sesi_masuk==2) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#42 td:nth-child(11)`).append(html);                                
                            
                        }if (data.sesi_masuk==3) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#43 td:nth-child(11)`).append(html);                                
                            
                        }if (data.sesi_masuk==4) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#44 td:nth-child(11)`).append(html);                                
                            
                        }if (data.sesi_masuk==5) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#45 td:nth-child(11)`).append(html);                                
                            
                        }if (data.sesi_masuk==7) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#47 td:nth-child(11)`).append(html);                                
                            
                        }if (data.sesi_masuk==8) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#48 td:nth-child(11)`).append(html);                                
                            
                        }if (data.sesi_masuk==9) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#49 td:nth-child(11)`).append(html);                                
                            
                        }if (data.sesi_masuk==10) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#50 td:nth-child(11)`).append(html);                                
                            
                        }
                    }                    
                }// END OF Kelas D3TI3C

                // KELAS D4RPL1A
                else if (data.kelas_nama=='D4RPL1A') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(12)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(12)`).html(istirahat); 
                    $(`#16 td:nth-child(12)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(12)`).html(istirahat); 
                    $(`#26 td:nth-child(12)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(12)`).html(istirahat); 
                    $(`#36 td:nth-child(12)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(12)`).html(istirahat); 
                    $(`#46 td:nth-child(12)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(12)`).html(istirahat);     
                    if (data.hari=='senin') {                            
                        if (data.sesi_masuk==1) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#1 td:nth-child(12)`).append(html);                                
                            
                        }if (data.sesi_masuk==2) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#2 td:nth-child(12)`).append(html);                                
                            
                        }if (data.sesi_masuk==3) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#3 td:nth-child(12)`).append(html);                                
                            
                        }if (data.sesi_masuk==4) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#4 td:nth-child(12)`).append(html);                                
                            
                        }if (data.sesi_masuk==5) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#5 td:nth-child(12)`).append(html);                                
                            
                        }if (data.sesi_masuk==7) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#7 td:nth-child(12)`).append(html);                                
                            
                        }if (data.sesi_masuk==8) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#8 td:nth-child(12)`).append(html);                                
                            
                        }if (data.sesi_masuk==9) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#9 td:nth-child(12)`).append(html);                                
                            
                        }if (data.sesi_masuk==10) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#10 td:nth-child(12)`).append(html);                                
                            
                        }                            
                    }else if (data.hari=='selasa') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#11 td:nth-child(12)`).append(html);                                
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#12 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#13 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#14 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#15 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#17 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#18 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#19 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#20 td:nth-child(12)`).append(html);
                                
                            }
                    }else if (data.hari=='rabu') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#21 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#22 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#23 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#24 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#25 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#27 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#28 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#29 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#30 td:nth-child(12)`).append(html);
                                
                            }
                    }else if (data.hari=='kamis') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#31 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#32 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#33 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#34 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#35 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#37 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#38 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#39 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#40 td:nth-child(12)`).append(html);
                                
                            }
                    }else if (data.hari=='jumat') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#41 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#42 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#43 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#44 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#45 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#47 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#48 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#49 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#50 td:nth-child(12)`).append(html);
                                
                            }
                    }                    
                }// END OF Kelas D4RPL1A
                
                // KELAS D4RPL2A
                else if (data.kelas_nama=='D4RPL2A') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(13)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(13)`).html(istirahat); 
                    $(`#16 td:nth-child(13)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(13)`).html(istirahat); 
                    $(`#26 td:nth-child(13)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(13)`).html(istirahat); 
                    $(`#36 td:nth-child(13)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(13)`).html(istirahat); 
                    $(`#46 td:nth-child(13)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(13)`).html(istirahat);     
                    if (data.hari=='senin') {                            
                        if (data.sesi_masuk==1) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#1 td:nth-child(13)`).append(html);
                            
                        }if (data.sesi_masuk==2) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#2 td:nth-child(13)`).append(html);
                            
                        }if (data.sesi_masuk==3) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#3 td:nth-child(13)`).append(html);
                            
                        }if (data.sesi_masuk==4) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#4 td:nth-child(13)`).append(html);
                            
                        }if (data.sesi_masuk==5) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#5 td:nth-child(13)`).append(html);
                            
                        }if (data.sesi_masuk==7) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#7 td:nth-child(13)`).append(html);
                            
                        }if (data.sesi_masuk==8) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#8 td:nth-child(13)`).append(html);
                            
                        }if (data.sesi_masuk==9) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#9 td:nth-child(13)`).append(html);
                            
                        }if (data.sesi_masuk==10) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#10 td:nth-child(13)`).append(html);
                            
                        }                            
                    }else if (data.hari=='selasa') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#11 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#12 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#13 td:nth-child(13)`).append(html);                                
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#14 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#15 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#17 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#18 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#19 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#20 td:nth-child(13)`).append(html);
                                
                            }
                    }else if (data.hari=='rabu') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#21 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#22 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#23 td:nth-child(13)`).append(html);                                
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#24 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#25 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#27 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#28 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#29 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#30 td:nth-child(13)`).append(html);
                                
                            }
                    }else if (data.hari=='kamis') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#31 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#32 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#33 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#34 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#35 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#37 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#38 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#39 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#40 td:nth-child(13)`).append(html);
                                
                            }
                    }else if (data.hari=='jumat') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#41 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#42 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#43 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#44 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#45 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#47 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#48 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#49 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#50 td:nth-child(13)`).append(html);
                                
                            }
                    }                    
                }// END OF Kelas D4RPL2A

                // KELAS D4RPL2B
                else if (data.kelas_nama=='D4RPL2B') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(14)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(14)`).html(istirahat); 
                    $(`#16 td:nth-child(14)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(14)`).html(istirahat); 
                    $(`#26 td:nth-child(14)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(14)`).html(istirahat); 
                    $(`#36 td:nth-child(14)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(14)`).html(istirahat); 
                    $(`#46 td:nth-child(14)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(14)`).html(istirahat);     
                    if (data.hari=='senin') {                            
                        if (data.sesi_masuk==1) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#1 td:nth-child(14)`).append(html);
                            
                        }if (data.sesi_masuk==2) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#2 td:nth-child(14)`).append(html);                            
                            
                        }if (data.sesi_masuk==3) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#3 td:nth-child(14)`).append(html);
                            
                        }if (data.sesi_masuk==4) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#4 td:nth-child(14)`).append(html);
                            
                        }if (data.sesi_masuk==5) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#5 td:nth-child(14)`).append(html);
                            
                        }if (data.sesi_masuk==7) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#7 td:nth-child(14)`).append(html);
                            
                        }if (data.sesi_masuk==8) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#8 td:nth-child(14)`).append(html);
                            
                        }if (data.sesi_masuk==9) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#9 td:nth-child(14)`).append(html);
                            
                        }if (data.sesi_masuk==10) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#10 td:nth-child(14)`).append(html);
                            
                        }                            
                    }else if (data.hari=='selasa') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#11 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#12 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#13 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#14 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#15 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#17 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#18 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#19 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#20 td:nth-child(14)`).append(html);
                                
                            }
                    }else if (data.hari=='rabu') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#21 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#22 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#23 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#24 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#25 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#27 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#28 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#29 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#30 td:nth-child(14)`).append(html);
                                
                            }
                    }else if (data.hari=='kamis') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#31 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#32 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#33 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#34 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#35 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#37 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#38 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#39 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#40 td:nth-child(14)`).append(html);
                                
                            }
                    }else if (data.hari=='jumat') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#41 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#42 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#43 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#44 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#45 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#47 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#48 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#49 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#50 td:nth-child(14)`).append(html);
                                
                            }
                    }                    
                }// END OF Kelas D4RPL2B

                 // KELAS D4RPL3A
                 else if (data.kelas_nama=='D4RPL3A') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(15)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(15)`).html(istirahat); 
                    $(`#16 td:nth-child(15)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(15)`).html(istirahat); 
                    $(`#26 td:nth-child(15)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(15)`).html(istirahat); 
                    $(`#36 td:nth-child(15)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(15)`).html(istirahat); 
                    $(`#46 td:nth-child(15)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(15)`).html(istirahat);     
                    if (data.hari=='senin') {                            
                        if (data.sesi_masuk==1) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#1 td:nth-child(15)`).append(html);
                            
                        }if (data.sesi_masuk==2) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#2 td:nth-child(15)`).append(html);
                            
                        }if (data.sesi_masuk==3) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#3 td:nth-child(15)`).append(html);
                            
                        }if (data.sesi_masuk==4) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#4 td:nth-child(15)`).append(html);
                            
                        }if (data.sesi_masuk==5) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#5 td:nth-child(15)`).append(html);
                            
                        }if (data.sesi_masuk==7) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#7 td:nth-child(15)`).append(html);
                            
                        }if (data.sesi_masuk==8) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#8 td:nth-child(15)`).append(html);
                            
                        }if (data.sesi_masuk==9) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#9 td:nth-child(15)`).append(html);
                            
                        }if (data.sesi_masuk==10) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                </a>
                            `;
                            $(`#10 td:nth-child(15)`).append(html);
                            
                        }                            
                    }else if (data.hari=='selasa') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#11 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#12 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#13 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#14 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#15 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#17 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#18 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#19 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#20 td:nth-child(15)`).append(html);
                                
                            }
                    }else if (data.hari=='rabu') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#21 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#22 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#23 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#24 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#25 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#27 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#28 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#29 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#30 td:nth-child(15)`).append(html);
                                
                            }
                    }else if (data.hari=='kamis') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#31 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#32 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#33 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#34 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#35 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#37 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#38 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#39 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#40 td:nth-child(15)`).append(html);
                                
                            }
                    }else if (data.hari=='jumat') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#41 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#42 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#43 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#44 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#45 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#47 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#48 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#49 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) ${data.dosen_nama}
                                    </a>
                                `;
                                $(`#50 td:nth-child(15)`).append(html);
                                
                            }
                    }                    
                }// END OF Kelas D4RPL3A
            }// END OF FOR                        
        });// END Of Axios                   
    });                 
</script>
@endpush
@endsection