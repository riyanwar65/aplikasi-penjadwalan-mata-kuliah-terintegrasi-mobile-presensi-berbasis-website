@extends('master')
@section('title','Cek')
@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">                        
        <div class="row"> 
            <div class="col-md-12">
                <div class="alert alert-warning alert-block">
                    {{-- <button type="button" class="close" data-dismiss="alert">×</button>                      --}}
                    <strong><h5>Perhatian</h5></strong> <br>
                    Pada bagian ini merupakan pengecekan jadwal berdasarkan lab<br>
                    Jika terdapat mata kuliah yang bentrok maka di edit cukup menggeser mata kuliah yang bentrok tersebut <br>
                    <strong>Tambahkan juga jenis praktik atau teori dengan cara klik mata kuliah yang terkait lalu pilih jenisnya klik simpan</strong>
                    <strong>Tombol Publish itu digunakan ketika pengeditan jadwal bentrok sudah dilakukan</strong>                    
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="col-md-6">
                            <select name="ruangan" id="ruangan_id" class="form-control">
                                <option value="" disabled selected>Pilih</option>
                                @foreach ($schedules as $key=>$value)                
                                        <option value="{{$value->ruangan_id}}">{{$value->ruangan_nama}}</option>            
                                @endforeach
                            </select>             
                        </div>   
                        <div class="col-md-6">
                            <button class="btn btn-sm btn-info" onclick="filter()">Pilih</button>            
                            <button class="btn btn-sm btn-success" onclick="finish()">Publish</button>  
                        </div>           
                    </div>
                </div>
            </div> 
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body" id="show-data">
                        <table class="table table-striped table-bordered" id="detail"> 
                            <thead id="head">
                            
                            </thead>   
                            <tbody id="body"></tbody>                
                        </table>
                    </div>
                </div>
            </div>            
            {{-- <div class="col-md-12">
                <div class="card">
                    <div class="card-body tempatgeser" id="">
                        <div class="draggable" id="drag_{{$sort[$i]}}" data-kd="{{$sort[$i]}}">
                            {{$sort[$i]}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body droppable" id="" data-ehem="ehem"></div>
                </div>
            </div> --}}
        </div>        
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="tambahjenis" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    {{-- <form action="{{route('tambahjenis')}}" method="post">
    @csrf --}}
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Praktek atau Teori</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id="pt">
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" onclick="simpantipe()">Simpan</button>
        </div>
      </div>
    {{-- </form> --}}
    </div>
  </div>

@push('script')
{{-- <script src="{{asset('assets/js/jquery-ui.min.js')}}"></script>  --}}
<script src="{{asset('assets/js/axios.min.js')}}"></script>
{{-- <script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script> --}}
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>  
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" ></script>
<script src="{{asset('assets/js/sweetalert2.js')}}"></script>    
{{-- <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script> --}}
<script>   

    function finish() {
        Swal.fire({
            title: 'Peringatan',
            text: `Apakah anda sudah mengedit semua jadwal yang bentrok dan tidak teratur?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Publish'
        }).then((result) => {
            if (result.value) {
                axios.post(`publish`,{
                    tahun_ajaran:"{{$tahun_ajaran}}",
                    semester:"{{$semester}}"
                }).then((response)=>{
                    Swal.fire({
                        title:'Sukses',
                        text:"Data Berhasil di Publish",
                        icon:'success',
                    }).then(()=>{
                        // console.log(data);
                        window.location.href="{{route('jadwal.index')}}";
                    });
                });                                                      
            }
        })
    }

    function draggable(nama_dosen,sesi,hari,bkd){        
        $(".draggable").draggable({ cursor: "crosshair", revert: "invalid"});        
        $(".tempatgeser").droppable({ accept: ".draggable", drop: function(event, ui) {             
            var coba = [];
            // console.log($(event.target).attr('data-sesi'));
            var idtemp = $(ui.draggable).attr("data-temp"); 
            var pindahsesi = $(event.target).attr('data-sesi');
            var kelas = $(ui.draggable).attr("data-kelas");
            var hari = $(event.target).attr('id');
            var ruangan = $(ui.draggable).attr("data-ruangan");
            axios.post('perubahansesi',{
                temp:idtemp,
                pindahsesi:pindahsesi,
                hari:hari,
                kelas:kelas,
                ruangan_id:ruangan
            }).then((response)=>{
                if (response.data.status==false) {
                    Swal.fire({
                        title:'Peringatan', 
                        text:response.data.Message,
                        icon:'error'
                    }) 
                }else{
                    console.log(ruangan);
                    console.log(pindahsesi+" "+hari);            
                    var dropped = ui.draggable;
                    var droppedOn = $(this);
                    $(dropped).detach().css({top: 0,left: 0}).appendTo(droppedOn); 
                }
                // console.log(response);                
            });                 
        }});        
    }    
    var penamaan_sesi = 1;
    var tampungan = [];     
    var array = [];
    function filter() {
        $(".sesi").html("");
        $("#senin").html("");
        $("#selasa").html("");
        $("#rabu").html("");
        $("#kamis").html("");
        $("#jumat").html(""); 
        $('#body').html("");  
        var no_sesi=1;
        var no = 1; 
        var r = $('#ruangan_id').val();
        console.log("{{$semester}}");
        axios.post(`/filterlab`,{
            ruangan:$('#ruangan_id').val(),
            tahun_ajaran:"{{$tahun_ajaran}}",
            semester:"{{$semester}}"
        }).then((res)=>{
            // console.log(res.data);
            $("#head").html(`
                <tr>                
                    <th>sesi</th>
                    <th>senin</th>
                    <th>selasa</th>
                    <th>rabu</th>
                    <th>kamis</th>
                    <th>jumat</th>
                </tr>
            `);            
            var a = 0;
            var b = 1;
            for (let index = 1; index <=res.data.sesi.length; index++) {                
                var sesi = res.data.sesi[a++];
                // console.log(sesi);
                var html = `                                    
                        <tr id="${no_sesi++}">
                            <td>${sesi.jam_masuk+"-"+sesi.jam_keluar}</td>                            
                            <td class="tempatgeser droppable" data-sesi=${sesi.id_sesi} id="senin"></td>
                            <td class="tempatgeser droppable" data-sesi=${sesi.id_sesi} id="selasa"></td>
                            <td class="tempatgeser droppable" data-sesi=${sesi.id_sesi} id="rabu"></td>
                            <td class="tempatgeser droppable" data-sesi=${sesi.id_sesi} id="kamis"></td>
                            <td class="tempatgeser droppable" data-sesi=${sesi.id_sesi} id="jumat"></td>
                        </tr>`;
                $('#body').append(html);                
            }                                                          
            // for (let i = 0; i < res.data.generate.length; i++) {
            //     for (let j =0; j <res.data.generate[i].length;j++) {                    
            //         console.log(res.data.generate[i][j].sesi[j]);                    
            //     }
            // }      
            var ini = 0;                  
            // for (let k = 1; k<= res.data.sesi.length; k++) {
                // console.log($("#body tr").attr("id"));
                for (let i = 0; i < res.data.generate.length; i++) {
                    // for (let j =0; j <res.data.generate[i].length;j++) {                    
                        var sesiDB = res.data.generate[i].sesi;
                        var hari = res.data.generate[i].hari;
                        var data = res.data.generate[i];
                        console.log(data);
                        if (sesiDB===1) {                            
                            if (hari==="senin") {
                                var html=`
                                    <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${r}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">                                        
                                        ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#1 td:nth-child(2)`).append(html);
                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="selasa") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#1 td:nth-child(3)`).append(html);
                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="rabu") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#1 td:nth-child(4)`).append(html);
                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="kamis") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#1 td:nth-child(5)`).append(html);
                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="jumat") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#1 td:nth-child(6)`).append(html);
                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                        }
                        if (sesiDB===2) {
                            if (hari==="senin") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#2 td:nth-child(2)`).append(html);
                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="selasa") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#2 td:nth-child(3)`).append(html);
                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="rabu") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#2 td:nth-child(4)`).append(html);
                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="kamis") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#2 td:nth-child(5)`).append(html);
                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="jumat") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#2 td:nth-child(6)`).append(html);
                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                        } 
                        if (sesiDB===3) {
                            if (hari==="senin") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#3 td:nth-child(2)`).append(html);
                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="selasa") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#3 td:nth-child(3)`).append(html);
                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="rabu") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#3 td:nth-child(4)`).append(html);
                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="kamis") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#3 td:nth-child(5)`).append(html);
                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="jumat") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#3 td:nth-child(6)`).append(html);
                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                        }
                        if (sesiDB===4) {
                            if (hari==="senin") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#4 td:nth-child(2)`).append(html);
                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="selasa") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#4 td:nth-child(3)`).append(html);
                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="rabu") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#4 td:nth-child(4)`).append(html);
                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="kamis") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#4 td:nth-child(5)`).append(html);
                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="jumat") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#4 td:nth-child(6)`).append(html);
                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                        } 
                        if (sesiDB===5) {
                            if (hari==="senin") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#5 td:nth-child(2)`).append(html);
                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="selasa") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#5 td:nth-child(3)`).append(html);
                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="rabu") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#5 td:nth-child(4)`).append(html);
                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="kamis") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#5 td:nth-child(5)`).append(html);
                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="jumat") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#5 td:nth-child(6)`).append(html);
                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                        }
                        if (6==6) {
                            if ("senin") {
                                $(`#6 td:nth-child(2)`).removeClass("tempatgeser droppable ui-droppable");
                                var html=`
                                    <strong>Istirahat</strong>
                                `;
                                $(`#6 td:nth-child(2)`).html(html);                                
                                // draggable(data.dosen,sesiDB,hari);
                            }
                            if ("selasa") {
                                $(`#6 td:nth-child(3)`).removeClass("tempatgeser droppable ui-droppable");
                                var html=`
                                    <strong>Istirahat</strong>
                                `;
                                $(`#6 td:nth-child(3)`).html(html);                                
                                // draggable(data.dosen,sesiDB,hari);
                            }
                            if ("rabu") {
                                $(`#6 td:nth-child(4)`).removeClass("tempatgeser droppable ui-droppable");
                                var html=`
                                    <strong>Istirahat</strong>
                                `;
                                $(`#6 td:nth-child(4)`).html(html);                                
                            }
                            if ("kamis") {
                                $(`#6 td:nth-child(5)`).removeClass("tempatgeser droppable ui-droppable");
                                var html=`
                                    <strong>Istirahat</strong>
                                `;
                                $(`#6 td:nth-child(5)`).html(html);                                
                            }
                            if ("jumat") {
                                $(`#6 td:nth-child(6)`).removeClass("tempatgeser droppable ui-droppable");
                                var html=`
                                    <strong>Istirahat</strong>
                                `;
                                $(`#6 td:nth-child(6)`).html(html);                                
                            }
                        } 
                        if (sesiDB===7) {
                            if (hari==="senin") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#7 td:nth-child(2)`).append(html);
                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="selasa") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#7 td:nth-child(3)`).append(html);
                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="rabu") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#7 td:nth-child(4)`).append(html);                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="kamis") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#7 td:nth-child(5)`).append(html);
                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="jumat") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#7 td:nth-child(6)`).append(html);                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                        }
                        if (sesiDB===8) {
                            if (hari==="senin") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#8 td:nth-child(2)`).append(html);                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="selasa") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#8 td:nth-child(3)`).append(html);                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="rabu") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#8 td:nth-child(4)`).append(html);                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="kamis") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#8 td:nth-child(5)`).append(html);                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="jumat") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#8 td:nth-child(6)`).append(html);                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                        }
                        if (sesiDB===9) {
                            if (hari==="senin") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#9 td:nth-child(2)`).append(html);                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="selasa") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#9 td:nth-child(3)`).append(html);                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="rabu") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#9 td:nth-child(4)`).append(html);                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="kamis") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#9 td:nth-child(5)`).append(html);                               
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="jumat") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#9 td:nth-child(6)`).append(html);                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                        }
                        if (sesiDB===10) {
                            if (hari==="senin") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#10 td:nth-child(2)`).append(html);                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="selasa") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#10 td:nth-child(3)`).append(html);                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="rabu") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#10 td:nth-child(4)`).append(html);                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="kamis") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#10 td:nth-child(5)`).append(html);                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                            if (hari==="jumat") {
                                var html=`
                                <a href="#modaltambah" class="badge badge-pill badge-info draggable" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-toggle="modal" data-target="#tambahjenis" style="color:white; font-family:'Calibri';">
                                    ${data.dosen+" "+data.matkul+" "+data.kelas+" "+"("+data.jenis+")"} 
                                    </a>
                                `;
                                $(`#10 td:nth-child(6)`).append(html);                                
                                draggable(data.dosen,sesiDB,hari,data.bkd);
                            }
                        }                   
                    // }
                }    
            // }
            // $(".sesi").html("");
               
        });
    }
    $('#pt').html("");
    $(document).on('click','a[href="#modaltambah"]',function(){
        var temp = $(this).data('temp'); 
        var dosen = $(this).data('dosen');
        var matkul = $(this).data('matkul');
        var kelas = $(this).data('kelas');
        var html = `
            Informasi : <br>
            ${dosen} <br>
            ${matkul} <br>
            ${kelas} <br>
            <input type="hidden" value="${temp}" id="temp" name="id_temp_jadwal"> <br>            
            <select name="tipe" class="form-control" id="tipe">                
                <option value="praktek">Praktek</option>
                <option value="teori">Teori</option>
            </select>
        `;
        $('#pt').html(html);        
    });         
    function simpantipe() {
        var temp = $('#temp').val();
        console.log(temp);
        var tipe = $('#tipe').val();
        axios.post('/tambahjenis',{
            "_token": "{{ csrf_token() }}",
            temp:temp,
            tipe:tipe,
        }).then((response)=>{
            Swal.fire({
                title:'Sukses',
                text:response.data.message,
                icon:'success',
            }).then(()=>{
                window.location.reload();
            });            
        });
    }
// console.log(data_drop);
// console.log(data);
</script>

@endpush
@endsection