@extends('master')
@section('title','Cek')
@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">                        
        <div class="row"> 
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="col-md-6">
                            <select name="ruangan" id="ruangan-test" class="">
                                <option value="" disabled selected>Pilih</option>
                                @foreach ($schedules as $key=>$value)                
                                        <option value="{{$value->ruangan_id}}">{{$value->ruangan_nama}}</option>            
                                @endforeach
                            </select>         
                            <button class="btn btn-sm btn-info" onclick="filter()">Pilih</button>            
                            <button class="btn btn-sm btn-success" onclick="finish()">Publish</button>      
                        </div>   
                        <div class="col-md-6">                            
                        </div>           
                    </div>
                </div>
            </div> 
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body" id="show-data">
                        <table class="table table-striped table-bordered" id="detail"> 
                            <thead id="head">
                                <tr id="kelas">                                    
                                </tr>
                            </thead>   
                            <tbody id="body">
                                
                            </tbody>                
                        </table>
                    </div>
                </div>
            </div>         
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="tambahjenis" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    {{-- <form action="{{route('tambahjenis')}}" method="post">
    @csrf --}}
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Praktek atau Teori</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id="pt">
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick="simpantipe()">Simpan</button>
        </div>
      </div>
    {{-- </form> --}}
    </div>
  </div>
@push('script')
{{-- <script src="{{asset('assets/js/jquery-ui.min.js')}}"></script>  --}}
<script src="{{asset('assets/js/axios.min.js')}}"></script>
{{-- <script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script> --}}
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>  
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" ></script>
{{-- <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script> --}}
<script>        
    var a = 0;
    var b = 0;
    var c = 0;   
    var jmlh = 1;    
    var jm = 1;     
    function finish() {
        Swal.fire({
            title: 'Peringatan',
            text: `Apakah anda sudah mengedit semua jadwal yang bentrok dan tidak teratur?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Publish'
        }).then((result) => {
            if (result.value) {
                axios.post(`publish`,{
                    tahun_ajaran:"{{$tahun_ajaran}}",
                    semester:"{{$semester}}"
                }).then((response)=>{
                    Swal.fire({
                        title:'Sukses',
                        text:"Data Berhasil di Publish",
                        icon:'success',
                    }).then(()=>{
                        // console.log(data);
                        window.location.href="{{route('jadwal.index')}}";
                    });
                });                                                      
            }
        })
    }

    function istirahat() {
        var istirahat=`
            <strong>Istirahat</strong>
        `;
        $(`#6 td:nth-child(3)`).removeClass("tempatgeser droppable ui-droppable");                        
        $(`#6 td:nth-child(3)`).html(istirahat); 
        $(`#16 td:nth-child(3)`).removeClass("tempatgeser droppable ui-droppable");                        
        $(`#16 td:nth-child(3)`).html(istirahat); 
        $(`#26 td:nth-child(3)`).removeClass("tempatgeser droppable ui-droppable");                        
        $(`#26 td:nth-child(3)`).html(istirahat); 
        $(`#36 td:nth-child(3)`).removeClass("tempatgeser droppable ui-droppable");                        
        $(`#36 td:nth-child(3)`).html(istirahat); 
        $(`#46 td:nth-child(3)`).removeClass("tempatgeser droppable ui-droppable");                        
        $(`#46 td:nth-child(3)`).html(istirahat); 
    }

    $('#show-data').hide();
    function draggable(){        
        $(".draggable").draggable({ cursor: "crosshair", revert: "invalid"});        
        $(".tempatgeser").droppable({ accept: ".draggable", drop: function(event, ui) {             
            var coba = [];
            // console.log($(event.target).attr('data-sesi'));
            var id_temp_drag = $(ui.draggable).attr("data-temp");            
            var kddosen = $(ui.draggable).attr("data-kddos");                              
            var ruangan_drag = $(ui.draggable).attr("data-ruangan");
            var kd_matkul_drag = $(ui.draggable).attr("data-kdmatkul");            
            var kelas_drag = $(ui.draggable).attr("data-kelas");
            var kelas_drop = $(event.target).attr('id');
            var hari_drop = $(event.target).attr('data-hari');
            var sesi_drop = $(event.target).attr('data-sesi');
            var sesi_drag = $(ui.draggable).attr('data-sesitarget');
            console.log(id_temp_drag);
            var ruangan = $(ui.draggable).attr("data-ruangan");                 
            axios.post('perubahansesi',{
                temp_id:id_temp_drag,
                kd_dosen:kddosen,
                kd_matkul:kd_matkul_drag,
                ruangan:ruangan_drag,
                kelas_bawaan:kelas_drag,
                kelas_target:kelas_drop,
                hari_target:hari_drop,
                sesi_target:sesi_drop,
            }).then((response)=>{     
                    if (response.data.status===false) { 
                        console.log(response.data.data);                       
                        Swal.fire({
                            title:'Peringatan',
                            text:response.data.message,
                            icon:'error',
                        });                        
                    }else{          
                        console.log(response.data.data);                       
                        var dropped = ui.draggable;
                        var droppedOn = $(this);
                        $(dropped).detach().css({top: 0,left: 0}).appendTo(droppedOn);                 
                    }                               
            });                 
            // var dropped = ui.draggable;
            // var droppedOn = $(this);
            // $(dropped).detach().css({top: 0,left: 0}).appendTo(droppedOn);                 
        }});        
    }     
    
    function filter() {                      
        $("#kelas").html('');   
        $("#D3TI1A").html('');   
        $("#D3TI1B").html('');
        $("#D3TI1C").html('');
        $("#D3TI2A").html('');
        $("#D3TI2B").html('');
        $("#D3TI2C").html('');
        $("#D3TI3A").html('');
        $("#D3TI3B").html('');
        $("#D3TI3C").html('');
        $("#D4RPL1A").html('');
        $("#D4RPL2A").html('');
        $("#D4RPL2B").html('');
        $("#D4RPL3A").html('');
        $("#body").html("");
        $('#show-data').show();  
        axios.get(`kelas-test`).then((res)=>{   
            $("#kelas").html(`
                <th>Hari</th>
                <th>Jam</th>  
            `);           
            for (let i = 0; i < res.data.length; i++) {
                var data = res.data[i].kelas_nama;                
                var html = `                                
                    <th>${data}</th>
                `;
                $("#kelas").append(html);
            }           
        });                          
        axios.post(`hari-sesi`,{
            ruangan:$('#ruangan-test').val(),
            tahun_ajaran:"{{$tahun_ajaran}}",
            semester:"{{$semester}}"
        }).then((res)=>{                              
            for (var i = 0; i < res.data.harisesi.length; i++) {                
                for (var j = 0; j < res.data.harisesi[i].length; j++) {                     
                    var hari = res.data.harisesi[i][j].hari;
                    var sesi = res.data.harisesi[i][j].sesi;                                                          
                    var html = `
                        <tr id="${jmlh++}">
                            <td id="${hari}">${hari}</td>
                            <td>${sesi.jam_masuk} - ${sesi.jam_keluar}</td>   
                            <td class="tempatgeser" id="D3TI1A" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D3TI1B" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D3TI1C" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D3TI2A" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D3TI2B" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D3TI2C" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                          
                            <td class="tempatgeser" id="D3TI3A" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D3TI3B" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D3TI3C" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                          
                            <td class="tempatgeser" id="D4RPL1A" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D4RPL2A" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D4RPL2B" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D4RPL3A" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                                 
                        </tr>
                    `;                    
                    $("#body").append(html);                    
                }                
            }     
            jmlh=1;
            var hari;
            for (let k = 0; k < res.data.kelas.length; k++) {                
                var kelas = res.data.kelas[k].kelas_nama;
                var data = res.data.kelas[k];                
                // KELAS D3TI1A                
                if (data.kelas_nama=='D3TI1A') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(3)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(3)`).html(istirahat); 
                    $(`#16 td:nth-child(3)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(3)`).html(istirahat); 
                    $(`#26 td:nth-child(3)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(3)`).html(istirahat); 
                    $(`#36 td:nth-child(3)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(3)`).html(istirahat); 
                    $(`#46 td:nth-child(3)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(3)`).html(istirahat); 
                        if (data.hari=='senin') {                            
                                if (data.sesi_masuk==1) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">                                                                                                                    
                                            ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#1 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#2 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#3 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#4 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#5 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#7 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#8 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#9 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#10 td:nth-child(3)`).append(html);
                                    draggable()
                                }                            
                        }else if (data.hari=='selasa') {
                            if (data.sesi_masuk==1) {                                
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#11 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#12 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#13 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#14 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#15 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#17 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#18 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#19 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#20 td:nth-child(3)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='rabu') {
                            if (data.sesi_masuk==1) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#21 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#22 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#23 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#24 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#25 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#27 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#28 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#29 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#30 td:nth-child(3)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='kamis') {
                            if (data.sesi_masuk==1) {                                
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#31 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#32 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#33 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#34 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#35 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#37 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#38 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#39 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#40 td:nth-child(3)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='jumat') {
                            if (data.sesi_masuk==1) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#41 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#42 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#43 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#44 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#45 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#47 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#48 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#49 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#50 td:nth-child(3)`).append(html);
                                    draggable()
                                }
                        }                    
                }
                // END OF KELAS D3TI1A

                // KELAS D3TI1B
                else if (data.kelas_nama=='D3TI1B') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(4)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(4)`).html(istirahat); 
                    $(`#16 td:nth-child(4)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(4)`).html(istirahat); 
                    $(`#26 td:nth-child(4)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(4)`).html(istirahat); 
                    $(`#36 td:nth-child(4)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(4)`).html(istirahat); 
                    $(`#46 td:nth-child(4)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(4)`).html(istirahat);     
                        if (data.hari=='senin') {                            
                                if (data.sesi_masuk==1) {
                                    $(`#1 td:nth-child(4)`).html("");
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#1 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#2 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#3 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#4 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#5 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#7 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#8 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#9 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#10 td:nth-child(4)`).append(html);
                                    draggable()
                                }                            
                        }else if (data.hari=='selasa') {
                            if (data.sesi_masuk==1) {                                
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#11 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#12 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#13 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#14 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#15 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#17 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#18 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#19 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#20 td:nth-child(4)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='rabu') {
                            if (data.sesi_masuk==1) {                                
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#21 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#22 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#23 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#24 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#25 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#27 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#28 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#29 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#30 td:nth-child(4)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='kamis') {
                            if (data.sesi_masuk==1) {                                
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#31 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#32 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#33 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#34 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#35 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#37 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#38 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#39 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#40 td:nth-child(4)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='jumat') {
                            if (data.sesi_masuk==1) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#41 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#42 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#43 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#44 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#45 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#47 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#48 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#49 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#50 td:nth-child(4)`).append(html);
                                    draggable()
                                }
                        }                    
                }// END OF Kelas D3TI1B

                // KELAS D3TI1C
                else if (data.kelas_nama=='D3TI1C') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(5)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(5)`).html(istirahat); 
                    $(`#16 td:nth-child(5)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(5)`).html(istirahat); 
                    $(`#26 td:nth-child(5)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(5)`).html(istirahat); 
                    $(`#36 td:nth-child(5)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(5)`).html(istirahat); 
                    $(`#46 td:nth-child(5)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(5)`).html(istirahat);     
                    if (data.hari=='senin') {                            
                                if (data.sesi_masuk==1) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#1 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#2 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#3 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#4 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#5 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#7 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#8 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#9 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#10 td:nth-child(5)`).append(html);
                                    draggable()
                                }                            
                        }else if (data.hari=='selasa') {
                            if (data.sesi_masuk==1) {                                
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#11 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#12 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#13 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#14 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#15 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#17 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#18 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#19 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#20 td:nth-child(5)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='rabu') {
                            if (data.sesi_masuk==1) {                                
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#21 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#22 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#23 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#24 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#25 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#27 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#28 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#29 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#30 td:nth-child(5)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='kamis') {
                            if (data.sesi_masuk==1) {                                
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#31 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#32 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#33 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#34 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#35 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#37 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#38 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#39 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#40 td:nth-child(5)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='jumat') {
                            if (data.sesi_masuk==1) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#41 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#42 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#43 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#44 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#45 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#47 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#48 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#49 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#50 td:nth-child(5)`).append(html);
                                    draggable()
                                }
                        }                    
                }// END OF Kelas D3TI1C

                // KELAS D3TI2A
                else if (data.kelas_nama=='D3TI2A') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(6)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(6)`).html(istirahat); 
                    $(`#16 td:nth-child(6)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(6)`).html(istirahat); 
                    $(`#26 td:nth-child(6)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(6)`).html(istirahat); 
                    $(`#36 td:nth-child(6)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(6)`).html(istirahat); 
                    $(`#46 td:nth-child(6)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(6)`).html(istirahat);     
                        if (data.hari=='senin') {                            
                                if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#1 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#2 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#3 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#4 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#5 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#7 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#8 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#9 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#10 td:nth-child(6)`).append(html);
                                    draggable()
                                }                            
                        }else if (data.hari=='selasa') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#11 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#12 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#13 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#14 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#15 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#17 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#18 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#19 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#20 td:nth-child(6)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='rabu') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#21 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#22 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#23 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#24 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#25 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#27 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#28 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#29 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#30 td:nth-child(6)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='kamis') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#31 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#32 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#33 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#34 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#35 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#37 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#38 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#39 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#40 td:nth-child(6)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='jumat') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#41 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#42 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#43 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#44 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#45 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#47 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#48 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#49 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#50 td:nth-child(6)`).append(html);
                                    draggable()
                                }
                        }                    
                }// END OF Kelas D3TI2A
                
                // KELAS D3TI2B
                else if (data.kelas_nama=='D3TI2B') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(7)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(7)`).html(istirahat); 
                    $(`#16 td:nth-child(7)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(7)`).html(istirahat); 
                    $(`#26 td:nth-child(7)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(7)`).html(istirahat); 
                    $(`#36 td:nth-child(7)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(7)`).html(istirahat); 
                    $(`#46 td:nth-child(7)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(7)`).html(istirahat);     
                        if (data.hari=='senin') {                            
                                if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#1 td:nth-child(7)`).append(html);                                    
                                    draggable()
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#2 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#3 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#4 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#5 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#7 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#8 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#9 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#10 td:nth-child(7)`).append(html);
                                    draggable()
                                }                            
                        }else if (data.hari=='selasa') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#11 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#12 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#13 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#14 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#15 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#17 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#18 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#19 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#20 td:nth-child(7)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='rabu') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#21 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#22 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#23 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#24 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#25 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#27 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#28 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#29 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#30 td:nth-child(7)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='kamis') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#31 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#32 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#33 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#34 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#35 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#37 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#38 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#39 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#40 td:nth-child(7)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='jumat') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#41 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#42 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#43 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#44 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#45 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#47 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#48 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#49 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#50 td:nth-child(7)`).append(html);
                                    draggable()
                                }
                        }                    
                }// END OF Kelas D3TI2B

                // KELAS D3TI2C
                else if (data.kelas_nama=='D3TI2C') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(8)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(8)`).html(istirahat); 
                    $(`#16 td:nth-child(8)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(8)`).html(istirahat); 
                    $(`#26 td:nth-child(8)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(8)`).html(istirahat); 
                    $(`#36 td:nth-child(8)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(8)`).html(istirahat); 
                    $(`#46 td:nth-child(8)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(8)`).html(istirahat);     
                        if (data.hari=='senin') {                            
                                if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#1 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#2 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#3 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#4 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#5 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#7 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#8 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#9 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#10 td:nth-child(8)`).append(html);
                                    draggable()
                                }                            
                        }else if (data.hari=='selasa') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#11 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#12 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#13 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#14 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#15 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#17 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#18 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#19 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#20 td:nth-child(8)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='rabu') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#21 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#22 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#23 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#24 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#25 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#27 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#28 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#29 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#30 td:nth-child(8)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='kamis') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#31 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#32 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#33 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#34 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#35 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#37 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#38 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#39 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#40 td:nth-child(8)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='jumat') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#41 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#42 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#43 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#44 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#45 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#47 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#48 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#49 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#50 td:nth-child(8)`).append(html);
                                    draggable()
                                }
                        }                    
                }// END OF Kelas D3TI2C

                // KELAS D3TI3A
                else if (data.kelas_nama=='D3TI3A') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(9)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(9)`).html(istirahat); 
                    $(`#16 td:nth-child(9)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(9)`).html(istirahat); 
                    $(`#26 td:nth-child(9)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(9)`).html(istirahat); 
                    $(`#36 td:nth-child(9)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(9)`).html(istirahat); 
                    $(`#46 td:nth-child(9)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(9)`).html(istirahat);     
                    if (data.hari=='senin') {                            
                        if (data.sesi_masuk==1) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#1 td:nth-child(9)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==2) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#2 td:nth-child(9)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==3) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#3 td:nth-child(9)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==4) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#4 td:nth-child(9)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==5) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#5 td:nth-child(9)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==7) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#7 td:nth-child(9)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==8) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#8 td:nth-child(9)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==9) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#9 td:nth-child(9)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==10) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#10 td:nth-child(9)`).append(html);
                            draggable()
                        }                            
                    }else if (data.hari=='selasa') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#11 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#12 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#13 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#14 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#15 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#17 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#18 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#19 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#20 td:nth-child(9)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='rabu') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#21 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#22 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#23 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#24 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#25 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#27 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#28 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#29 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#30 td:nth-child(9)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='kamis') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#31 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#32 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#33 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#34 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#35 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#37 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#38 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#39 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#40 td:nth-child(9)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='jumat') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#41 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#42 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#43 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#44 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#45 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#47 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#48 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#49 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#50 td:nth-child(9)`).append(html);
                                draggable()
                            }
                    }                    
                }// END OF Kelas D3TI3A  
                
                // KELAS D3TI3B
                else if (data.kelas_nama=='D3TI3B') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(10)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(10)`).html(istirahat); 
                    $(`#16 td:nth-child(10)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(10)`).html(istirahat); 
                    $(`#26 td:nth-child(10)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(10)`).html(istirahat); 
                    $(`#36 td:nth-child(10)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(10)`).html(istirahat); 
                    $(`#46 td:nth-child(10)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(10)`).html(istirahat);     
                    if (data.hari=='senin') {                            
                        if (data.sesi_masuk==1) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#1 td:nth-child(10)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==2) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#2 td:nth-child(10)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==3) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#3 td:nth-child(10)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==4) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#4 td:nth-child(10)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==5) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#5 td:nth-child(10)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==7) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#7 td:nth-child(10)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==8) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#8 td:nth-child(10)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==9) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#9 td:nth-child(10)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==10) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#10 td:nth-child(10)`).append(html);
                            draggable()
                        }                            
                    }else if (data.hari=='selasa') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#11 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#12 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#13 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#14 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#15 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#17 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#18 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#19 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#20 td:nth-child(10)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='rabu') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#21 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#22 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#23 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#24 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#25 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#27 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#28 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#29 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#30 td:nth-child(10)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='kamis') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#31 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#32 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#33 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#34 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#35 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#37 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#38 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#39 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#40 td:nth-child(10)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='jumat') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#41 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#42 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#43 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#44 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#45 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#47 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#48 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#49 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#50 td:nth-child(10)`).append(html);
                                draggable()
                            }
                    }                    
                }// END OF Kelas D3TI3B   
                
                // KELAS D3TI3C
                else if (data.kelas_nama=='D3TI3C') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(11)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(11)`).html(istirahat); 
                    $(`#16 td:nth-child(11)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(11)`).html(istirahat); 
                    $(`#26 td:nth-child(11)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(11)`).html(istirahat); 
                    $(`#36 td:nth-child(11)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(11)`).html(istirahat); 
                    $(`#46 td:nth-child(11)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(11)`).html(istirahat);     
                    if (data.hari=='senin') {                            
                        if (data.sesi_masuk==1) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#1 td:nth-child(11)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==2) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#2 td:nth-child(11)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==3) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#3 td:nth-child(11)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==4) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#4 td:nth-child(11)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==5) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#5 td:nth-child(11)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==7) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#7 td:nth-child(11)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==8) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#8 td:nth-child(11)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==9) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#9 td:nth-child(11)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==10) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#10 td:nth-child(11)`).append(html);
                            draggable()
                        }                            
                    }else if (data.hari=='selasa') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#11 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#12 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#13 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#14 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#15 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#17 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#18 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#19 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#20 td:nth-child(11)`).append(html);                                
                                draggable()
                            }
                    }else if (data.hari=='rabu') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#21 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#22 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#23 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#24 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#25 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#27 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#28 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#29 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#30 td:nth-child(11)`).append(html);                                
                                draggable()
                            }
                    }else if (data.hari=='kamis') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#31 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#32 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#33 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#34 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#35 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#37 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#38 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#39 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#40 td:nth-child(11)`).append(html);                                
                                draggable()
                            }
                    }else if (data.hari=='jumat') {
                        if (data.sesi_masuk==1) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#41 td:nth-child(11)`).append(html);                                
                            draggable()
                        }if (data.sesi_masuk==2) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#42 td:nth-child(11)`).append(html);                                
                            draggable()
                        }if (data.sesi_masuk==3) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#43 td:nth-child(11)`).append(html);                                
                            draggable()
                        }if (data.sesi_masuk==4) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#44 td:nth-child(11)`).append(html);                                
                            draggable()
                        }if (data.sesi_masuk==5) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#45 td:nth-child(11)`).append(html);                                
                            draggable()
                        }if (data.sesi_masuk==7) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#47 td:nth-child(11)`).append(html);                                
                            draggable()
                        }if (data.sesi_masuk==8) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#48 td:nth-child(11)`).append(html);                                
                            draggable()
                        }if (data.sesi_masuk==9) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#49 td:nth-child(11)`).append(html);                                
                            draggable()
                        }if (data.sesi_masuk==10) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#50 td:nth-child(11)`).append(html);                                
                            draggable()
                        }
                    }                    
                }// END OF Kelas D3TI3C

                // KELAS D4RPL1A
                else if (data.kelas_nama=='D4RPL1A') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(12)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(12)`).html(istirahat); 
                    $(`#16 td:nth-child(12)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(12)`).html(istirahat); 
                    $(`#26 td:nth-child(12)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(12)`).html(istirahat); 
                    $(`#36 td:nth-child(12)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(12)`).html(istirahat); 
                    $(`#46 td:nth-child(12)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(12)`).html(istirahat);     
                    if (data.hari=='senin') {                            
                        if (data.sesi_masuk==1) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#1 td:nth-child(12)`).append(html);                                
                            draggable()
                        }if (data.sesi_masuk==2) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#2 td:nth-child(12)`).append(html);                                
                            draggable()
                        }if (data.sesi_masuk==3) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#3 td:nth-child(12)`).append(html);                                
                            draggable()
                        }if (data.sesi_masuk==4) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#4 td:nth-child(12)`).append(html);                                
                            draggable()
                        }if (data.sesi_masuk==5) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#5 td:nth-child(12)`).append(html);                                
                            draggable()
                        }if (data.sesi_masuk==7) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#7 td:nth-child(12)`).append(html);                                
                            draggable()
                        }if (data.sesi_masuk==8) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#8 td:nth-child(12)`).append(html);                                
                            draggable()
                        }if (data.sesi_masuk==9) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#9 td:nth-child(12)`).append(html);                                
                            draggable()
                        }if (data.sesi_masuk==10) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#10 td:nth-child(12)`).append(html);                                
                            draggable()
                        }                            
                    }else if (data.hari=='selasa') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#11 td:nth-child(12)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#12 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#13 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#14 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#15 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#17 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#18 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#19 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#20 td:nth-child(12)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='rabu') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#21 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#22 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#23 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#24 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#25 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#27 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#28 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#29 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#30 td:nth-child(12)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='kamis') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#31 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#32 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#33 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#34 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#35 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#37 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#38 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#39 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#40 td:nth-child(12)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='jumat') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#41 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#42 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#43 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#44 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#45 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#47 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#48 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#49 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#50 td:nth-child(12)`).append(html);
                                draggable()
                            }
                    }                    
                }// END OF Kelas D4RPL1A
                
                // KELAS D4RPL2A
                else if (data.kelas_nama=='D4RPL2A') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(13)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(13)`).html(istirahat); 
                    $(`#16 td:nth-child(13)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(13)`).html(istirahat); 
                    $(`#26 td:nth-child(13)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(13)`).html(istirahat); 
                    $(`#36 td:nth-child(13)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(13)`).html(istirahat); 
                    $(`#46 td:nth-child(13)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(13)`).html(istirahat);     
                    if (data.hari=='senin') {                            
                        if (data.sesi_masuk==1) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#1 td:nth-child(13)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==2) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#2 td:nth-child(13)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==3) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#3 td:nth-child(13)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==4) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#4 td:nth-child(13)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==5) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#5 td:nth-child(13)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==7) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#7 td:nth-child(13)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==8) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#8 td:nth-child(13)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==9) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#9 td:nth-child(13)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==10) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#10 td:nth-child(13)`).append(html);
                            draggable()
                        }                            
                    }else if (data.hari=='selasa') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#11 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#12 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#13 td:nth-child(13)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#14 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#15 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#17 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#18 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#19 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#20 td:nth-child(13)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='rabu') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#21 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#22 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#23 td:nth-child(13)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#24 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#25 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#27 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#28 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#29 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#30 td:nth-child(13)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='kamis') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#31 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#32 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#33 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#34 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#35 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#37 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#38 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#39 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#40 td:nth-child(13)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='jumat') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#41 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#42 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#43 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#44 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#45 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#47 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#48 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#49 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#50 td:nth-child(13)`).append(html);
                                draggable()
                            }
                    }                    
                }// END OF Kelas D4RPL2A

                // KELAS D4RPL2B
                else if (data.kelas_nama=='D4RPL2B') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(14)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(14)`).html(istirahat); 
                    $(`#16 td:nth-child(14)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(14)`).html(istirahat); 
                    $(`#26 td:nth-child(14)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(14)`).html(istirahat); 
                    $(`#36 td:nth-child(14)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(14)`).html(istirahat); 
                    $(`#46 td:nth-child(14)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(14)`).html(istirahat);     
                    if (data.hari=='senin') {                            
                        if (data.sesi_masuk==1) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#1 td:nth-child(14)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==2) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#2 td:nth-child(14)`).append(html);                            
                            draggable()
                        }if (data.sesi_masuk==3) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#3 td:nth-child(14)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==4) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#4 td:nth-child(14)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==5) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#5 td:nth-child(14)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==7) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#7 td:nth-child(14)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==8) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#8 td:nth-child(14)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==9) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#9 td:nth-child(14)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==10) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#10 td:nth-child(14)`).append(html);
                            draggable()
                        }                            
                    }else if (data.hari=='selasa') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#11 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#12 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#13 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#14 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#15 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#17 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#18 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#19 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#20 td:nth-child(14)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='rabu') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#21 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#22 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#23 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#24 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#25 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#27 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#28 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#29 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#30 td:nth-child(14)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='kamis') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#31 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#32 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#33 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#34 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#35 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#37 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#38 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#39 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#40 td:nth-child(14)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='jumat') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#41 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#42 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#43 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#44 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#45 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#47 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#48 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#49 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#50 td:nth-child(14)`).append(html);
                                draggable()
                            }
                    }                    
                }// END OF Kelas D4RPL2B

                 // KELAS D4RPL3A
                 else if (data.kelas_nama=='D4RPL3A') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(15)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(15)`).html(istirahat); 
                    $(`#16 td:nth-child(15)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(15)`).html(istirahat); 
                    $(`#26 td:nth-child(15)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(15)`).html(istirahat); 
                    $(`#36 td:nth-child(15)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(15)`).html(istirahat); 
                    $(`#46 td:nth-child(15)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(15)`).html(istirahat);     
                    if (data.hari=='senin') {                            
                        if (data.sesi_masuk==1) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#1 td:nth-child(15)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==2) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#2 td:nth-child(15)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==3) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#3 td:nth-child(15)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==4) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#4 td:nth-child(15)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==5) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#5 td:nth-child(15)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==7) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#7 td:nth-child(15)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==8) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#8 td:nth-child(15)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==9) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#9 td:nth-child(15)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==10) {
                            var html = `
                                <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#10 td:nth-child(15)`).append(html);
                            draggable()
                        }                            
                    }else if (data.hari=='selasa') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#11 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#12 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#13 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#14 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#15 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#17 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#18 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#19 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#20 td:nth-child(15)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='rabu') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#21 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#22 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#23 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#24 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#25 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#27 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#28 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#29 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#30 td:nth-child(15)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='kamis') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#31 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#32 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#33 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#34 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#35 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#37 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#38 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#39 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#40 td:nth-child(15)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='jumat') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#41 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#42 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#43 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#44 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#45 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#47 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#48 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#49 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#50 td:nth-child(15)`).append(html);
                                draggable()
                            }
                    }                    
                }// END OF Kelas D4RPL3A
            }// END OF FOR                        
        });
        
    }
    $('#pt').html("");
    $(document).on('click','a[href="#modaltambah"]',function(){
        var temp = $(this).data('temp'); 
        var dosen = $(this).data('dosen');
        var matkul = $(this).data('matkul');
        var kelas = $(this).data('kelas');        
        var kdmatkul = $(this).data('kdmatkul');
        var kddos = $(this).data('kddos');
        var html = `
            Informasi : <br>
            ${dosen} <br>
            ${matkul} <br>
            ${kelas} <br>
            <input type="hidden" value="${temp}" id="temp" name="id_temp_jadwal">
            <input type="hidden" value="${kdmatkul}" id="kdmatkul" name="kdmatkul"> 
            <input type="hidden" value="${kddos}" id="kddos" name="kddos">  
            <input type="hidden" value="${kelas}" id="kelas_target" name="kelas">  
            <select name="tipe" class="form-control" id="tipe">                
                <option value="praktek">Praktek</option>
                <option value="teori">Teori</option>
            </select>
        `;
        $('#pt').html(html);        
    });  
    function simpantipe() {
        var temp = $('#temp').val();        
        var tipe = $('#tipe').val();
        var kddos = $('#kddos').val();
        var kdmatkul = $("#kdmatkul").val();
        var k = $("#kelas_target").val(); 
        console.log(k);
        axios.post('/tambahjenis',{
            "_token": "{{ csrf_token() }}",
            temp:temp,
            tipe:tipe,
            kddos:kddos,
            kdmatkul:kdmatkul,
            kelas:k
        }).then((response)=>{                 
            console.log(response.data.prakteksks)       
            if (response.data.status===true) {
                Swal.fire({
                    title:'Sukses',
                    text:response.data.message,
                    icon:'success',
                }).then(()=>{
                    window.location.reload();
                });               
            }else{
                Swal.fire({
                    title:'Error',
                    text:response.data.message,
                    icon:'error',
                }).then(()=>{
                    window.location.reload();
                });            
            }            
        });
    }
</script>
</script>
@endpush
@endsection