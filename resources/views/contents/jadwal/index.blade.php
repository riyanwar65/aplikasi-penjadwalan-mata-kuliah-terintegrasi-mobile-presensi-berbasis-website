@extends('master')
@section('title','Jadwal')
@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">            
                <div class="col-md-12">                    
                @if ($message = Session::get('error'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if ($message = Session::get('alert-danger'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>                         
                            @foreach ($errors->all() as $error)
                                    {{ $error }} <br>
                            @endforeach
                    </div>
                @endif                
                <div class="col-md-12 col-sm-12">
                    <div class="card">
                        <div class="card-body">       
                            <form action="{{route('cekjadwal')}}" method="post" target="_blank">
                                @csrf                                
                                    <label for=""><strong>Tahun Ajaran</strong></label><br>
                                    <select style="width: 10%" name="tahun_ajaran" id="tahun_ajaran" class="form form-control">
                                        <option value="0" disabled selected>Pilih</option>
                                        @foreach ($tahun_ajaran as $tahun)
                                            <option value="{{$tahun->tahun_ajaran}}">{{$tahun->tahun_ajaran}}</option>
                                        @endforeach
                                    </select>                                                                      
                                    <label for=""><strong>Pilih Jenis Semester</strong></label><br>
                                    <select style="width: 10%" name="semester" id="semester" class="form form-control">
                                        <option value="0" disabled selected>Pilih</option>
                                        <option value="ganjil">Ganjil</option>
                                        <option value="genap">Genap</option>
                                    </select>                                                                
                                    <label for="">&nbsp;</label><br>
                                    <button type="submit" class="btn btn-sm btn-success">CEK</button>   
                                    <button type="button" onclick="filter()" class="btn btn-sm btn-warning">LIHAT</button>                                      
                                    @if (count($cek_bkd)>0)
                                    <a href="{{route('generate.index')}}" class="btn btn-sm btn-info"><i class="fa fa-arrow-right"></i> Go Generate</a>                                                                                         
                                    @else
                                        
                                    @endif                                     
                            </form>
                            <form action="{{route('export.jadwal')}}" method="post">
                                @csrf 
                                <input type="hidden" name="semester" id="idsemester">
                                <input type="hidden" name="tahun_ajaran" id="idtahunajaran">
                                <button type="submit" class="btn btn-sm btn-danger">Export</button>
                            </form>
                            {{-- <a href="{{route('testing_ya',1)}}" class="btn btn-sm btn-info"><i class="fa fa-arrow-right"></i> Go Generate</a> <br>                   --}}
                            {{-- <a href="{{route('cekjadwal')}}" class="btn btn-sm btn-info"><i class="fa fa-check"></i> Cek</a> --}}
                            {{-- <a href="{{route('test.cek')}}" class="btn btn-sm btn-info"><i class="fa fa-check"></i> Cek</a> --}}
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body" id="show-data">
                            <table class="table table-striped table-bordered" id="detail"> 
                                <thead id="head">  
                                    <tr id="kelas">                                                            
                                        <th>Hari</th>
                                        <th>Jam</th>  
                                    </tr>                                  
                                </thead>   
                                <tbody id="body">
                                    
                                </tbody>                
                            </table>
                        </div>
                    </div>
                </div>
            </div>           
        </div>        
    </div><!-- .animated -->        
</div><!-- .content -->
@push('script')
{{-- <script src="{{asset('assets/js/jquery-ui.min.js')}}"></script>  --}}
<script src="{{asset('assets/js/axios.min.js')}}"></script>
{{-- <script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script> --}}
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>  
{{-- <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" ></script> --}}
<script src="{{asset('assets/js/sweetalert2.js')}}"></script>    
{{-- <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script> --}}



<script>        
    var a = 0;
    var b = 0;
    var c = 0;   
    var jmlh = 1;        
    $("show-data").hide();

    var semester = $("#semester").val();
    var tahun_ajaran = $("#tahun_ajaran").val();
    $("#tahun_ajaran").on('change',function(){               
        $('#idtahunajaran').val($(this).val());
    });
    $("#semester").on('change',function(){        
        $('#idsemester').val($(this).val());
    });
    function filter() {   
        var semester = $("#semester").val();
        console.log(semester);
        var tahun_ajaran = $("#tahun_ajaran").val();
        $("show-data").show();                   
        $("#kelas").html('');   
        $("#D3TI1A").html('');   
        $("#D3TI1B").html('');
        $("#D3TI1C").html('');
        $("#D3TI2A").html('');
        $("#D3TI2B").html('');
        $("#D3TI2C").html('');
        $("#D3TI3A").html('');
        $("#D3TI3B").html('');
        $("#D3TI3C").html('');
        $("#D4RPL1A").html('');
        $("#D4RPL2A").html('');
        $("#D4RPL2B").html('');
        $("#D4RPL3A").html('');
        $("#body").html("");
        $('#show-data').show();  
        axios.get(`kelas-test`).then((res)=>{   
            $("#kelas").html(`
                <th>Hari</th>
                <th>Jam</th>  
            `);           
            for (let i = 0; i < res.data.length; i++) {
                var data = res.data[i].kelas_nama;
                console.log(data);
                var html = `                                
                    <th>${data}</th>
                `;
                $("#kelas").append(html);
            }           
        });                          
        axios.post(`datajadwal`,{            
            tahun_ajaran:tahun_ajaran,
            semester:semester
        }).then((res)=>{                  
            console.log(res.data.kelas);
            for (var i = 0; i < res.data.harisesi.length; i++) {                
                for (var j = 0; j < res.data.harisesi[i].length; j++) {                     
                    var hari = res.data.harisesi[i][j].hari;
                    var sesi = res.data.harisesi[i][j].sesi;                                                          
                    var html = `
                        <tr id="${jmlh++}">
                            <td id="${hari}">${hari}</td>
                            <td>${sesi.jam_masuk} - ${sesi.jam_keluar}</td>   
                            <td class="tempatgeser" id="D3TI1A" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D3TI1B" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D3TI1C" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D3TI2A" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D3TI2B" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D3TI2C" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                          
                            <td class="tempatgeser" id="D3TI3A" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D3TI3B" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D3TI3C" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                          
                            <td class="tempatgeser" id="D4RPL1A" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D4RPL2B" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D4RPL2C" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D4RPL3A" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                                 
                        </tr>
                    `;                    
                    $("#body").append(html);                    
                }                
            }     
            jmlh=1;
            var hari;
            for (let k = 0; k < res.data.kelas.length; k++) {
                // console.log(res.data.kelas[k].kelas_nama);
                var kelas = res.data.kelas[k].kelas_nama;
                var data = res.data.kelas[k];
                // console.log(data);
                // KELAS D3TI1A           
                console.log(data);     
                if (data.kelas_nama=='D3TI1A') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(3)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(3)`).html(istirahat); 
                    $(`#16 td:nth-child(3)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(3)`).html(istirahat); 
                    $(`#26 td:nth-child(3)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(3)`).html(istirahat); 
                    $(`#36 td:nth-child(3)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(3)`).html(istirahat); 
                    $(`#46 td:nth-child(3)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(3)`).html(istirahat); 
                        if (data.hari=='senin') {                            
                                if (data.sesi_masuk==1) {                                    
                                    var html = `                                        
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">                                        
                                         ${data.nama_matkul} (${data.ruangan_nama})
                                        </a>
                                    `;
                                    $(`#1 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#2 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#3 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#4 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#5 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#7 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#8 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#9 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#10 td:nth-child(3)`).append(html);
                                    
                                }                            
                        }else if (data.hari=='selasa') {
                            if (data.sesi_masuk==1) {                                
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#11 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#12 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#13 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#14 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#15 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#17 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#18 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#19 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#20 td:nth-child(3)`).append(html);
                                    
                                }
                        }else if (data.hari=='rabu') {
                            if (data.sesi_masuk==1) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#21 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#22 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#23 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#24 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#25 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#27 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#28 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#29 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#30 td:nth-child(3)`).append(html);
                                    
                                }
                        }else if (data.hari=='kamis') {
                            if (data.sesi_masuk==1) {                                
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#31 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#32 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#33 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#34 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#35 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#37 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#38 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#39 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#40 td:nth-child(3)`).append(html);
                                    
                                }
                        }else if (data.hari=='jumat') {
                            if (data.sesi_masuk==1) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#41 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#42 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#43 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#44 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#45 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#47 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#48 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#49 td:nth-child(3)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#50 td:nth-child(3)`).append(html);
                                    
                                }
                        }                    
                }
                // END OF KELAS D3TI1A

                // KELAS D3TI1B
                else if (data.kelas_nama=='D3TI1B') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(4)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(4)`).html(istirahat); 
                    $(`#16 td:nth-child(4)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(4)`).html(istirahat); 
                    $(`#26 td:nth-child(4)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(4)`).html(istirahat); 
                    $(`#36 td:nth-child(4)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(4)`).html(istirahat); 
                    $(`#46 td:nth-child(4)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(4)`).html(istirahat);     
                        if (data.hari=='senin') {                            
                                if (data.sesi_masuk==1) {
                                    $(`#1 td:nth-child(4)`).html("");
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#1 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#2 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#3 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#4 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#5 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#7 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#8 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#9 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#10 td:nth-child(4)`).append(html);
                                    
                                }                            
                        }else if (data.hari=='selasa') {
                            if (data.sesi_masuk==1) {                                
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#11 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#12 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#13 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#14 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#15 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#17 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#18 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#19 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#20 td:nth-child(4)`).append(html);
                                    
                                }
                        }else if (data.hari=='rabu') {
                            if (data.sesi_masuk==1) {                                
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#21 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#22 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#23 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#24 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#25 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#27 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#28 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#29 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#30 td:nth-child(4)`).append(html);
                                    
                                }
                        }else if (data.hari=='kamis') {
                            if (data.sesi_masuk==1) {                                
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#31 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#32 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#33 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#34 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#35 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#37 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#38 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#39 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#40 td:nth-child(4)`).append(html);
                                    
                                }
                        }else if (data.hari=='jumat') {
                            if (data.sesi_masuk==1) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#41 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#42 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#43 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#44 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#45 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#47 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#48 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#49 td:nth-child(4)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#50 td:nth-child(4)`).append(html);
                                    
                                }
                        }                    
                }// END OF Kelas D3TI1B

                // KELAS D3TI1C
                else if (data.kelas_nama=='D3TI1C') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(5)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(5)`).html(istirahat); 
                    $(`#16 td:nth-child(5)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(5)`).html(istirahat); 
                    $(`#26 td:nth-child(5)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(5)`).html(istirahat); 
                    $(`#36 td:nth-child(5)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(5)`).html(istirahat); 
                    $(`#46 td:nth-child(5)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(5)`).html(istirahat);     
                    if (data.hari=='senin') {                            
                                if (data.sesi_masuk==1) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#1 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#2 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#3 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#4 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#5 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#7 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#8 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#9 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#10 td:nth-child(5)`).append(html);
                                    
                                }                            
                        }else if (data.hari=='selasa') {
                            if (data.sesi_masuk==1) {                                
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#11 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#12 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#13 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#14 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#15 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#17 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#18 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#19 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#20 td:nth-child(5)`).append(html);
                                    
                                }
                        }else if (data.hari=='rabu') {
                            if (data.sesi_masuk==1) {                                
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#21 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#22 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#23 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#24 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#25 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#27 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#28 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#29 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#30 td:nth-child(5)`).append(html);
                                    
                                }
                        }else if (data.hari=='kamis') {
                            if (data.sesi_masuk==1) {                                
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#31 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#32 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#33 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#34 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#35 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#37 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#38 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#39 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#40 td:nth-child(5)`).append(html);
                                    
                                }
                        }else if (data.hari=='jumat') {
                            if (data.sesi_masuk==1) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#41 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#42 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#43 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#44 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#45 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#47 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#48 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#49 td:nth-child(5)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#50 td:nth-child(5)`).append(html);
                                    
                                }
                        }                    
                }// END OF Kelas D3TI1C

                // KELAS D3TI2A
                else if (data.kelas_nama=='D3TI2A') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(6)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(6)`).html(istirahat); 
                    $(`#16 td:nth-child(6)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(6)`).html(istirahat); 
                    $(`#26 td:nth-child(6)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(6)`).html(istirahat); 
                    $(`#36 td:nth-child(6)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(6)`).html(istirahat); 
                    $(`#46 td:nth-child(6)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(6)`).html(istirahat);     
                        if (data.hari=='senin') {                            
                                if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#1 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#2 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#3 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#4 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#5 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#7 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#8 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#9 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#10 td:nth-child(6)`).append(html);
                                    
                                }                            
                        }else if (data.hari=='selasa') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#11 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#12 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#13 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#14 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#15 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#17 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#18 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#19 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#20 td:nth-child(6)`).append(html);
                                    
                                }
                        }else if (data.hari=='rabu') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#21 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#22 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#23 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#24 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#25 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#27 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#28 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#29 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#30 td:nth-child(6)`).append(html);
                                    
                                }
                        }else if (data.hari=='kamis') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#31 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#32 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#33 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#34 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#35 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#37 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#38 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#39 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#40 td:nth-child(6)`).append(html);
                                    
                                }
                        }else if (data.hari=='jumat') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#41 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#42 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#43 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#44 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#45 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#47 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#48 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#49 td:nth-child(6)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#50 td:nth-child(6)`).append(html);
                                    
                                }
                        }                    
                }// END OF Kelas D3TI2A
                
                // KELAS D3TI2B
                else if (data.kelas_nama=='D3TI2B') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(7)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(7)`).html(istirahat); 
                    $(`#16 td:nth-child(7)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(7)`).html(istirahat); 
                    $(`#26 td:nth-child(7)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(7)`).html(istirahat); 
                    $(`#36 td:nth-child(7)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(7)`).html(istirahat); 
                    $(`#46 td:nth-child(7)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(7)`).html(istirahat);     
                        if (data.hari=='senin') {                            
                                if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#1 td:nth-child(7)`).append(html);                                    
                                    
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#2 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#3 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#4 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#5 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#7 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#8 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#9 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#10 td:nth-child(7)`).append(html);
                                    
                                }                            
                        }else if (data.hari=='selasa') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#11 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#12 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#13 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#14 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#15 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#17 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#18 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#19 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#20 td:nth-child(7)`).append(html);
                                    
                                }
                        }else if (data.hari=='rabu') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#21 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#22 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#23 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#24 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#25 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#27 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#28 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#29 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#30 td:nth-child(7)`).append(html);
                                    
                                }
                        }else if (data.hari=='kamis') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#31 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#32 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#33 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#34 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#35 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#37 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#38 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#39 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#40 td:nth-child(7)`).append(html);
                                    
                                }
                        }else if (data.hari=='jumat') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#41 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#42 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#43 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#44 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#45 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#47 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#48 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#49 td:nth-child(7)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#50 td:nth-child(7)`).append(html);
                                    
                                }
                        }                    
                }// END OF Kelas D3TI2B

                // KELAS D3TI2C
                else if (data.kelas_nama=='D3TI2C') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(8)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(8)`).html(istirahat); 
                    $(`#16 td:nth-child(8)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(8)`).html(istirahat); 
                    $(`#26 td:nth-child(8)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(8)`).html(istirahat); 
                    $(`#36 td:nth-child(8)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(8)`).html(istirahat); 
                    $(`#46 td:nth-child(8)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(8)`).html(istirahat);     
                        if (data.hari=='senin') {                            
                                if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#1 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#2 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#3 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#4 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#5 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#7 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#8 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#9 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#10 td:nth-child(8)`).append(html);
                                    
                                }                            
                        }else if (data.hari=='selasa') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#11 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#12 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#13 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#14 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#15 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#17 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#18 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#19 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#20 td:nth-child(8)`).append(html);
                                    
                                }
                        }else if (data.hari=='rabu') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#21 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#22 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#23 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#24 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#25 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#27 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#28 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#29 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#30 td:nth-child(8)`).append(html);
                                    
                                }
                        }else if (data.hari=='kamis') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#31 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#32 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#33 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#34 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#35 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#37 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#38 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#39 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#40 td:nth-child(8)`).append(html);
                                    
                                }
                        }else if (data.hari=='jumat') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#41 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#42 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#43 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#44 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#45 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#47 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#48 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#49 td:nth-child(8)`).append(html);
                                    
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.nama_matkul} (${data.ruangan_nama}) 
                                        </a>
                                    `;
                                    $(`#50 td:nth-child(8)`).append(html);
                                    
                                }
                        }                    
                }// END OF Kelas D3TI2C

                // KELAS D3TI3A
                else if (data.kelas_nama=='D3TI3A') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(9)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(9)`).html(istirahat); 
                    $(`#16 td:nth-child(9)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(9)`).html(istirahat); 
                    $(`#26 td:nth-child(9)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(9)`).html(istirahat); 
                    $(`#36 td:nth-child(9)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(9)`).html(istirahat); 
                    $(`#46 td:nth-child(9)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(9)`).html(istirahat);     
                    if (data.hari=='senin') {                            
                        if (data.sesi_masuk==1) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#1 td:nth-child(9)`).append(html);
                            
                        }if (data.sesi_masuk==2) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#2 td:nth-child(9)`).append(html);
                            
                        }if (data.sesi_masuk==3) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#3 td:nth-child(9)`).append(html);
                            
                        }if (data.sesi_masuk==4) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#4 td:nth-child(9)`).append(html);
                            
                        }if (data.sesi_masuk==5) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#5 td:nth-child(9)`).append(html);
                            
                        }if (data.sesi_masuk==7) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#7 td:nth-child(9)`).append(html);
                            
                        }if (data.sesi_masuk==8) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#8 td:nth-child(9)`).append(html);
                            
                        }if (data.sesi_masuk==9) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#9 td:nth-child(9)`).append(html);
                            
                        }if (data.sesi_masuk==10) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#10 td:nth-child(9)`).append(html);
                            
                        }                            
                    }else if (data.hari=='selasa') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#11 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#12 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#13 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#14 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#15 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#17 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#18 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#19 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#20 td:nth-child(9)`).append(html);
                                
                            }
                    }else if (data.hari=='rabu') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#21 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#22 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#23 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#24 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#25 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#27 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#28 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#29 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#30 td:nth-child(9)`).append(html);
                                
                            }
                    }else if (data.hari=='kamis') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#31 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#32 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#33 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#34 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#35 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#37 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#38 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#39 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#40 td:nth-child(9)`).append(html);
                                
                            }
                    }else if (data.hari=='jumat') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#41 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#42 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#43 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#44 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#45 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#47 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#48 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#49 td:nth-child(9)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#50 td:nth-child(9)`).append(html);
                                
                            }
                    }                    
                }// END OF Kelas D3TI3A  
                
                // KELAS D3TI3B
                else if (data.kelas_nama=='D3TI3B') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(10)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(10)`).html(istirahat); 
                    $(`#16 td:nth-child(10)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(10)`).html(istirahat); 
                    $(`#26 td:nth-child(10)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(10)`).html(istirahat); 
                    $(`#36 td:nth-child(10)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(10)`).html(istirahat); 
                    $(`#46 td:nth-child(10)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(10)`).html(istirahat);     
                    if (data.hari=='senin') {                            
                        if (data.sesi_masuk==1) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#1 td:nth-child(10)`).append(html);
                            
                        }if (data.sesi_masuk==2) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#2 td:nth-child(10)`).append(html);
                            
                        }if (data.sesi_masuk==3) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#3 td:nth-child(10)`).append(html);
                            
                        }if (data.sesi_masuk==4) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#4 td:nth-child(10)`).append(html);
                            
                        }if (data.sesi_masuk==5) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#5 td:nth-child(10)`).append(html);
                            
                        }if (data.sesi_masuk==7) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#7 td:nth-child(10)`).append(html);
                            
                        }if (data.sesi_masuk==8) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#8 td:nth-child(10)`).append(html);
                            
                        }if (data.sesi_masuk==9) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#9 td:nth-child(10)`).append(html);
                            
                        }if (data.sesi_masuk==10) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#10 td:nth-child(10)`).append(html);
                            
                        }                            
                    }else if (data.hari=='selasa') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#11 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#12 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#13 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#14 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#15 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#17 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#18 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#19 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#20 td:nth-child(10)`).append(html);
                                
                            }
                    }else if (data.hari=='rabu') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#21 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#22 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#23 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#24 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#25 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#27 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#28 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#29 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#30 td:nth-child(10)`).append(html);
                                
                            }
                    }else if (data.hari=='kamis') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#31 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#32 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#33 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#34 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#35 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#37 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#38 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#39 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#40 td:nth-child(10)`).append(html);
                                
                            }
                    }else if (data.hari=='jumat') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#41 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#42 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#43 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#44 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#45 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#47 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#48 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#49 td:nth-child(10)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#50 td:nth-child(10)`).append(html);
                                
                            }
                    }                    
                }// END OF Kelas D3TI3B   
                
                // KELAS D3TI3C
                else if (data.kelas_nama=='D3TI3C') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(11)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(11)`).html(istirahat); 
                    $(`#16 td:nth-child(11)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(11)`).html(istirahat); 
                    $(`#26 td:nth-child(11)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(11)`).html(istirahat); 
                    $(`#36 td:nth-child(11)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(11)`).html(istirahat); 
                    $(`#46 td:nth-child(11)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(11)`).html(istirahat);     
                    if (data.hari=='senin') {                            
                        if (data.sesi_masuk==1) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#1 td:nth-child(11)`).append(html);
                            
                        }if (data.sesi_masuk==2) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#2 td:nth-child(11)`).append(html);
                            
                        }if (data.sesi_masuk==3) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#3 td:nth-child(11)`).append(html);
                            
                        }if (data.sesi_masuk==4) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#4 td:nth-child(11)`).append(html);
                            
                        }if (data.sesi_masuk==5) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#5 td:nth-child(11)`).append(html);
                            
                        }if (data.sesi_masuk==7) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#7 td:nth-child(11)`).append(html);
                            
                        }if (data.sesi_masuk==8) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#8 td:nth-child(11)`).append(html);
                            
                        }if (data.sesi_masuk==9) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#9 td:nth-child(11)`).append(html);
                            
                        }if (data.sesi_masuk==10) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#10 td:nth-child(11)`).append(html);
                            
                        }                            
                    }else if (data.hari=='selasa') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#11 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#12 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#13 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#14 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#15 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#17 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#18 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#19 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#20 td:nth-child(11)`).append(html);                                
                                
                            }
                    }else if (data.hari=='rabu') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#21 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#22 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#23 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#24 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#25 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#27 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#28 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#29 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#30 td:nth-child(11)`).append(html);                                
                                
                            }
                    }else if (data.hari=='kamis') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#31 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#32 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#33 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#34 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#35 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#37 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#38 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#39 td:nth-child(11)`).append(html);                                
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#40 td:nth-child(11)`).append(html);                                
                                
                            }
                    }else if (data.hari=='jumat') {
                        if (data.sesi_masuk==1) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#41 td:nth-child(11)`).append(html);                                
                            
                        }if (data.sesi_masuk==2) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#42 td:nth-child(11)`).append(html);                                
                            
                        }if (data.sesi_masuk==3) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#43 td:nth-child(11)`).append(html);                                
                            
                        }if (data.sesi_masuk==4) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#44 td:nth-child(11)`).append(html);                                
                            
                        }if (data.sesi_masuk==5) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#45 td:nth-child(11)`).append(html);                                
                            
                        }if (data.sesi_masuk==7) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#47 td:nth-child(11)`).append(html);                                
                            
                        }if (data.sesi_masuk==8) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#48 td:nth-child(11)`).append(html);                                
                            
                        }if (data.sesi_masuk==9) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#49 td:nth-child(11)`).append(html);                                
                            
                        }if (data.sesi_masuk==10) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#50 td:nth-child(11)`).append(html);                                
                            
                        }
                    }                    
                }// END OF Kelas D3TI3C

                // KELAS D4RPL1A
                else if (data.kelas_nama=='D4RPL1A') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(12)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(12)`).html(istirahat); 
                    $(`#16 td:nth-child(12)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(12)`).html(istirahat); 
                    $(`#26 td:nth-child(12)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(12)`).html(istirahat); 
                    $(`#36 td:nth-child(12)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(12)`).html(istirahat); 
                    $(`#46 td:nth-child(12)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(12)`).html(istirahat);     
                    if (data.hari=='senin') {                            
                        if (data.sesi_masuk==1) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#1 td:nth-child(12)`).append(html);                                
                            
                        }if (data.sesi_masuk==2) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#2 td:nth-child(12)`).append(html);                                
                            
                        }if (data.sesi_masuk==3) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#3 td:nth-child(12)`).append(html);                                
                            
                        }if (data.sesi_masuk==4) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#4 td:nth-child(12)`).append(html);                                
                            
                        }if (data.sesi_masuk==5) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#5 td:nth-child(12)`).append(html);                                
                            
                        }if (data.sesi_masuk==7) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#7 td:nth-child(12)`).append(html);                                
                            
                        }if (data.sesi_masuk==8) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#8 td:nth-child(12)`).append(html);                                
                            
                        }if (data.sesi_masuk==9) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#9 td:nth-child(12)`).append(html);                                
                            
                        }if (data.sesi_masuk==10) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#10 td:nth-child(12)`).append(html);                                
                            
                        }                            
                    }else if (data.hari=='selasa') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#11 td:nth-child(12)`).append(html);                                
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#12 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#13 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#14 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#15 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#17 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#18 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#19 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#20 td:nth-child(12)`).append(html);
                                
                            }
                    }else if (data.hari=='rabu') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#21 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#22 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#23 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#24 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#25 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#27 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#28 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#29 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#30 td:nth-child(12)`).append(html);
                                
                            }
                    }else if (data.hari=='kamis') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#31 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#32 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#33 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#34 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#35 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#37 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#38 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#39 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#40 td:nth-child(12)`).append(html);
                                
                            }
                    }else if (data.hari=='jumat') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#41 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#42 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#43 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#44 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#45 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#47 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#48 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#49 td:nth-child(12)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#50 td:nth-child(12)`).append(html);
                                
                            }
                    }                    
                }// END OF Kelas D4RPL1A
                
                // KELAS D4RPL2A
                else if (data.kelas_nama=='D4RPL2A') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(13)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(13)`).html(istirahat); 
                    $(`#16 td:nth-child(13)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(13)`).html(istirahat); 
                    $(`#26 td:nth-child(13)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(13)`).html(istirahat); 
                    $(`#36 td:nth-child(13)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(13)`).html(istirahat); 
                    $(`#46 td:nth-child(13)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(13)`).html(istirahat);     
                    if (data.hari=='senin') {                            
                        if (data.sesi_masuk==1) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#1 td:nth-child(13)`).append(html);
                            
                        }if (data.sesi_masuk==2) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#2 td:nth-child(13)`).append(html);
                            
                        }if (data.sesi_masuk==3) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#3 td:nth-child(13)`).append(html);
                            
                        }if (data.sesi_masuk==4) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#4 td:nth-child(13)`).append(html);
                            
                        }if (data.sesi_masuk==5) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#5 td:nth-child(13)`).append(html);
                            
                        }if (data.sesi_masuk==7) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#7 td:nth-child(13)`).append(html);
                            
                        }if (data.sesi_masuk==8) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#8 td:nth-child(13)`).append(html);
                            
                        }if (data.sesi_masuk==9) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#9 td:nth-child(13)`).append(html);
                            
                        }if (data.sesi_masuk==10) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#10 td:nth-child(13)`).append(html);
                            
                        }                            
                    }else if (data.hari=='selasa') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#11 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#12 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#13 td:nth-child(13)`).append(html);                                
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#14 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#15 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#17 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#18 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#19 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#20 td:nth-child(13)`).append(html);
                                
                            }
                    }else if (data.hari=='rabu') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#21 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#22 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#23 td:nth-child(13)`).append(html);                                
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#24 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#25 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#27 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#28 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#29 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#30 td:nth-child(13)`).append(html);
                                
                            }
                    }else if (data.hari=='kamis') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#31 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#32 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#33 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#34 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#35 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#37 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#38 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#39 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#40 td:nth-child(13)`).append(html);
                                
                            }
                    }else if (data.hari=='jumat') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#41 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#42 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#43 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#44 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#45 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#47 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#48 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#49 td:nth-child(13)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#50 td:nth-child(13)`).append(html);
                                
                            }
                    }                    
                }// END OF Kelas D4RPL2A

                // KELAS D4RPL2B
                else if (data.kelas_nama=='D4RPL2B') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(14)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(14)`).html(istirahat); 
                    $(`#16 td:nth-child(14)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(14)`).html(istirahat); 
                    $(`#26 td:nth-child(14)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(14)`).html(istirahat); 
                    $(`#36 td:nth-child(14)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(14)`).html(istirahat); 
                    $(`#46 td:nth-child(14)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(14)`).html(istirahat);     
                    if (data.hari=='senin') {                            
                        if (data.sesi_masuk==1) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#1 td:nth-child(14)`).append(html);
                            
                        }if (data.sesi_masuk==2) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#2 td:nth-child(14)`).append(html);                            
                            
                        }if (data.sesi_masuk==3) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#3 td:nth-child(14)`).append(html);
                            
                        }if (data.sesi_masuk==4) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#4 td:nth-child(14)`).append(html);
                            
                        }if (data.sesi_masuk==5) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#5 td:nth-child(14)`).append(html);
                            
                        }if (data.sesi_masuk==7) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#7 td:nth-child(14)`).append(html);
                            
                        }if (data.sesi_masuk==8) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#8 td:nth-child(14)`).append(html);
                            
                        }if (data.sesi_masuk==9) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#9 td:nth-child(14)`).append(html);
                            
                        }if (data.sesi_masuk==10) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#10 td:nth-child(14)`).append(html);
                            
                        }                            
                    }else if (data.hari=='selasa') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#11 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#12 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#13 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#14 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#15 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#17 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#18 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#19 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#20 td:nth-child(14)`).append(html);
                                
                            }
                    }else if (data.hari=='rabu') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#21 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#22 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#23 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#24 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#25 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#27 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#28 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#29 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#30 td:nth-child(14)`).append(html);
                                
                            }
                    }else if (data.hari=='kamis') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#31 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#32 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#33 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#34 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#35 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#37 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#38 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#39 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#40 td:nth-child(14)`).append(html);
                                
                            }
                    }else if (data.hari=='jumat') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#41 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#42 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#43 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#44 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#45 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#47 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#48 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#49 td:nth-child(14)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#50 td:nth-child(14)`).append(html);
                                
                            }
                    }                    
                }// END OF Kelas D4RPL2B

                 // KELAS D4RPL3A
                 else if (data.kelas_nama=='D4RPL3A') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(15)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(15)`).html(istirahat); 
                    $(`#16 td:nth-child(15)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(15)`).html(istirahat); 
                    $(`#26 td:nth-child(15)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(15)`).html(istirahat); 
                    $(`#36 td:nth-child(15)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(15)`).html(istirahat); 
                    $(`#46 td:nth-child(15)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(15)`).html(istirahat);     
                    if (data.hari=='senin') {                            
                        if (data.sesi_masuk==1) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#1 td:nth-child(15)`).append(html);
                            
                        }if (data.sesi_masuk==2) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#2 td:nth-child(15)`).append(html);
                            
                        }if (data.sesi_masuk==3) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#3 td:nth-child(15)`).append(html);
                            
                        }if (data.sesi_masuk==4) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#4 td:nth-child(15)`).append(html);
                            
                        }if (data.sesi_masuk==5) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#5 td:nth-child(15)`).append(html);
                            
                        }if (data.sesi_masuk==7) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#7 td:nth-child(15)`).append(html);
                            
                        }if (data.sesi_masuk==8) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#8 td:nth-child(15)`).append(html);
                            
                        }if (data.sesi_masuk==9) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#9 td:nth-child(15)`).append(html);
                            
                        }if (data.sesi_masuk==10) {
                            var html = `
                                <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.nama_matkul} (${data.ruangan_nama}) 
                                </a>
                            `;
                            $(`#10 td:nth-child(15)`).append(html);
                            
                        }                            
                    }else if (data.hari=='selasa') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#11 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#12 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#13 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#14 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#15 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#17 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#18 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#19 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#20 td:nth-child(15)`).append(html);
                                
                            }
                    }else if (data.hari=='rabu') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#21 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#22 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#23 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#24 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#25 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#27 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#28 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#29 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#30 td:nth-child(15)`).append(html);
                                
                            }
                    }else if (data.hari=='kamis') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#31 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#32 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#33 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#34 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#35 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#37 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#38 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#39 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#40 td:nth-child(15)`).append(html);
                                
                            }
                    }else if (data.hari=='jumat') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#41 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#42 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#43 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#44 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#45 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#47 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#48 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#49 td:nth-child(15)`).append(html);
                                
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#modaltambah" class="btn btn-sm draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.nama_matkul} (${data.ruangan_nama}) 
                                    </a>
                                `;
                                $(`#50 td:nth-child(15)`).append(html);
                                
                            }
                    }                    
                }// END OF Kelas D4RPL3A
            }// END OF FOR                        
        });        
    }
</script>
@endpush
@endsection