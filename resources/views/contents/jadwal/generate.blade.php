@extends('master')
@section('title','Generate')
@section('content')
<div class="cssload-container" id="loadingDiv">
    <div class="cssload-l">L</div>
    <div class="cssload-circle"></div>
    <div class="cssload-square"></div>
    <div class="cssload-triangle"></div>
    <div class="cssload-d">D</div>
    <div class="cssload-i">I</div>
    <div class="cssload-n">N</div>
    <div class="cssload-g">G</div>
</div>
<div class="content mt-3">        
    <div class="animated fadeIn">        
        <div class="row">  
            <div class="col-md-12">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if ($message = Session::get('error'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>                         
                            @foreach ($errors->all() as $error)
                                  {{ $error }} <br>
                            @endforeach
                    </div>
                @endif       
            </div>
            <div class="card col-md-12">                                         
                <div class="card-body">                                                                    
                        <div class="col-md-12">
                            <h3>Pilih Semester yang mau di generate</h3>
                        </div>
                        <br><br>
                        {{-- <form action="{{route('generate.post')}}" method="post"> --}}
                            @csrf
                            <div class="col-md-4">
                                <label for=""><strong>Tahun Ajaran</strong></label><br>
                                <select name="tahun_ajaran" id="tahun_ajaran" class="form form-control">
                                    <option value="0" disabled selected>Pilih</option>
                                    @foreach ($tahun_ajaran as $tahun)
                                        <option value="{{$tahun->tahun_ajaran}}">{{$tahun->tahun_ajaran}}</option>
                                    @endforeach
                                </select>
                            </div>       
                            <div class="col-md-4">
                                <label for=""><strong>Pilih Jenis Semester</strong></label><br>
                                <select name="semester" id="semester" class="form form-control">
                                    <option value="0" disabled selected>Pilih</option>
                                    <option value="ganjil">Ganjil</option>
                                    <option value="genap">Genap</option>
                                </select>
                            </div>       
                            <div class="col-md-4">
                                <label for="">&nbsp;</label><br>
                                <button type="button" class="btn btn-sm btn-info" onclick="loading()">Generate</button>                                                            
                            </div>
                        {{-- </form> --}}
                </div>
            </div>
        </div>
    </div>
</div>
@push('script')
<script src="{{asset('assets/js/axios.min.js')}}"></script>
<script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/sweetalert2.js')}}"></script>    
{{-- <script src="{{asset('assets/js/pagination.js')}}"></script>
<script src="{{asset('assets/js/pagination.min.js')}}"></script> --}}
<script src="{{asset('assets/js/simplePagination.js')}}"></script>
<script>    
    $("#loadingDiv").hide();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    function loading(){
        var tahunajaran= $('#tahun_ajaran').val();
        var semester = $('#semester').val();        
        console.log(semester);
        console.log(tahunajaran);        
        if (tahunajaran===null||semester===null) {
            Swal.fire(
                'Error',
                `Input Tahun Ajaran dan Semester Tidak Boleh Kosong`,
                'error'
            );
        }else{
            axios.post(`/cekgenerate`,{
                _token:"{{csrf_token()}}",
                semester:semester,
                tahunajaran:tahunajaran
            }).then((res)=>{
                if (res.data.status==true) {
                    Swal.fire({
                        title: 'Peringatan',
                        text: `${res.data.message}`,
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Generate'
                    }).then((result) => {
                        if (result.value) {  
                            $.ajax({
                            url: '/generate',
                            type:'POST',
                            data:{ "_token": "{{ csrf_token() }}",'tahunajaran':tahunajaran,'semester':semester},
                            dataType: 'json',
                            beforeSend: function() { $('#loadingDiv').show(); },
                            complete: function() { $('#loadingDiv').hide(); },
                            success:function(data){                                
                                Swal.fire({
                                    title:'Sukses',
                                    text:data.message,
                                    icon:'success',
                                }).then(()=>{
                                    // window.location.href="{{route('jadwal.index')}}";
                                });
                            }
                        });                                      
                        }
                    });
                }else{
                    $.ajax({
                        url: '/generate',
                        type:'POST',
                        data:{ "_token": "{{ csrf_token() }}",'tahunajaran':tahunajaran,'semester':semester},
                        dataType: 'json',
                        beforeSend: function() { $('#loadingDiv').show(); },
                        complete: function() { $('#loadingDiv').hide(); },
                        success:function(data){
                            if (data.status===false) {
                                Swal.fire(
                                    'Error',
                                    `${data.message}`,
                                    'error'
                                );
                            }else{
                                Swal.fire({
                                    title:'Sukses',
                                    text:data.message,
                                    icon:'success',
                                }).then(()=>{
                                    console.log(data);
                                    // window.location.href="{{route('jadwal.index')}}";
                                });  
                            }
                        }
                    });
                }
            });            
        }        
    }
    
</script>
@endpush
@endsection