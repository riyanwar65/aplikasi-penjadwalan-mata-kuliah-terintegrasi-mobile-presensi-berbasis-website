@extends('master')
@section('title','Cek')
@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">            
            <div class="col-md-12">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            @if ($message = Session::get('alert-danger'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>                         
                        @foreach ($errors->all() as $error)
                                {{ $error }} <br>
                        @endforeach
                </div>
            @endif
            </div>
            {{-- <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-6">
                            <h4>Jadwal Sementara</h4>                         
                        </div>
                        <div class="col-md-6">                            
                            @foreach ($schedules as $key=>$s)
                            <input type="hidden" name="bkdid_{{$key+1}}" id="bkd_{{$key+1}}" value="{{$s->kode_bkd}}">
                            <input type="hidden" name="kodematkulid_{{$key+1}}" id="matkul_{{$key+1}}" value="{{$s->kode_bkd}}">                     
                            @endforeach
                            <button onclick="ceksemua()" class="btn btn-sm btn-success"><i class="fa fa-check"></i> Cek Semua</button>                            
                        </div>
                    </div>
                    <div class="card-body">
                        <table width="100%" class="table table-striped table-bordered">
                            <tr>
                                <td>No.</td>
                                <td>Nama Dosen</td>
                                <td>Nama Matkul</td>
                                <td>Hari</td>
                                <td>Jam Mengajar</td>
                            </tr>
                            @foreach ($schedule as $key=>$value)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$value->dosen_nama}}</td>
                                    <td>{{$value->nama_matkul}}</td>
                                    <td>{{$value->hari}}</td>
                                    <td>{{$value->jam_masuk."-".$value->jam_keluar}}</td>
                                </tr>
                            @endforeach
                        </table>
                        <br>
                        {{$schedule->links()}}
                    </div>
                </div>
            </div> --}}
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4>Hasil Generate Jadwal</h4>
                    </div>
                    <div class="card-body">
                        <table width="100%" id="bootstrap-data-table-export" class="table table-striped table-bordered responsive">
                            <thead>
                                <tr>
                                <th>Dosen</th>    
                                <th>Matkul</th>
                                <th>Kelas</th>
                                <th>hari</th>                                
                                <th>Jam</th>                                
                                </tr>                            
                            </thead>
                            <tbody>
                            @foreach ($schedules as $item)
                                <tr>
                                    <td>{{$item->dosen_nama}}</td>
                                    <td>{{$item->nama_matkul}}</td>
                                    <td>{{$item->kelas_nama}}</td>
                                    <td>{{$item->hari}}</td>
                                    <td>{{$item->jam_masuk.'-'.$item->jam_keluar}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>                        
                    </div>
                </div>
            </div>
            {{-- <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4>BKD</h4>
                    </div>
                    <div class="card-body">
                        <table width=100% id="bkd" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                <th>Dosen</th>    
                                <th>Matkul</th>
                                <th>Kelas</th>
                                <th>Jumlah SKS</th>                                                                                                
                                </tr>                            
                            </thead>
                            <tbody>
                            @foreach ($bkd as $b)
                                <tr>
                                    <td>{{$b->dosen_nama}}</td>
                                    <td>{{$b->nama_matkul}}</td>
                                    <td>{{$b->kelas_nama}}</td>
                                    <td>{{$b->total_sks}}</td>                                    
                                </tr>
                            @endforeach
                            </tbody>
                        </table>                        
                    </div>
                </div>
            </div> --}}
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4>Data Berlebih atau Kekurangan dari Jadwal Yang Tergenerate</h4>
                    </div>
                    <div class="card-body">                                                
                            {{-- Jumlah SKS yang Harus di kurangi atau di tambah --}}                            
                            <table id="sks" width="100%" class="table table-striped table-bordered responsive">
                                <thead>
                                    <tr>
                                        <th>Dosen</th>
                                        <th>Mata Kuliah</th>
                                        <th>Kelas</th>
                                        <th>Jadwal SKS Tergenerate</th>
                                        <th>Total SKS BKD</th>
                                        <th>Keterangan</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $d)                                
                                        <tr>
                                            <td>{{$d['dosen_nama']}}</td>
                                            <td>{{$d['mata_kuliah']}}</td>
                                            <td>{{$d['kelas']}}</td>
                                            <td>{{$d['jadwal_sks']}}</td>
                                            <td>{{$d['bkd_total_sks']}}</td>
                                            <td>{{$d['message']}}</td>
                                            <td>
                                                @if ($d['ditambah']==true)
                                                    <a href="#tambah" id="klik_tambah" class="btn btn-sm btn-success" data-sks="{{$d['sks']}}" data-bkd="{{$d['kode_bkd']}}" data-tahunajaran="{{$d['tahun_ajaran']}}" data-semester="{{$d['semester']}}" data-kdmatkul="{{$d['kode_matkul']}}" data-toggle="modal" data-target="#tambah" >Tambah</a>
                                                @else
                                                    <a href="#kurang" class="btn btn-sm btn-danger" data-sks="{{$d['sks']}}" data-bkd="{{$d['kode_bkd']}}" data-tahunajaran="{{$d['tahun_ajaran']}}" data-semester="{{$d['semester']}}" data-kdmatkul="{{$d['kode_matkul']}}" data-toggle="modal" data-target="#kurangi">Kurangi</a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>                            
                            {{-- End Of Jumlah SKS --}}                                                                          
                    </div>
                </div>
            </div> 
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4>Data Tidak Tergenerate</h4>
                    </div>
                    <div class="card-body">                                                
                            {{-- Jumlah SKS yang Harus di kurangi atau di tambah --}}                            
                            <table id="sks_2" width="100%" class="table table-striped table-bordered responsive">
                                <thead>
                                    <tr>
                                        <th>Dosen</th>
                                        <th>Mata Kuliah</th>
                                        <th>Kelas</th>                                        
                                        <th>Total SKS</th>                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($belum_tergenerate as $bt)                                
                                        <tr>
                                            <td>{{$bt->dosen_nama}}</td>
                                            <td>{{$bt->nama_matkul}}</td>
                                            <td>{{$bt->kelas_nama}}</td>
                                            <td>{{$bt->total_sks}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>                            
                            {{-- End Of Jumlah SKS --}}                                                                          
                    </div>
                </div>
            </div>                           
        </div>
    </div>
</div>

<!-- Modal Di tambah -->
<div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah SKS</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="tambahsks">                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="tambahsks()">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Di Kurangi -->
<div class="modal fade" id="kurangi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

@push('script')
<script src="{{asset('assets/js/axios.min.js')}}"></script>
<script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/sweetalert2.js')}}"></script>    
<script src="{{asset('assets/js/simplePagination.js')}}"></script>
<script>
    $(document).ready(function() {
        $('#bkd').DataTable();
        $('#sks').DataTable();
        $('#sks_2').DataTable();
    }); 

    var data_tambah;

    $(document).on('click','a[href="#tambah"]',function(){         
        var sks = $(this).data('sks');
        var bkd = $(this).data('bkd');
        var tahunajaran = $(this).data('tahunajaran');
        var semester = $(this).data('semester');
        var kdmatkul = $(this).data('kdmatkul');
        var hitung = 0;
        data_tambah = {
            sks:sks,bkd:bkd,tahunajaran:tahunajaran,semester:semester,kdmatkul:kdmatkul
        };
        if (sks>1) {
            var html = `
                <p id="cekavailable">

                </p>
                    <div class="col-md-8">
                        <select name="hari" id="hari" class="form-control">
                            <option value=null selected disable>Pilih</option>
                            <option value="senin">Senin</option>
                            <option value="selasa">Selasa</option>
                            <option value="rabu">Rabu</option>
                            <option value="kamis">Kamis</option>
                            <option value="jumat">Jumat</option>
                        </select>
                    </div>            
                    <div class="col-md-4">
                        <button type="button" class="btn btn-sm btn-success" onclick="cekhari()">Cek Hari</button>
                        <a href="#addmore" class="btn btn-sm btn-danger"><i class="fa fa-plus"></i></a>
                    </div>
                    <br><br>   
                    <div class="col-md-12" id="pilihruangan">
                        <select name="ruangan" id="ruangan" class="form-control">                                       
                        </select>
                    </div>            
            `;
            $('#tambahsks').html(html);        
            $('#pilihruangan').hide();        
        }else{
            var html = `
                <p id="cekavailable">

                </p>
                    <div class="col-md-8">
                        <select name="hari" id="hari" class="form-control">
                            <option value=null selected disable>Pilih</option>
                            <option value="senin">Senin</option>
                            <option value="selasa">Selasa</option>
                            <option value="rabu">Rabu</option>
                            <option value="kamis">Kamis</option>
                            <option value="jumat">Jumat</option>
                        </select>
                    </div>            
                    <div class="col-md-4">
                        <button type="button" class="btn btn-sm btn-success" onclick="cekhari()">Cek Hari</button>                    
                    </div>
                    <br><br>   
                    <div class="col-md-12" id="pilihruangan">
                        <select name="ruangan" id="ruangan" class="form-control">                                       
                        </select>
                    </div>            
            `;
            $('#tambahsks').html(html);        
            $('#pilihruangan').hide();        
        }   
        $(document).on('click','a[href="#addmore"]',function(){         
            if (hitung>sks) {
                return false;
            }
        })

    });    
    function tambahsks() {
        console.log(data_tambah);
    }        
    function cekhari() {
        let hari = $("#hari").val();
        if ($("#hari").val()=="null") {         
            $('#pilihruangan').hide(); 
        }else{
            axios.get(`/cekhari/${hari}`).then((res)=>{
                if (res.data.status===false) {
                    var html = `
                    <div class="alert alert-warning alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>${res.data.message}</strong> <br>                                      
                    </div>
                    `;
                    $("#cekavailable").html(html);    
                    $('#pilihruangan').hide(); 
                }else{      
                    $('#ruangan').html(" ");                                                                                                                                       
                    var html = `
                    <div class="alert alert-warning alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>Jam Available : </strong> <br>              
                        <div id="jamavailable">  
                            
                        </div>
                        <br>
                        <strong>Ruangan Available : </strong> <br>
                        <div id="ruanganavailable">  
                            
                        </div>
                    </div>
                    `;                    
                    $("#cekavailable").html(html);
                    Object.keys(res.data.sesi).map(function(key, index) {                                            
                        var jam = `${res.data.sesi[key].jam_masuk+"-"+res.data.sesi[key].jam_keluar} <br>`;
                        $("#jamavailable").append(jam);
                    });
                    Object.keys(res.data.ruangan).map(function(key, index) {                                                                    
                        var ruangan = `${res.data.ruangan[key].ruangan_nama} <br>`;
                        $("#ruanganavailable").append(ruangan);                        
                        var pilih_ruangan = `
                            <option value="${res.data.ruangan[key].ruangan_id}">${res.data.ruangan[key].ruangan_nama}</option>
                        `;
                        $('#ruangan').append(pilih_ruangan);                                                                                                                         
                    });                               
                    // $('#ruangan').detach();
                }                                                      
            });
        }
        $('#pilihruangan').show();        
    }

</script>
<script>    
</script>
@endpush
@endsection