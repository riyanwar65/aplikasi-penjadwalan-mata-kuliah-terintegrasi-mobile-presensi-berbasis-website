<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    @if(!empty($data_kromosom))
        <select class="form-control select2" id="myAction">
        @foreach ($data_kromosom as $key => $kromosom)
            <option value="{{ $key+1 }}"
            @if ($id == ($key+1))
                selected="selected">
            @else
                >
            @endif
                    @if ($kromosom['value_schedules'] == 1)
                        Jadwal Terbaik
                    @else
                        Jadwal {{ $key+1 }}
                    @endif
            </li>
            </option>
        @endforeach
        </select>
    @endif
    </div>
    <div class="col-md-12">
    {{--
    @if(!empty($data_kromosom))
    <ul class="nav nav-tabs nav-justified">
        @foreach ($data_kromosom as $key => $kromosom)
            @if ($id == ($key+1))
                <li role="presentation" class="active">
            @else
                <li role="presentation">
            @endif
                <a href="{{ URL::Route('admin.generates.result', $key+1) }}">
                    @if ($kromosom['value_schedules'] == 1)
                        Jadwal Terbaik
                    @else
                        Jadwal {{ $key+1 }}
                    @endif
                </a>
            </li>
        @endforeach
    </ul>
    @endif
    --}}
    <br>
    <div class="alert alert-success alert-dismissable">
        <button aria-hidden="true" class="close" data-dismiss="alert" type="button">
            ×
        </button>
        <h4>
            Nilai Fitness : 1 / 1 + ( 0 {{ $value_schedule->value_process }} ) = {{ $value_schedule->value }}
            <br>
        </h4>
        <h4>
            Crossover : {{ $crossover->value }}
            <br>
        </h4>
        <h4>
            Mutasi : {{ $mutasi->value }}
            <br>
        </h4>
    </div>
    <div class="panel-body table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
                <tr class="info">
                    <th style="text-align:center;">
                        No.
                    </th>
                    <th style="text-align:center;">
                        Hari
                    </th>
                    <th style="text-align:center;">
                        Jam
                    </th>
                    <th style="text-align:center;">
                        Nama Ruangan
                    </th>
                    {{-- <th style="text-align:center;">
                        Kapasitas Ruangan
                    </th> --}}
                    <th style="text-align:center;">
                        Mata Kuliah
                    </th>
                    <th style="text-align:center;">
                        Dosen Pengampu
                    </th>
                    <th style="text-align:center;">
                        Semester
                    </th>
                    <th style="text-align:center;">
                        SKS
                    </th>
                    <th style="text-align:center;">
                        Kelas
                    </th>
                </tr>
            </thead>
            <tbody>
            @foreach($schedules as $key => $schedule)
                <tr>
                    <td align="center">
                        {{ $key + 1 }}
                    </td>
                    <td >
                        {{
                            isset($schedule->hari) ? $schedule->hari : ''
                        }}
                    </td>
                    <td >
                        {{
                            isset($schedule->jam_awal) ? $schedule->jam_awal."-".$schedule->jam_akhir : ''
                        }}
                    </td>
                    <td >
                        {{
                            isset($schedule->ruangan_nama) ? $schedule->ruangan_nama : ''
                        }}
                    </td>
                    {{-- <td >
                        {{
                            isset($schedule->room->capacity) ? $schedule->room->capacity : ''
                        }}
                    </td> --}}
                    <td >
                        {{
                            isset($schedule->nama_matkul) ? $schedule->nama_matkul : ''
                        }}
                    </td>
                    <td >
                        {{
                            isset($schedule->dosen_nama) ? $schedule->dosen_nama : ''
                        }}
                    </td>
                    <td >
                        {{
                            isset($schedule->semester) ? $schedule->semester : ''
                        }}
                    </td>
                    <td >
                        {{
                            isset($schedule->total_sks) ? $schedule->total_sks : ''
                        }}
                    </td>
                    <td >
                        {{
                            isset($schedule->kelas_nama) ? $schedule->kelas_nama : ''
                        }}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
</body>
</html>