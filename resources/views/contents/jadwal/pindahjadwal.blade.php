@extends('master')
@section('title','Jadwal')
@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">                        
        <div class="row"> 
            <div class="col-md-12">                
                <div class="card">
                    <div class="card-body">
                        <table width="30%">
                            @foreach ($dosen as $dc)                                
                            <tr style="height:50px;">                        
                                <td style="padding-left:10px;color:{{$dc->color_font}}; background-color:{{$dc->color_utama}}; font-family:'Calibri';">{{$dc->dosen_nama}}</td>                                    
                            </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div> 
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body" id="show-data">
                        @php
                            $cek = DB::connection("presensi")->table('jadwal')
                                  ->join('temp_jadwal','temp_jadwal.kelas','jadwal.kelas')
                                  ->where('temp_jadwal.jurusan',Session::get('jurusan'))
                                  ->where('jadwal.status_perkuliahan','Aktif')
                                  ->get();
                        @endphp     

                        @if (count($cek)>0)                            
                            <table class="table table-striped table-bordered" id="detail"> 
                                <thead id="head">
                                    <tr id="kelas">                                    
                                    </tr>
                                </thead>   
                                <tbody id="body">
                                    
                                </tbody>                
                            </table>
                        @else
                        @endif
                    </div>
                </div>
            </div>         
        </div>
    </div>
</div>

<div class="modal fade" id="tambahjenis" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    {{-- <form action="{{route('tambahjenis')}}" method="post">
    @csrf --}}
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Pilih Ruangan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <input type="hidden" name="kddosen" id="dosen_kode">
            <input type="hidden" name="kode_mk" id="kode_matkul">
            <input type="hidden" name="kelaskode" id="kelas_kode">
            <select name="hari" class="form-control" id="haripindah">
                
            </select> <br>
            <select name="ruangan" class="form-control" id="ruanganpindah">

            </select>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick="updateruangan()">Update</button>
        </div>
      </div>
    {{-- </form> --}}
    </div>
</div>
@push('script')
{{-- <script src="{{asset('assets/js/jquery-ui.min.js')}}"></script>  --}}
<script src="{{asset('assets/js/axios.min.js')}}"></script>
{{-- <script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script> --}}
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>  
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" ></script>
{{-- <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script> --}}
<script>        
    var a = 0;
    var b = 0;
    var c = 0;   
    var jmlh = 1;    
    var jm = 1;         
    function istirahat() {
        var istirahat=`
            <strong>Istirahat</strong>
        `;
        $(`#6 td:nth-child(3)`).removeClass("tempatgeser droppable ui-droppable");                        
        $(`#6 td:nth-child(3)`).html(istirahat); 
        $(`#16 td:nth-child(3)`).removeClass("tempatgeser droppable ui-droppable");                        
        $(`#16 td:nth-child(3)`).html(istirahat); 
        $(`#26 td:nth-child(3)`).removeClass("tempatgeser droppable ui-droppable");                        
        $(`#26 td:nth-child(3)`).html(istirahat); 
        $(`#36 td:nth-child(3)`).removeClass("tempatgeser droppable ui-droppable");                        
        $(`#36 td:nth-child(3)`).html(istirahat); 
        $(`#46 td:nth-child(3)`).removeClass("tempatgeser droppable ui-droppable");                        
        $(`#46 td:nth-child(3)`).html(istirahat); 
    }
    
    function draggable(){        
        $(".draggable").draggable({ cursor: "crosshair", revert: "invalid"});        
        $(".tempatgeser").droppable({ accept: ".draggable", drop: function(event, ui) {             
            var coba = [];
            // console.log($(event.target).attr('data-sesi'));
            var id_temp_drag = $(ui.draggable).attr("data-temp");            
            var kddosen = $(ui.draggable).attr("data-kddos");                              
            var ruangan_drag = $(ui.draggable).attr("data-ruangan");
            var kd_matkul_drag = $(ui.draggable).attr("data-kdmatkul");            
            var kelas_drag = $(ui.draggable).attr("data-kelas");
            var hari_drag = $(ui.draggable).attr("data-hari");
            var kelas_drop = $(event.target).attr('id');
            var hari_drop = $(event.target).attr('data-hari');
            var sesi_drop = $(event.target).attr('data-sesi');
            var sesi_drag = $(ui.draggable).attr('data-sesitarget');
            var ruangan = $(ui.draggable).attr("data-ruangan");                 
            console.log(kddosen+" "+hari_drag+ +kd_matkul_drag);            
            axios.post('updatepindah',{
                temp_id:id_temp_drag,
                kd_dosen:kddosen,
                kd_matkul:kd_matkul_drag,
                ruangan:ruangan_drag,
                kelas_bawaan:kelas_drag,
                hari_drag:hari_drag,
                kelas_target:kelas_drop,
                hari_target:hari_drop,
                sesi_target:sesi_drop,
            }).then((response)=>{     
                    if (response.data.status===false) { 
                        console.log(response.data.data);                       
                        Swal.fire({
                            title:'Peringatan',
                            text:response.data.message,
                            icon:'error',
                        });                        
                    }else{          
                        console.log(response.data.data);                       
                        var dropped = ui.draggable;
                        var droppedOn = $(this);
                        $(dropped).detach().css({top: 0,left: 0}).appendTo(droppedOn);                                        
                    }                               
            }); 
                           
            // var dropped = ui.draggable;
            // var droppedOn = $(this);
            // $(dropped).detach().css({top: 0,left: 0}).appendTo(droppedOn);                 
        }});        
    }     
        
    
    $(document).ready(function(){
        $("#kelas").html('');         
        $("#D3TI1A").html('');   
        $("#D3TI1B").html('');
        $("#D3TI1C").html('');
        $("#D3TI2A").html('');
        $("#D3TI2B").html('');
        $("#D3TI2C").html('');
        $("#D3TI3A").html('');
        $("#D3TI3B").html('');
        $("#D3TI3C").html('');
        $("#D4RPL1A").html('');
        $("#D4RPL2A").html('');
        $("#D4RPL2B").html('');
        $("#D4RPL3A").html('');
        $("#body").html("");        
        axios.get(`kelas-test`).then((res)=>{   
            $("#kelas").html(`
                <th>Hari</th>
                <th>Jam</th>  
            `);           
            for (let i = 0; i < res.data.length; i++) {
                var data = res.data[i].kelas_nama;                
                var html = `                                
                    <th>${data}</th>
                `;
                $("#kelas").append(html);
            }           
        });                          
        axios.post(`pindahjadwalpost`).then((res)=>{                              
            for (var i = 0; i < res.data.harisesi.length; i++) {                
                for (var j = 0; j < res.data.harisesi[i].length; j++) {                     
                    var hari = res.data.harisesi[i][j].hari;
                    var sesi = res.data.harisesi[i][j].sesi;                                                          
                    var html = `
                        <tr id="${jmlh++}">
                            <td id="${hari}">${hari}</td>
                            <td>${sesi.jam_masuk} - ${sesi.jam_keluar}</td>   
                            <td class="tempatgeser" id="D3TI1A" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D3TI1B" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D3TI1C" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D3TI2A" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D3TI2B" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D3TI2C" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                          
                            <td class="tempatgeser" id="D3TI3A" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D3TI3B" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D3TI3C" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                          
                            <td class="tempatgeser" id="D4RPL1A" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D4RPL2A" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D4RPL2B" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                         
                            <td class="tempatgeser" id="D4RPL3A" data-hari="${hari}" data-sesi="${sesi.id_sesi}"></td>                                 
                        </tr>
                    `;                    
                    $("#body").append(html);                    
                }                
            }     
            jmlh=1;
            var hari;
            for (let k = 0; k < res.data.kelas.length; k++) {                
                var kelas = res.data.kelas[k].kelas_nama;
                var data = res.data.kelas[k];                
                // KELAS D3TI1A                
                if (data.kelas_nama=='D3TI1A') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(3)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(3)`).html(istirahat); 
                    $(`#16 td:nth-child(3)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(3)`).html(istirahat); 
                    $(`#26 td:nth-child(3)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(3)`).html(istirahat); 
                    $(`#36 td:nth-child(3)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(3)`).html(istirahat); 
                    $(`#46 td:nth-child(3)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(3)`).html(istirahat); 
                        if (data.hari=='senin') {                            
                                if (data.sesi_masuk==1) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">                                                                                                                    
                                            ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#1 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#2 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#3 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#4 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#5 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#7 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#8 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#9 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#10 td:nth-child(3)`).append(html);
                                    draggable()
                                }                            
                        }else if (data.hari=='selasa') {
                            if (data.sesi_masuk==1) {                                
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#11 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#12 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#13 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#14 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#15 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#17 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#18 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#19 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#20 td:nth-child(3)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='rabu') {
                            if (data.sesi_masuk==1) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#21 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#22 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#23 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#24 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#25 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#27 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#28 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#29 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#30 td:nth-child(3)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='kamis') {
                            if (data.sesi_masuk==1) {                                
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#31 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#32 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#33 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#34 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#35 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#37 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#38 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#39 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#40 td:nth-child(3)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='jumat') {
                            if (data.sesi_masuk==1) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#41 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#42 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#43 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#44 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#45 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#47 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#48 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#49 td:nth-child(3)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#50 td:nth-child(3)`).append(html);
                                    draggable()
                                }
                        }                    
                }
                // END OF KELAS D3TI1A

                // KELAS D3TI1B
                else if (data.kelas_nama=='D3TI1B') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(4)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(4)`).html(istirahat); 
                    $(`#16 td:nth-child(4)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(4)`).html(istirahat); 
                    $(`#26 td:nth-child(4)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(4)`).html(istirahat); 
                    $(`#36 td:nth-child(4)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(4)`).html(istirahat); 
                    $(`#46 td:nth-child(4)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(4)`).html(istirahat);     
                        if (data.hari=='senin') {                            
                                if (data.sesi_masuk==1) {
                                    $(`#1 td:nth-child(4)`).html("");
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#1 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#2 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#3 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#4 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#5 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#7 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#8 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#9 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#10 td:nth-child(4)`).append(html);
                                    draggable()
                                }                            
                        }else if (data.hari=='selasa') {
                            if (data.sesi_masuk==1) {                                
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#11 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#12 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#13 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#14 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#15 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#17 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#18 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#19 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#20 td:nth-child(4)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='rabu') {
                            if (data.sesi_masuk==1) {                                
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#21 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#22 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#23 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#24 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#25 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#27 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#28 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#29 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#30 td:nth-child(4)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='kamis') {
                            if (data.sesi_masuk==1) {                                
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#31 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#32 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#33 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#34 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#35 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#37 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#38 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#39 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#40 td:nth-child(4)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='jumat') {
                            if (data.sesi_masuk==1) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#41 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#42 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#43 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#44 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#45 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#47 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#48 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#49 td:nth-child(4)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#50 td:nth-child(4)`).append(html);
                                    draggable()
                                }
                        }                    
                }// END OF Kelas D3TI1B

                // KELAS D3TI1C
                else if (data.kelas_nama=='D3TI1C') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(5)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(5)`).html(istirahat); 
                    $(`#16 td:nth-child(5)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(5)`).html(istirahat); 
                    $(`#26 td:nth-child(5)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(5)`).html(istirahat); 
                    $(`#36 td:nth-child(5)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(5)`).html(istirahat); 
                    $(`#46 td:nth-child(5)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(5)`).html(istirahat);     
                    if (data.hari=='senin') {                            
                                if (data.sesi_masuk==1) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#1 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#2 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#3 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#4 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#5 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#7 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#8 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#9 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#10 td:nth-child(5)`).append(html);
                                    draggable()
                                }                            
                        }else if (data.hari=='selasa') {
                            if (data.sesi_masuk==1) {                                
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#11 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#12 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#13 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#14 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#15 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#17 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#18 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#19 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#20 td:nth-child(5)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='rabu') {
                            if (data.sesi_masuk==1) {                                
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#21 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#22 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#23 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#24 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#25 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#27 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#28 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#29 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#30 td:nth-child(5)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='kamis') {
                            if (data.sesi_masuk==1) {                                
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#31 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#32 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#33 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#34 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#35 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#37 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#38 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#39 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#40 td:nth-child(5)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='jumat') {
                            if (data.sesi_masuk==1) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#41 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#42 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#43 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#44 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#45 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#47 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#48 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#49 td:nth-child(5)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {                                    
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#50 td:nth-child(5)`).append(html);
                                    draggable()
                                }
                        }                    
                }// END OF Kelas D3TI1C

                // KELAS D3TI2A
                else if (data.kelas_nama=='D3TI2A') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(6)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(6)`).html(istirahat); 
                    $(`#16 td:nth-child(6)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(6)`).html(istirahat); 
                    $(`#26 td:nth-child(6)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(6)`).html(istirahat); 
                    $(`#36 td:nth-child(6)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(6)`).html(istirahat); 
                    $(`#46 td:nth-child(6)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(6)`).html(istirahat);     
                        if (data.hari=='senin') {                            
                                if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#1 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#2 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#3 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#4 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#5 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#7 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#8 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#9 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#10 td:nth-child(6)`).append(html);
                                    draggable()
                                }                            
                        }else if (data.hari=='selasa') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#11 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#12 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#13 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#14 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#15 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#17 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#18 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#19 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#20 td:nth-child(6)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='rabu') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#21 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#22 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#23 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#24 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#25 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#27 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#28 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#29 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#30 td:nth-child(6)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='kamis') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#31 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#32 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#33 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#34 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#35 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#37 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#38 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#39 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#40 td:nth-child(6)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='jumat') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#41 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#42 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#43 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#44 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#45 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#47 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#48 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#49 td:nth-child(6)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#50 td:nth-child(6)`).append(html);
                                    draggable()
                                }
                        }                    
                }// END OF Kelas D3TI2A
                
                // KELAS D3TI2B
                else if (data.kelas_nama=='D3TI2B') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(7)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(7)`).html(istirahat); 
                    $(`#16 td:nth-child(7)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(7)`).html(istirahat); 
                    $(`#26 td:nth-child(7)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(7)`).html(istirahat); 
                    $(`#36 td:nth-child(7)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(7)`).html(istirahat); 
                    $(`#46 td:nth-child(7)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(7)`).html(istirahat);     
                        if (data.hari=='senin') {                            
                                if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#1 td:nth-child(7)`).append(html);                                    
                                    draggable()
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#2 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#3 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#4 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#5 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#7 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#8 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#9 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#10 td:nth-child(7)`).append(html);
                                    draggable()
                                }                            
                        }else if (data.hari=='selasa') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#11 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#12 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#13 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#14 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#15 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#17 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#18 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#19 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#20 td:nth-child(7)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='rabu') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#21 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#22 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#23 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#24 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#25 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#27 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#28 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#29 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#30 td:nth-child(7)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='kamis') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#31 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#32 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#33 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#34 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#35 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#37 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#38 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#39 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#40 td:nth-child(7)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='jumat') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#41 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#42 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#43 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#44 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#45 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#47 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#48 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#49 td:nth-child(7)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#50 td:nth-child(7)`).append(html);
                                    draggable()
                                }
                        }                    
                }// END OF Kelas D3TI2B

                // KELAS D3TI2C
                else if (data.kelas_nama=='D3TI2C') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(8)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(8)`).html(istirahat); 
                    $(`#16 td:nth-child(8)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(8)`).html(istirahat); 
                    $(`#26 td:nth-child(8)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(8)`).html(istirahat); 
                    $(`#36 td:nth-child(8)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(8)`).html(istirahat); 
                    $(`#46 td:nth-child(8)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(8)`).html(istirahat);     
                        if (data.hari=='senin') {                            
                                if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#1 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#2 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#3 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#4 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#5 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#7 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#8 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#9 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#10 td:nth-child(8)`).append(html);
                                    draggable()
                                }                            
                        }else if (data.hari=='selasa') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#11 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#12 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#13 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#14 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#15 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#17 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#18 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#19 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#20 td:nth-child(8)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='rabu') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#21 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#22 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#23 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#24 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#25 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#27 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#28 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#29 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#30 td:nth-child(8)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='kamis') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#31 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#32 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#33 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#34 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#35 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#37 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#38 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#39 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#40 td:nth-child(8)`).append(html);
                                    draggable()
                                }
                        }else if (data.hari=='jumat') {
                            if (data.sesi_masuk==1) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#41 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==2) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#42 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==3) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#43 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==4) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#44 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==5) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#45 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==7) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#47 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==8) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#48 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==9) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#49 td:nth-child(8)`).append(html);
                                    draggable()
                                }if (data.sesi_masuk==10) {
                                    var html = `
                                        <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                         ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                        </a>
                                    `;
                                    $(`#50 td:nth-child(8)`).append(html);
                                    draggable()
                                }
                        }                    
                }// END OF Kelas D3TI2C

                // KELAS D3TI3A
                else if (data.kelas_nama=='D3TI3A') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(9)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(9)`).html(istirahat); 
                    $(`#16 td:nth-child(9)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(9)`).html(istirahat); 
                    $(`#26 td:nth-child(9)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(9)`).html(istirahat); 
                    $(`#36 td:nth-child(9)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(9)`).html(istirahat); 
                    $(`#46 td:nth-child(9)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(9)`).html(istirahat);     
                    if (data.hari=='senin') {                            
                        if (data.sesi_masuk==1) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#1 td:nth-child(9)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==2) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#2 td:nth-child(9)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==3) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#3 td:nth-child(9)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==4) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#4 td:nth-child(9)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==5) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#5 td:nth-child(9)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==7) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#7 td:nth-child(9)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==8) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#8 td:nth-child(9)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==9) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#9 td:nth-child(9)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==10) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#10 td:nth-child(9)`).append(html);
                            draggable()
                        }                            
                    }else if (data.hari=='selasa') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#11 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#12 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#13 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#14 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#15 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#17 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#18 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#19 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#20 td:nth-child(9)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='rabu') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#21 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#22 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#23 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#24 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#25 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#27 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#28 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#29 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#30 td:nth-child(9)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='kamis') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#31 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#32 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#33 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#34 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#35 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#37 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#38 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#39 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#40 td:nth-child(9)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='jumat') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#41 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#42 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#43 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#44 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#45 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#47 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#48 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#49 td:nth-child(9)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#50 td:nth-child(9)`).append(html);
                                draggable()
                            }
                    }                    
                }// END OF Kelas D3TI3A  
                
                // KELAS D3TI3B
                else if (data.kelas_nama=='D3TI3B') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(10)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(10)`).html(istirahat); 
                    $(`#16 td:nth-child(10)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(10)`).html(istirahat); 
                    $(`#26 td:nth-child(10)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(10)`).html(istirahat); 
                    $(`#36 td:nth-child(10)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(10)`).html(istirahat); 
                    $(`#46 td:nth-child(10)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(10)`).html(istirahat);     
                    if (data.hari=='senin') {                            
                        if (data.sesi_masuk==1) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#1 td:nth-child(10)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==2) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#2 td:nth-child(10)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==3) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#3 td:nth-child(10)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==4) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#4 td:nth-child(10)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==5) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#5 td:nth-child(10)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==7) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#7 td:nth-child(10)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==8) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#8 td:nth-child(10)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==9) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#9 td:nth-child(10)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==10) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#10 td:nth-child(10)`).append(html);
                            draggable()
                        }                            
                    }else if (data.hari=='selasa') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#11 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#12 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#13 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#14 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#15 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#17 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#18 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#19 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#20 td:nth-child(10)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='rabu') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#21 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#22 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#23 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#24 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#25 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#27 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#28 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#29 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#30 td:nth-child(10)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='kamis') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#31 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#32 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#33 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#34 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#35 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#37 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#38 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#39 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#40 td:nth-child(10)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='jumat') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#41 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#42 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#43 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#44 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#45 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#47 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#48 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#49 td:nth-child(10)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#50 td:nth-child(10)`).append(html);
                                draggable()
                            }
                    }                    
                }// END OF Kelas D3TI3B   
                
                // KELAS D3TI3C
                else if (data.kelas_nama=='D3TI3C') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(11)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(11)`).html(istirahat); 
                    $(`#16 td:nth-child(11)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(11)`).html(istirahat); 
                    $(`#26 td:nth-child(11)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(11)`).html(istirahat); 
                    $(`#36 td:nth-child(11)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(11)`).html(istirahat); 
                    $(`#46 td:nth-child(11)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(11)`).html(istirahat);     
                    if (data.hari=='senin') {                            
                        if (data.sesi_masuk==1) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#1 td:nth-child(11)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==2) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#2 td:nth-child(11)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==3) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#3 td:nth-child(11)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==4) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#4 td:nth-child(11)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==5) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#5 td:nth-child(11)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==7) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#7 td:nth-child(11)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==8) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#8 td:nth-child(11)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==9) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#9 td:nth-child(11)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==10) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#10 td:nth-child(11)`).append(html);
                            draggable()
                        }                            
                    }else if (data.hari=='selasa') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#11 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#12 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#13 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#14 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#15 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#17 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#18 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#19 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#20 td:nth-child(11)`).append(html);                                
                                draggable()
                            }
                    }else if (data.hari=='rabu') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#21 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#22 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#23 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#24 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#25 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#27 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#28 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#29 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#30 td:nth-child(11)`).append(html);                                
                                draggable()
                            }
                    }else if (data.hari=='kamis') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#31 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#32 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#33 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#34 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#35 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#37 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#38 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#39 td:nth-child(11)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#40 td:nth-child(11)`).append(html);                                
                                draggable()
                            }
                    }else if (data.hari=='jumat') {
                        if (data.sesi_masuk==1) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#41 td:nth-child(11)`).append(html);                                
                            draggable()
                        }if (data.sesi_masuk==2) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#42 td:nth-child(11)`).append(html);                                
                            draggable()
                        }if (data.sesi_masuk==3) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#43 td:nth-child(11)`).append(html);                                
                            draggable()
                        }if (data.sesi_masuk==4) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#44 td:nth-child(11)`).append(html);                                
                            draggable()
                        }if (data.sesi_masuk==5) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#45 td:nth-child(11)`).append(html);                                
                            draggable()
                        }if (data.sesi_masuk==7) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#47 td:nth-child(11)`).append(html);                                
                            draggable()
                        }if (data.sesi_masuk==8) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#48 td:nth-child(11)`).append(html);                                
                            draggable()
                        }if (data.sesi_masuk==9) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#49 td:nth-child(11)`).append(html);                                
                            draggable()
                        }if (data.sesi_masuk==10) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#50 td:nth-child(11)`).append(html);                                
                            draggable()
                        }
                    }                    
                }// END OF Kelas D3TI3C

                // KELAS D4RPL1A
                else if (data.kelas_nama=='D4RPL1A') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(12)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(12)`).html(istirahat); 
                    $(`#16 td:nth-child(12)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(12)`).html(istirahat); 
                    $(`#26 td:nth-child(12)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(12)`).html(istirahat); 
                    $(`#36 td:nth-child(12)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(12)`).html(istirahat); 
                    $(`#46 td:nth-child(12)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(12)`).html(istirahat);     
                    if (data.hari=='senin') {                            
                        if (data.sesi_masuk==1) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#1 td:nth-child(12)`).append(html);                                
                            draggable()
                        }if (data.sesi_masuk==2) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#2 td:nth-child(12)`).append(html);                                
                            draggable()
                        }if (data.sesi_masuk==3) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#3 td:nth-child(12)`).append(html);                                
                            draggable()
                        }if (data.sesi_masuk==4) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#4 td:nth-child(12)`).append(html);                                
                            draggable()
                        }if (data.sesi_masuk==5) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#5 td:nth-child(12)`).append(html);                                
                            draggable()
                        }if (data.sesi_masuk==7) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#7 td:nth-child(12)`).append(html);                                
                            draggable()
                        }if (data.sesi_masuk==8) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#8 td:nth-child(12)`).append(html);                                
                            draggable()
                        }if (data.sesi_masuk==9) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#9 td:nth-child(12)`).append(html);                                
                            draggable()
                        }if (data.sesi_masuk==10) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#10 td:nth-child(12)`).append(html);                                
                            draggable()
                        }                            
                    }else if (data.hari=='selasa') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#11 td:nth-child(12)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#12 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#13 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#14 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#15 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#17 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#18 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#19 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#20 td:nth-child(12)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='rabu') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#21 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#22 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#23 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#24 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#25 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#27 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#28 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#29 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#30 td:nth-child(12)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='kamis') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#31 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#32 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#33 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#34 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#35 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#37 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#38 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#39 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#40 td:nth-child(12)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='jumat') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#41 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#42 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#43 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#44 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#45 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#47 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#48 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#49 td:nth-child(12)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#50 td:nth-child(12)`).append(html);
                                draggable()
                            }
                    }                    
                }// END OF Kelas D4RPL1A
                
                // KELAS D4RPL2A
                else if (data.kelas_nama=='D4RPL2A') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(13)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(13)`).html(istirahat); 
                    $(`#16 td:nth-child(13)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(13)`).html(istirahat); 
                    $(`#26 td:nth-child(13)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(13)`).html(istirahat); 
                    $(`#36 td:nth-child(13)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(13)`).html(istirahat); 
                    $(`#46 td:nth-child(13)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(13)`).html(istirahat);     
                    if (data.hari=='senin') {                            
                        if (data.sesi_masuk==1) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#1 td:nth-child(13)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==2) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#2 td:nth-child(13)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==3) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#3 td:nth-child(13)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==4) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#4 td:nth-child(13)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==5) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#5 td:nth-child(13)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==7) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#7 td:nth-child(13)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==8) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#8 td:nth-child(13)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==9) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#9 td:nth-child(13)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==10) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#10 td:nth-child(13)`).append(html);
                            draggable()
                        }                            
                    }else if (data.hari=='selasa') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#11 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#12 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#13 td:nth-child(13)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#14 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#15 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#17 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#18 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#19 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#20 td:nth-child(13)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='rabu') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#21 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#22 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#23 td:nth-child(13)`).append(html);                                
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#24 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#25 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#27 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#28 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#29 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#30 td:nth-child(13)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='kamis') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#31 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#32 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#33 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#34 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#35 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#37 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#38 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#39 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#40 td:nth-child(13)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='jumat') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#41 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#42 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#43 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#44 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#45 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#47 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#48 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#49 td:nth-child(13)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#50 td:nth-child(13)`).append(html);
                                draggable()
                            }
                    }                    
                }// END OF Kelas D4RPL2A

                // KELAS D4RPL2B
                else if (data.kelas_nama=='D4RPL2B') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(14)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(14)`).html(istirahat); 
                    $(`#16 td:nth-child(14)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(14)`).html(istirahat); 
                    $(`#26 td:nth-child(14)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(14)`).html(istirahat); 
                    $(`#36 td:nth-child(14)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(14)`).html(istirahat); 
                    $(`#46 td:nth-child(14)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(14)`).html(istirahat);     
                    if (data.hari=='senin') {                            
                        if (data.sesi_masuk==1) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#1 td:nth-child(14)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==2) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#2 td:nth-child(14)`).append(html);                            
                            draggable()
                        }if (data.sesi_masuk==3) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#3 td:nth-child(14)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==4) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#4 td:nth-child(14)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==5) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#5 td:nth-child(14)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==7) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#7 td:nth-child(14)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==8) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#8 td:nth-child(14)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==9) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#9 td:nth-child(14)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==10) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#10 td:nth-child(14)`).append(html);
                            draggable()
                        }                            
                    }else if (data.hari=='selasa') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#11 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#12 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#13 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#14 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#15 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#17 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#18 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#19 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#20 td:nth-child(14)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='rabu') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#21 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#22 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#23 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#24 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#25 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#27 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#28 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#29 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#30 td:nth-child(14)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='kamis') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#31 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#32 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#33 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#34 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#35 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#37 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#38 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#39 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#40 td:nth-child(14)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='jumat') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#41 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#42 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#43 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#44 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#45 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#47 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#48 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#49 td:nth-child(14)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#50 td:nth-child(14)`).append(html);
                                draggable()
                            }
                    }                    
                }// END OF Kelas D4RPL2B

                 // KELAS D4RPL3A
                 else if (data.kelas_nama=='D4RPL3A') {
                    var istirahat=`
                        <strong>Istirahat</strong>
                    `;
                    $(`#6 td:nth-child(15)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#6 td:nth-child(15)`).html(istirahat); 
                    $(`#16 td:nth-child(15)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#16 td:nth-child(15)`).html(istirahat); 
                    $(`#26 td:nth-child(15)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#26 td:nth-child(15)`).html(istirahat); 
                    $(`#36 td:nth-child(15)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#36 td:nth-child(15)`).html(istirahat); 
                    $(`#46 td:nth-child(15)`).removeClass("tempatgeser droppable ui-droppable");                        
                    $(`#46 td:nth-child(15)`).html(istirahat);     
                    if (data.hari=='senin') {                            
                        if (data.sesi_masuk==1) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#1 td:nth-child(15)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==2) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#2 td:nth-child(15)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==3) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#3 td:nth-child(15)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==4) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#4 td:nth-child(15)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==5) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#5 td:nth-child(15)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==7) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#7 td:nth-child(15)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==8) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#8 td:nth-child(15)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==9) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#9 td:nth-child(15)`).append(html);
                            draggable()
                        }if (data.sesi_masuk==10) {
                            var html = `
                                <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                 ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                </a>
                            `;
                            $(`#10 td:nth-child(15)`).append(html);
                            draggable()
                        }                            
                    }else if (data.hari=='selasa') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#11 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#12 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#13 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#14 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#15 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#17 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#18 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#19 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#20 td:nth-child(15)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='rabu') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#21 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#22 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#23 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#24 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#25 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#27 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#28 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#29 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#30 td:nth-child(15)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='kamis') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#31 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#32 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#33 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#34 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#35 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#37 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#38 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#39 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#40 td:nth-child(15)`).append(html);
                                draggable()
                            }
                    }else if (data.hari=='jumat') {
                        if (data.sesi_masuk==1) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#41 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==2) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#42 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==3) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#43 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==4) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#44 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==5) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#45 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==7) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#47 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==8) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#48 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==9) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#49 td:nth-child(15)`).append(html);
                                draggable()
                            }if (data.sesi_masuk==10) {
                                var html = `
                                    <a href="#gantiruangan" class="btn draggable" data-sesitarget="${data.sesi_masuk}" data-temp="${data.id_temp_jadwal}" data-matkul="${data.nama_matkul}" data-kelas="${data.kelas}" data-kddos="${data.dosen_kode}" data-dosen="${data.dosen_nama}" data-kdmatkul="${data.kode_matkul}" data-ruangan="${data.ruangan_id}" data-hari="${data.hari}" data-toggle="modal" data-target="#tambahjenis" style="color:${data.color_font}; background-color:${data.color_utama}; font-family:'Calibri';">
                                     ${data.dosen_nama+" "+data.nama_matkul+" "+"("+data.jenis+")"}
                                    </a>
                                `;
                                $(`#50 td:nth-child(15)`).append(html);
                                draggable()
                            }
                    }                    
                }// END OF Kelas D4RPL3A
            }// END OF FOR                        
        });
        
    });    
    $(document).on('click','a[href="#gantiruangan"]',function() {
        // var hari = $(this).attr('data-hari');
        $('#ruanganpindah').hide('');  
        $('#haripindah').html("");  
        var dskode = $(this).attr('data-kddos');
        var kelas = $(this).attr('data-kelas');
        var kdmatkul = $(this).attr('data-kdmatkul'); 
        $('#dosen_kode').val(dskode);
        $('#kode_matkul').val(kdmatkul);
        $('#kelas_kode').val(kelas);               
       axios.post('harirequest',{
        //    hari:hari,
           kddos:dskode,
           kelas:kelas,
           kdmatkul:kdmatkul
       }).then((res)=>{
            $('#haripindah').html("<option disabled selected>PILIH</option>");   
            res.data.data.forEach(element => {
               console.log(element.hari);
               var html = `
                <option value="${element.hari}">${element.hari}</option>
               `;
                $('#haripindah').append(html);
            });            
       });         
    });         
    $('#haripindah').on('change',function(){
        console.log();
        var hari = $(this).val();
        axios.post('gantiruangan',{
            hari:hari
        }).then((res)=>{
            $('#ruanganpindah').html("");
            $('#ruanganpindah').show("");
            res.data.data.forEach(element => {
                console.log(element);
                var html = `
                    <option value="${element.ruangan_id}">${element.ruangan_nama}</option>
                `;
                $('#ruanganpindah').append(html);
            });
        });
    });

    function updateruangan(){
        var dosenkode = $('#dosen_kode').val();
        var kelas = $('#kelas_kode').val();
        var matkul = $('#kode_matkul').val();
        var hari = $('#haripindah').val();
        var ruangan = $('#ruanganpindah').val();
        if (hari===null) {
            Swal.fire({
                title:'Error',
                text:"Harus Pilih Hari",
                icon:'error',
            });
        }else{
            Swal.fire({
            title: 'Peringatan',
            text: `Apakah anda yakin untuk mengganti ruangan?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ganti'
        }).then((result) => {
            if (result.value) {
                axios.post('updateruangan',{
                    dosenkode:dosenkode,
                    kelas:kelas,
                    matkul:matkul,
                    ruangan:ruangan,
                    hari:hari
                }).then((res)=>{
                    Swal.fire({
                        title:'Sukses',
                        text:`${res.data.message}`,
                        icon:'success',
                    }).then(()=>{                        
                        window.location.href="{{route('pindahjadwal')}}";
                    });
                });                                    
            }
        })
        }
        console.log(hari===null);
        
    }
</script>
</script>
@endpush
@endsection