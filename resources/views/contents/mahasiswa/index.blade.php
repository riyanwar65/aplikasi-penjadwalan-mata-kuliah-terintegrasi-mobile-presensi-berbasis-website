@extends('master')
@section('title','Daftar Mahasiswa')
@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Mahasiswa</strong>
                    </div>
                    <div class="card-body">
                        <table id="mahasiswa-datatable" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>NIM</th>
                                    <th>Nama</th>
                                    <th>Kelas</th>
                                    <th>Angkatan</th>
                                    <th>Prodi</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="mahasiswa"> 
                                @foreach ($mhs as $mahasiswa)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$mahasiswa->mahasiswa_nim}}</td>
                                        <td>{{$mahasiswa->mahasiswa_nama}}</td>
                                        <td>{{$mahasiswa->kelas_nama}}</td>
                                        <td>{{$mahasiswa->tahun_index}}</td>
                                        <td>{{$mahasiswa->jurusan_nama}}</td>
                                        <td align="center">
                                            <a href="#detailMhs" data-mhskode="{{$mahasiswa->mahasiswa_kode}}" title="Lihat Detail" data-toggle="modal" data-target="#exampleModal" class="btn btn-success btn-sm" title="Detail"><i class="fa fa-eye"></i></a>
                                            <a href="#resetUUid" data-userid="{{$mahasiswa->user_id}}" data-nama="{{$mahasiswa->mahasiswa_nama}}" class="btn btn-danger btn-sm" title="Reset UUID"><i class="fa fa-refresh"> </i></a>
                                        </td>
                                    </tr>
                                @endforeach                              
                            </tbody>
                        </table>
                    </div>                
                </div>
            </div>           
        </div>
    </div><!-- .animated -->
</div><!-- .content -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Detail Mahasiswa</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="modal-body" id="detailMahasiswa">
                    
                </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>
    </div>
</div>
@push('script')    
    <script src="{{asset('assets/js/axios.min.js')}}"></script>
    <script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/js/sweetalert2.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#mahasiswa-datatable').DataTable();
        });        
    </script>
    <script>            
        $(document).on('click','a[href="#resetUUid"]',function(){
            var userId=$(this).data('userid');
            var nama = $(this).data('nama');
            Swal.fire({
                title: 'Peringatan',
                text: `Apa kamu yakin untuk reset UUID Device ${nama} ?`,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Reset'
                }).then((result) => {
                    if (result.value) {
                        axios.put(`/mahasiswa/${userId}`).then((response)=>{                        
                            console.log(response.data);
                            if (response.data.status===false) {
                            Swal.fire({
                                title:'Peringatan',
                                text:response.data.Message,
                                icon:'error'
                            })    
                            }else{
                                Swal.fire(
                                    'Sukses',
                                    `${response.data.Message}`,
                                    'success'
                                )
                            }                        
                        }).catch(error => {
                            console.log(error.message);
                        });                     
                    }
                })
        });

        $(document).on('click','a[href="#detailMhs"]',function(){
            var mhsKode = $(this).data('mhskode');
            axios.get(`mahasiswa/${mhsKode}`).then((response)=>{    
                console.log(response.data);
                var data = response.data.mahasiswa_detail;        
                var bulan = ['Januari','Febuari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
                var split = data.mahasiswa_biodata.mahasiswa_tanggal_lahir.split("-");
                var _bulan = new Date(split[1]).getMonth();
                console.log(data);
                if (data.mahasiswa_foto_name===null) {
                    var html = `                    
                    <div class="row">
                        <div class="col-md-5">
                           <h3>Tidak ada Foto</h3>                            
                        </div>
                        <div class="col-md-7">
                            <table width="100%" style="font-size:12px">
                                <tr>
                                    <td>Nama</td>
                                    <td width=5% align="center">:</td>
                                    <td align="center">${data.mahasiswa_nama}</td>
                                </tr>
                                <tr>
                                    <td>Agama</td>
                                    <td width=5% align="center">:</td>
                                    <td align="center">${data.mahasiswa_biodata.mahasiswa_agama_label}</td>
                                </tr>
                                <tr>
                                    <td>TTL</td>
                                    <td width=5% align="center">:</td>
                                    <td align="center">${data.mahasiswa_biodata.mahasiswa_tempat_lahir.toUpperCase()+", "+split[2]+"-"+split[1]+"-"+split[0]}</td>
                                </tr>                   
                                <tr>
                                    <td>Alamat</td>
                                    <td width=5% align="center">:</td>
                                    <td align="center">${data.mahasiswa_biodata.mahasiswa_alamat}</td>
                                </tr>    
                                <tr>
                                    <td>Program Studi</td>
                                    <td width=5% align="center">:</td>
                                    <td align="center">${data.program_studi_kode}</td>
                                </tr> 
                                <tr>
                                    <td>Kelas</td>
                                    <td width=5% align="center">:</td>
                                    <td align="center">${data.kelas_kode}</td>
                                </tr>                                   
                                <tr>
                                    <td>Angkatan</td>
                                    <td width=5% align="center">:</td>
                                    <td align="center">${data.tahun_index}</td>
                                </tr>                                    
                                <tr>
                                    <td>NIM</td>
                                    <td width=5% align="center">:</td>
                                    <td align="center">${data.mahasiswa_nim}</td>
                                </tr>                                                                             
                            </table>
                        </div>
                    </div>
                    `;
                }else{
                    var html = `                    
                    <div class="row">
                        <div class="col-md-5">
                            <img src="http://pmb.polindra.ac.id/polindra/images/mahasiswa/foto/${data.tahun_index}/${data.mahasiswa_foto_name}" width="100%">                            
                        </div>
                        <div class="col-md-7">
                            <table width="100%" style="font-size:12px">
                                <tr>
                                    <td>Nama</td>
                                    <td width=5% align="center">:</td>
                                    <td align="center">${data.mahasiswa_nama}</td>
                                </tr>
                                <tr>
                                    <td>Agama</td>
                                    <td width=5% align="center">:</td>
                                    <td align="center">${data.mahasiswa_biodata.mahasiswa_agama_label}</td>
                                </tr>
                                <tr>
                                    <td>TTL</td>
                                    <td width=5% align="center">:</td>
                                    <td align="center">${data.mahasiswa_biodata.mahasiswa_tempat_lahir.toUpperCase()+", "+split[2]+"-"+split[1]+"-"+split[0]}</td>
                                </tr>                   
                                <tr>
                                    <td>Alamat</td>
                                    <td width=5% align="center">:</td>
                                    <td align="center">${data.mahasiswa_biodata.mahasiswa_alamat}</td>
                                </tr>    
                                <tr>
                                    <td>Program Studi</td>
                                    <td width=5% align="center">:</td>
                                    <td align="center">${data.program_studi_kode}</td>
                                </tr> 
                                <tr>
                                    <td>Kelas</td>
                                    <td width=5% align="center">:</td>
                                    <td align="center">${data.kelas_kode}</td>
                                </tr>                                   
                                <tr>
                                    <td>Angkatan</td>
                                    <td width=5% align="center">:</td>
                                    <td align="center">${data.tahun_index}</td>
                                </tr>                                    
                                <tr>
                                    <td>NIM</td>
                                    <td width=5% align="center">:</td>
                                    <td align="center">${data.mahasiswa_nim}</td>
                                </tr>                                                                             
                            </table>
                        </div>
                    </div>
                    `;
                }                
                $('#detailMahasiswa').html(html);
            }); 
        });           
    </script>
@endpush
@endsection