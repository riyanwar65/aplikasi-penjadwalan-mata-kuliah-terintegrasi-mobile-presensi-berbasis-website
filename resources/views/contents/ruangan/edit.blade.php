@extends('master')
@section('title','Edit Ruangan')
@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">            
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong>Form Edit Ruangan</strong>
                    </div>
                    <div class="card-body">
                        <form action="{{route('ruangan.update',$ruangan->ruangan_id)}}" method="post">
                            @csrf
                            @method('PUT')
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Latitude</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="number" class="form-control" name="latitude" id="" value="{{$ruangan->latitude}}">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Longitude</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="number" class="form-control" name="longitude" id="" value="{{$ruangan->longitude}}">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Radius</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="number" class="form-control" name="radius" id="" min="1" value="{{$ruangan->radius}}">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-sm btn-success">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection