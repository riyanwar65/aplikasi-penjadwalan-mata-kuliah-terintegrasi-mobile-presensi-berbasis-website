@extends('master')
@section('title','Ruangan')
@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">            
            <div class="col-md-12">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if ($message = Session::get('alert-danger'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>                         
                            @foreach ($errors->all() as $error)
                                  {{ $error }} <br>
                            @endforeach
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-6 col-sm-4">
                            <div class="text-left">
                                <strong class="card-title">Manajemen Ruangan</strong>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-4">                            
                        </div>
                    </div>
                    <div class="card-body">
                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                            <thead>
                                <tr>                                    
                                    <th>No</th>
                                    <th>Nama Ruangan</th>
                                    <th>Jenis Ruangan</th>
                                    <th>Latitude</th>
                                    <th>Longitude</th>                                    
                                    <th>Radius</th>
                                    <th>Jurusan</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>  
                                @foreach ($ruangan as $value)                                                                    
                                <tr>
                                    <td>{{$key++}}</td>
                                    <td>{{$value->ruangan_nama}}</td>
                                    <td>{{$value->ruangan_jenis}}</td>                                    
                                    <td>{{$value->latitude}}</td>
                                    <td>{{$value->longitude}}</td>
                                    <td>{{$value->radius}}</td>
                                    <td>{{$value->jurusan}}</td>                                    
                                    <td>
                                        <a href="{{route('ruangan.edit',$value->ruangan_id)}}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
                                        <a href="#hapus" data-id="{{$value->ruangan_id}}" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>                
                </div>
            </div>           
        </div>        
    </div><!-- .animated -->        
</div><!-- .content -->
@push('script')
<script src="{{asset('assets/js/axios.min.js')}}"></script>
<script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/sweetalert2.js')}}"></script>
<script src="{{asset('assets/js/wickedpicker.min.js')}}"></script>
<script>
    $('.jam_masuk').wickedpicker({twentyFour: true});
    $('.jam_keluar').wickedpicker({twentyFour: true});
</script>
<script>
    $(document).on('click','a[href="#hapus"]',function(){
            var id = $(this).data('id');                   
            Swal.fire({
                title: 'Peringatan',
                text: `Apa Kamu yakin ingin menghapusnya ?`,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Hapus'
                }).then((result) => {
                    if (result.value) {                        
                        axios.delete(`/ruangan/${id}`,{_token:"{{csrf_token()}}"}).then((response)=>{
                            Swal.fire(
                                'Sukses',
                                `${response.data.message}`,
                                'success'
                            ).then(function(){
                                window.location.reload();
                            });            
                        });
                    }
                })
        });
</script>
@endpush
@endsection