<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('assets/style-email.css')}}">
    <title>Send Email</title>
</head>
<body style="width:100% !important; margin:0 !important; padding:0 !important; -webkit-text-size-adjust:none; -ms-text-size-adjust:none; background-color:#FFFFFF;">
    <table cellpadding="0" cellspacing="0" border="0" id="backgroundTable" style="height:auto !important; margin:0; padding:0; width:100% !important; background-color:#FFFFFF; color:#222222;">
        <tr>
            <td>
             <div id="tablewrap" style="width:100% !important; max-width:600px !important; text-align:center !important; margin-top:0 !important; margin-right: auto !important; margin-bottom:0 !important; margin-left: auto !important;">
                  <table id="contenttable" width="600" align="center" cellpadding="0" cellspacing="0" border="0" style="background-color:#FFFFFF; text-align:center !important; margin-top:0 !important; margin-right: auto !important; margin-bottom:0 !important; margin-left: auto !important; border:none; width: 100% !important; max-width:600px !important;">
                <tr>
                    <td width="100%">
                        <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td width="20%" bgcolor="#ffffff" align="left">
                                    <a href="https://www.polindra.ac.id/">
                                        <img src="{{ $message->embed(public_path().'/images/Polindra.png') }}" width="100%" alt="">                                        
                                    </a>
                                </td>
                                <td align="center" style="font-size: 25px;font-family: Arial, Helvetica, sans-serif">APLIKASI MANAJEMEN PENJADWALAN</td>
                            </tr>
                       </table>
                       <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="25" width="100%">
                            <tr>
                                <td width="100%" bgcolor="#ffffff" style="text-align:left;">
                                    <p style="color:#222222; font-family:Arial, Helvetica, sans-serif; font-size:15px; line-height:19px; margin-top:0; margin-bottom:20px; padding:0; font-weight:normal;">
                                        Dear {{$nama}},                                                                          
                                    </p>
                                    <p style="color:#222222; font-family:Arial, Helvetica, sans-serif; font-size:15px; line-height:19px; margin-top:0; margin-bottom:20px; padding:0; font-weight:normal;">
                                        <p>Untuk dapat menggunakan aplikasi Web Manajemen Penjadwalan maka untuk login silahkan menggunakan akun berikut.</p>
                                        <p>
                                            Username : {{$username}}<br>
                                            Password : {{$password}}<br>
                                        </p>
                                        
                                    </p>
                                        <table border="0" cellspacing="0" cellpadding="0" width="100%" class="emailwrapto100pc">                                          
                                        </table>
        
                                </td>
                            </tr>
                       </table>                       
                       <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="25" width="100%">
                            <tr>
                                <td width="100%" bgcolor="#ffffff" style="text-align:left;">
                                    <p style="color:#222222; font-family:Arial, Helvetica, sans-serif; font-size:11px; line-height:14px; margin-top:0; margin-bottom:15px; padding:0; font-weight:normal;">
                                        Alamat : <span style="color:#2489B3; font-weight:bold; ">Jl. Lohbener, Legok, Kec. Lohbener, Kabupaten Indramayu, Jawa Barat 45252</span>                                                                                
                                    </p>
                                    {{-- <p style="color:#222222; font-family:Arial, Helvetica, sans-serif; font-size:11px; line-height:14px; margin-top:0; margin-bottom:15px; padding:0; font-weight:normal;">
                                        Copyright 2013 Your Company. All Rights Reserved.<br>
                                        If you no longer wish to receive emails from us, you may <a style="color:#2489B3; font-weight:normal; text-decoration:underline;" href="#">unsubscribe</a>.
                                    </p> --}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            </div>
            </td>
        </tr>
    </table> 
</body>
</html>