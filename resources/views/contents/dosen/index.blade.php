@extends('master')
@section('title','Daftar Dosen')
@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Dosen</strong>
                    </div>
                    <div class="card-body">
                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>NIDN</th>
                                    <th>Nama</th>
                                    <th>Jenis Dosen</th>                                    
                                    <th>Prodi</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="mahasiswa">                                                
                                @foreach ($dosen as $key=>$value)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$value->dosen_nidn}}</td>
                                        <td>{{$value->dosen_gelar_depan." ".$value->dosen_nama.", ".$value->dosen_gelar_belakang}}</td>
                                        @if ($value->dosen_jenis=='dosen_luar')                                            
                                            <td>Dosen Luar</td>
                                        @else
                                            <td>Homebase</td>
                                        @endif                                    
                                        <td>{{$value->program_studi_kode}}</td> 
                                        <td>
                                            <a href="#detailDosen" data-dosenkode="{{$value->user_id}}" title="Lihat Detail" data-toggle="modal" data-target="#exampleModal" class="btn btn-success btn-sm" title="Detail"><i class="fa fa-eye"></i></a>
                                            <a href="#resetUUID" data-userid="{{$value->user_id}}" data-nama="{{$value->dosen_gelar_depan." ".$value->dosen_nama.", ".$value->dosen_gelar_belakang}}" class="btn btn-danger btn-sm" title="Reset UUID"><i class="fa fa-refresh"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>                
                </div>
            </div>           
        </div>
    </div><!-- .animated -->
</div><!-- .content -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Detail Dosen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="modal-body" id="detailDosen">
                    
                </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>
    </div>
</div>
@push('script')
    <script src="{{asset('assets/js/axios.min.js')}}"></script>
    <script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/js/sweetalert2.js')}}"></script>
    <script>       
    $(document).on('click','a[href="#resetUUID"]',function(){
            var userId=$(this).data('userid');
            var nama = $(this).data('nama');            
            Swal.fire({
                title: 'Peringatan',
                text: `Apa kamu yakin untuk reset UUID Device ${nama} ?`,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Reset'
                }).then((result) => {
                    if (result.value) {                        
                        axios.put(`/dosen/${userId}`).then((response)=>{    
                            // console.log(response);
                            if (response.data.status==false) {
                            Swal.fire({
                                title:'Peringatan', 
                                text:response.data.Message,
                                icon:'error'
                            })    
                            }else{
                                Swal.fire(
                                    'Sukses',
                                    `${response.data.Message}`,
                                    'success' 
                                )
                            }                        
                        });                     
                    }
                });
        });
        $(document).on('click','a[href="#detailDosen"]',function(){
            var userid = $(this).data('dosenkode');
            $('#detailDosen').html(" "); 
            axios.get(`/dosen/${userid}`).then((response)=>{                        
                if (response.data.dosen_foto==null) {
                    var html = ` <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <strong>Foto Tidak Ada</strong>
                            </div>
                            <div class="col-md-8 col-sm">
                                <strong>Nama : </strong>${response.data.dosen_gelar_depan+" "+response.data.dosen_nama+","+response.data.dosen_gelar_belakang} <br>
                                <strong>NIK : </strong>${response.data.dosen_nik} <br>
                                <strong>NIP : </strong>${response.data.dosen_nip} <br>
                                <strong>Email : </strong>${response.data.dosen_email} <br>                                
                                <strong>Dosen Jenis : </strong>${response.data.dosen_jenis} <br>
                                <strong>Alamat : </strong>${response.data.alamat} <br>
                                <strong>Program Studi : </strong>${response.data.program_studi_kode} <br>
                                <strong>Pendidikan Tertinggi : </strong>${response.data.pendidikan_tertinggi} <br>                            
                            </div>
                        </div>`;
                    $('#detailDosen').html(html); 
                }
                if (response.data.dosen_foto.dosen_foto_name==null) {
                    var html = `
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <strong>Foto Tidak Ada</strong>
                            </div>
                            <div class="col-md-8 col-sm">
                                <strong>Nama : </strong>${response.data.dosen_gelar_depan+" "+response.data.dosen_nama+","+response.data.dosen_gelar_belakang} <br>
                                <strong>NIK : </strong>${response.data.dosen_nik} <br>
                                <strong>NIP : </strong>${response.data.dosen_nip} <br>
                                <strong>Email : </strong>${response.data.dosen_email} <br>                                
                                <strong>Dosen Jenis : </strong>${response.data.dosen_jenis} <br>
                                <strong>Alamat : </strong>${response.data.alamat} <br>
                                <strong>Program Studi : </strong>${response.data.program_studi_kode} <br>
                                <strong>Pendidikan Tertinggi : </strong>${response.data.pendidikan_tertinggi} <br>                            
                            </div>
                        </div>
                    `;
                    $('#detailDosen').html(html); 
                }else{
                    var path_folder = response.data.dosen_foto.dosen_foto_name.split(".");
                    // console.log(path_folder);
                    var html = `
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <img src="http://pmb.polindra.ac.id/polindra/images/dosen/foto/${path_folder[0]}/${response.data.dosen_foto.dosen_foto_name}">
                            </div>
                            <div class="col-md-8 col-sm">
                                <strong>Nama : </strong>${response.data.dosen_gelar_depan+" "+response.data.dosen_nama+","+response.data.dosen_gelar_belakang} <br>
                                <strong>NIK : </strong>${response.data.dosen_nik} <br>
                                <strong>NIP : </strong>${response.data.dosen_nip} <br>
                                <strong>Email : </strong>${response.data.dosen_email} <br>                                
                                <strong>Dosen Jenis : </strong>${response.data.dosen_jenis} <br>
                                <strong>Alamat : </strong>${response.data.alamat} <br>
                                <strong>Program Studi : </strong>${response.data.program_studi_kode} <br>
                                <strong>Pendidikan Tertinggi : </strong>${response.data.pendidikan_tertinggi} <br>                            
                            </div>
                        </div>
                    `;
                    $('#detailDosen').html(html); 
                }
            });
        });
    </script>
@endpush
@endsection