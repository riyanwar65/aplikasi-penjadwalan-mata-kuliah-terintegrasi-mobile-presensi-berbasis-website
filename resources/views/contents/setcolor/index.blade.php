@extends('master')
@section('title','Set Color Picker')
@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">   
            <div class="col-md-12">
                @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>{{ $message }}</strong>
                </div>
                @endif
                @if ($message = Session::get('error'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>                         
                            @foreach ($errors->all() as $error)
                                    {{ $error }} <br>
                            @endforeach
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#tambah">
                            Tambah Warna
                        </button>
                    </div>
                    <div class="card-body">                        
                        <table id="bootstrap-data-table-export" class="table table-stripped table-bordered">
                            <thead>
                                <tr>
                                    <td>No</td>
                                    <td>Keterangan</td>
                                    <td>Action</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($color as $key=>$v)                                    
                                <tr>
                                    <td>{{$key+1}}</td>
                                    @if ($v->dosen_gelar_depan==NULL||$v->dosen_gelar_depan=="")
                                        <td style="background-color:{{$v->color_utama}}; color:{{$v->color_font}};">{{$v->dosen_nama.",".$v->dosen_gelar_belakang}}</td>    
                                    @else
                                        <td style="background-color:{{$v->color_utama}}; color:{{$v->color_font}};">{{$v->dosen_gelar_depan.",".$v->dosen_nama.",".$v->dosen_gelar_belakang}}</td>
                                    @endif                                    
                                    <td>
                                        <a href="#edit" data-toggle="modal" data-target="#update" data-id="{{$v->id_color}}" data-kddosen="{{$v->dosen_kode}}" data-warnautama="{{$v->color_utama}}" data-warnafont="{{$v->color_font}}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
                                        <a href="#hapus" data-id="{{$v->id_color}}" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>                     
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="tambah" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form action="{{route('setcolor.post')}}" method="post">
        @csrf    
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Warna</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label for=""><strong>Pilih Dosen</strong></label>
                <select name="dosen" id="select2" class="form-control" style="width: 100%;">
                    <option disabled selected>Pilih</option>
                    @foreach ($dosen as $value)                        
                        <option value="{{$value->dosen_kode}}">{{$value->dosen_gelar_depan.$value->dosen_nama.",".$value->dosen_gelar_belakang}}</option>
                    @endforeach
                </select><br>
                <label for=""><strong>Warna Utama</strong></label>                
                    <input type="color" name="warna_utama" class="form-control" />                     
                <label for=""><strong>Warna Font</strong></label>                
                    <input type="color" name="warna_font" class="form-control" />                     
                {{-- <input type="text" name="" class="form-control" id="" placeholder="Warna"> --}}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-info">Simpan</button>
            </div>
        </div>
    </form>   
  </div>
</div>

<div class="modal fade" id="update" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
    <form id="actionupdate" method="post">
        @csrf    
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Warna</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label for=""><strong>Nama Dosen</strong></label>
                <input type="hidden" name="dosen" id="dosen" class="form-control" readonly>
                <label for=""><strong>Warna Utama</strong></label>                
                    <input type="color" name="warna_utama" id="wu" class="form-control" />                     
                <label for=""><strong>Warna Font</strong></label>                
                    <input type="color" name="warna_font" id="wf" class="form-control" />                     
                {{-- <input type="text" name="" class="form-control" id="" placeholder="Warna"> --}}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-info">Update</button>
            </div>
        </div>
    </form>   
</div>
</div>
@push('script')
    {{-- <script src="https://code.jquery.com/jquery-2.2.2.min.js"></script> --}}
    <script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
{{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.3.6/js/bootstrap-colorpicker.js"></script>    
    <script src="{{asset('assets/js/sweetalert2.js')}}"></script>   
    <script src="{{asset('assets/js/select2.min.js')}}"></script>
    <script type="text/javascript">
        $('#select2').select2({
            width:'resolve',
        });
        $('#warna_primary').colorpicker();
        $('#warna_font').colorpicker();               
        $('#warna_primary_2').colorpicker({
                color:"#ff0000",
        });
        $('#warna_font_2').colorpicker({
                color:"#00000",
        }); 
        $(document).on('click','a[href="#edit"]',function(){
            var id = $(this).data('id');
            var warnautama = $(this).data('warnautama');
            var warnafont = $(this).data('warnafont');             
            console.log(warnautama);
            var kodedosen =  $(this).data('kddosen');            
            $('#wu').val(warnautama);
            $('#wf').val(warnafont);            
            $('#dosen').val(kodedosen);
            var url = '{{ route("setcolor.update", ":id") }}';
                url = url.replace(':id', id);                
            $('#actionupdate').attr('action',url);            
        });
        $(document).on('click','a[href="#hapus"]',function(){
            var id = $(this).attr('data-id');
            Swal.fire({
                title: 'Peringatan',
                text: `Apakah anda yakin ingin menghapusnya ?`,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Hapus'
            }).then((result) => {
                if (result.value) {
                    axios.get(`delete-color/${id}`).then((res)=>{                   
                        if (res.data.status===false) {                 
                            Swal.fire({
                                title:'Peringatan',
                                text:res.data.message,
                                icon:'error',
                            });     
                        }else{                    
                            Swal.fire({
                                title:'Sukses',
                                text:res.data.message,
                                icon:'success',
                            }).then(()=>{
                                window.location.reload();
                            });  
                        }
                    });                                                      
                }
            })            
        });
    </script>
@endpush
@endsection