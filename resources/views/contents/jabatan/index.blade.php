@extends('master')
@section('title','Master Data Jabatan')    
@section('content')
<div class="content mt-3">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
        </div>
    @endif
    <div id="message">

    </div>
    <div class="animated fadeIn">
        <div class="row"> 
            @if (strtolower(Session::get('jabatan'))=="admin jurusan"
                    ||strtolower(Session::get('jabatan'))=="admin")
                @include('permissions.adminjurusan.pendataanjabatan')
            @endif
            @if (strtolower(Session::get('jabatan'))=="0")
                @include('permissions.adminsiakad.masterjabatan')
                @include('permissions.adminsiakad.pendataanjabatan')
            @endif            
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form id="formedit" method="post">
            @csrf
            @method('PUT')
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Jabatan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                    <div class="modal-body" id="editmodal">
                        
                    </div>
                <div class="modal-footer">
                <button type="submit" class="btn btn-success">Update</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
    </div>
</div>
@push('script')
<script src="{{asset('assets/js/axios.min.js')}}"></script>
<script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/sweetalert2.js')}}"></script>
<script>    
    $('#jabatanpost').on('submit',function(event){
        event.preventDefault();                
        var jabatan = $('#jbtn').val();        
        axios.post(`{{route('jabatan.store')}}`, {
            _token:"{{csrf_token()}}",
            jabatan:jabatan,
        })
        .then(function (response) {
            if (response.data.status==false) {                    
                var message = `
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>                         
                    ${response.data.message}
                </div>
                `;
                $('#message').html(message);
            }else{
                var message = `
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>                         
                    ${response.data.message}
                </div>
                `;
                $('#message').html(message);                    
                var html = `
                    <tr>
                        <td>${response.data.jabatan.jabatan}</td>
                        <td align="center">
                            <a href="#edit" data-idedit="${response.data.jabatan.id}" data-toggle="modal" data-target="#exampleModal" class="btn btn-sm btn-warning" title="Edit"><i class="fa fa-edit"></i></a>
                            <a href="#delete" data-id="${response.data.jabatan.id}" class="btn btn-sm btn-danger" title="Hapus"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                    {{$jabatan->links()}}
                `;            
                $('#jabatan').append(html);
            }
        }).catch(function (error) {
            console.log(error);
        });        
    });

    $(document).on('click','a[href="#delete"]',function(){
        var dataId=$(this).data('id');        
        Swal.fire({
                title: 'Peringatan',
                text: `Apa kamu yakin untuk Menghapusnya ?`,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Hapus'
                }).then((result) => {
                    if (result.value) {  
                        axios.delete(`jabatan/${dataId}`,{_token:"{{csrf_token()}}"}).then((res)=>{
                            console.log(res.data);
                            if(res.data.status===false){
                                Swal.fire({
                                    title:'Sukses',
                                    text:res.data.message,
                                    icon:'success',
                                }).then(()=>{
                                    window.location.href="{{route('jabatan.index')}}";
                                });                                
                            }else{
                                Swal.fire({
                                    title:'Peringatan',
                                    text:res.data.message,
                                    icon:'error',
                                });   
                            }
                        }).catch((error)=>{
                            console.log(error);
                        });                                       
                    }
                })
    });
</script>
<script>
    $(document).on('click','a[href="#editjabatan"]',function(){        
        var dataIdedit=$(this).data('idedit');
        axios.get(`jabatan/${dataIdedit}/edit`,{_token:"{{csrf_token()}}"}).then((res)=>{            
            console.log(res);
            var html = `
                <input type="text" name="jabatan" id="jbtn" class="form-control" value="${res.data.jabatan}" placeholder="Masukan jabatan">                                
            `;
            $('#editmodal').html(html);
            var action = '{{ route("jabatan.update", ":dataId") }}';
            action = action.replace(':dataId', dataIdedit);
            $('#formedit').attr('action',action);
            // var message = `
            //     <div class="alert alert-success alert-block">
            //         <button type="button" class="close" data-dismiss="alert">×</button>                         
            //         ${response.data.message}
            //     </div>
            //     `;
            // $('#message').html(message); 
        }).catch((error)=>{
            console.log(error);
        });
    });
</script>
<script>
    $(document).on('click','a[href="#hapusjabatandosen"]',function(){
        var dataId=$(this).data('id');
        console.log(dataId);
        Swal.fire({
                title: 'Peringatan',
                text: `Apa kamu yakin untuk Menghapusnya ?`,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Hapus'
        }).then((result) => {
            if (result.value) {  
                axios.delete(`jabatan-pegawai/${dataId}`,{_token:"{{csrf_token()}}"}).then((res)=>{
                    console.log(res.data);                            
                        Swal.fire({
                            title:'Sukses',
                            text:res.data.message,
                            icon:'success',
                        }).then(()=>{
                            window.location.href="{{route('jabatan.index')}}";
                        });                                                            
                }).catch((error)=>{
                    console.log(error);
                });                                       
            }
        });
    });
</script>
<script>
    $(document).on('click','a[href="#hapusjabatanadmin"]',function(){
        var dataId=$(this).data('id');
        console.log(dataId);
        Swal.fire({
                title: 'Peringatan',
                text: `Apa kamu yakin untuk Menghapusnya ?`,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Hapus'
                }).then((result) => {
                    if (result.value) {  
                        axios.delete(`regis-admin/${dataId}`,{_token:"{{csrf_token()}}"}).then((res)=>{
                            console.log(res.data);                            
                                Swal.fire({
                                    title:'Sukses',
                                    text:res.data.message,
                                    icon:'success',
                                }).then(()=>{
                                    window.location.href="{{route('jabatan.index')}}";
                                });                                                            
                        }).catch((error)=>{
                            console.log(error);
                        });                                       
                    }
        });
    });
</script>   
@endpush
@endsection
