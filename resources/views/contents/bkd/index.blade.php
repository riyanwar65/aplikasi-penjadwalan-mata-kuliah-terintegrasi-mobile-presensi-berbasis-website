@extends('master')
@section('title','Beban Kinerja Dosen')    
@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if ($message = Session::get('error'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>                         
                            @foreach ($errors->all() as $error)
                                  {{ $error }} <br>
                            @endforeach
                    </div>
                @endif
                <br>
                {{-- <div class="card">
                    <div class="card-body">                        
                        <form action="{{route('bkd-export')}}" method="post"> 
                            @csrf
                            <div class="col-md-12">
                                <h3>Export</h3>
                            </div>
                            <br><br>
                            <div class="col-md-3">
                                <label for=""><strong>Tahun Ajaran</strong></label><br>
                                <select name="tahun_ajaran" id="" class="form form-control">
                                    <option value="0" disabled selected>Pilih</option>
                                    @foreach ($tahun_ajaran as $tahun)
                                        <option value="{{$tahun->tahun_ajaran}}">{{$tahun->tahun_ajaran}}</option>
                                    @endforeach
                                </select>
                            </div>       
                            <div class="col-md-3">
                                <label for=""><strong>Pilih Jenis Semester</strong></label><br>
                                <select name="semester" id="" class="form form-control">
                                    <option value="0" disabled selected>Pilih</option>
                                    <option value="ganjil">Ganjil</option>
                                    <option value="genap">Genap</option>
                                </select>
                            </div>                                                                                               
                            <div class="col-md-3">
                                <label for=""><strong>Pilih Jenis Export</strong></label><br>
                                <select name="export" id="" class="form form-control">
                                    <option value="0" disabled selected>Pilih</option>
                                    <option value="pdf">PDF</option>
                                    <option value="excel">Excel</option>
                                </select>
                            </div>    
                            <div class="col-md-3">
                                <label for="">&nbsp;</label><br>
                                <button type="submit" class="btn btn-sm btn-success">Export</button>
                            </div>
                        </form>
                    </div>
                </div> --}}                
                {{-- <div class="card">
                    <div class="card-body">                                                
                            <div class="col-md-12">
                                <h3>Filter Tampilan</h3>
                            </div>
                            <br><br>
                            <div class="col-md-4">
                                <label for=""><strong>Tahun Ajaran</strong></label><br>
                                <select name="tahun_ajaran" id="tahun_ajaran" class="form form-control">
                                    <option value="0" disabled selected>Pilih</option>
                                    @foreach ($tahun_ajaran as $tahun)
                                        <option value="{{$tahun->tahun_ajaran}}">{{$tahun->tahun_ajaran}}</option>
                                    @endforeach
                                </select>
                            </div>       
                            <div class="col-md-4">
                                <label for=""><strong>Pilih Jenis Semester</strong></label><br>
                                <select name="semester" id="semester" class="form form-control">
                                    <option value="0" disabled selected>Pilih</option>
                                    <option value="ganjil">Ganjil</option>
                                    <option value="genap">Genap</option>
                                </select>
                            </div>       
                            <div class="col-md-4">
                                <label for="">&nbsp;</label><br>
                                <button type="button" onclick="filter()" class="btn btn-sm btn-info">Filter</button>                                
                            </div>
                    </div>
                </div> --}}
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-6 col-sm-4">
                            <div class="text-left">
                                <strong class="card-title">Beban Kinerja Dosen</strong>                                                                                                       
                            </div>                            
                        </div>
                        <div class="col-md-6 col-sm-4">                            
                            <div class="text-right">
                                <table align=right>
                                    <tr>
                                        <td>
                                            @if (strtolower(Session::get('jabatan'))=="sekertaris jurusan"
                                            ||strtolower(Session::get('jabatan'))=="sekertaris"
                                            ||strtolower(Session::get('jabatan'))=="sekjur")
                                            <a href="{{route('bkd.create')}}" class="btn btn-sm btn-success" > <i class="fa fa-plus"></i> Tambah BKD</a>
                                            @else                                    
                                            
                                            @endif  
                                        </td>
                                        <td>  
                                            <form action="{{route('export-bkd')}}" method="post">   
                                                @csrf
                                                <input type="hidden" name="semester" id="jenissemester">                             
                                                <input type="hidden" name="tahun_ajaran" id="tahunajaran">                             
                                                <button type="submit" class="btn btn-sm btn-warning">Export PDF</button>
                                            </form>                                           
                                        </td>
                                    </tr>
                                </table>                                    
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Dosen</th>
                                    <th>Tahun Ajaran</th>
                                    <th>Mata Kuliah</th>
                                    <th>Kelas</th>
                                    <th>Semester</th>
                                    <th>SKS Teori</th>
                                    <th>SKS Praktek</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody> 
                                @foreach ($bkd as $key=>$b)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$b->dosen_nama}}</td>
                                        <td>{{$b->tahun_ajaran}}</td>  
                                        <td>{{$b->nama_matkul}}</td>                                        
                                        <td>{{$b->kelas_nama}}</td>
                                        <td>{{$b->semester}}</td>
                                        <td>{{$b->jumlah_sks_teori}}</td>
                                        <td>{{$b->jumlah_sks_praktek}}</td>
                                        <td>
                                            <a href="{{url('/bkd/'.$b->kode_bkd.'/edit')}}" class="btn btn-warning btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
                                            <a href="#hapusbkd" data-kodebkd="{{$b->kode_bkd}}" class="btn btn-danger btn-sm" title="Hapus"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach                                  
                            </tbody>
                        </table>
                        <div id="paginate"></div>
                    </div>      
                    
                    <div class="card-body">
                        <div id="coba">

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@push('script')
    <script src="{{asset('assets/js/axios.min.js')}}"></script>
    <script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/js/sweetalert2.js')}}"></script>    
    {{-- <script src="{{asset('assets/js/pagination.js')}}"></script>
    <script src="{{asset('assets/js/pagination.min.js')}}"></script> --}}
    <script src="{{asset('assets/js/simplePagination.js')}}"></script>
    <script>        
        $('#bkd-filter').hide();
        $('#export').hide();     
        var $rows = $('#bkd tr');
        $('#search').keyup(function() {
            var value = $(this).val().toLowerCase();
            $("#bkd tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });        
         $(document).on('click','a[href="#hapusbkd"]',function(){
            var kodebkd = $(this).data('kodebkd');                   
            Swal.fire({
                title: 'Peringatan',
                text: `Apa Kamu yakin ingin menghapusnya ?`,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Hapus'
                }).then((result) => {
                    if (result.value) {
                        axios.delete(`/bkd/${kodebkd}`).then((response)=>{                            
                            Swal.fire(
                                    'Sukses',
                                    `${response.data.message}`,
                                    'success'
                            ).then(function(){
                                location.reload();
                            });                            
                        });
                    }
                })
        });

        function filter(){
            $('#bkd').html("");
            var semester = $('#semester option').filter(":selected").val();
            var tahun_ajaran = $('#tahun_ajaran option').filter(":selected").val();
            document.getElementById('jenissemester').value = semester;
            document.getElementById('tahunajaran').value = tahun_ajaran;            
            axios.post(`/filter-bkd/`,{
                semester : semester,
                tahun_ajaran:tahun_ajaran,
            }).then((response)=>{                      
                var count = 1;
                if (response.data.status===true) {                    
                    const newArray = response.data.bkd.map((item)=>{                    
                    var html = `
                        <tr>
                            <td>${count++}</td>
                            <td>${item['dosen_gelar_depan']+' '+item['dosen_nama']+","+item['dosen_gelar_belakang']}</td>
                            <td>${item['tahun_ajaran']}</td>
                            <td>${item['nama_matkul']}</td>
                            <td>${item['kelas_nama']}</td>
                            <td>${item['semester']}</td>
                            <td>${item['jumlah_sks_teori']}</td>
                            <td>${item['jumlah_sks_praktek']}</td>
                            <td>
                                <a href="/bkd/${item['kode_bkd']}/edit" class="btn btn-warning btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
                                <a href="#hapusbkd" data-kodebkd="${item['kode_bkd']}" class="btn btn-danger btn-sm" title="Hapus"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    `;
                    $('#bkd').append(html);
                    var items = $('#bkd tr');
                    console.log(items.length);
                    var numItems = items.length;
                    var perPage = 5;   
                    items.slice(perPage).hide();
                    $("#paginate").pagination({
                        items: numItems,
                        itemsOnPage: perPage,
                        cssStyle: "light-theme",
                        onPageClick: function(pageNumber) {
                            var showFrom = perPage * (pageNumber - 1);
                            var showTo = showFrom + perPage;
                            items.hide().slice(showFrom, showTo).show();
                        }
                    });
                    $('#export').show();
                    });
                }else{
                    $('#export').hide();
                    $('#bkd').html("");
                    Swal.fire(
                        'Error',
                        `${response.data.message}`,
                        'error'
                    )
                    $('#export').hide();
                    $('#bkd').html("");
                }                                               
            });
            $('#bkd-filter').show();                     
            $('#bkd').html(""); 
            function checkFragment() {                
                var hash = window.location.hash || "#page-1";
                hash = hash.match(/^#page-(\d+)$/);
                if(hash) {
                    $("paginate").pagination("selectPage", parseInt(hash[1]));
                }
            };
            $(window).bind("popstate", checkFragment);
            checkFragment();
              
        }            
    </script>
@endpush
@endsection