@extends('master')
@section('title','Create Beban Kinerja Dosen')    
@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                @if (count($errors) > 0)
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>                         
                            @foreach ($errors->all() as $error)
                                {{ $error }} <br>
                            @endforeach
                    </div>
                @endif
                <form action="{{route('bkd.store')}}" method="post" enctype="multipart/form-data" class="form-horizontal">   
                    @csrf                         
                    <div class="card">
                    <div class="card-header">
                        <strong>BKD</strong> Form Input
                    </div>
                        <div class="card-body card-block">
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="text-input" class=" form-control-label">Pilih Jurusan</label></div>
                                <div class="col-12 col-md-9">
                                    <select name="prodi" id="programstudi" class="form-control">
                                        <option value="0" disabled="true" selected="true">Pilih</option>
                                        @foreach ($program_studi as $ps)
                                            <option value="{{$ps->program_studi_kode}}">{{$ps->program_studi_nama}}</option>
                                        @endforeach
                                        {{-- @foreach ($jurusan as $jr)
                                            <option value="{{$jr->jurusan_kode}}">{{$jr->jurusan_nama}}</option>
                                        @endforeach --}}
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="email-input" class=" form-control-label">Semester</label></div>
                                <div class="col-12 col-md-9">
                                    <select name="semester" id="semester" class="form-control">
                                        <option value="0" disabled="true" selected="true">Pilih</option>
                                        @foreach ($semester as $s)
                                            <option value="{{$s->semester}}">{{$s->semester}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group" id="dosenprodi">
                                <div class="col col-md-3"><label for="text-input" class=" form-control-label">Dosen</label></div>
                                <div class="col-12 col-md-9">
                                    <select name="dosenprodi" id="datadosenprodi" class="form-control">
                                        
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group" id="datamatakuliah">
                                <div class="col col-md-3"><label for="text-input" class=" form-control-label">Mata Kuliah</label></div>
                                <div class="col-12 col-md-9">
                                    <select name="matkul" id="datamatkul" class="form-control">                                                                             
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group" id="kelas">
                                <div class="col col-md-3"><label for="text-input" class=" form-control-label">Kelas </label></div>
                                <div class="col-12 col-md-9">
                                    <select name="kelas" id="datakelas" class="form-control">                                        
                                    </select>
                                </div>
                            </div>                            
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="email-input" class=" form-control-label">Tahun Ajaran</label></div>
                                <div class="col-4 col-md-3"><input type="number" name="awal" placeholder="Tahun Ajaran" min="2000"  class="form-control"></div>                                
                                /
                                <div class="col-4 col-md-3"><input type="number" name="akhir" placeholder="Tahun Ajaran" min="2000" class="form-control"></div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="password-input" class=" form-control-label">SKS Praktek</label></div>
                                <div class="col-12 col-md-9"><input type="number" name="sks_praktek" placeholder="SKS Praktek" min="0" max="20" class="form-control"></div>
                            </div>                            
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">SKS Teori</label></div>
                                <div class="col-12 col-md-9"><input type="number" name="sks_teori" placeholder="SKS Teori" min="0" max="20" class="form-control"></div>
                            </div>                                
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success btn-sm">
                                <i class="fa fa-dot-circle-o"></i> Simpan
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>            
    </div>
</div>
@push('script')
    <script src="{{asset('assets/js/axios.min.js')}}"></script>
    <script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/js/sweetalert2.js')}}"></script>
    <script src="{{asset('assets/js/select2.min.js')}}"></script>
    <script>
        $('#datadosenprodi').select2({
            width:'resolve',
        });
        $('#datakelas').select2({
            width:'resolve',
        });
        $('#datamatkul').select2({
            width:'resolve',
        });
    </script>
    <script>
        $('#dosenprodi').hide();
        $('#kelas').hide();
        $('#datamatakuliah').hide();                            
            $('#programstudi').on('change',function(){
                // alert(this.value);
                $('#datadosenprodi').html(`<option value="0" disabled="true" selected="true">Pilih</option>`);
                $('#datakelas').html(`<option value="0" disabled="true" selected="true">Pilih</option>`);
                $('#datamatkul').html(`<option value="0" disabled="true" selected="true">Pilih</option>`);
                axios.get(`/dosen-prodi/${this.value}`).then((response)=>{                 
                    var data = response.data.dosen;
                    var panjang = response.data.dosen.length;                    
                    $('#datadosenprodi').children('option:not(:first)').remove();
                    for (let index = 0; index < panjang ; index++) {                        
                        $('#datadosenprodi').append(`                    
                            <option value="${data[index].dosen_kode}">${data[index].dosen_gelar_depan} ${data[index].dosen_nama}, ${data[index].dosen_gelar_belakang}</option>
                        `);
                    }
                    var panjang_data = response.data.dosen_luar.length;
                    var data_luar = response.data.dosen_luar;
                    for (let index = 0; index <panjang_data; index++) {                        
                           $('#datadosenprodi').append(`
                                <option value="${data_luar[index].dosen_kode}">${data_luar[index].dosen_gelar_depan} ${data_luar[index].dosen_nama}, ${data_luar[index].dosen_gelar_belakang}</option>
                           `)                     
                    }
                    $('#dosenprodi').show();
                }); 
                $('#datamatakuliah').show();                 
                var prodi = this.value;
                matkul(prodi);   
                $('#kelas').show();                              
            });

            function matkul(prodi) {                
                $('#semester').on('change',function(){
                    var semester = this.value;                                        
                    console.log(semester);
                    axios.get(`/bkd-matkul/${prodi}/${semester}`).then((response)=>{
                        var panjang = response.data.matkul.length;
                        var data = response.data.matkul;
                        $('#datamatkul').children('option:not(:first)').remove();
                        for (let index = 0; index < panjang; index++) {
                            $('#datamatkul').append(`
                                <option value="${data[index].kode_matkul}">${data[index].nama_matkul} (${data[index].program_studi})</option>
                            `);
                        }                    
                    });                       

                    axios.get(`/kelas-prodi/${prodi}/${semester}`).then((response)=>{
                        console.log(response.data);
                        var data = response.data.kelas;
                        var panjang = response.data.kelas.length;
                        $('#datakelas').children('option:not(:first)').remove();
                        for (let index = 0; index < panjang ; index++) {   
                            var cek = data[index].kelas_kode.split('18');
                            if (cek.length!=2) {
                                $('#datakelas').append(`                    
                                    <option value="${data[index].kelas_kode}">${data[index].kelas_nama}</option>
                                `);                                                           
                            }                                                    
                        }                        
                    });                  
                });
            }
    </script>
@endpush
@endsection