@extends('master')
@section('title','Mata Kuliah')
@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if ($message = Session::get('alert-danger'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>                         
                            @foreach ($errors->all() as $error)
                                  {{ $error }} <br>
                            @endforeach
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-6 col-sm-4">
                            <div class="text-left">
                                <strong class="card-title">Mata Kuliah</strong>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-4">
                            <div class="text-right">
                                <a href="#" class="btn btn-sm btn-success" data-toggle="modal" data-target="#exampleModal">Tambah Mata Kuliah</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Mata Kuliah</th>
                                    <th>Prodi</th>
                                    <td>Semester</td>
                                    <td>SKS Teori</td>                                    
                                    <td>SKS Praktik</td>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="matakuliah">   
                                @foreach ($matkul as $key=>$value)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$value->nama_matkul}}</td>
                                        <td>{{$value->program_studi_nama}}</td>
                                        <td>Semester {{$value->semester}}</td>
                                        <td>{{$value->sks_teori}}</td>
                                        <td>{{$value->sks_praktek}}</td>
                                        <td>
                                            <a href="#editmatakuliah" data-kodematkul="{{$value->kode_matkul}}" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#editmodal" class="btn btn-success btn-sm" title="Detail"><i class="fa fa-edit"></i></a>
                                            <a href="#hapusmatkul" data-kodematkul="{{$value->kode_matkul}}" data-matkul="{{$value->nama_matkul}}" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>                
                </div>
            </div>           
        </div>        
    </div><!-- .animated -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{route('matakuliah.store')}}" method="POST">
            @csrf                
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Mata Kuliah</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label for="">Mata Kuliah</label>
                <input type="text" name="nama_matkul" class="form-control" placeholder="Mata Kuliah">
                <label for="">Program Studi</label>                
                <select name="prodi" id="tambahprodi" class="form-control">
                    <option value="0" disabled="true" selected="true">-- PILIH --</option>
                    @foreach ($prodi as $item)
                        <option value="{{$item->program_studi_kode}}">{{$item->program_studi_nama}}</option>
                    @endforeach
                </select>
                <label for="">Semester</label>
                <input type="number" name="semester" class="form-control" placeholder="Semester" min="1" max="8">                
                <label for="">SKS Teori</label>
                <input type="number" name="teori" class="form-control" placeholder="SKS Teori" min="0" >
                <label for="">SKS Praktik</label>
                <input type="number" name="praktek" class="form-control" placeholder="SKS Praktik" min="0">                                     
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
            </div>
            </form>
        </div>
        </div>
    </div>

    <div class="modal fade" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="updatematkul" method="post">
                @csrf
                @method("PUT")
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Mata Kuliah</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <label for="">Mata Kuliah</label>
                    <input type="text" name="nama_matkul" id="namamatkul" class="form-control" placeholder="Mata Kuliah">
                    <label for="">Semester</label>
                    <input type="number" name="semester" id="semester" class="form-control" placeholder="Semester" min="1" max="8">
                    <label for="">Program Studi</label> 
                    <select name="prodi" id="editprodi" class="form-control">                                        
                        <option value="0" disabled="true" selected="true">-- PILIH --</option>
                        @foreach ($prodi as $item2)
                            <option value="{{$item2->program_studi_kode}}">{{$item2->program_studi_nama}}</option>
                        @endforeach
                    </select>
                    <label for="">SKS Teori</label>
                    <input type="number" name="teori" id="teori" class="form-control" placeholder="SKS Teori" min="0" >
                    <label for="">SKS Praktik</label>
                    <input type="number" name="praktek" id="praktek" class="form-control" placeholder="SKS Praktik" min="0">                
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-sm" onclick="formSubmit()">Update</button>
                </div>
            </form>                          
        </div>
        </div>
    </div>
</div><!-- .content -->
@push('script')
<script src="{{asset('assets/js/axios.min.js')}}"></script>
<script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/sweetalert2.js')}}"></script>

{{-- Get data --}}
<script>
    $(document).on('click','a[href="#editmatakuliah"]',function(){
        var kode_matkul = $(this).data('kodematkul');
        $('#editprodi option').prop('selected',false);
        axios.get(`matakuliah/${kode_matkul}/edit`,{
            headers: {
                'Content-Type': 'application/json',
            }
        }).then((response)=>{
            var url = '{{ route("matakuliah.update", ":kode_matkul") }}';;
            url = url.replace(':kode_matkul', kode_matkul);
            $('#updatematkul').attr('action',url);
            $('#namamatkul').val(response.data.matkul.nama_matkul);
            $('#semester').val(response.data.matkul.semester);
            $(`#editprodi option[value='${response.data.matkul.program_studi}']`).prop('selected',true);
            $('#teori').val(response.data.matkul.sks_teori);
            $('#praktek').val(response.data.matkul.sks_praktek);           
            function formSubmit() {
                $('#updatematkul').submit();                
            }
        });
    });

    $(document).on('click','a[href="#hapusmatkul"]',function(){
        var kode_matkul = $(this).data('kodematkul');
        var matkul = $(this).data('matkul');
        Swal.fire({
                title: 'Peringatan',
                text: `Apa Kamu yakin ingin menghapus ${matkul} ?`,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Hapus'
                }).then((result) => {
                    if (result.value) {
                        axios.delete(`/matakuliah/${kode_matkul}`).then((response)=>{                            
                            Swal.fire(
                                    'Sukses',
                                    `${response.data.message}`,
                                    'success'
                            ).then(function(){
                                location.reload();
                            });                            
                        });
                    }
                })
    });
</script>
@endpush
@endsection
