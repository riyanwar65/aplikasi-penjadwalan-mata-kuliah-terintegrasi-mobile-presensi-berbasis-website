<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">
    {{-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.jqueryui.min.css"> --}}
    {{-- <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">     --}}
    <link rel="stylesheet" href="{{asset('assets/js/wickedpicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/gayaan.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/bootstrap/dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/themify-icons/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/flag-icon-css/css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/selectFX/css/cs-skin-elastic.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/jqvmap/dist/jqvmap.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/jquery-ui.min.css')}}">
	<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />       
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.3.6/css/bootstrap-colorpicker.css" rel="stylesheet">
	
    {{-- <link rel="stylesheet" href="{{asset('vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}"> --}}
    <link rel="stylesheet" href="{{asset('assets/css/simplePagination.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{asset('assets/css/select2.min.css')}}">
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"> --}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.bootstrap4.min.css">
    <style>           
    @import url(http://fonts.googleapis.com/css?family=Open+Sans:700);
.cssload-container * {
	color: rgb(0,0,0);    
	font-size: 33px;
	font-weight: 700;
	font-family: 'Open Sans', sans-serif;
}
.cssload-container {
    margin-top: 20%;
    margin-left: 5%;
	position: absolute;
	width: 117px;
	height: 49px;
	left: 50%;
	transform: translate(-50%, -50%);
		-o-transform: translate(-50%, -50%);
		-ms-transform: translate(-50%, -50%);
		-webkit-transform: translate(-50%, -50%);
		-moz-transform: translate(-50%, -50%);
}
.cssload-container > div {
	position: absolute;
	transform-origin: center;
		-o-transform-origin: center;
		-ms-transform-origin: center;
		-webkit-transform-origin: center;
		-moz-transform-origin: center;
}
.cssload-l {
	left: 8px;
}
.cssload-i {
	left: 85px;
}
.cssload-d {
	left: 60px;
}
.cssload-n {
	left: 95px;
}
.cssload-g {
	left: 120px;
}
.cssload-square,
.cssload-circle,
.cssload-triangle {
	left: 29px;
}
.cssload-square {
	background: rgb(117,179,209);
	width: 23px;
	height: 23px;
	left: 31px;
	top: 12px;
	transform: scale(0);
		-o-transform: scale(0);
		-ms-transform: scale(0);
		-webkit-transform: scale(0);
		-moz-transform: scale(0);
	animation: cssload-shrinkgrow 3.45s ease-in-out infinite;
		-o-animation: cssload-shrinkgrow 3.45s ease-in-out infinite;
		-ms-animation: cssload-shrinkgrow 3.45s ease-in-out infinite;
		-webkit-animation: cssload-shrinkgrow 3.45s ease-in-out infinite;
		-moz-animation: cssload-shrinkgrow 3.45s ease-in-out infinite;
	animation-delay: 2.3s;
		-o-animation-delay: 2.3s;
		-ms-animation-delay: 2.3s;
		-webkit-animation-delay: 2.3s;
		-moz-animation-delay: 2.3s;
}
.cssload-circle {
	background: rgb(129,212,125);
	width: 26px;
	height: 26px;
	top: 10px;
	left: 29px;
	border-radius: 50%;
	animation: cssload-shrinkgrow 3.45s ease-in-out infinite;
		-o-animation: cssload-shrinkgrow 3.45s ease-in-out infinite;
		-ms-animation: cssload-shrinkgrow 3.45s ease-in-out infinite;
		-webkit-animation: cssload-shrinkgrow 3.45s ease-in-out infinite;
		-moz-animation: cssload-shrinkgrow 3.45s ease-in-out infinite;
	animation-delay: 0s;
		-o-animation-delay: 0s;
		-ms-animation-delay: 0s;
		-webkit-animation-delay: 0s;
		-moz-animation-delay: 0s;
}
.cssload-triangle {
	width: 0;
	height: 0;
	left: 29px;
	top: 11px;
	border-style: solid;
	border-width: 0 14.5px 24.1px 14.5px;
	border-color: transparent transparent rgb(210,121,140) transparent;
	transform: scale(0);
		-o-transform: scale(0);
		-ms-transform: scale(0);
		-webkit-transform: scale(0);
		-moz-transform: scale(0);
	animation: cssload-shrinkgrow 3.45s ease-in-out infinite;
		-o-animation: cssload-shrinkgrow 3.45s ease-in-out infinite;
		-ms-animation: cssload-shrinkgrow 3.45s ease-in-out infinite;
		-webkit-animation: cssload-shrinkgrow 3.45s ease-in-out infinite;
		-moz-animation: cssload-shrinkgrow 3.45s ease-in-out infinite;
	animation-delay: 1.15s;
		-o-animation-delay: 1.15s;
		-ms-animation-delay: 1.15s;
		-webkit-animation-delay: 1.15s;
		-moz-animation-delay: 1.15s;
}


@keyframes cssload-shrinkgrow {
	0% {
		transform: scale(0);
	}
	12.5% {
		transform: scale(1);
	}
	25% {
		transform: scale(1);
	}
	33% {
		transform: scale(0);
	}
	100% {
		transform: scale(0);
	}
}

@-o-keyframes cssload-shrinkgrow {
	0% {
		-o-transform: scale(0);
	}
	12.5% {
		-o-transform: scale(1);
	}
	25% {
		-o-transform: scale(1);
	}
	33% {
		-o-transform: scale(0);
	}
	100% {
		-o-transform: scale(0);
	}
}

@-ms-keyframes cssload-shrinkgrow {
	0% {
		-ms-transform: scale(0);
	}
	12.5% {
		-ms-transform: scale(1);
	}
	25% {
		-ms-transform: scale(1);
	}
	33% {
		-ms-transform: scale(0);
	}
	100% {
		-ms-transform: scale(0);
	}
}

@-webkit-keyframes cssload-shrinkgrow {
	0% {
		-webkit-transform: scale(0);
	}
	12.5% {
		-webkit-transform: scale(1);
	}
	25% {
		-webkit-transform: scale(1);
	}
	33% {
		-webkit-transform: scale(0);
	}
	100% {
		-webkit-transform: scale(0);
	}
}

@-moz-keyframes cssload-shrinkgrow {
	0% {
		-moz-transform: scale(0);
	}
	12.5% {
		-moz-transform: scale(1);
	}
	25% {
		-moz-transform: scale(1);
	}
	33% {
		-moz-transform: scale(0);
	}
	100% {
		-moz-transform: scale(0);
	}
}
    </style>
</head>
