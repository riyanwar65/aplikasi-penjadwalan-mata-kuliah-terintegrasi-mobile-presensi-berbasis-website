<!-- Left Panel -->
<aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="./">
                    {{-- <img src="images/logo.png" alt="Logo"> --}}
                </a>
                <a class="navbar-brand hidden" href="./">
                    {{-- <img src="images/logo2.png" alt="Logo"> --}}
                </a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    @if (strtolower(Session::get('jabatan'))=="sekertaris jurusan"
                         ||strtolower(Session::get('jabatan'))=="sekertaris"
                         ||strtolower(Session::get('jabatan'))=="sekjur"
                         ||strtolower(Session::get('jabatan'))=="sj")
                        @include('permissions.sekertarisjurusan.menu')
                    @endif
                    @if (strtolower(Session::get('jabatan'))=="admin jurusan"
                         ||strtolower(Session::get('jabatan'))=="admin")
                        @include('permissions.adminjurusan.menu')
                    @endif
                    @if (strtolower(Session::get('jabatan'))=="0")
                        @include('permissions.adminsiakad.menu')
                    @endif
                    {{-- <li class="active">
                        <a href="{{route('dashboard')}}"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                    </li>
                    <h3 class="menu-title">General</h3><!-- /.menu-title -->
                    <li>
                        <a href="{{route('dosen.index')}}"> <i class="menu-icon fa fa-user"></i>Dosen</a>                        
                    </li>
                    <li>
                        <a href="{{route('mahasiswa.index')}}"> <i class="menu-icon fa fa-user"></i>Mahasiswa</a>                        
                    </li>
                    <li>
                        <a href=""><i class="menu-icon fa fa-calendar-o"></i> Absensi</a>
                    </li>
                    <h3 class="menu-title">Master Data</h3><!-- /.menu-title -->

                    <li>
                        <a href="{{route('jabatan.index')}}"> <i class="menu-icon fa fa-tasks"></i>Jabatan</a>                       
                    </li>
                    <li>
                        <a href="{{route('matakuliah.index')}}"> <i class="menu-icon fa fa-edit"></i>Mata Kuliah</a>                       
                    </li>                    
                    <h3 class="menu-title">Setting</h3><!-- /.menu-title -->
                    <li>
                        <a href="#"><i class="menu-icon fa fa-cog"></i> Pindah Jadwal</a>
                    </li>
                    <li>
                        <a href="#"><i class="menu-icon fa fa-cog"></i> Jam Ganti</a>
                    </li>
                    <li>
                        <a href="{{route('sesi.index')}}"><i class="menu-icon fa fa-cog"></i>Sesi</a>
                    </li> 
                    <li>
                        <a href="{{route('ruangan.index')}}"><i class="menu-icon fa fa-cog"></i>Ruangan</a>
                    </li> 
                    <li>
                        <a href="{{route('regis-admin.index')}}"><i class="menu-icon fa fa-cog"></i> Regis Admin Jurusan</a>
                    </li>                       --}}
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->

    <!-- Left Panel -->