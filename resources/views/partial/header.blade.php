<header id="header" class="header">

            <div class="header-menu">

                <div class="col-sm-7">
                    <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                    <div class="header-left"> 
                        <h2>Selamat Datang <small>{{Session::get('nama')}}</small></h2>                        
                    </div>
                </div>

                <div class="col-sm-5">             
                    <table class="float-right">
                        <tr align=right>
                            <td>
                                @if (strtolower(Session::get('jabatan'))=="0")

                                @else
                                    <a href="#gantipassword" title="Ganti Password" data-toggle="modal" data-target="#gantipassword">
                                        <img class="user-avatar" width="30px" src="{{asset('images/cp.png')}}" alt="User Avatar">                                                      
                                    </a>
                                @endif                                
                                {{-- <a class="nav-link" href="#" title="Ganti Password"><i class="fa fa-key"></i></a> --}}
                            </td>
                            <td><a href="#logout" title="Logout">
                                <img class="user-avatar rounded-circle" width="30px" src="{{asset('images/power.png')}}" alt="User Avatar">                                                      
                            </a></td>
                        </tr>
                    </table>                                            
                </div>
            </div>

        </header>
        <!-- Modal -->
<div class="modal fade" id="gantipassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">        
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ganti Password</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">            
            <label for="">New Password</label>  <br>
            <input type="password" name="new_password" id="new_pass" placeholder="New Password" class="form-control">
            <label for="">Confirm Password</label>
            <input type="password" name="confirm_password" id="confirm_pass" placeholder="Confirm Password" class="form-control">
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" onclick="gantipassword()" class="btn btn-primary">Simpan</button>
        </div>
        </div>
    </div>
</div>
        <!-- /header -->
        <!-- Header-->
@push('script')
<script src="{{asset('assets/js/axios.min.js')}}"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>  
<script src="{{asset('assets/js/sweetalert2.js')}}"></script>   
<script>
    $(document).on('click','a[href="#logout"]',function(){
        Swal.fire({
            title: 'Peringatan',
            text: `Apakah Anda yakin untuk Logout ? `,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Logout'
        }).then((result) => {
            if (result.value) {
                window.location.href="{{route('logout')}}";
            }
        });
    }); 
    
    function gantipassword(){
        var userid = "{{Session::get('user_id')}}";
        var newpass = $('#new_pass').val();              
        var confirmpass = $('#confirm_pass').val();
        if (newpass==confirmpass) {
            axios.post('gantipassword',{
                userid:userid,
                newpass:newpass,
            }).then((response)=>{
                Swal.fire({
                    title:'Sukses',
                    text:"Password berhasil diganti",
                    icon:'success',
                });
            });
        }else{
            Swal.fire({
                title:'Error',
                text:"Password dengan Konfirmasi Password Tidak Sama",
                icon:'error',
            });
        }
    }
</script> 
@endpush