<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
    <table class="table table-stripped table-bordered">    
        <thead align="center" class="table-dark">
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Kelas</th>
                <th>Presensi</th>
                <th>Sakit</th>
                <th>Izin</th>
                <th>Dispen</th>
                <th>Tanpa Keterangan</th>
            </tr>  
        </thead>      
            <tbody align="center">            
                @foreach ($rekap as $key=>$value)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$value->mahasiswa_nama}}</td>
                    <td>{{$kelas}}</td>                
                    <td>
                        {{
                            DB::connection('presensi')
                            ->table('absensi')
                            ->where('mahasiswa_nim',$value->mahasiswa_nim)
                                ->where('status_kehadiran',"hadir")
                                ->count()
                            }}                    
                    </td>                
                    <td>
                        {{
                            DB::connection('presensi')
                            ->table('absensi')
                            ->where('mahasiswa_nim',$value->mahasiswa_nim)
                                ->where('status_kehadiran',"sakit")
                                ->count()  
                            }}
                    </td>
                    <td>
                        {{
                            DB::connection('presensi')
                            ->table('absensi')
                            ->where('mahasiswa_nim',$value->mahasiswa_nim)
                            ->where('status_kehadiran',"izin")
                                ->count()
                        }}
                    </td>
                    <td>
                        {{
                            DB::connection('presensi')
                            ->table('absensi')
                                ->where('mahasiswa_nim',$value->mahasiswa_nim)
                                ->where('status_kehadiran',"dispen")
                                ->count()
                        }}
                    </td>
                    <td>
                        {{
                            DB::connection('presensi')
                                ->table('absensi')
                                ->where('mahasiswa_nim',$value->mahasiswa_nim)
                                ->where('status_kehadiran',"tidak hadir")
                                ->count()
                        }}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>