<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <table width="100%" border="1">
        <thead>
            <tr align="center">
                <td rowspan=2>No</td>
                <td rowspan=2>Nama Dosen</td>
                <td rowspan=2>Sem</td>
                <td rowspan=2>Kelas</td>
                <td rowspan=2>Mata Kuliah</td>                                                                        
                <td colspan="2">SKS</td>                    
                <td rowspan="1">Total SKS</td>                                            
                    <tr align="center">
                        <td>Teori</td>
                        <td>Praktek</td>                        
                        <td>Jumlah</td>   
                    </tr>                  
            </tr>
            <tr>                
                <td align="center" colspan="9">{{strtoupper($excel[0]->jurusan_nama)}}</td>
            </tr>
        </thead>
        <tbody>
            @foreach ($excel as $key=>$item)                            
             <tr>
                <td>{{$key+1}}</td>
                <td>{{$item->dosen_gelar_depan." ".$item->dosen_nama.", ".$item->dosen_gelar_belakang}}</td>
                <td>{{$item->semester}}</td>
                <td>{{$item->kelas_nama}}</td>
                <td>{{$item->nama_matkul}}</td>
                <td>{{$item->jumlah_sks_teori}}</td>                
                <td>{{$item->jumlah_sks_praktek}}</td>
                <td>{{$item->jumlah_sks_teori+$item->jumlah_sks_praktek}}</td>                
             </tr>
             @endforeach      
        </tbody>
    </table>
</body>
</html>