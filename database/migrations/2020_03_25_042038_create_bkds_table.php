<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBkdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bkd', function (Blueprint $table) {
            $table->string('kode_bkd',10)->primary();
            $table->string('tahun_ajaran',40);
            $table->string('kode_dosen',200);
            $table->string('kode_matkul',20);
            $table->integer('semester')->unsigned();
            $table->integer('sks')->unsigned();
            $table->enum('jenis',['teori','praktek']);
            $table->integer('total_sks')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bkds');
    }
}
