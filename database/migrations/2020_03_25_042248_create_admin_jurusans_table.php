<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminJurusansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_jurusan', function (Blueprint $table) {
            $table->string('kode_admin_jurusan',200)->primary();
            $table->string('nik',200)->nullable();
            $table->string('nip',200)->nullable();   
            $table->string('nama_admin_jurusan',200);
            $table->string('email',150)->unique();
            $table->text('alamat');
            $table->string('no_hp',20);
            $table->integer('id_jabatan')->unsigned();                                    
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_jurusans');
    }
}
