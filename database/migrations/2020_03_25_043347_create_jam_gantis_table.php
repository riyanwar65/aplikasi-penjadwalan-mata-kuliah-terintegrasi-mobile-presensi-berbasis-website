<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJamGantisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jam_ganti', function (Blueprint $table) {
            $table->string('kode_jam_ganti',20)->primary();
            $table->integer('id_jadwal')->unsigned();
            $table->string('hari',50);
            $table->integer('sesi_mulai');
            $table->integer('sesi_akhir');
            $table->string('kode_ruangan',200);
            $table->string('status_perkuliahan',200);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jam_gantis');
    }
}
