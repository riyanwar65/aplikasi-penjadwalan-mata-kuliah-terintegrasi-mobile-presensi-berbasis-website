<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAbsensisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('absensi', function (Blueprint $table) {
            $table->id();
            $table->date('checkin_date');
            $table->time('checkin_time');                        
            $table->time('jam_mulai_buka_sesi');
            $table->text('keterangan');
            $table->enum('status_kehadiran',['hadir','tidak hadir','sakit','izin','dispen']);
            $table->integer('id_jadwal')->unsigned();
            $table->string('kode_mhs',200);
            $table->string('kode_dosen',200); 
            $table->string('kode_ruangan',200);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('absensis');
    }
}
