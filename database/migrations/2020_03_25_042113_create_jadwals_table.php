<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJadwalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwal', function (Blueprint $table) {
            $table->string('kode_jadwal',10)->primary();
            $table->string('tahun_ajaran',40);
            $table->string('kode_dosen',200);
            $table->string('kode_matkul',20);
            $table->string('kode_kelas',20);
            $table->string('hari',20);            
            $table->string('kode_ruangan',20);
            $table->integer('sesi_mulai')->unsigned();
            $table->integer('sesi_akhir')->unsigned();
            $table->integer('toleransi_keterlambatan')->unsigned();
            $table->enum('status_perkuliahan',['by_jam_mulai','by_jam_buka_sesi']);                        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jadwals');
    }
}
