<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\DB;
use App\Bkd;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['auth.logout']], function () {
    Route::get('login','Auth\LoginController@showLoginForm');
    Route::post('login','Auth\LoginController@login')->name('login'); 
    Route::get('lupapassword','Auth\LoginController@lupapassword')->name('lupa.password');
    Route::post('checkemail','Auth\LoginController@prosescekemail')->name('cek.email');
    Route::get('updatepassword/{token}','Auth\LoginController@updatepassword')->name('update.password');
    Route::post('updatepassword','Auth\LoginController@prosesupdatepassword')->name('proses.updatepassword');
});
// Route::post('forgot-password','LoginController@forgotPassword')->name('forgotpassword');

Route::get('logout','Auth\LoginController@logout')->name('logout');
// Auth::routes();
Route::group(['middleware' => ['auth.custom']], function () {
    Route::get('/','DashboardController@index')->name('dashboard');    
    Route::resource('bkd','BKDController');
    Route::resource('mahasiswa','MahasiswaController');
    Route::resource('dosen','DosenController');
    Route::resource('matakuliah','MataKuliahController');
    Route::resource('dosen','DosenController');
    Route::resource('regis-admin','RegistrasiAdminController');
    Route::resource('jabatan','JabatanController');
    Route::resource('jabatan-pegawai','JabatanPegawaiController');
    Route::resource('sesi','SesiController');
    Route::resource('ruangan', 'RuanganController');
    Route::resource('jadwal', 'JadwalController');
    Route::get('resetuuiddosen/{id}','DosenController@resetuuid');
    Route::post('jabatan-pegawai-admin','JabatanPegawaiController@jabatanadmin')->name('jabatan-pegawai-admin.store');
    Route::get('hasiljadwal/{id}','GenerateJadwalController@result')->name('testing_ya');
    Route::get('/program-studi','BKDController@programstudi')->name('programstudi');
    Route::get('/dosen-prodi/{id}','BKDController@dosenprodi');
    Route::get('/kelas-prodi/{id}/{semester}','BKDController@kelasprodi');
    Route::get('/data-matakuliah','MataKuliahController@datamatakuliah');
    Route::get('/bkd-matkul/{prodi}/{semester}','BKDController@bkdmatkul');
    Route::get('/editbkd','BKDController@dataeditbkd')->name('editbkd');
    Route::get('/data-jabatan','JabatanController@getdata')->name('data-jabatan');
    Route::get('/testcekdosen/{id}','BKDController@dosenprodi');
    Route::post('/filter-bkd','BKDExportController@filtertampilan')->name('filter-bkd');
    Route::post('/bkd-export-pdf','BKDExportController@exportpdf')->name('export-bkd');  
    Route::get('generate','GenerateJadwalController@index')->name('generate.index');   
    Route::post('cek','JadwalController@cek')->name('cekjadwal');   
    Route::get('pindahjadwal','PindahController@pindahjadwal')->name('pindahjadwal');   
    Route::post('ceksemua','JadwalController@ceksemua')->name('ceksemua');   
    Route::post('generate','GenerateJadwalController@generateJadwal')->name('generate.post');    
    Route::post('cekgenerate','GenerateJadwalController@cekgenerate')->name('cekgenerate');    
    Route::get('cekhari/{hari}','GenerateJadwalController@cekhari')->name('cekhari');
    Route::post('filterlab','CekController@editcek')->name('filterlab');
    // Route::get('filterlab','CekController@editcek')->name('filterlab');

    Route::get('kelas-test','TestController@kelas');
    Route::get('testdulu','TestController@index')->name("test.cek");
    Route::post('hari-sesi','TestController@harisesi')->name("harisesi");    
    
    Route::post('tambahjenis','CekController@tambahjenis')->name('tambahjenis');
    Route::post('perubahansesi','CekController@perubahansesi')->name('perubahansesi');
    Route::post('publish','CekController@publish')->name('publish');
    Route::post('gantipassword','Auth\LoginController@gantipassword')->name('gantipassword');   

    Route::post('datajadwal',"TestController@jadwalsemua")->name('datajadwal');
    Route::get('setcolor',"SetColorController@set")->name('setcolor');
    Route::post('setcolor',"SetColorController@setPost")->name('setcolor.post');
    Route::post('updatesetcolor/{id}',"SetColorController@setUpdate")->name('setcolor.update');

    Route::post('pindahjadwalpost','PindahController@pindah')->name('pindah');
    Route::post('updatepindah','PindahController@updatepindah')->name('updatepindah');
    Route::get('delete-color/{id}','SetColorController@hapus')->name('hapuscolor');
    Route::get('absensi','AbsensiController@index')->name('absensi');
    Route::get('absensi/{kelas}','AbsensiController@rekap')->name('rekapabsensi');
    Route::get('rekap','AbsensiController@rekap');
    Route::post('gantiruangan','PindahController@gantiruangan')->name('gantiruangan');
    Route::post('harirequest','PindahController@harirequest')->name('harirequest');
    Route::post('updateruangan','PindahController@updateruangan')->name('updateruangan');    
    Route::post('exportjadwal','JadwalController@export')->name('export.jadwal');    
});

